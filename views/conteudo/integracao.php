

<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* https://pt.wikipedia.org/wiki/Integra%C3%A7%C3%A3o_num%C3%A9rica 
https://www.math.tecnico.ulisboa.pt/~calves/courses/integra/ */ 
$this->title = 'Integração Numérica';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>


<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Integração Numérica - Motivação</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">





	Na engenharia ou em estudos afins, nos deparamos com funções que são difíceis de integrar, que possuem apenas dados tabelados ou mesmo que não possuem primitiva e, portanto, não podem ser integradas analiticamente. Como veremos nessa seção, a função pode ser aproximada por um polinômio, que é a base dos métodos que estudaremos a seguir.



<br><br>
  </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Integração </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

A técnica mais simples de integração numérica são as fórmulas de Newton-Cotes: Regra do Trapézio, Regra 1/3 de Simpson e a
Regra 3/8 de Simpson. 
<br><br>
Nessas fórmulas, a ordem do polinômio é ajustada para aproximar a integral. Por exemplo, na Regra do Trapézio uma reta é utilizada como polinômio aproximador.
<br><br>

      $$P_{1}(x) = a_{0}+a_{1}x$$

<br>
<p style="
    
 text-align: center;
">


<img src="imagens/integracao_introducao1.svg" style="width: 90%;">
</p><br>
</div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
            <p> A função em verde abaixo é  $\; f(x)= \frac{1}{x} \;$. Verifique a aproximação da integral da função na medida que aumenta-se o grau do polinômio.</p><br>

             <form id="geo">
                <input  value=" - Pontos" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="+ Pontos" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>

            <br><br>
            

            </div>
        </div>
    </div>
</div>






<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }

</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 1;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/INTEGRATIONintGeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-1.5,9,-1.5,5);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;

        ggbApplet.setValue('n',this.n)
        
        
        
       
    }

    function anterior (){
             if (this.n%2==0){
       
    }
        if (this.n >= 2){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
        if (this.n <= 15){
            this.n++;
            ggbApplet.setValue('n', this.n)
          
            

             
                
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>