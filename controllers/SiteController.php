<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Auth;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\TurmasAlunos;
use yii\authclient\ClientInterface;
use app\components\Sigaa;
use app\components\SigaaProfile;
use app\models\Matriculas;
use app\models\Vinculos;
use app\models\VinculosSelecionar;
use app\components\AuthHandler;
use yii\helpers\Url;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
                'successUrl' => Url::toRoute(['site/logar']),
            ],
        ];
    }

    /**
     * Autenticado remotamente.
     */
    public function onAuthSuccess($client)
    {

        (new AuthHandler($client))->handle();
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogar()
    {

       
        $client = new Sigaa();
        $handler = new AuthHandler($client);
        $cpfCnpj = $handler->buscarCpfCnpj();

        $modelMatriculas = new Matriculas();
        $modelMatriculas->qrCodeId = 0;
        $modelMatriculas->hash = 0;

        $auth = Auth::find()->where([
            //'source' => 'sigaa',
            'source_id' => $cpfCnpj,
        ])->one();

        if ($auth) {
            // Encontra quantos vinculos o usuario tem
            // Se tem soh 1, faz login direto
            // Se tem mais de 1, tem  escolher
            $user = $auth->user;
            $auth = Yii::$app->authManager;

            $vinculos = Vinculos::find()->where([
                'user_id' => $user->id,
            ])->all();
            if (count($vinculos) == 1){
                // Faz o login direto
                Yii::$app->user->login($user,0);
                // Grava o vinculo_id que o usuario logou na tabela user, campo vinculo_id
                $user->vinculo_id = $vinculos[0]->id;
                $user->save();
                return $this->render('index2', [
                    'modelMatriculas' => $modelMatriculas,
                ]); 
                
            }
            elseif (count($vinculos) >= 2){
                $model = new VinculosSelecionar();
                if ($model->load(Yii::$app->request->post())) {
                    // Dados jah validados pelo ActiveForm. Apenas faz o login
                    Yii::$app->user->login($user,0);
                    // Grava o vinculo_id que o usuario logou na tabela user, campo vinculo_id
                    $user->vinculo_id = $model->vinculo;
                    $user->save();
                    return $this->render('index2', [
                        'modelMatriculas' => $modelMatriculas,
                    ]); 
                } else {
                    $items = Vinculos::find()
                        ->select(['descricao'])
                        ->where(['user_id' => $user->id,])
                        ->indexBy('id')
                        ->column();
                    return $this->render('vinculos', [
                        'items' => $items,
                        'model' => $model,
                    ]);
                }
            }
            else{
                return $this->render('index2', [
                    'modelMatriculas' => $modelMatriculas,
                ]);
            }
            
        }
        else
            return $this->render('about');
    }    

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index2');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionCreditos()
    {
        return $this->render('creditos');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContato()
    {
        return $this->render('contato');
    }

    
}
