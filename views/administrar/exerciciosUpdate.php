<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model frontend\models\Exercicios */

$this->title =  $model->descricao;
$this->params['breadcrumbs'][] = ['label' => 'Exercicios', 'url' => ['exercicios-index']];
$this->params['breadcrumbs'][] = ['label' => $model->descricao, 'url' => ['exercicios-view', 'id' => $model->id]];
?>
<div class="exercicios-update">

	<?= Breadcrumbs::widget([
	  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
	<?= Alert::widget() ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('exercicios_form', [
        'model' => $model,
    ]) ?>

</div>
