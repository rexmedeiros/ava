<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\ExercicioWidget;

/* @var $this yii\web\View */


$this->title = 'Integração Numérica - Simpson 1/3';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Videoaula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/XUeANCkj4e8" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Regra 1/3 de Simpson </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            A regra $1/3$ de Simpson utiliza uma parábola para aproximar a função no intervalo de integração e assim tem como vantagem um erro menor do que a integral calculada pela regra do trapézio.
            <br><br>
Para calcular o polinômio de segundo grau é necessário ter $3$ pontos que interpolem a função.
Dois pontos são obtidos nas extremidades, sendo $x_{0}= a$ e $x_{2}=b$.


<br><br>
O ponto $x_{1}$ pode ser obtido de qualquer valor $x$ no intervalo $[a,b]$. Por convenção, faz-se
$x_{1}$ o ponto médio do intervalo.
<br><br>
Assim, $x_{1}= x_{0}+h$, de forma a ter dois subintervalos de mesmo tamanho, $h = \frac{b-a}{2}$.
<br><br>
De posse de $3$ pontos, pode-se encontrar a o polinômio de segundo grau que passa por todos os pontos utilizando a técnica de Lagrange:
<br><br><br>
<p style="
    
 text-align: center;
">


<img src="imagens/integracao1_3_.svg" style="width:90%;"/>
</p><br>
<div class="equacao">
$$
\begin{aligned}
P_{2} = &\; y_{0}\frac{(x - x_{1})(x - x_{2})}{(x_{0} - x_{1})(x_{0} - x_{2})} \\
    &+y_{1}\frac{(x - x_{0})(x - x_{2})}{(x_{1} - x_{0})(x_{1} - x_{2})}\\
    &+y_{2}\frac{(x - x_{0})(x - x_{1})}{(x_{2} - x_{0})(x_{2} - x_{1})},
\end{aligned}
$$
</div>
<br>
<br>
de forma que a integral pode ser aproximada pela seguinte fórmula:
<br><br>
$$ \int_{a}^{b} P_{2}dx   = \frac{h}{3}(y_{0}+4y_{1}+y_{2}). $$
<br>
Para polinômios de até terceiro grau, a Regra apresenta erro zero. Para funções de ordem maior o erro é calculado através da expressão:
<br><br>
$$ \varepsilon= -\frac{1}{90} f^{(4)}(\tau)h^5,$$
<br>
onde $\tau \in [a,b]$.
<br><br>
</div>
        </div>
    </div>
</div>




<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Regra 1/3 de Simpson Composta</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

De forma a melhorar a acurácia da Regra de Simpson $1/3$, pode-se dividir o intervalo $[a,b]$ em $2n$ subintervalos e aplicar a regra $n$ vezes.

<br><br>
Para $2n$ subintervalos de mesmo tamanho $h$, então
<br><br>
$$h = \frac{b-a}{2n}.$$
<br>
A cada $2$ subintervalos calcula-se a Regra $1/3$ de Simpson, de forma a obter:
<br><br>
<div class="equacao">
$$
\begin{aligned}
A_{1} &=\frac{h}{3}(y_{0}+4y_{1}+y_{2}),\\
A_{2} &= \frac{h}{3}(y_{2}+4y_{3}+y_{4}),\\
&...\\
A_{n} &= \frac{h}{3}(y_{2n-2}+4y_{2n-1}+y_{2n}).
\end{aligned}
$$ 
</div>

<br>

Se somar cada área calculada, obtêm-se a integral para a Regra $1/3$ Composta:
<br><br>

<div class="equacao">
$$I_{\frac{1}{3}}= \frac {h}{3}[y_{0}+4y_{1}+2y_{2}+4y_{3} \;\;... \;+ 2y_{2n-2}+4y_{2n-1}+y_{2n}].$$
</div>

<br>
O erro pode ser calculado como:
<br><br>
$$ \varepsilon = -\frac{n}{90} f^{(4)}(\tau)h^5,$$
<br>
onde novamente $\tau \in [a,b]$.

</div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
     <p>Calcular a integral abaixo com n = $3$  aplicações da Regra $1/3$ de Simpson:</p>
           <br>
            $$\int_{-2}^{2} \frac{x^2}{5} +2\sin(x) dx.$$ 

            
            <p>Solução:</p>
             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>

            <br>
            
            </div>
        </div>
    </div>
</div>







<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }

</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/INTEGRATION13GeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-4,6,-4.0,4);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}

     
       
    }

    function anterior (){
         if(n>=6 && n <=12){

             ggbApplet.setCoordSystem(-4,12,-4,4);
        }
        else{

             ggbApplet.setCoordSystem(-4,6,-4,4);
        }

             if (this.n%2==0){
       
    }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
         if(n>=4 && n <=10){

             ggbApplet.setCoordSystem(-4,12,-4,4);
        }
        
        else{

             ggbApplet.setCoordSystem(-4,6,-4,4);
        }


        if (this.n <= 14){
            this.n++;
            ggbApplet.setValue('n', this.n)
          
            

             
                
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;">
                  <img src="imagensDev/Massao/simpson13.svg"alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/Hxb_gP6Wk38" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/integracaoSimpson1.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método de Simpson 1/3',
          'todosExercicios' => $todosExercicios[6],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>

</div>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>