<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "turmas".
 *
 * @property integer $id
 * @property integer $id_turma
 * @property string $codigo
 * @property string $nome
 * @property string $horario
 * @property string $turma
 * @property string $subturma
 * @property integer $ano
 * @property integer $periodo
 *
 * @property Respostas[] $respostas
 * @property TurmasAlunos[] $turmasAlunos
 * @property TurmasDocentes[] $turmasDocentes
 */
class Turmas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'turmas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_turma', 'codigo', 'nome', 'horario', 'turma', 'ano', 'periodo'], 'required'],
            [['id_turma', 'ano', 'periodo'], 'integer'],
            [['codigo'], 'string', 'max' => 15],
            [['nome'], 'string', 'max' => 150],
            [['horario'], 'string', 'max' => 200],
            [['turma', 'subturma'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_turma' => 'Id Turma',
            'codigo' => 'Codigo',
            'nome' => 'Nome',
            'horario' => 'Horario',
            'turma' => 'Turma',
            'subturma' => 'Subturma',
            'ano' => 'Ano',
            'periodo' => 'Periodo',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurmasAlunos()
    {
        return $this->hasMany(TurmasAlunos::className(), ['turma_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurmasDocentes()
    {
        return $this->hasMany(TurmasDocentes::className(), ['turma_id' => 'id']);
    }
}
