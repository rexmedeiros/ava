<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ExerciciosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Exercícios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exercicios-index">
    
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Importar Exercícios do Multiprova', ['exercicios-create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'descricao', 'label' => 'Descrição do exercício:'],
            ['attribute' => 'id', 'label' => 'ID do exercício:'],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'urlCreator' => function ($action, $model, $key, $index, $thisa) {
                    //return string;                    
                    if ($action === "update"){
                        return Url::base().'/administrar/exercicios-update?id='.$key;
                    }
                    if ($action === "delete"){
                        return Url::base().'/administrar/exercicios-delete?id='.$key;
                    }
                },
            ],
        ],
    ]); ?>
</div>
