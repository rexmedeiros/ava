<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\ExercicioWidget;

/* @var $this yii\web\View */


$this->title = 'Integração Numérica - Simpson 3/8';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Videoaula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/I9KpfcO_Jjk" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Regra 3/8 de Simpson </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

Na regra regra $1/3$ de Simpson a função $f(x)$ é aproximada por um polinômio de segundo grau no intervalo de integração $[a,b]$. Já na egra regra $3/8$ de Simpson, aproxima-se a função por um polinômio de terceiro grau, de forma que são necessários $4$ pontos para obter esse
polinômio por interpolação. Para ter os pontos, divide-se o intervalo $[a, b]$ em três subintervalos igualmente espaçados de $h$. Dessa forma,
 
$$
h = \frac{b-a}{3},
$$

Desta forma, $x_{0} = a$, $x_{1} = a + 2h$, $x_{2} = a + 3h$ e $x_{3} = a+3h$. Assim, o polinômio de terceiro grau $P_3(x)$ pode ser obtido pela fórmula de Lagrange.

<br><br>


<p style="text-align: center;">
<img src="imagens/simpson3_8.svg" style="width:70%;"/>
</p>

<br>
A integral pelo método de Simpson $3/8$ é
<br><br>
<div class="equacao">
$${\int_{a}^{b}} {P_{3}(x)dx }  = \frac{3h}{8}(y_{0}+3y_{1}+3y_{2}+y_{3}). $$
</div>

<br>
O erro pode de integração é expresso por:
<br><br>
<div class="equacao">
$$\varepsilon_{3/8}={\int_{a}^{b}} \frac{f^{(4)}(\tau) }{4!}(x-x_{0})(x-x_{1})(x-x_{2})(x-x_{3})dx.$$
</div>

<br>
Subistituindo $ \;u = \frac{x-a}{h}$, e considerando $x_{i}=a+ih$,
<br><br>
$$ 
\varepsilon_{3/8}= -\frac{3}{80} f^{(4)}(\tau)h^5,
$$
<br>
onde $\tau$ pertence ao intervalo $[a,b]$.
<br><br>
</div>
        </div>
    </div>
</div>




<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Regra 3/8 de Simpson Composto</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

De forma a diminuir o erro da Regra de Simpson $3/8$, pode-se aplicar a Regra $n$ vezes  ao dividir o intervalo $[a,b]$ em $3n$ subintervalos. Para $3n$ subintervalos de mesmo tamanho $h$, então
<br><br>
$$h = \frac{b-a}{3n}.$$
<br>
A cada $3$ subintervalos calcula-se a Regra $3/8$ de Simpson, de forma a obter:
<br><br>
<div class="equacao">
$$ 
\begin{aligned}
A_{1} &=\frac{3h}{8}(y_{0}+3y_{1}+3y_{2}+y_{3}), \\
A_{2} &= \frac{3h}{8}(y_{3}+3y_{4}+3y_{5}+y_{6}),\\
A_{3} &= \frac{3h}{8}(y_{6}+3y_{7}+3y_{8}+y_{9}), \\
 & \dots \\ 

A_{n} &= \frac{3h}{8}(y_{n-3}+3y_{n-2}+3y_{n-1}+y_{n}).
\end{aligned}
$$ 
</div>

Somando todas as áreas calculadas, obtêm-se a integral para a Regra $3/8$ Composta:

<div class="equacao">
$$I_{\frac{3}{8}}= \frac {3h}{8}[y_{0}+3y_{1}+3y_{2}+2y_{3}+3y_{4} +3y_{5}\;\;... \;+ 2y_{3n-3}+3y_{3n-2}+3y_{3n-1}+y_{3n}].$$
</div>

O erro pode ser cálculado como sendo:
$$ 
\varepsilon= -\frac{3n}{80} f^{(4)}(\tau)h^5,
$$
onde $\tau \in [a,b]$.
</div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
         <p>Calcular a integral abaixo com n = $2$  aplicações da Regra $3/8$ de Simpson:</p>
         <br>
            $$\int_{-2}^{2} \frac{x^2}{5} +2\sin(x) dx.$$ 

            
            <p>Solução:</p>
             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>
            <br>
              </div>
        </div>
    </div>
</div>






<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }

</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/INTEGRATION38GeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-4,6,-4,4);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}

        if(this.n%2 == 1){
        a = ggbApplet.getValue('A(1,1)').toFixed(decimais);
        b = ggbApplet.getValue('B(1,1)').toFixed(decimais);
    }
        
       
    }

    function anterior (){
         if(n>=6 && n <=13){

             ggbApplet.setCoordSystem(-4,12,-4,4);
        }
        else{

             ggbApplet.setCoordSystem(-4,6,-4,4);
        }

             if (this.n%2==0){
       
    }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
         if(n>=4 && n <=11){

             ggbApplet.setCoordSystem(-4,12,-4,4);
        }
        
        else{

             ggbApplet.setCoordSystem(-4,6,-4,4);
        }


        if (this.n <= 16){
            this.n++;
            ggbApplet.setValue('n', this.n)
          
            

             
                
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;">
                  <img src="imagensDev/Massao/simpson38.svg" alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/S4UNcLJ0gNU" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/integracaoSimpson2.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método de Simpson 3/8',
          'todosExercicios' => $todosExercicios[5],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>

</div>
<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>