<?php
use yii\helpers\Html;
use app\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Sistemas Lineares - Decomposição LU';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>



<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Critérios de Convergência - vídeo aula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/0KhoP01Ir9g" allowfullscreen></iframe>
               </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Critérios de convergência para os métodos iterativos</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                Ao contrário dos métodos exatos, como o método da eliminação de Gauss e da decomposição LU, que obtém a solução exata após um número exato de passos,  não é sempre que um método iterativo tem sucesso. <br><br>
                Apesar de terem certas vantagens como utilizar menos memória do computador e serem mais imunes a erros de arrendondamento, é preciso seguir certas condições para ter certeza que esses métodos estão convergindo para a solução desejada. <br><br>
                Veremos a seguir alguns critérios que devem ser satisfeitos para garantir a convergência utilizando os métodos de Jacobi e Gauss-Seidel. É importante salientar que a satisfação desses critérios garante a convergência; porém, caso eles não sejam satisfeitos, não podemos concluir nada em relação a mesma.
                <br><br>
                Para estudar os critérios de convergência, devemos introduzir as normas linha e coluna de uma matriz.
            </p>

            </div>
        </div>
    </div>
</div>

<br><br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Norma Linha</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                A norma linha de uma matriz $B$  de dimensão $n\times m$, denominada $\parallel B\parallel_L$, é definida como sendo a maior soma dos elementos de uma linha, onde cada elemento é tomado em valor absoluto. Ou seja,
                $$
                 \parallel B\parallel_L = \max_{1\leq i \leq n}\{L_i\},
                $$
                onde
                $$
                  L_i = \sum_j |b_{ij}|.
                $$
                Como exemplo, vamos calcular a norma linha da seguinte matriz:
                $$
                B = \begin{bmatrix}2 & -0.2 & -0.1 \\ -0.2 & -1 & -0.2 \\ -0.2 & -0.3 & 3 \end{bmatrix}
                $$

                Calculando a soma dos elementos de cada linha (tomados em valor absoluto), temos:

                $$
                L_1 \rightarrow \mid 2 \mid + \mid -0.2 \mid + \mid -0.1 \mid  = 2.3 \\
                L_2 \rightarrow  \mid -0.2 \mid + \mid -1\mid + \mid -0.2 \mid  = 1.4 \\
                L_3 \rightarrow  \mid -0.2 \mid + \mid -0.3 \mid + \mid 3\mid  = 3.5 \\
                $$

                Logo, $\parallel B \parallel _{L} = \max\{2.3, 1.4, 3.5\}$ = 3.5. 
            
            </div>
        </div>
    </div>
</div>
<br><br>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Norma Coluna</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                Analogamente à norma linha, a norma coluna de uma matriz $B$  de dimensão $n\times m$, denominada $\parallel B\parallel_C$, é definida como sendo a maior soma dos elementos de uma coluna, onde cada elemento da coluna é tomado em valor absoluto. Ou seja,
                $$
                 \parallel B\parallel_C = \max_{1\leq j \leq m}\{C_j\},
                $$
                onde
                $$
                  C_j = \sum_i |b_{ij}|.
                $$
                Como exemplo, vamos calcular a norma coluna da mesma matriz $B$: 
                $$
                B = \begin{bmatrix}2 & -0.2 & -0.1 \\ -0.2 & -1 & -0.2 \\ -0.2 & -0.3 & 3 \end{bmatrix}
                $$

                Calculando a soma dos elementos de cada coluna (tomados em valor absoluto), temos:

                $$
                C_1 \rightarrow \mid 2 \mid + \mid -0.2 \mid + \mid -0.2 \mid  = 2.4 \\
                C_2 \rightarrow  \mid -0.2 \mid + \mid -1\mid + \mid -0.3 \mid  = 1.5 \\
                C_3 \rightarrow  \mid -0.1 \mid + \mid -0.2 \mid + \mid 3\mid  = 3.3 \\
                $$

                Logo, $\parallel B \parallel _{C} = \max\{2.4, 1.5, 3.3\}$ = 3.3. 
            
            </div>
        </div>
    </div>
</div>
<br><br>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Número de Sassenfeld</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
            O número de Sassefeld é outro parâmetro utilizado para verificar a convergência dos métodos iterativos. Para uma matriz $B = [b_{ij}]_{n\times n}$, o número de sassenfeld $\beta$ é:

            $$
                \beta_B = \max_{1\leq i \leq n} \{\beta_i\},
            $$
            em que
            $$
            \beta _{i} = \sum_{j=1}^{i-1} \beta _{j}| b_{ij}| + \sum_{j=i+1}^{n} |b_{ij}|.
            $$
            
            Como exemplo, vamos calcular o número de Sassenfeld da matriz $B$:            

            $$
            B = \begin{bmatrix}0 & -0.2 & -0.2 \\ -0.75 & 0 & -0.25 \\ -0.5 & -0.5 & 0 \end{bmatrix}
            $$ 

            Devemos primeiramente calcular os betas: 

            <div class="equacao">
            $$
            \begin{aligned}            
            \beta_1 &= |-0.2| + |-0.2| = 0.4 \\
            \beta_2 &= |-0.75|*\beta_1 + |-0.25| \\
                    &= 0.75*0.4 + 0.25 = 0.55 \\
            \beta_3 &= |-0.5|*\beta_1 + |-0.5|*\beta_2 \\
                    &= 0.5*0.4 + 0.5*0.55 = 0.475.
            \end{aligned}
            $$
            </div>            
            Logo, o número de Sassenfeld é $\beta_B = \max\{0.4; 0.55; 0.475\} = 0.55$.

            </p>
            </div>
        </div>
    </div>
</div>

<br><br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Convergência - Método de Jacobi</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">                    
            <p align="justify">                        
           Seja $Ax=b$ um sistema linear com matriz de iteração $B$. O método de Jacobi converge caso um dos critérios abaixo seja satisfeito:
           <blockquote>
           <ul>
             <li>$||B||_L < 1$ (critério das linhas)</li>
             <li>$||B||_C < 1$ (critério das colunas)</li>
           </ul>
           </blockquote>    
           Caso nenhum critério seja atendido, nada podemos afirmar sobre a convergência pelo método de Jacobi.

            </p>
            </div>
        </div>
    </div>
</div>
<br><br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Convergência - Método de Gauss-Seidel</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">                    
            <p align="justify">                        
           Seja $Ax=b$ um sistema linear com matriz de iteração $B$. O método de Gauss-Seidel converge caso um dos critérios abaixo seja satisfeito:
           <blockquote>
           <ul>
             <li>$||B||_L < 1$ (critério das linhas)</li>
             <li>$\beta_B < 1$ (critério de Sassenfeld)</li>
           </ul>
           </blockquote>
           Caso nenhum critério seja atendido, nada podemos afirmar sobre a convergência pelo método de Gauss-Seidel.
            </p>
            </div>
        </div>
    </div>
</div>
<br><br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Critério das Linhas versus Diagonal Dominante</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">                    
            <p align="justify">                        
            o critério da norma linha ($||B||_L < 1$) tem um equivalente denominado critério da diagonal dominante. Nesse critério, basta verificar se a matriz de coeficientes $A$ do sistema tem os elementos de sua diagonal principal maiores em módulo do que a soma absoluta dos outros elementos da linha correspondente. Exemplo:<br><br>
                $$
                A =\begin{bmatrix}10 & 2 & 1 \\ 1 & 5 & 1 \\ 2 & 3 & 10 \end{bmatrix}
                $$

                $$
                 L_1 \rightarrow  \mid 2 \mid + \mid 1 \mid = 3 <  \mid 10 \mid  \\
                 L_2 \rightarrow  \mid 1 \mid + \mid 1 \mid  = 2 < \mid 5 \mid   \\
                 L_3 \rightarrow  \mid 2 \mid + \mid 3 \mid  = 5 <  \mid 10 \mid   
                $$ <br>
                Logo, verificamos que a matriz de coeficientes desse sistema tem a diagonal dominante e portanto o critério da norma linha é satisfeito.
            </p>
            </div>
        </div>
    </div>
</div>
<br><br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
            Agora que já vimos todos os critérios de convergência, podemos verificar se um dado sistema tem garantia de convergência para algum dos métodos iterativos. É importante salientar que os critérios são independentes, ou seja, se um for satisfeito, o outro não precisa ser. <br><br>

            Para <b>Jacobi</b>, deve ser satisfeito o critério da norma linha <strong>ou</strong> o critério da norma coluna.   <br><br>

            Para <b>Gauss-Seidel</b>, deve ser satisfeito o critério da  norma linha  <strong>ou</strong>  o critério de Sassenfeld. <br><br>

            Sabendo disso, verifique se podemos obter a solução do sistema abaixo utilizando os métodos de Jacobi e de Gauss-Seidel. <br><br>

            $$ \begin{aligned} 5x_{1} + x_{2} + x_{3} = 5
                \\ 3x_{1} + 4x_{2} + x_{3} = 6
                \\ 3x_{1} + 3x_{2} +6x_{3} = 0
            \end{aligned}$$ 

            Sabemos que: 

            $$
            A = \begin{bmatrix}5 & 1 & 1 \\ 3 & 4 & 1 \\ 3 & 3 & 6 \end{bmatrix}. 
            $$ 

            O primeiro passo é obter a matriz de iteração B. Divimos primeiro cada elemento de A pelo elemento da diagonal principal da linha correspondente:

            $$
            A = \begin{bmatrix}1 & \frac{1}{5} & \frac{1}{5} \\ \frac{3}{4} & 1 & \frac{1}{4} \\ \frac{3}{6} & \frac{3}{6} & 1 \end{bmatrix} 
            $$ 

            Depois diminuímos A da matriz identidade e obtemos B = I-A: 

            <div class="equacao">
            $$
            B = \begin{bmatrix}1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}
            \space - \begin{bmatrix}1 & 0.2 & 0.2 \\ 0.75 & 1 & 0.25 \\ 0.5 & 0.5 & 1 \end{bmatrix} 
            \space = \begin{bmatrix}0 & -0.2 & -0.2 \\ -0.75 & 0 & -0.25 \\ -0.5 & -0.5 & 0 \end{bmatrix} 
            $$
            </div> 

            Critério das linhas:

            $$
            L_1 \rightarrow  \mid -0.2 \mid + \mid -0.2 \mid = 0.4 \\
            L_2 \rightarrow  \mid -0.75 \mid + \mid -0.25 \mid  =  1  \\
            L_3 \rightarrow  \mid -0.5 \mid + \mid -0.5 \mid  = 1   
            $$ 

            Como $\parallel B \parallel _{L} = 1$, então a norma linha não é menor do que um e o critério das linhas não foi satisfeito. Logo, ainda não podemos afirmar nada sobre a convergência. 
            <br><br>

            Critério das colunas: 
            $$
            C_1 \rightarrow  \mid -0.75 \mid + \mid -0.5 \mid = 1.25 \\
            C_2 \rightarrow  \mid -0.2 \mid + \mid -0.5 \mid  =  0.7  \\
            C_3 \rightarrow  \mid -0.2 \mid + \mid -0.25 \mid  = 0.45   
            $$

            Como $\parallel B \parallel _{L} = 1.25 > 1$, o critério das colunas também não foi satisfeito e não podemos afirmar nada sobre a convergência desse sistema utilizando o método de Jacobi. <br><br>

            Falta testar o critério de Sassenfeld para saber se o sistema convergirá para Gauss-Seidel.

             <div class="equacao">
            $$
            \begin{aligned}
            \beta_1 &= |-0.2| + |-0.2| = 0.4 \\
            \beta_2 &= |-0.75|*\beta_1 + |-0.25| \\
                    &= 0.75*0.4 + 0.25 = 0.55 \\
            \beta_3 &= |-0.5|*\beta_1 + |-0.5|*\beta_2 \\
                    &= 0.5*0.4 + 0.5*0.55 = 0.475.
            \end{aligned}
            $$
            </div>
            Logo, $\beta_B = \max\{0.4; 0.55; 0.475\} = 0.55 < 1$. Concluímos que o método de Gauss-Seidel irá convergir para esse sistema.
            </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Critérios de Convergência',
          'todosExercicios' => $todosExercicios[22],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>
</div>

<script>
    renderMathInElement(document.body, {
        delimiters: [
            {left: "$$", right: "$$", display: true},
            {left: "$", right: "$", display: false}
        ]
    });
</script>
