<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Questoes]].
 *
 * @see Questoes
 */
class QuestoesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Questoes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Questoes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
