<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionIndex()
    {
        $auth = Yii::$app->authManager;

        // add "responderQuestao" permission 
        $responderQuestao = $auth->createPermission('responderQuestao');
        $responderQuestao->description = 'Responder uma questao e inserir o resultado no Banco de Dados';
        $auth->add($responderQuestao);

        // add "acessarEstatisticasAluno" permission 
        $acessarEstatisticasAluno = $auth->createPermission('acessarEstatisticasAluno');
        $acessarEstatisticasAluno->description = 'Acessar estatisticas de um aluno';
        $auth->add($acessarEstatisticasAluno);

        // add "acessarEstatisticasTurma" permission
        $acessarEstatisticasTurma = $auth->createPermission('acessarEstatisticasTurma');
        $acessarEstatisticasTurma->description = 'Acessar estatisticas de um turma';
        $auth->add($acessarEstatisticasTurma);

        // add "updateSite" permission 
        $atualizarSite = $auth->createPermission('atualizarSite');
        $atualizarSite->description = 'Atualizar o banco de dados do site';
        $auth->add($atualizarSite);

        // add "aluno" role and give this role "responderQuestao" and  "acessarEstatisticasAluno" permissions
        $aluno = $auth->createRole('aluno');
        $aluno->description = "Aluno UFRN";
        $auth->add($aluno);
        $auth->addChild($aluno, $responderQuestao);
        $auth->addChild($aluno, $acessarEstatisticasAluno);

        // add "docente" role and give this role the "acessarEstatisticasTurma" permission
        $docente = $auth->createRole('docente');
        $docente->description = "Docente UFRN";
        $auth->add($docente);
        $auth->addChild($docente, $acessarEstatisticasTurma);        

        // add "admin" role and give this role the "atualizarSite" permission
        // as well as the permissions of "aluno" and "docente" roles
        $admin = $auth->createRole('admin');
        $admin->description = "Usuario adminstrador";
        $auth->add($admin);
        $auth->addChild($admin, $atualizarSite);
        $auth->addChild($admin, $aluno);
        $auth->addChild($admin, $docente);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        //$auth->assign($logado, 1);
        //$auth->assign($admin, 2);
    }
}