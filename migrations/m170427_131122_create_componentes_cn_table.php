<?php

use yii\db\Migration;
use mdm\admin\components\Configs;

/**
 * Handles the creation of table `componentes_cn`.
 */
class m170427_131122_create_componentes_cn_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $userTable = Configs::instance()->userTable;
        
        // Componentes de CN da UFRN 
        if ($this->db->schema->getTableSchema('componentes_cn', true) === null) {  
            $this->createTable('componentes_cn', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer()->notNull(),
                'codigo' => $this->string(15)->notNull(),
                'nome' => $this->string(100)->notNull(),
                'created_at' => $this->integer(),
                'updated_at' => $this->integer(),
            ]);

            $this->addForeignKey('fk-componentes_cn-user_id-user-id', 'componentes_cn', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

            // creates index for column `codigo`
            $this->createIndex(
                'idx-componentes_cn-codigo',
                'componentes_cn',
                'codigo'
            );
        }

        // Exercicios  - Eventos em que cada prova tem somente uma questao 
        if ($this->db->schema->getTableSchema('exercicios', true) === null) { 
            $this->createTable('exercicios', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer()->notNull(),
                'descricao' => $this->string(150)->notNull(),
                'evento_id' => $this->integer()->notNull(),
                'imagens' => $this->smallInteger()->notNull(),
                'created_at' => $this->integer(),
            ]);

            $this->addForeignKey('fk-exercicios-user_id-user-id', 'exercicios', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        }

        // Questoes  - Cada exercicio pode ter varias questoes 
        if ($this->db->schema->getTableSchema('questoes', true) === null) { 
            $this->createTable('questoes', [
                'id' => $this->primaryKey(),
                'exercicios_id' => $this->integer()->notNull(),
                'enunciado' => $this->text()->notNull(),
                'id_questao_instanciada' => $this->integer(),
                'usos' => $this->integer()->defaultValue(0),
            ]);

            $this->addForeignKey('fk-questoes-exercicios_id-exercicios-id', 'questoes', 'exercicios_id', 'exercicios', 'id', 'CASCADE', 'CASCADE');
        }

        // Alternativas  - Cada questao pode ter varias alternativas 
        if ($this->db->schema->getTableSchema('alternativas', true) === null) { 
            $this->createTable('alternativas', [
                'id' => $this->primaryKey(),
                'questoes_id' => $this->integer()->notNull(),
                'alternativa' => $this->text()->notNull(),
                'letra' => $this->string(2),
                'matriz' => $this->string(2),
            ]);

            $this->addForeignKey('fk-alternativas-questoes_id-questoes-id', 'alternativas', 'questoes_id', 'questoes', 'id', 'CASCADE', 'CASCADE');
        }

        // Respostas das questoes
        if ($this->db->schema->getTableSchema('respostas', true) === null) { 
            $this->createTable('respostas', [
                'id' => $this->primaryKey(),
                'vinculo_id' => $this->integer()->notNull(),
                'exercicio_id' => $this->integer()->notNull(),
                'questao_id' => $this->integer()->notNull(),
                'turma_id' => $this->integer()->notNull(),
                'resposta' => $this->string(2),
                'matriz' => $this->string(2),
                'acertou' => $this->smallinteger()->defaultValue(0),
                'created_at' => $this->integer(),

            ]);

            $this->addForeignKey('fk-respostas-vinculo_id-alunos-id', 'respostas', 'vinculo_id', 'vinculos', 'id', 'CASCADE', 'CASCADE');
            $this->addForeignKey('fk-respostas-exercicio_id-exercicios-id', 'respostas', 'exercicio_id', 'exercicios', 'id', 'CASCADE', 'CASCADE');
            $this->addForeignKey('fk-respostas-questao_id-questoes-id', 'respostas', 'questao_id', 'questoes', 'id', 'CASCADE', 'CASCADE');
            $this->addForeignKey('fk-respostas-turma_id-turmas-id', 'respostas', 'turma_id', 'turmas', 'id', 'CASCADE', 'CASCADE');
        }


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex(
            'idx-componentes_cn-codigo',
            'componentes_cn'
        );
        $this->dropTable('componentes_cn');

        $this->dropTable('respostas');      

        $this->dropTable('alternativas');
        $this->dropTable('questoes');
        $this->dropTable('exercicios');


    }
}
