<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model frontend\models\ComponentesCn */

$this->title = 'Novo Componente CN';
$this->params['breadcrumbs'][] = ['label' => 'Componentes', 'url' => ['componentes-cn-index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="componentes-cn-create">

    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('componentesCn_Form', [
        'model' => $model,
    ]) ?>

</div>
