<?php

namespace app\controllers;

use Yii;
use app\models\ComponentesCn;
use app\models\ComponentesCnSearch;
use app\models\CadastroExercicio;
use app\models\Exercicios;
use app\models\ExerciciosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use app\models\UploadForm;
use yii\web\UploadedFile;

/**
 * AdminController implements the CRUD actions for ComponentesCn model.
 */
class AdministrarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'componentes-cn-index',
                            'componentes-cn-view',
                            'componentes-cn-create',
                            'componentes-cn-update',
                            'componentes-cn-delete',
                            'exercicios-index',
                            'exercicios-view',
                            'exercicios-create',
                            'exercicios-update',
                            'exercicios-delete',
                            'cadastrar-exercicio',
                            'migrar',
                        ],
                        'allow' => true,
                        'roles' => ['admin'],
                    ]                 
                ],
                'denyCallback' => function ($rule, $action) {
                    return $action->controller->redirect('/site/acesso-negado');
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'componentes-cn-delete' => ['POST'],
                ],
                'actions' => [
                    'exercicios-delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ComponentesCn models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ComponentesCnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('componentesCnIndex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /*

            CADASTRO DE COMPONENTES CURRICULARES 
            DE COMPUTACAO NUMERICA
    */

    /**
     * Lists all ComponentesCn models.
     * @return mixed
     */
    public function actionComponentesCnIndex()
    {
        $searchModel = new ComponentesCnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('componentesCnIndex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ComponentesCn model.
     * @param integer $id
     * @return mixed
     */
    public function actionComponentesCnView($id)
    {
        return $this->render('componentesCnView', [
            'model' => $this->findModelComponentesCn($id),
        ]);
    }

    /**
     * Creates a new ComponentesCn model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionComponentesCnCreate()
    {
        $model = new ComponentesCn();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            if ($model->save()){
                return $this->redirect(['componentes-cn-view', 'id' => $model->id]);    
            }        
        } else {
            return $this->render('componentesCnCreate', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ComponentesCn model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionComponentesCnUpdate($id)
    {
        $model = $this->findModelComponentesCn($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['componentes-cn-view', 'id' => $model->id]);
        } else {
            return $this->render('componentesCnUpdate', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ComponentesCn model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionComponentesCnDelete($id)
    {
        $this->findModelComponentesCn($id)->delete();

        return $this->redirect(['componentes-cn-index']);
    }

    /**
     * Finds the ComponentesCn model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ComponentesCn the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelComponentesCn($id)
    {
        if (($model = ComponentesCn::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /*

            CADASTRO DE EXERCICIOS DO MULTIPROVA

    */

    /**
     * Creates a new ComponentesCn model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCadastrarExercicio()
    {
        $model = new CadastroExercicio();

        if ($model->load(Yii::$app->request->post())) {
            
            $model->arquivo = UploadedFile::getInstance($model, 'arquivo');            
            $uploadOk = $model->upload();

            if ($uploadOk === true) {
                // file is uploaded successfully
                // Insere as informacoes no BD
                $resumo = $model->gravarExercicio();                   
            }             
            return $this->render('cadastrarExercicioOk', ['model' => $model, 'resumo' => $resumo]);

        } else {
            return $this->render('cadastrarExercicio', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Lists all Exercicios models.
     * @return mixed
     */
    public function actionExerciciosIndex()
    {
        $searchModel = new ExerciciosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('exerciciosIndex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Exercicios model.
     * @param integer $id
     * @return mixed
     */
    public function actionExerciciosView($id)
    {
        return $this->render('exerciciosView', [
            'model' => $this->findModelExercicios($id),
        ]);
    }

    /**
     * Creates a new Exercicios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionExerciciosCreate()
    {
        $model = new CadastroExercicio();

        if ($model->load(Yii::$app->request->post())) {
            
            $model->arquivo = UploadedFile::getInstance($model, 'arquivo');            
            $uploadOk = $model->upload();

            if ($uploadOk === true) {
                // file is uploaded successfully
                // Insere as informacoes no BD
                $resumo = $model->gravarExercicio();    
            }             
            return $this->render('exerciciosCreated', ['model' => $model, 'resumo' => $resumo]);

        } else {
            return $this->render('exerciciosCreate', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Exercicios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionExerciciosUpdate($id)
    {
        $model = $this->findModelExercicios($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['exercicios-view', 'id' => $model->id]);
        } else {
            return $this->render('exerciciosUpdate', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Exercicios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionExerciciosDelete($id)
    {
        $this->findModelExercicios($id)->delete();

        return $this->redirect(['exercicios-index']);
    }

    /**
     * Migrar Exercicios.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    */
    
    public function actionMigrar($id)
    {       
        $modelCadastroExercicios = New CadastroExercicio();
        if ($modelCadastroExercicios->gerarImagens($id)) 
        echo "Sucesso";                
    }  
    /**
     * Finds the Exercicios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Exercicios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelExercicios($id) 
    {
        if (($model = Exercicios::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }    



}

