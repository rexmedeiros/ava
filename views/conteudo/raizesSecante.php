

<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View 
https://pt.slideshare.net/renangpsoares/aula-07-renan-cn
http://www1.univap.br/spilling/CN/CN_Capt2.pdf
http://paginas.fe.up.pt/~faf/mnum/mnum-faf-handout.pdf


*/




$this->title = 'Raizes';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i>Vídeoaula</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="https://www.youtube.com/embed/2Ncue_rkH0Q" allowfullscreen></iframe>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Secante - Introdução</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        No método de Newton precisamos calcular a derivada da função, o que pode
        ser inconveniente. O método da Secante é uma alternativa ao método de Newton, onde se faz
        uso de uma reta secante como uma aproximação da reta tangente em um determinado ponto.

        <br>

      </div>
    </div>
  </div>
</div>

<!--
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Critérios de convergência</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


                Assim como o método de Newton, para que o método da Secante convirja alguns critérios precisam ser
                satisfeitos:<br><br>


                <p style=" padding-left: 5%">
                    1. Encontrar uma estimativa $\;x0\;$e$ \; x1\;$ próximo suficiente da raiz
                    <br></p>
                <p style=" padding-left: 5%">
                    2. Não ter pontos de máximos ou mínimos próximos a raiz , $\;F'(x) = 0$.
                    <br></p>
                <p style=" padding-left: 5%">
                    3. Não ter ponto de inflexão na raiz,$\;F''(x) = 0$.
                    <br></p>
                <br>


                A imagem abaixo mostra uma região de inflexão na raiz.
                <br><br>
                <p style="
  text-align: center;

  
">
                    <img src="imagens/secante23.png">
                </p>

                <br><br>


                Verificamos que dado dois pontos $\;x_{1}\;$ e $\;x_{2}\;$, a próxima iteração calcula um $\;x_{3}\;$
                mais afastado. Como o método diverge, não há vantagem utiliza-lo nesse caso.
                <br><br>
            </div>
        </div>
    </div>
</div>
-->

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"> </i> Método da Secante</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Deseja-se encontrar um valor específico de $x$ que satisfaz a equação
        $$
        f(x) = 0.
        $$

        No método da secante são necessários dois chutes iniciais para que uma nova estimativa da raiz
        seja determinada. Através de uma análise (ex. gráfica), encontramos dois valores,
        $x_0$ e $x_1$, que sejam próximos o suficiente da raiz procurada. A próxima estimativa da raiz,
        $x_2$, é a abscissa do ponto de intersecção entre o eixo $X$ e a reta que passa pelos pontos
        $(x_0,f(x_0))$ e $(x_1,f(x_1))$, conforme ilustração abaixo.

        <br>
        <p style="text-align: center;">
          <img src="imagens/secante.png">
        </p>

        <br>
        O valor de $x_2$ é obtido por análise trigonométrica:


        <div class="equacao">
          $$\tan(\alpha) = \frac{f(x_{1})}{x_{1}\,- \; x_{2}}. $$
        </div>
        Isolando $x_2$:
        <div class="equacao">
          $$x_{2} = x_{1} - \frac{f(x_{1})}{\tan(\alpha) }.$$
        </div>
        O valor de $\tan (\alpha)$ pode ser escrito como:
        <div class="equacao">
          $$\; \tan (\alpha)= \frac{f(x_{0})\;-\;f(x_{1})}{x_{0}\, -\;x_{1} }.$$
        </div>

        Então, para cada iteração do método, encontra-se a próxima aproximação da raiz da seguinte forma:
        <br>
        <div class="equacao">
          $$x_{n+1} = x_{n}- \frac{f(x_{n}) \cdot (x_{n-1} -x_{n})}{f(x_{n-1})-f(x_{n})}.$$
        </div>
        <br>
        O procedimento é repetido até que o critério de parada seja satisfeito, ou seja, até que:
        $$
        E_r = \left|\frac{x_{n+1}-x_n}{x_{n+1}}\right| < \epsilon.
        $$

        <br><br>

      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        Utilizar método de Newton para encontrar aproximação da raiz para a função $x^{2} +x -5$ com critério de parada $E_{r} < 0.03$.

        <br><br>

        <strong>Estimativa inicial</strong>

        <br>
        Primeiramente, fazemos uma análise gráfica para encontrar dois pontos iniciais relativamente próximos da
        raiz.


        <br>
        <br>


        <p style="
  text-align: center;

  
">
          <img src="imagens/secante3.PNG">
        </p>

        <br>

        Escolhemos $x_{0}=3.01$ e $x_{1}=3$.

        <br><br>

        <strong>Iteração $1$ </strong> <br><br>

        Encontrar o valor aproximado de $\overline x$:
        <br><br>


        <div class="equacao">
          $$
          \begin{aligned}
          x_{2} &= x_{1}- \frac{f(x_{1}) (x_{0} -x_{1})}{f(x_{0})-f(x_{1})} \\
          &=3- \frac{7\cdot 0.01}{7,0701-7} \\
          &= 3 - 0,99857 = 2,00143
          \end{aligned}
          $$
        </div>

        <br>

        Calculando o erro relativo para verificar o critério de parada:
        <br>

        <div class="equacao">
          $$
          \begin{aligned}
          E_{r} &= \left| \frac{x_2 -x_1}{x_2}\right| \\
          &= \left|\frac{2,00143-3}{2,00143}\right| > 0.03 \\
          \end{aligned}
          $$
        </div>
        <br>

        Como o critério não foi satisfeito, o método continua.

        <br>
        <br>
        <strong>Iteração $2$ </strong>
        <br><br>

        Encontrar $\;x_{3}$:
        <br><br>

        <div class="equacao">
          $$
          \begin{aligned}
          x_{3} &= x_{2}- \frac{f(x_{2}) (x_{1} -x_{2})}{f(x_{1 })-f(x_{2})}\\
          &= 2,00143 - \frac{1,00714 \cdot 0,99857}{7 -1,00714} \\
          &= 1,83361 \\
          \end{aligned}
          $$
        </div>

        <br>
        Calculando o erro:
        <br><br>

        <div class="equacao">
          $$
          \begin{aligned}
            E_{r} &= \left| \frac{x_3 -x_2}{x_3}\right| \\
            &= \left|\frac{1,83361-2,00143}{1,83361}\right| > 0.03 \\
          \end{aligned}
          $$
        </div>

        <br>
        Logo o método continua.

        <br><br>

        <strong>Iteração $3$</strong>
        <br><br>

        Encontrar $\;x_{4}$:
        <br><br>

        <div class="equacao">
          $$
          \begin{aligned}
          x_{4} &= x_{3}- \frac{f(x_{3}) (x_{2}-x_{3})}{f(x_{2})-f(x_{3})}\\
          &= 1,83361- \frac{0,19574 \cdot 0,16782}{1,00713-0,19574} \\
          &= 1,79313 \\
          \end{aligned}
          $$
        </div>


        <br>
        Calculando o erro:
        <br><br>

        <div class="equacao">
          $$
          \begin{aligned}
            E_{r} &= \left| \frac{x_4 -x_3}{x_4}\right| \\
            &= \left|\frac{1,79313-1,83361}{1,79313}\right| < 0.03 \\
          \end{aligned}
          $$
        </div>
        <br><br>

        Chegando na precisão desejada.
        <br><br>
        A raiz aproximada então é $\;x\;= 1.79313$.


        <br><br>
      </div>

    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="conteudo">
        <p>Use o método da secante para encontrar a raiz positiva da equação abaixo com  $E_r < 0.01$.</p><br>
        <p style=" padding-left: 5%"> $$ \frac{x^2}{5} - 2x = 3.$$
        </p>

        <br>

        <p>Solução:</p>
        <form id="geo">
          <input value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

          <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">
        </form>
        <div id="applet_container" align="center"></div>

        <br>
        <ol id="demo" style=" padding-left: 17px">

          <li>
            Fazendo-se uma análise gráfica, podemos usar $x_0 = 11.9$ e $x_1 = 11.7$ como chutes iniciais.

          </li>
          <br><br>
        </ol>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p>
        <p style="text-align: center;"">
        <img src="imagensDev/Massao/secante.svg" alt="" style="width:90%;"/>
        </p>
        </p>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/K82wOCyccZA" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/raizesSecante.sce"></script>
          </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function diminuirFonte() {
        document.getElementById("conteudo").style.fontSize = "1em";
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {
        "id": "ggbApplet",
        "prerelease": false,
        "width": 600,
        "height": 400,
        "showToolBar": false,
        "borderColor": null,
        "showMenuBar": false,
        "showAlgebraInput": false,
        "showResetIcon": true,
        "enableLabelDrags": false,
        "enableShiftDragZoom": true,
        "enableRightClick": false,
        "capturingThreshold": null,
        "showToolBarHelp": false,
        "errorDialogsActive": true,
        "useBrowserForJS": true,
        "filename": "<?= Url::base() ?>/geogebra/RAIZESBisettGeoGebraSite.ggb"
    };
    var views = {
        'is3D': 0,
        'AV': 1,
        'SV': 0,
        'CV': 0,
        'EV2': 0,
        'CP': 0,
        'PC': 0,
        'DA': 0,
        'FI': 0,
        'PV': 0,
        'macro': 0
    };
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function () {
        applet.inject('applet_container');
    }


    function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(10, 13, -2, 2);
            ggbApplet.setValue('bissec', false); // Valor de h
            ggbApplet.setValue('falseposic', false)  // Mostra Euler;
            ggbApplet.setValue('newton', false)  // Mostra Heun;
            ggbApplet.setValue('secante', true)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n) {
        // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 4;
        ero = ggbApplet.getValue('err(X_{atual}(1,0),X_{anterior 1}(1,0))').toFixed(6);
        ggbApplet.setValue('n', this.n)

        xant1 = ggbApplet.getValue('X_{anterior 1}(1,0)').toFixed(decimais);
        xant0 = ggbApplet.getValue('X_{anterior 0}(1,0)').toFixed(decimais);
        Fxant1 = ggbApplet.getValue('f_1(X_{anterior 1}(1,0))').toFixed(decimais);
        Fxant0 = ggbApplet.getValue('f_1(X_{anterior 0}(1,0))').toFixed(decimais);
        erro = ggbApplet.getValue('err(X_{atual}(1,0),X_{anterior 1}(1,0))').toFixed(decimais);

        xatual = ggbApplet.getValue('X_{atual}(1,0)').toFixed(decimais);
        switch (n) {
            case 1:
                saida = "Iteração 1: \
                <div class=\"equacao\">\
                \$\$ \
                \\begin{aligned} \
                     x_{2} =& x_{1} - \\frac{f(x_{1})  (x_{0} - x_{1})}{f(x_{0})-f(x_{1})}  \\\\ \
                    =&  " + xant1 + " -  \\frac{f(" + xant1 + ")  (" + xant0 + "   - " + xant1 + ")}{f(" + xant0 + ")-f(" + xant1 + ")}  \\\\ \
                    =& " + xant1 + " -  \\frac{" + Fxant1 + "  (" + xant0 + "   - " + xant1 + ")}{" + Fxant0 + "-" + Fxant1 + "}  \\\\ \
                    =& " + xatual + " \
                    \\end{aligned}\
                    \$\$\
                    </div>";

                break;
            case 2:
                saida = "Verifica o erro relativo: \
                  \$\$\
                    \\begin{aligned} \
                           E_{r}  =&  \\frac{|x_2 - x_1|}{x_2} \\\\ \
                            =& \\left| \\frac{" + xatual + " - " + xant1 + "}{" + xatual + "} \\right| \\\\ \
                            =& " + erro + " > 0.001. \
                    \\end{aligned} \
                 \$\$\
                \ O método continua. ";

                break;
            case 3:
                saida = "Iteração 2: \
                <div class='equacao'> \
                \$\$\
               \\begin{aligned} \
                      x_{3}  =&  " + xant1 + " -  \\frac{f(" + xant1 + ") (" + xant0 + "   - " + xant1 + ")}{f(" + xant0 + ")-f(" + xant1 + ")}  \\\\ \
                      =&  " + xant1 + " -  \\frac{" + Fxant1 + " (" + xant0 + "   - " + xant1 + ")}{" + Fxant0 + "-" +
                    Fxant1 + "}  \\\\ \
                                     =& " + xatual + " \
                    \\end{aligned}\
                    \$\$\
                    </div>";

                break;
            case 4:
                saida = "Verifica o erro relativo: \
                  \$\$\
                    \\begin{aligned} \
                           E_{r}  =&  \\frac{|x_3 - x_2|}{x_3} \\\\ \
                            =& \\left| \\frac{" + xatual + " - " + xant1 + "}{" + xatual + "} \\right| \\\\ \
                            =& " + erro + " > 0.001. \
                    \\end{aligned} \
                 \$\$\
                \ O método continua. ";

                break;
            case 5:
                saida = "Iteração 3: \
               <div class='equacao'>\
               \$\$\
               \\begin{aligned} \
                     x_{4} =&  " + xant1 + " -  \\frac{f(" + xant1 + ") (" + xant0 + "   - " + xant1 + ")}{f(" + xant0 + ")-f(" + xant1 + ")}  \\\\ \
                          =&  " + xant1 + " -  \\frac{" + Fxant1 + " (" + xant0 + "   - " + xant1 + ")}{" + Fxant0 + "-" +
                    Fxant1 + "}  \\\\ \
                           =& " + xatual + " \
                    \\end{aligned}\
                    \$\$\
                    </div>";


                break;
            case 6:
                saida = "Verifica o erro relativo: \
                  \$\$\
                    \\begin{aligned} \
                           E_{r}  =&  \\frac{|x_3 - x_2|}{x_3} \\\\ \
                            =& \\left| \\frac{" + xatual + " - " + xant1 + "}{" + xatual + "} \\right| \\\\ \
                            =& " + erro + "4 < 0.001. \
                    \\end{aligned} \
                 \$\$\
                \ Logo  a raiz é  $ \\; x = \\;" + xatual + " \\;$ com três algarismos significativos. ";
                break;



            //alert('Default case');
        }


        return saida;
    }

    function anterior() {
        if (this.n % 2 == 0) {
            ggbApplet.evalCommand("  ZoomIn[1/(" + n + "*" + n + "),(11.32455,0)]")
        }
        if (this.n >= 1) {
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);

            removerItem(this.n + 1);
            return true;
        }
        else {
            alert("Vá para a próxima etapa.");
            return false;
        }


    }

    function proximo() {
        if (this.n <= 5) {

            this.n++;
            ggbApplet.setValue('n', this.n)

            if (this.n % 2 == 0) {
                ggbApplet.evalCommand("  ZoomIn[" + n + "*" + n + ",(11.32455,0)]")
            }


            texto = mensagens(this.n);
            acrescentarItem(this.n, texto)

            renderizarKatex()
            return true;
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId, texto) {
        var entry = document.createElement('li');
        entry.innerHTML = texto;
        //entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id', 'item' + this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid) {
        var item = document.getElementById('item' + itemid);
        list.removeChild(item);
    }

</script>

<script type="text/javascript">
    function renderizarKatex(){
        renderMathInElement(document.body,{delimiters: [
                {left: "$$", right: "$$", display: true},
                {left: "$", right: "$", display: false}
            ]});
    }
</script>



<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>