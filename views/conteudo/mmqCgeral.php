<?php

use yii\helpers\Html;
use app\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'MMQ - Caso Geral';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>
<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Introdução</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p align="justify">
                    Após estudar a regressão linear e a regressão polinomial, veremos agora o caso geral. Suponha que temos uma função que é dada por uma combinação linear de várias funções, na forma:

                    <div class="equacao">
                        $$
                        y = a_0f_0(x) + a_1f_1(x) + a_2f_2(x) + ... + a_mf_m(x),
                        $$
                    </div>

                    onde $f_0, f_1, ... , f_m$ são $m + 1$ funções-base. <br><br>

                    A regressão linear é um caso particular do modelo acima, bastando tomar $m=1$, $f_0 = 1$ e $ f_1 = x$. A regressão polinomial é obtida quando as funções-base forem monômios simples: $f_0 = x^0 = 1, f_1 = x, f_2 = x^2, ... , f_m = x^m$. <br><br>

                    Suponha que temos um conjunto de $n+1$ pontos $\left\{{(x_0,y_0), (x_1, y_1), ... , (x_n, y_n)}\right\}$ e queremos obter o conjunto de coeficientes ${a_0, a_1, ..., a_m}$ que melhor aproximem uma função $y$ como descrito acima. Devemos resolver um sistema da forma: 

                    <div class="equacao">
                    $$
                    \begin{cases}a_0f_0(x_0) + a_1f_1(x_0)+ \dots+a_mf_m(x_0) = y_0 
                    \\a_0f_0(x_1) + a_1f_1(x_1)+ \dots+a_mf_m(x_1) = y_1
                    \\ \vdots
                    \\ a_0f_0(x_n) +a_1f_1(x_n)+ \dots + a_mf_m(x_n) = y_n 
                    \end{cases}
                    $$ 
                    </div>

                    Podemos utilizar o método dos mínimos quadrados para achar a melhor solução para esse sistema. De forma similar como fizemos anteriormente, devemos minimizar a soma dos quadrados dos resíduos, dada nesse caso por 

                    <div class="equacao">
                    $$
                    S_r = \sum_{i=1}^n \left(y_i - \sum_{j=0}^m a_jf_{ji}(x_i)\right)^2.
                    $$ 
                    </div>

                    Tomando a derivada dessa quantidade em relação a cada uma dos coeficientes e igualando a equação resultante a zero, podemos escrever as equações resultantes na forma matricial 

                    $$
                    (F^TF)A = F^TY,
                    $$

                    onde a matriz $A$ é o vetor coluna de coeficientes (incógnitas) e a matriz $F$ é uma matriz de de dimensão $n(m+1)$, sendo $n$ o número de pontos coletados e $m$ o número de funções-base: 

                    <div class="equacao">
                    $$
                    F = \begin{bmatrix}
                        f_{0}(x) \space f_{1}(x)\space \dots \space f_{m}(x)
                        \\f_{0}(x) \space f_{1}(x)\space \dots \space f_{m}(x)
                        \\ \vdots
                        \\f_{0}(x) \space f_{1}(x)\space \dots \space f_{m}(x)
                        \end{bmatrix}.
                    $$ 
                    </div>

                    De forma alternativa, podemos expressar a equação matricial da seguinte forma:

                    <div class="equacao">
                    $$
                    \begin{bmatrix}
                    \sum f_{0}(x)f_{0}(x) \space \sum f_{0}(x)f_{1}(x)\space \dots \space \sum f_{0}(x)f_{m}(x)
                    \\ \sum f_{1}(x)f_{0}(x) \space \sum f_{1}(x)f_{1}(x)\space \dots \space \sum f_{1}(x)f_{m}(x)
                    \\ \vdots
                    \\ \sum f_{m}(x)f_{0}(x) \space \sum f_{m}(x)f_{1}(x)\space \dots \space \sum f_{m}(x)f_{m}(x)
                    \end{bmatrix}
                    *
                    \begin{bmatrix}
                    a_0
                    \\ a_1
                    \\ \vdots
                    \\ a_m
                    \end{bmatrix}
                    = 
                    \begin{bmatrix}
                    \sum f_0(x)y
                    \\ \sum f_1(x)y
                    \\ \vdots
                    \\ \sum f_m(x)y
                    \end{bmatrix}.
                    $$ 
                    </div>
                </p>
			</div>
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <p align="justify">
            Encontre a função $f(x) = a_0cos(x) + a_1e^x$ que melhor se ajusta ao conjunto de pontos: <br>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>${x_i}$</th>
                    <th>${y_i}$</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th>${0}$</th>
                    <td>${3.18}$</td>
                </tr>
                    <tr>
                    <th>${1}$</th>
                    <td>${3.9}$</td>
                </tr>
                    <tr>
                    <th>${2}$</th>
                    <td>${6.5}$</td>
                </tr>
                <tr>
                    <th>${3}$</th>
                    <td>${17.82}$</td>
                </tr>
                </tbody>
            </table>

            <b>Solução:</b>
            Para achar os melhores coeficientes $a_0$ e $a_1$ que ajustam nossa função, devemos resolver um sistema matricial na forma: <br><br>

            <div class="equacao">
            $$
            \begin{bmatrix} \sum f_0(x)f_0(x) & \sum f_0(x)f_1(x)
            \\ \sum f_1(x)f_0(x) & \sum f_1(x)f_1(x)
            \end{bmatrix} 

            \begin{bmatrix}a_0  \\ a_1 \end{bmatrix} 
            = 
            \begin{bmatrix} \sum f_0(x)y \\ \sum f_1(x)y  \end{bmatrix},
            $$</div>

            onde temos que $f_0(x) = cos(x)$ e $f_1(x) = e^x$, ou seja, <br>
            <div class="equacao">
            $$
            \begin{bmatrix} \sum cos(x)cos(x) & \sum cos(x)e^x
            \\ \sum e^xcos(x) & \sum e^x*e^x
            \end{bmatrix} 

            \begin{bmatrix}a_0  \\ a_1 \end{bmatrix} 
            = 
            \begin{bmatrix} \sum cos(x)*y \\ \sum e^x*y  \end{bmatrix}.
            $$ 
            </div>
            <br>
            Calculando os somatórios: <br>

            <table class="table table-bordered">
                <thead>
                 <tr>
                    <th>$ \sum cos(x)cos(x) = \sum (cos(x))^2 = cos(0)^2 + cos(1)^2 + cos(2)^2 + cos(3)^2 =  2.4452$</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th>$ \sum cos(x)e^x = cos(0)*e^0 + cos(1)*e^1 + cos(2)^*e^2 + cos(3)*e^3 =  -20.491$</th>
                  </tr>
                   <tr>
                    <th>$ \sum e^xe^x = \sum e^{2x} = e^{2*0} + e^{2*1} + e^{2*2} + e^{2*3} = 466.42$</th>
                  </tr>
                   <tr>
                    <th>$ \sum cos(x)*y = cos(0)*3.18 + cos(1)*3.9 + cos(2)*6.5 + cos(3)*17.82 =  -15.059$</th>
                  </tr>
                   <tr>
                    <th>$ \sum e^x*y = e^0*3.18 + e^1*3.9 + e^2*6.5 + e^3*17.82 =  419.73$</th>
                  </tr>
                </tbody>
            </table>

            Montando o sistema, temos:

            <div class="equacao">
            $$
            \begin{bmatrix} 2.4452 & -20.491
            \\ -20.491 & 466.42
            \end{bmatrix} 

            \begin{bmatrix}a_0  \\ a_1 \end{bmatrix} 
            = 
            \begin{bmatrix} -15.059 \\ 419.73  \end{bmatrix}.
            $$ 
            </div>

            Resolvendo esse sistema, obtemos:  <br>

            $$
                a_0 = 2.1882,\\
                a_1 = 0.99603.
            $$

            Dessa forma, encontramos  a função que melhor aproxima o conjunto de pontos:

            <div class="equacao">
            $$f(x) = 2.1882cos(x) + 0.99603e^x.$$ 
            </div>

            O gráfico na Figura 1 ilustra o resultado obtido. <br><br>

            <p style="text-align: center;">
                <img src="imagens/CasoGeralIMG.svg" alt=""   style="width:90%;"/><br>
                (Figura 1)
            </p>            

            Para obter o erro quadrático da aproximação, podemos calcular o vetor do erro $\vec e$ e fazer $EQ = |\vec e|^2 = e^Te$, sendo o vetor $\vec e$ dado pela diferença entre os valores de $Y$ e os valores da função obtidos para cada ponto: 

            <div class="equacao">
            $$
            \vec e = \begin{bmatrix}3.18
                \\3.9
                \\6.5
                \\17.82
                \end{bmatrix}
                -
                \begin{bmatrix}2.1882cos(0) + 0.99603e^0
                \\2.1882cos(1) + 0.99603e^1
                \\2.1882cos(2) + 0.99603e^2
                \\2.1882cos(3) + 0.99603e^3
                \end{bmatrix}
                =
                \begin{bmatrix}3.18
                \\3.9
                \\6.5
                \\17.82
                \end{bmatrix}
                -
                \begin{bmatrix}3.1842
                \\3.8898
                \\6.4491
                \\17.839
                \end{bmatrix}
                =
                \begin{bmatrix}-0.0042
                \\0.0102
                \\0.0509
                \\-0.019
                \end{bmatrix}
            $$ </div>

            e

            <div class="equacao">
            $$
            \begin{aligned}
            |\vec e|^2 &= e^Te \\ 
                &=
                \begin{bmatrix}-0.0042 & 0.0102 & 0.0509 & -0.019
                \end{bmatrix}                
                \begin{bmatrix}-0.0042
                \\0.0102
                \\0.0509
                \\-0.019
                \end{bmatrix} \\                
                &= 3.0735\times 10^{-3}.
            \end{aligned}
            $$ 
            </div>

            Ou seja, o erro quadrático total cometido foi de $3.0735\times 10^{-3}$.
            </p>
      </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>

            <br>
            
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/mmmGeral.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
                'titulo' => 'Exercícios - MMQ Caso Geral',
                'todosExercicios' => $todosExercicios[23],
                'modeloResposta' => $modeloResposta,
            ]) ?>
    </div>
</div>

<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":1000,"height":600,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":false,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/exemploGeral.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-0.5,10,-1,6);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}     
    }

    function anterior (){
         if(n>=0){
             ggbApplet.setCoordSystem(-0.5,10,-1,6);
        }
        else{
             ggbApplet.setCoordSystem(-0.5,10,-1,6);
        }
             if (this.n%2==0){
    }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }                                  
    }

    function proximo (){
         if(n <=4){
             ggbApplet.setCoordSystem(-0.5,10,-1,6);
        }
        else{
             ggbApplet.setCoordSystem(-0.5,10,-1,6);
        }

        if (this.n <= 4){
            this.n++;
            ggbApplet.setValue('n', this.n)
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }
</script>

<script>
  renderMathInElement(document.body, {
    delimiters: [{
        left: "$$",
        right: "$$",
        display: true
      },
      {
        left: "$",
        right: "$",
        display: false
      }
    ]
  });
</script>