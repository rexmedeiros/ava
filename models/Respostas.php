<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "respostas".
 *
 * @property integer $id
 * @property integer $vinculo_id
 * @property integer $exercicio_id
 * @property integer $questao_id
 * @property integer $turma_id
 * @property string $resposta
 * @property string $matriz
 * @property integer $acertou
 * @property integer $created_at
 *
 * @property Vinculos $vinculo
 * @property Questoes $questao
 * @property Turmas $turma
 */
class Respostas extends \yii\db\ActiveRecord
{

    public $total;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'respostas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vinculo_id', 'exercicio_id', 'questao_id', 'turma_id','resposta'], 'required', 'message' => 'Selecione uma alternativa.'],
            [['vinculo_id',  'exercicio_id', 'questao_id', 'turma_id', 'acertou'], 'integer'],
            [['resposta', 'matriz'], 'string'],
            [['vinculo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vinculos::className(), 'targetAttribute' => ['vinculo_id' => 'id']],
            [['exercicio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Exercicios::className(), 'targetAttribute' => ['exercicio_id' => 'id']],
            [['questao_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questoes::className(), 'targetAttribute' => ['questao_id' => 'id']],
            [['turma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Turmas::className(), 'targetAttribute' => ['turma_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vinculo_id' => 'Vinculo ID',
            'questao_id' => 'Questao ID',
            'turma_id' => 'Turma ID',
            'resposta' => 'Escolha a alternativa:',
            'matriz' => 'Matriz',
            'acertou' => 'Acertou',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVinculo()
    {
        return $this->hasOne(Vinculos::className(), ['id' => 'vinculo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercicio()
    {
        return $this->hasOne(Exercicios::className(), ['id' => 'exercicio_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestao()
    {
        return $this->hasOne(Questoes::className(), ['id' => 'questao_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurma()
    {
        return $this->hasOne(Turmas::className(), ['id' => 'turma_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function processar($idExercicio)
    {
        $idQuestao = $this->questao_id;
        $questao = Questoes::findOne($idQuestao);
        $alternativas = $questao->alternativas;
        // Procura a correta
        $correta = '';
        $acertou = 0;
        $matriz  = '';
        foreach ($alternativas as $alternativa){
            if ($alternativa->matriz == 'a'){
                if ($this->resposta == $alternativa->letra){
                    $acertou = 1;
                    $correta = $alternativa->letra;
                }                    
            }
            if ($this->resposta == $alternativa->letra){
                $matriz = $alternativa->matriz;          
            }
        }
        
        $deveInserirResposta = false;
        if (!Yii::$app->user->isGuest){
            // Ver se eh aluno
            $autorizacao = \Yii::$app->authManager;
            $atribuicoes = $autorizacao->getAssignments(Yii::$app->user->id);
            if (array_key_exists('aluno', $atribuicoes)){
                $componentesAva = new ComponentesCn();
                $matrizCodigos = $componentesAva->getMatrizComponentes();
                if ($matrizCodigos){
                    $turmasAluno = new TurmasAlunos();
                    $turmaAva = $turmasAluno->getTurmaAlunoComponente($matrizCodigos);
                    if ($turmaAva){
                        $deveInserirResposta = true;
                    }
                }
            }
        }
        
        if ($deveInserirResposta){
        
            // Complementa as informacoes para depois salvar as infos na tab Respostas
            $this->vinculo_id = Yii::$app->user->identity->vinculo_id;
            $this->exercicio_id = $idExercicio;
            $this->turma_id = $turmaAva->turma_id;
            $this->matriz = $matriz;
            $this->acertou = $acertou;

            // agora salva
            $this->save();
        }

        $questao->usos += 1;
        $questao->save();

        $saida = [
            'idQuestao' => $idQuestao,
            'acertou' => $acertou,
        ];
        return $saida;
    }

    











}
