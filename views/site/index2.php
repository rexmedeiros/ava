<?php
//use yii\authclient\Clients\Sigaa;
//use yii\helpers\ArrayHelper;
use \yiister\gentelella\widgets\Panel;
use yii\helpers\Html;


$attributes = array();
//$cliente = new Sigaa;

//$response = $cliente->api('ensino-services/services/consulta/listavinculos/usuario', 'GET');

//$attributes = $cliente->api('ensino-services/services/consulta/listavinculos/usuario', 'GET');
//$attributes = $response;
//$idUsu = ArrayHelper::getValue($attributes, [0,'idUsuario'], $default = null);
//$idUsu = null;
//$attributes = $cliente->getUserAttributes();

/* @var $this yii\web\View */

$this->title = 'Escola de Ciências e Tecnologia/UFRN';
?>

 
<div class="site-index">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-xs-12">
            <div id="w0" class="x_panel">
                <div class="x_title">
                    <h3><i class="fa fa-laptop"></i> Ambiente Virtual de Aprendizagem para Computação Numérica</h2>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-6 col-xs-12">
            <?php
            Panel::begin(
                [
                    'header' => 'Resumo da teoria',
                    'icon' => 'book',
                    'collapsable' => false,
                ]
            )
            ?>
            <p>Acesso rápido a resumos dos conteúdos de CN</p>
            <div align="center">
            <?= Html::img('@web/imagens/ilustracaoConteudo.png', ['alt' => 'Conteúdo']) ?>
            </div>
            <?php Panel::end() ?>
        </div>

        <div class="col-md-6 col-xs-12">
            <?php
            Panel::begin(
                [
                    'header' => 'Videoaulas',
                    'icon' => 'video-camera',
                    'removable' => false,
                ]
            )
            ?>
            <p>Sala de aula invertida</p>
            <div align="center">
            <?= Html::img('@web/imagens/ilustracaoVideoaulas.jpg', ['alt' => 'GeoGebra']) ?>
            </div>

            <?php Panel::end() ?>
        </div>
        
    </div>

    <div class="row">

        <div class="col-md-6 col-xs-12">
            <?php
            Panel::begin(
                [
                    'header' => 'GeoGebra',
                    'icon' => 'area-chart',
                    'removable' => false,
                ]
            )
            ?>
            <p>Ilustrações dos conceitos usando o GeoGebra</p>
            <div align="center">
            <?= Html::img('@web/imagens/ilustracaoGeogebra.png', ['alt' => 'GeoGebra']) ?>
            </div>
            <?php Panel::end() ?>
        </div>

        <div class="col-md-6 col-xs-12">
            <?php
            Panel::begin(
                [
                    'header' => 'Exercícios dinâmicos',
                    'icon' => 'dot-circle-o',
                    'collapsable' => false,
                ]
            )
            ?>
            <p>Questões objetivas com gabarito!</p>
            <div align="center">
            <?= Html::img('@web/imagens/ilustracaoMultiprova.png', ['alt' => 'Exercícios']) ?>
            </div>
            <?php Panel::end() ?>
        </div>
    </div>

<div class="row">
        <div class="col-md-6 col-xs-12">
            <?php
            Panel::begin(
                [
                    'header' => 'Seu desempenho',
                    'icon' => 'pie-chart',
                    'removable' => false,
                ]
            )
            ?>
            <p>Estatísticas sobre a resolução dos exercícios</p>
            <div align="center">
            <?= Html::img('@web/imagens/ilustracaoEstatisticas.png', ['alt' => 'Estatísticas']) ?>
            </div>
            <?php Panel::end() ?>
        </div>
        <div class="col-md-6 col-xs-12">
            <?php
            Panel::begin(
                [
                    'header' => 'Integração SIGAA',
                    'icon' => 'arrows-h',
                    'collapsable' => false,
                ]
            )
            ?>
            <p>Integrado com a API do SIGAA</p>
            <div align="center">
            <?= Html::img('@web/imagens/ilustracaoSIGAA.jpg', ['alt' => 'Sigaa']) ?>
            </div>
            <?php Panel::end() ?>
        </div>

    </div>    

<!--

    <div class="jumbotron">
        <h1>Site de CN Criado!</h1>

        <p class="lead">Para acessar as formatações possíveis para as páginas do site, Clique no botão abaixo e acesse a página demonstração desse template.</p>

        <p>
            <a class="btn btn-lg btn-success" target="_blank" href="http://gentelella.yiister.ru">Veja o site Demo</a>
            <a class="btn btn-lg btn-success" target="_blank" href="https://colorlib.com/polygon/gentelella/index.html">Esse também</a>
        </p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading - </h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.
                    <?= print_r($attributes) ?>

                </p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
<a href='http://www.freepik.com/free-vector/technological-devices-design_953321.htm'>Designed by Freepik</a>

-->

