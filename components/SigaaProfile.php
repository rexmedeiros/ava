<?php
namespace app\components;

use app\models\Auth;
//use mdm\admin\models\User;
use app\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
use app\components\Sigaa;
use yii\web\HttpException;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class SigaaProfile
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function getInfoUsuario()
    {
        try{
            $info = $this->client->api(
                'usuario/v0.1/usuarios/info', 'GET'
            );
        }
        catch (\Exception $e){
            throw new HttpException(404,"Falha na comunicação com o SIGAA (Info). Tente repetir o Login.");
            
        }
        return $info;
    }

    

    public function getVinculos($cpfCnpj)
    {
        try{
            $dados = $this->client->api(
                'vinculo/v0.1/vinculos', 
                'GET',
                [
                    'cpf' => $cpfCnpj,
                    'ativo' => true
                ]
            );
        }
        catch (\Exception $e){
            throw new HttpException(404,"Falha na comunicação com o SIGAA (Vinculos). Tente repetir o Login.");
            
        }
        
        $vinculos['discentes'] = array();
        $vinculos['docentes'] = array();
        $nDocente = 0;
        $nDiscente = 0;
        if (count($dados)){
            foreach ($dados as $vinculo){
                if ($vinculo['id-tipo-vinculo'] == 1){
                    // Discente
                    $vinculos['discentes'][$nDiscente]['id'] = $vinculo['id-vinculo'];
                    $vinculos['discentes'][$nDiscente]['matricula'] = $vinculo['identificador'];
                    $vinculos['discentes'][$nDiscente]['curso'] = $vinculo['lotacao'];
                    $vinculos['discentes'][$nDiscente]['anoIngresso'] = 0;
                    $vinculos['discentes'][$nDiscente]['periodoIngresso'] = 0;
                    $vinculos['discentes'][$nDiscente]['idCurso'] = $vinculo['id-lotacao'];;
                    $nDiscente++;
                }
                elseif($vinculo['id-tipo-vinculo'] == 2){
                    // Docente
                    $vinculos['docentes'][$nDocente]['id'] = $vinculo['id-vinculo'];
                    $vinculos['docentes'][$nDocente]['siape'] = $vinculo['identificador'];
                    $vinculos['docentes'][$nDocente]['lotacao'] = $vinculo['lotacao'];
                    $nDocente++;
                }
            }   
        }

        return $vinculos;
    }

    /**
     * @param Array $vinculos
     */
    public function getTurmasDiscente($vinculos)
    {
        // Vai em cada um dos vinculos de aluno
        $disciplinas = array();
        foreach ($vinculos['discentes'] as $matriculas){
            
            $idAluno = $matriculas['id'];
            $disciplinas[$idAluno]['matricula'] = $matriculas['matricula'];
            $disciplinas[$idAluno]['curso'] = $matriculas['curso'];
            $disciplinas[$idAluno]['anoIngresso'] = $matriculas['anoIngresso'];
            $disciplinas[$idAluno]['periodoIngresso'] = $matriculas['periodoIngresso'];
            $disciplinas[$idAluno]['idCurso'] = $matriculas['idCurso'];

            // Para cada vinculo, ver em quais disciplinas esta curdando
            try{
                $dados = $this->client->api(
                    'turma/v0.1/turmas',
                    'GET',
                    [
                        'id-discente' => $idAluno,
                        'id-situacao-turma' => 1
                    ]
                );
            }
            catch (\Exception $e){
                throw new HttpException(404,"Falha na comunicação com o SIGAA (Turmas Discente). Tente repetir o Login.");
                
            }
            
            $idTurma = 0; //inicia idTurma
            $disciplinas[$idAluno]['disciplinas'] = array();
            if (count($dados)){
                foreach ($dados as $matricula){
                    $idTurma = $matricula['id-turma'];


                    $disciplinas[$idAluno]['disciplinas'][$idTurma]['idTurma'] = $idTurma;
                    $disciplinas[$idAluno]['disciplinas'][$idTurma]['codigo'] = $matricula['codigo-componente']; 

                    $disciplinas[$idAluno]['disciplinas'][$idTurma]['nome'] = $matricula['nome-componente'];
                    $disciplinas[$idAluno]['disciplinas'][$idTurma]['horario'] = $matricula['descricao-horario'];
                    $disciplinas[$idAluno]['disciplinas'][$idTurma]['turma'] = $matricula['codigo-turma']; 
                    $disciplinas[$idAluno]['disciplinas'][$idTurma]['subturma'] = ""; 
                    if (strlen($matricula['codigo-turma']) == 3){
                        $disciplinas[$idAluno]['disciplinas'][$idTurma]['turma'] = substr($matricula['codigo-turma'],0,2);
                        $disciplinas[$idAluno]['disciplinas'][$idTurma]['subturma'] = substr($matricula['codigo-turma'],2,1);
                    } 
                    $disciplinas[$idAluno]['disciplinas'][$idTurma]['ano'] = $matricula['ano'];
                    $disciplinas[$idAluno]['disciplinas'][$idTurma]['periodo'] = $matricula['periodo'];
                    $disciplinas[$idAluno]['disciplinas'][$idTurma]['idAluno'] = $idAluno;

                }
                
            }
        }
        return $disciplinas;   
    }

     

    public function getTurmasDocente($idVinculo)
    {
            
        // Pega as turmas do docente
        try{
            $dados = $this->client->api(
                    'turma/v0.1/turmas',
                    'GET',
                    [
                        'id-docente' => $idVinculo,
                        'id-situacao-turma' => 1
                    ]
                );
        }
        catch (\Exception $e){
            throw new HttpException(404,"Falha na comunicação com o SIGAA (Turmas Docentes). Tente repetir o Login.");
        }

        $turmas = array();
        foreach ($dados as $k => $turma){
            $idTurma = $turma['id-turma'];
            $turmas[$idTurma]['codigo'] = $turma['codigo-componente'];
            //$turmas[$idTurma]['nome'] = mb_convert_case($turma['nomeComponente'], MB_CASE_TITLE, "UTF-8");
            $turmas[$idTurma]['nome'] = $turma['nome-componente'];
            $turmas[$idTurma]['horario'] = $turma['descricao-horario'];
            $turmas[$idTurma]['turma'] = $turma['codigo-turma'];
            $turmas[$idTurma]['subturma'] = '';
            if (strlen($turma['codigo-turma']) == 3){
                $turmas[$idTurma]['turma'] = substr($turma['codigo-turma'],0,2);
                $turmas[$idTurma]['subturma'] = substr($turma['codigo-turma'],2,1);
            }
            $turmas[$idTurma]['idTurma'] = $turma['id-turma'];
            $turmas[$idTurma]['idTurmaAgrupadora'] = 0;  
            $turmas[$idTurma]['ano'] = $turma['ano'];
            $turmas[$idTurma]['periodo'] = $turma['periodo'];
            $turmas[$idTurma]['qtdMatriculados'] = -1;
        }
 
        return $turmas;
    }

    public function getParticipantesDiscentes($idTurma){
        if ($idTurma){
            $offset = 0;
            $limit = 30;
            $dados = array();
            while(1){
                try{
                    $temp = $this->client->api(
                        'turma/v0.1/participantes',
                        'GET',
                        [
                            'id-turma' => $idTurma,
                            'id-tipo-participante' => 4,
                            'limit' => $limit,
                            'offset' => $offset,
                        ]
                    );
                }
                catch (\Exception $e){
                    throw new HttpException(404,"Falha na comunicação com o SIGAA (Participantes). Tente repetir o Login.");
                }
                if (count($temp)){
                    foreach ($temp as $dado){
                        $dados[] = $dado;
                    }
                }
                $offset += $limit;
                if (count($temp) < $limit){
                    break;
                }
            }
            $participantes = array();
            if (count($dados)){
                foreach ($dados as $k => $aluno){
                    $participantes[$k]['matricula'] = $aluno['identificador'];
// TODO : Mudar para id-usuario
                    $participantes[$k]['idUsuario'] = $aluno['cpf-cnpj'];
                    $participantes[$k]['id'] = $aluno['id-participante'];
                    $participantes[$k]['email'] = $aluno['email'];
                    $participantes[$k]['nome'] = $aluno['nome'];
                    $participantes[$k]['idFoto'] = $aluno['id-foto'];
                    $participantes[$k]['chaveFoto'] = $aluno['chave-foto'];
                    $participantes[$k]['curso'] = $aluno['lotacao'];
                }
            }
            
            return $participantes;
        }
        return false;
    }

    public function advinhePeriodoAtual()
    {
        $ano = date('Y');
        $mes = date('m');
        $periodo = $mes <= 7 ? 1 : 2;
        if ($periodo == 1){
            $inicio = strtotime($ano."-02-01");
            $fim = strtotime($ano."-07-15");
        }
        else{
            $inicio = strtotime($ano."-07-16");
            $fim = strtotime($ano."-12-24");
        }
        return [
            'ano' => $ano, 
            'periodo' => $periodo,
            'inicio' => $inicio,
            'fim' => $fim,
        ];
    }   

    public function getSemestreAtual(){
        try{
            $temp = $this->client->api(
                'calendario/v0.1/calendarios',
                'GET',
                [
                    'vigente' => true,
                    'sigla-nivel-ensino' => 'G',
                ]
            );
        }
        catch (\Exception $e){
            throw new HttpException(404,"Falha na comunicação com o SIGAA (Participantes). Tente repetir o Login.");
        }
        $semestre = [];
        if (count($temp)){
            $tempoAtual = time()*1000;
            foreach ($temp as $dado){
                if ($dado['inicio-periodo']<$tempoAtual and $dado['fim-periodo']>$tempoAtual){
                    $semestre = [
                        'ano' => $dado['ano'],
                        'periodo' => $dado['periodo'],
                        'inicio' => $dado['inicio-periodo']/1000,
                        'fim' => $dado['fim-periodo']/1000,
                    ];
                    break;
                }
            }
        }
        if (count($semestre)){
            return $semestre;
        }
        else{
            return $this->advinhePeriodoAtual();
        }
    }
}
