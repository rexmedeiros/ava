<?php
namespace app\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LoginForm;
use yii\web\ForbiddenHttpException;
use app\models\Questoes;
use app\models\Alternativas;
use app\models\Respostas;
use app\models\Alunos;
use app\models\TurmasAlunos;
use app\models\Exercicios;



/**
 * Conteudo controller
 */
class ConteudoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'ponto-flutuante',
                            'ponto-sistemas',
                            'ponto-erro',
                            'integracao',
                            'integ-trapezio', 
                            'integ-simpson13', 
                            'integ-simpson38',
                            'raizes', 
                            'edo',
                            'edo-euler',
                            'raizes-bissection',
                            'raizes-falsepos',
                            'raizes-newton',
                            'raizes-secante',
                            'edo-heun',
                            'edo-rungekutta',
                            'edo-sistemas', 
                            'edo-ordemsuperior',
                            'edo-pontomedio',
                            'sislinear',
                            'sislin-tri',
                            'sislin-gauss',
                            'sislin-lu',
                            'sislin-jacobi',
                            'sislin-seidel',
                            'sislin-cconvergencia',
                            'mmq',
                            'mmq-rlinear',
                            'mmq-rpolin',
                            'mmq-cgeral',
                            'mmq-linearizacao',
                            'interpolacao',
                            'interp-lagrange',
                            'interp-newton',
                            'serie-taylor',
                            


                        ],
                        'allow' => true,
                        'roles' => ['?','@','aluno','docente','admin'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    return $action->controller->redirect(['site/acesso-negado']);
                }
            ],
        ];
    }


    /*
            PONTO FLUTUANTE

    */

        public function actionPontoFlutuante()
    {
        return $this->render('pontoFlutuante');
    }
         public function actionPontoSistemas()
    {
        return $this->render('pontoSistemas');
    }
     public function actionPontoErro()
    {
        return $this->render('pontoErroFlutuante');
    }

     /*
            SÉRIE DE TAYLOR

    */

        public function actionSerieTaylor()
        {
            return $this->render('serieTaylor');
        }


    /*
    
            CONTEUDO RAIZES
    /*

    /**
     * Displays Raizes - Introducao.
     *
     * @return mixed
     */
    public function actionRaizes()
    {
        return $this->render('raizes');
    }

    /**
     * Displays Bisseccao.
     *
     * @return mixed
     */
    public function actionRaizesBissection()
    {
     
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [12];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

         return $this->render('raizesBissection',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }

    /**
     * Displays FalsaPosicao.
     *
     * @return mixed
     */
    public function actionRaizesFalsepos()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [15];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

         return $this->render('raizesFalsepos',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);

    }


     /**
     * Displays Newton.
     *
     * @return mixed
     */
    public function actionRaizesNewton()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [14];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

         return $this->render('raizesNewton',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);

    }


     /**
     * Displays Newton.
     *
     * @return mixed
     */
    public function actionRaizesSecante()
    {
        return $this->render('raizesSecante');
    }

    /*
            FIM CONTEUDO RAIZES
    /*   

    /*
            CONTEUDO SISTEMAS LINEARES
    /*

    /**
     * Displays Sistemas Lineares - Introducao.
     *
     * @return mixed
     */
    public function actionSislinear()
    {
        return $this->render('sislinear');
    }   

    /**
     * Displays Sistemas Triangulares.
     *
     * @return mixed
     */
    public function actionSislinTri()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [16];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

         return $this->render('sislinTri',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }   

    /**
     * Displays Gauss.
     *
     * @return mixed
     */
    public function actionSislinGauss()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [17];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

         return $this->render('sislinGauss',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }   

    /**
     * Displays LU.
     *
     * @return mixed
     */
    public function actionSislinLu()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [18];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

         return $this->render('sislinLu',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    } 
    /**
     * Displays Jacobi.
     *
     * @return mixed
     */
    public function actionSislinJacobi()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [19];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

         return $this->render('sislinJacobi',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    } 
    /**
     * Displays Gauss Seidel.
     *
     * @return mixed
     */
    public function actionSislinSeidel()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [20];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

         return $this->render('sislinSeidel',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    } 
     /**
     * Displays Critérios de convergência.
     *
     * @return mixed
     */
    public function actionSislinCconvergencia()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [22];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

         return $this->render('sislinCconvergencia',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
        return $this->render('sislinCconvergencia');
    } 


    /*
            FIM CONTEUDO SISTEMAS LINEARES
    /*   


     /*
            CONTEUDO MMQ
    /*

    /**
     * Displays MMQ - Introducao.
     *
     * @return mixed
     */
    public function actionMmq()
    {
        return $this->render('mmq');
    }

    /**
     * Displays MMQ - Regressão Linear.
     *
     * @return mixed
     */
    public function actionMmqRlinear()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [10];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('mmqRlinear',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }      

     /**
     * Displays MMQ - Regressão Polinomial.
     *
     * @return mixed
     */
    public function actionMmqRpolin()
    {
        
        return $this->render('mmqRpolin');
    }  
   
    /**
     * Displays MMQ - Caso Geral.
     *
     * @return mixed
     */
    public function actionMmqCgeral()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [23];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('mmqCgeral',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    } 

    /**
     * Displays MMQ - Linearização.
     *
     * @return mixed
     */
    public function actionMmqLinearizacao()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [24];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('mmqLinearizacao',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    } 



    /*
            FIM CONTEUDO MMQ
    /*   







      /*
            CONTEUDO INTERPOLAÇÃO
    /*

    /**
     * Displays Interpolação - Introducao.
     *
     * @return mixed
     */
    public function actionInterpolacao()
    {
        return $this->render('interpolacao');
    }


    /**
     * Displays Interpolação - Lagrange.
     *
     * @return mixed
     */
    public function actionInterpLagrange()
    {
        return $this->render('interpLagrange');
    }

    /**
     * Displays Interpolação - Lagrange.
     *
     * @return mixed
     */
    public function actionInterpNewton()
    {
        return $this->render('interpNewton');
    }



     



    /*
            FIM CONTEUDO INTERPOLACAO
    /*






    /*
            CONTEUDO INTEGRACAO
    /*

    /**
     * Displays Integracao - Introducao.
     *
     * @return mixed
     */
    public function actionIntegracao()
    {
        return $this->render('integracao');
    }    

    /**
     * Displays Trapezio.
     *
     * @return mixed
     */
    public function actionIntegTrapezio()
    {

        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [7];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('integTrapezio',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }    
     
    /**
     * Displays Simpson13.
     *
     * @return mixed
     */
    public function actionIntegSimpson13()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [6];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('integSimpson13',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }    

    /**
     * Displays Simpson38.
     *
     * @return mixed
     */
    public function actionIntegSimpson38()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [5];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('integSimpson38',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }

    /*
            FIM CONTEUDO INTEGRACAO
    /*     
    
    /*
            CONTEUDO EDO
    /*

    /**
     * Displays EDO - Introducao.
     *
     * @return mixed
     */
    public function actionEdo()
    {
        return $this->render('edo');
    }

    /**
     * Displays Euler.
     *
     * @return mixed
     */
    public function actionEdoEuler()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [4];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('edoEuler',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }

    /**
     * Displays Heun.
     *
     * @return mixed
     */
    public function actionEdoHeun()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [3];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('edoHeun',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);


    }

    /**
     * Displays PontoMedio.
     *
     * @return mixed
     */
    public function actionEdoPontomedio()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [2];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('edoPontomedio',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }

    /**
     * Displays RungeKutta.
     *
     * @return mixed
     */
    public function actionEdoRungekutta()
    {
        $modeloResposta = new Respostas();                
        
        $idExerciciosInserir = [1];
        $todosExercicios = $this->gerarExercicios($modeloResposta,$idExerciciosInserir);

        return $this->render('edoRungekutta',[
            'todosExercicios' => $todosExercicios,
            'modeloResposta' => $modeloResposta,
        ]);
    }

    /**
     * Displays SistemasEDO.
     *
     * @return mixed
     */
    public function actionEdoSistemas()
    {
        return $this->render('edoSistemas');
    }

    /**
     * Displays EDO - Ordem Superior.
     *
     * @return mixed
     */
    public function actionEdoOrdemsuperior()
    {
        return $this->render('edoOrdemsuperior');
    }

    /*
            FIM CONTEUDO EDO
    /* 


    /*
            PAGINAS ESTATICAS
            NAO ALTERAR DAQUI PARA BAIXO
    /*

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }            
 

    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /*

        NAO ALTERAR O METODO ABAIXO
        POIS ELE EH UTILIZADO PARA
        GERAR OS EXERCICIOS

    */

    public function gerarExercicios($modeloResposta,$idExerciciosInserir)
    {
        
        // Eh importante para dizer se acertou a questao (flash)
        foreach ($idExerciciosInserir as $idExercicio){
            $todasQuestoes[$idExercicio]['saida'] = array();
        }

        if ($modeloResposta->load(Yii::$app->request->post()) ) {
            $idQuestao = $modeloResposta->questao_id;
            $umaQuestao = Questoes::findOne($modeloResposta->questao_id);
            $idExercicio = $umaQuestao->exercicios_id;
            $todasQuestoes[$idExercicio]['saida'] = $modeloResposta->processar($idExercicio);
            if ($todasQuestoes[$idExercicio]['saida']['acertou']){
                Yii::$app->session->setFlash('success', 'Você acertou!');
            }
            else{
                Yii::$app->session->setFlash('error', 'Você errou!');
            }

            // Define questao como sendo a que acabou de ser respondida
            
            foreach ($idExerciciosInserir as $idExerFor){
                if ($idExerFor == $idExercicio){
                    $todasQuestoes[$idExercicio]['questao'] = Questoes::findOne($idQuestao);
                    $todasQuestoes[$idExercicio]['alternativasFormatadas'] = $todasQuestoes[$idExercicio]['questao']->alternativasFormatadas;
                } else{
                    $modeloQuestao = new Questoes();
                    $todasQuestoes[$idExerFor]['questao'] = $modeloQuestao->find()->where(['exercicios_id' => $idExerFor])->orderBy('usos')->one();
                    $todasQuestoes[$idExerFor]['alternativasFormatadas'] = $todasQuestoes[$idExerFor]['questao']->alternativasFormatadas;
                }

                
            }
            
        }else{
            // pega novas questoes
            foreach ($idExerciciosInserir as $idExercicio){
                $modeloQuestao = new Questoes();
                $todasQuestoes[$idExercicio]['questao'] = $modeloQuestao->find()->where(['exercicios_id' => $idExercicio])->orderBy('usos')->one();
                $todasQuestoes[$idExercicio]['alternativasFormatadas'] = $todasQuestoes[$idExercicio]['questao']->alternativasFormatadas;
            }
                         
        }
        return $todasQuestoes;
    } 


}
