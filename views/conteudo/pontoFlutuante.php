<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Integração Numérica - Simpson 1/3';
$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
<script 
src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-AMS-MML_HTMLorMML' async>
</script>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { equationNumbers: { autoNumber: "AMS" } }
});
</script>
    

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-area-chart"></i> Introdução</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                A utilização da notação de ponto flutuante é muito grande em computadores, tanto para cálculos matemáticos com precisão, renderização de imagens digitais (criação de imagens pelo computador) entre outros. Os processadores atuais se dedicam muito em aperfeiçoar a técnica de seus chips para a aritmética de ponto flutuante, devido à demanda de aplicativos gráficos e jogos 3D que se utilizam muito deste recurso.
                <br><br>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Notação em ponto flutuante</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

A notação em ponto flutuante é analoga a representação cientifica, e vários assuntos são consequência, como sistemas em ponto flutuante, e esse no qual por motivos de capacidade de processamento defini-se padrões para sistemas computacionais. Também é estudo a forma como esses números são representados no computador, valores minímos e máximos,
erros de arredondamento e até aritmética em ponto flutuante.
<br><br>
Todos esses assuntos serão abordados em sequência.

<br><br>

</div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>  Conversão de base  </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

Na representação em ponto flutuante, existe a conversão da base dos numeros. No computador, a variavel decimal é salva na memoria em binario. O computador realiza
uma conversao decimal para binario.
<br><br>
Existem outros tipos de conversoes, como converrsao octal e hexadecimal. Para realizar a conversao binaria, a parte inteira do numero é divida por 2 sucessivamente.
Sempre que a divisao tiver resto 1, o numero binario corespondente é 1 na posição i relativa a ordem da divisão, nos outros casos sera 0.
<br>
 
<p style="
    
 text-align: center;
">


<img src="imagens/decimalBinarioInteiro.PNG">
</p>

<br>
Se o resultado da divisão for igual a 0, as divisões sucessivas param. De maneira analoga, a parte fracionaria do numero deciamal pode ser encontrada realizando
uma multiplicação sucessiva por 2. Sempre que o produto da multiplição por dois for maior que 1, a fração binaria na posiçao i será igual a 1, e caso seja menor que 1,
será igual a 0.
<br><br>
<p style="
    
 text-align: center;
">


<img src="imagens/decimalBinarioDecimal.PNG">
</p>
<br>
A multiplicação sucessiva deve ser relizada somente com a parte fracionaria, e quando essa for igual a 0 o metodo para.Da mesma forma que a conversão para binario, pode ser realizada a conversão hexadeciaml ou octal. A diferença é que em vez da divisão ser de 2, será de 16 na hexadecimal
e 8 na octal.
<br><br>
A utilização da muitplicação ou divisão sucessiva ocorre quando o numero esta na base 10. Caso queira-se converter o numero da base binaria para base 10, basta erxpandir e elevar 
o numero binario a i relativo a ordem. O mesmo ocorre em relaçao a conversão octal e hexadecimal, só ocorrendo uma mudança de base.
<br><br>
<p style="
    
 text-align: center;
">


<img src="imagens/binarioDecimal.PNG">
</p>
<br>
</div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">   
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
     <p>Verifique abaixo como os números são representados, de modo a alterar a  base.</p>
          
         <br>
           
            <div id="applet_container" align="center"></div>

            <br>
            
            </div>
        </div>
    </div>
</div>







<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }

</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/binario2.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-4,6,-4,4);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}

     
       
    }

    function anterior (){
         if(n>=6 && n <=12){

             ggbApplet.setCoordSystem(-4,12,-4,4);
        }
        else{

             ggbApplet.setCoordSystem(-4,6,-4,4);
        }

             if (this.n%2==0){
       
    }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
         if(n>=4 && n <=10){

             ggbApplet.setCoordSystem(-4,12,-4,4);
        }
        
        else{

             ggbApplet.setCoordSystem(-4,6,-4,4);
        }


        if (this.n <= 14){
            this.n++;
            ggbApplet.setValue('n', this.n)
          
            

             
                
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>