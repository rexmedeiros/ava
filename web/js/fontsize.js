    function getSize() {
      size = $( ".x_content" ).css( "font-size" );
      size = parseInt(size, 10);
      $( "#font-size" ).text(  size  );
    }

    function fonteInicial() {
      x = $( ".x_content" ).css( "font-size" );
      x = parseInt(size, 10);
      return x;
    }

    //get inital font size
    getSize();
    x = fonteInicial();

    $( "#up" ).on( "click", function() {

      // parse font size, if less than 50 increase font size
      if ((size + 2) <= 23) {
        $( ".x_content" ).css( "font-size", "+=2" );
        $( "#font-size" ).text(  size += 2 );
      }
    });

    $( "#down" ).on( "click", function() {
      if ((size - 2) >= 13) {
        $( ".x_content" ).css( "font-size", "-=2" );
        $( "#font-size" ).text(  size -= 2 );
      }
    });

    $( "#default" ).on( "click", function() {
        $( ".x_content" ).css( "font-size", x );
        $( "#font-size" ).text(  x );
        size = x;
    });