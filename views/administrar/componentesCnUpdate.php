<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model frontend\models\ComponentesCn */

$this->title = 'Atualizar componente: ' . $model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Componentes', 'url' => ['componentes-cn-index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['componentes-cn-view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>

<div class="componentes-cn-update">

    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('componentesCn_Form', [
        'model' => $model,
    ]) ?>

</div>
