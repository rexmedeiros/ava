<?php
namespace app\components;

use yii\base\Widget;

class ExercicioWidget extends Widget
{
    public $titulo;
    public $todosExercicios;
    public $modeloResposta;
    //public $alternativasFormatadas;
    //public $saida;
    

    public function run()
    {
        return $this->render('exercicioWidget',[
            'titulo' => $this->titulo,
            'questao' => $this->todosExercicios['questao'],
            'alternativasFormatadas' => $this->todosExercicios['alternativasFormatadas'],
            'modeloResposta' => $this->modeloResposta,
            'saida' => $this->todosExercicios['saida'],
        ]);
    }
}

?>