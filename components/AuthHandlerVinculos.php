<?php
namespace app\components;

#use mdm\admin\models\Auth;
use app\models\Auth;
//use mdm\admin\models\User;
use app\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
#use yii\authclient\Clients\Sigaa;
use app\components\Sigaa;
use app\components\SigaaProfile; 
use app\models\Docentes;
use app\models\Turmas; 
use app\models\TurmasDocentes;
use app\models\Alunos;
use app\models\TurmasAlunos;
use yii\filters\AccessControl;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        
        // Busda dados do SIGAA
        $clienteProfile = new SigaaProfile($this->client);

        $idUsuario = $clienteProfile->getIdUsuario();  // idUsuario
   
        $perfil = $clienteProfile->getNomeEmail($idUsuario); // nomeEmail
        $nome = $perfil['nome'];
        $email = $perfil['email'];
        $foto['foto_id'] = 0;
        $foto['foto_chave'] = "0";


        $vinculos = $clienteProfile->getVinculos(); // Vinculos

        // Busca os codigos de CN cadastrados
        //$codigosCN = ComponentesCN::find()->select(['codigo'])->indexBy('codigo')->asArray()->all();

        // Ver se eh aluno e se eh aluno de CN
        $perfil['discente'] = false;
        if (count($vinculos['discentes'])){
            $turmasDiscente = $clienteProfile->getTurmasDiscente($vinculos);
            $turmas = $this->turmaCnDiscente($turmasDiscente,$codigosCN);
            if ($turmas){
                $perfil['discente'] = true;
                // pega foto
                $idAlunoTemp = $turmas[0]['idAluno'];
                $idTurmaTemp = $turmas[0]['idTurma'];
                $foto = $clienteProfile->getFoto($idAlunoTemp,$idTurmaTemp,'discentes');
                //$fotoId = 
            }
        }      
  
        // Ver se eh professor
        $perfil['docente'] = false;
        if (count($vinculos['docentes'])){
            $perfil['docente'] = true;
            $turmasDocente = $clienteProfile->getTurmasDocente($codigosCN);
            $turmas = $this->turmaCnDocente($turmasDocente,$codigosCN);
            // pega foto
            if (count($turmasDocente)){
                $codigosTemp = array_keys($turmasDocente);
                $idTurmaTemp = $codigosTemp[0];    
            }
            
            $idDocenteTemp = $vinculos['docentes'][0]['id'];
            $foto = $clienteProfile->getFoto($idDocenteTemp,$idTurmaTemp,'docentes');
        }   
        $perfil['foto_id'] = $foto['foto_id'];
        $perfil['foto_chave'] = $foto['foto_chave'];



        // Agora autenticacao no site e atualizacao do banco de dados
        $attributes = $this->client->getUserAttributes();
        $id = ArrayHelper::getValue($attributes, 'id');


        /* @var Auth $auth */
        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $idUsuario,
        ])->one();

        if ($perfil['docente'] || $perfil['discente']){
            if (Yii::$app->user->isGuest) {
                if ($auth) { // login
                    /* @var User $user */
                    $user = $auth->user;

                    // Atualiza as tabelas docentes, alunos, turmas, etc.
                    $this->atualizaInfoUsuario($user, $perfil, $vinculos, $turmas);

                    Yii::$app->user->login($user,23423434);
                } else { // signup    
                    // Mudar isso quando colocar o sistema em producao            
                    //if ($email !== null && User::find()->where(['email' => $email])->exists()) {
                    if (0) {
                        Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $this->client->getTitle()]),
                        ]);
                    } else {
                        $password = Yii::$app->security->generateRandomString(6);
                        $user = new User([
                            'username' => $idUsuario,
                            'email' => $email,
                            'password' => $password,
                            'name' => $nome,
                            'foto_id' => $foto['foto_id'],
                            'foto_chave' => $foto['foto_chave']
                        ]);
                        $user->generateAuthKey();
                        $user->generatePasswordResetToken();

                        $transaction = User::getDb()->beginTransaction();

                        if ($user->save()) {
                            $auth = new Auth([
                                'user_id' => $user->id,
                                'source' => $this->client->getId(),
                                'source_id' => $idUsuario,
                            ]);
                            if ($auth->save()) {

                                // Atualiza as tabelas docentes, alunos, turmas, etc.
                                $this->atualizaInfoUsuario($user, $perfil, $vinculos, $turmas);

                                // Confirma as insercoes no banco e faz o login
                                $transaction->commit();
                                Yii::$app->user->login($user, 133445566);
                            } else {
                                Yii::$app->getSession()->setFlash('error', [
                                    Yii::t('app', 'Unable to save {client} account: {errors}', [
                                        'client' => $this->client->getTitle(),
                                        'errors' => json_encode($auth->getErrors()),
                                    ]),
                                ]);
                            }
                        } else {
                            Yii::$app->getSession()->setFlash('error', [
                                Yii::t('app', 'Unable to save user: {errors}', [
                                    'client' => $this->client->getTitle(),
                                    'errors' => json_encode($user->getErrors()),
                                ]),
                            ]);
                        }
                    }
                }
            } else { // user already logged in
                if (!$auth) { // add auth provider
                    $auth = new Auth([
                        'user_id' => Yii::$app->user->id,
                        'source' => $this->client->getId(),
                        'source_id' => $idUsuario
                    ]);
                    if ($auth->save()) {
                        /** @var User $user */
                        $user = $auth->user;
                        // Atualiza as tabelas docentes, alunos, turmas, etc.
                        $this->atualizaInfoUsuario($user, $perfil, $vinculos, $turmas);
                        Yii::$app->getSession()->setFlash('success', [
                            Yii::t('app', 'Linked {client} account.', [
                                'client' => $this->client->getTitle()
                            ]),
                        ]);
                    } else {
                        Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', 'Unable to link {client} account: {errors}', [
                                'client' => $this->client->getTitle(),
                                'errors' => json_encode($auth->getErrors()),
                            ]),
                        ]);
                    }
                } else { // there's existing auth
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app',
                            'Usuário \'{usuario}\' atualmente logado. Assim, não é possível fazer login com a conta do {client}.',
                            ['usuario' => Yii::$app->user->shortName, 'client' => $this->client->getTitle()]),
                    ]);
                }
            }
        }
        else{
            Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', "Somente docentes ou alunos matriculados nos cursos de Computação ou Cálculo Numérico podem ter acesso a esse sistema"),
                        ]);
        }
    
    }


    /**
     * @param Array $disciplinas
     * @param Array $codigosCN
     */
    public function turmaCnDiscente($disciplinas,$codigosCN)
    {
        $ehAlunoCN = array();
        if (count($disciplinas) && count($codigosCN)){
            $arrayCodigosCN = array_keys($codigosCN);
            foreach ($disciplinas as $cursos){
                if (count($cursos['disciplinas'])){
                    foreach ($cursos['disciplinas'] as $turma){
                        if (in_array($turma['codigo'], $arrayCodigosCN) ){
                            $ehAlunoCN[0] = $turma;
                            $ehAlunoCN[0]['dados']['matricula'] = $cursos['matricula'];
                            $ehAlunoCN[0]['dados']['anoIngresso'] = $cursos['anoIngresso'];
                            $ehAlunoCN[0]['dados']['periodoIngresso'] = $cursos['periodoIngresso'];
                            $ehAlunoCN[0]['dados']['curso'] = $cursos['curso'];
                            break;
                        }
                    }
                }    

            }
        }

        return $ehAlunoCN;
    }

    /**
     * @param Array $disciplinas
     * @param Array $codigosCN
     */
    public function turmaCnDocente($disciplinas,$codigosCN)
    {
        $turmas = array();
        if (count($codigosCN)){
            $keysArrayCodigosCN = array_keys($codigosCN);
            foreach ($disciplinas as $turma){
                if (in_array($turma['codigo'], $keysArrayCodigosCN)){                    
                    $turmas[] = $turma;
                }    
            }
        }        
        return $turmas;
    }


    /**
     * @param Int $idTurmaCn
    */
    private function atualizaTurmaCN($turmas)
    {
        foreach ($turmas as $turma){
            $idTurmaCn = $turma['idTurma'];
            // Ver se a turma de CN ja existe no BD
            $turmaCn = Turmas::find()->where([
                'id_turma' => $idTurmaCn,
            ])->one();

            if ($turmaCn){
                // Existe, pega o ID. 
                $turmaIdCn[] = $turmaCn->id;
            }
            else{
                // Nao exsite ... adicione a turma a tabela turmas
                $novaTurmaCn = new Turmas([
                    'id_turma' => $idTurmaCn,
                    'codigo' => $turma['codigo'],
                    'nome' => $turma['nome'],
                    'horario' => $turma['horario'],
                    'turma' => $turma['turma'],
                    'subturma' => $turma['subturma'],
                    'ano' => $turma['ano'],
                    'periodo' => $turma['periodo'],
                ]);
                $novaTurmaCn->save();
                $turmaIdCn[] = $novaTurmaCn->id;
            }
        }
        return $turmaIdCn;
    }


    /**
     * @param User $user
     */
    private function atualizaInfoUsuario(User $user, $perfil, $vinculos, $turmas)
    {
        /*
        $attributes = $this->client->getUserAttributes();
        $user->name = $perfil['nome'];
        $user->save();
        */

        // Gerenciamento de autorização
        $autorizacao = \Yii::$app->authManager;

        // Insere dados na tabela Docente
        if ($perfil['docente']){

            // Ver se ja tem usuario docente
            $siape = $vinculos['docentes'][0]['siape'];
            $docente = Docentes::find()->where(['siape' => $siape])->one();
            if (!($docente)){ // Inclui na tabela docente
                $docente = new Docentes([
                    'user_id' => $user->id,
                    'siape' => $vinculos['docentes'][0]['siape'],
                    'lotacao' => substr($vinculos['docentes'][0]['lotacao'], 11, strlen($vinculos['docentes'][0]['lotacao']) -1 ),
                ]);
                $docente->save();
            }

            // Se o docente ministra turma de CN, atualiza as tabelas
            if (count($turmas)){

                // Verifica a tabela Turmas                
                $turmaIdCn = $this->atualizaTurmaCN($turmas);

                // Adiciona a tabela turmas_docentes, se necessario
                foreach ($turmaIdCn as $tempId){
                    if (!(TurmasDocentes::find()->where(['user_id' => $user->id, 'turma_id' => $tempId])->exists())){
                        $turmaCnDocente = new TurmasDocentes([
                            'user_id' => $user->id,
                            'turma_id' => $tempId,
                        ]);
                        $turmaCnDocente->save();
                    }
                }
                
            }
            
            // Agora atribui os papeis para os usuarios, se necessario
            if (!( $autorizacao->getRolesByUser($user->id) )){
                $docenteRole = $autorizacao->getRole('docente');
                $autorizacao->assign($docenteRole, $user->id);
            }
        }
        // 
        
        if ($perfil['discente']){
            // Ver se o aluno (matricula atual) jah existe na tabela
            $matricula = $turmas[0]['dados']['matricula'];    
            $aluno = Alunos::find()->where(['matricula' => $matricula])->one();
            if (!($aluno)){
                // Verifica se ja existe um aluno ATIVO com esse user_id. Se existir, desativa
                $aluno = Alunos::find()->where(['user_id' => $user->id, 'ativo' => 1])->one();
                if ($aluno){
                    // Desativa
                    $aluno->ativo = 0;
                    $aluno->save();

                    // Verifica se ele tem alguma turma ativa. Se tem, desativa
                    $alunoTurma = TurmasAlunos::find()->where(['aluno_id' => $aluno->id, 'ativo' => 1])->one();
                    if ($alunoTurma){
                        // Desativa turma
                        $alunoTurma->ativo = 0;
                        $alunoTurma->save();
                    }
                }
                // Agora cria o novo aluno (matricula nova)
                $aluno = new Alunos([
                    'user_id' => $user->id,
                    'matricula' => $turmas[0]['dados']['matricula'],
                    'ano_ingresso' => $turmas[0]['dados']['anoIngresso'],
                    'periodo_ingresso' => $turmas[0]['dados']['periodoIngresso'],
                    'curso' => $turmas[0]['dados']['curso'],
                    'ativo' => 1,
                ]);
                $aluno->save();
            } 

            // Atualiza a tabela Turmas, se necessario
            $turmaIdCn = $this->atualizaTurmaCN($turmas);
            $tempTurmaIdCn = $turmaIdCn[0];
            // Verifica se ja nao existe uma outra turma ativa. Se existir , desative e adicione a nova, se necessario
            $alunoTurma = TurmasAlunos::find()->where(['aluno_id' => $aluno->id, 'ativo' => 1])->one();
            $criarNovaTurmaAluno = 0;
            if ($alunoTurma) {
                // Existe, verifica se eh a turma atual
                if ($alunoTurma->turma_id != $tempTurmaIdCn){
                    // Tem que desativar essa turma e criar uma nova
                    $alunoTurma->ativo = 0;
                    $alunoTurma->save();
                    $criarNovaTurmaAluno = 1;
                }            
            } 
            else{  // Nao tem, crie uma nova
                $criarNovaTurmaAluno = 1;
            }
            // Enfim ...
            if ($criarNovaTurmaAluno){
                $turmaCnAluno = new TurmasAlunos([
                    'aluno_id' => $aluno->id,
                    'turma_id' => $tempTurmaIdCn,
                ]);
                $turmaCnAluno->save();                
            }

            // Agora atribui os papeis para o aluno, se necessario
            if (!( $autorizacao->getRolesByUser($user->id) )){
                $discenteRole = $autorizacao->getRole('aluno');
                $autorizacao->assign($discenteRole, $user->id);
            }
        }
         // Atualiza foto
        $user->foto_id = $perfil['foto_id'];
        $user->foto_chave = $perfil['foto_chave'];
        $user->save();        
    }

}

















