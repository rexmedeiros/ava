<?php
use yii\helpers\Html;
use frontend\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Interpolação - Newton';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Interpolação de Newton- vídeo aula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/HwTs2bP_obU" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>

<br><br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Introdução</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            O polinômio interpolador de Newton foi desenvolvido de maneira que, para calcularmos um polinômio para $n+1$ pontos, utilizamos o polinômio que já temos (para $n$ pontos) e adicionamos um novo termo.

            Esse polinômio tem o seguinte formato: <br><br>

            <div class="equacao">
            $$
            \begin{aligned}
            P_n(x) = &\;  b_0 \\
                    &+ b_1(x-x_0) \\
                    &+ b_2(x-x_0)(x-x_1) \\
                    &+ ... + \\
                    &+b_n(x-x_0)(x-x_1) \dots (x-x_{n-2})(x-x_{n-1}),
            \end{aligned}
            $$
            </div> <br>

            onde os $b_i$ são coeficientes a serem calculados. <br><br>

            Podemos observar que, se tivéssemos apenas dois termos para esse polinômio teríamos uma reta; caso quiséssemos adicionar outro ponto para melhorar a aproximação, adicionaríamos ao polinômio um termo de segundo grau, obtendo uma parábola e assim por diante.  <br><br>

            Utilizando essa fórmula, devemos nos assegurar que <br><br>

            $$
            P_n(x_i) = y_i  \space
            $$ 
            para $i=0,1,...n$ <br><br>

            Para $x=x_0$, temos que todos os termos vão ser cancelados (exceto o primeiro) e teremos 
            $y_0 = b_0$. <br><br>

            Para $x=x_1$, temos que todos os termos vão ser cancelados, menos o primeiro e o segundo, e teremos <br>
            $$y_1 = y_0 + b_1(x_1-x_0)$$ 
            $$ b_1 = \frac{y_1-y_0}{x_1-x_0}$$ <br>

            Podemos generalizar essa análise para os pontos seguintes. Os pontos $(x_i,y_i)$, são usados para calcular os coeficientes $b_0, b_1, \dots, b_n$. Para isso, devemos definir uma nova operação denominada <b>diferença dividida</b>. Obteremos que: <br><br>
            <div class="equacao">
            $$b_0 = f[x_0] = \triangle ^0y_i,$$
            $$b_1 = f[x_0, x_1] = \triangle ^1y_i,$$
           $$ b_2 = f[x_0, x_1, x_2] = \triangle ^2y_i,$$
           $$\vdots$$
            $$b_n = f[x_0, x_1, x_2, \dots , x_n] = \triangle ^ny_i,$$<br>
            </div>

            onde definimos o termo $\triangle ^n$ como a diferença dividida de ordem $n$. 
            <br><br>

            Os operadores de diferenças divididas são definidos por: <br>

            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>ordem $0$</td>
                                  <td>${\triangle ^0y_i = f[x_0] = y_0}$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>ordem $1$</td>
                                  <td>$\triangle ^1y_i = f[x_0,x_1] = \frac{f[x_1]-f[x_0]}{x_1-x_0}$</td>
                                </tr>
                                <tr>
                                 <td>ordem $2$</td>
                                <td>$\triangle ^2y_i = f[x_0,x_1,x_2] = \frac{f[x_2,x_1]-f[x_1,x_0]}{x_2-x_0}$</td>
                                </tr>

                                <tr>
                                 <td>$ \ \ \ \ \ \ \ \vdots $</td>
                                <td>${}$</td>
                                </tr>

                                <tr>
                                 <td>ordem $n$</td>
                                <td>$\triangle ^ny_i = f[x_0,x_1,...,x_n] = \frac{f[x_1,..,x_n]-f[x_0,..,x_{n-1}]}{x_n-x_0}$</td>
                                </tr>
                              </tbody>
        </table>
        <b>Obs:</b> Um conjunto de $n$ pontos haverá uma ordem máxima $n-1$ de diferença dividida.



              

          </div>
        </div>
    </div>
</div>

<br><br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Tabela das diferenças divididas</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
            Vamos ver agora como calcular todas as diferenças divididas possíveis para um conjunto de pontos organizando os resultados em uma tabela. Veremos que é bem mais simples do que parece. <br><br>
            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$0$</td>
                                  <td>$1.008$</td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$0.2$</td>
                                  <td>$1.064$</td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$0.3$</td>
                                  <td>$1.125$</td>
                                </tr>

                                <tr>
                                 <td>$3$</td>
                                  <td>$0.5$</td>
                                  <td>$1.343$</td>
                                </tr>
                              </tbody>
        </table>

        Dado os conjuntos de pontos acima, vamos organizar a nossa tabela de diferenças divididas. Sabemos que, como temos $4$ pontos, temos uma diferença dividida de ordem máxima $3$, ou seja, teremos quatro diferenças divididas (ordem $0$, $1$, $2$ e $3$). Teremos: <br><br>

        <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$\triangle ^0y_i$</td>
                                  <td>$ \triangle ^1y_i$</td>
                                  <td>$ \triangle ^2y_i$</td>
                                  <td>$ \triangle ^3y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$0$</td>
                                  <td>$1.008$</td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$0.2$</td>
                                  <td>$1.064$</td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$0.3$</td>
                                  <td>$1.125$</td>
                                  <td></td>
                                  <td></td>
                                   <td></td>
                                </tr>

                                <tr>
                                 <td>$3$</td>
                                  <td>$0.5$</td>
                                  <td>$1.343$</td>
                                  <td></td>
                                  <td></td> 
                                   <td></td>
                                </tr>
                              </tbody>
             </table>

        Sabemos que a diferença dividida de ordem $0$ $\triangle ^0y_i = y_i$, então não precisamos calcular nada. <br><br>

        Para as diferenças divididas de  $1ª$ ordem $\triangle ^1y_i$, devemos calcular para cada subintervalo de $[x_i,x_{i+1}]$. <br><br>

        Para $[x_0, x_1]$:<br>
        <div class="equacao">
          $$f[x_0,x_1] = \frac{y_1-y_0}{x_1-x_0} = \frac{1.064-1.008}{0.2-0} = 0.280$$ 
        </div>

        Para $[x_1, x_2]$:<br>

         <div class="equacao">
        $$f[x_1,x_2] = \frac{y_2-y_1}{x_2-x_1} = \frac{1.125-1.064}{0.3-0.2} = 0.610$$ 
        </div>

        Para $[x_2, x_3]$:<br>

         <div class="equacao">
        $$f[x_2,x_3] = \frac{y_3-y_2}{x_3-x_2} = \frac{1.343-1.125}{0.5-0.3} = 1.090$$ 
        </div>

        Atualizando a tabela com os resultados obtidos, temos: <br><br>
        <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$\triangle ^0y_i$</td>
                                  <td>$ \triangle ^1y_i$</td>
                                  <td>$ \triangle ^2y_i$</td>
                                  <td>$ \triangle ^3y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$0$</td>
                                  <td>$1.008$</td>
                                  <td>$0.280$</td>
                                  <td></td>
                                   <td></td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$0.2$</td>
                                  <td>$1.064$</td>
                                  <td>$0.610$</td>
                                  <td></td>
                                   <td></td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$0.3$</td>
                                  <td>$1.125$</td>
                                  <td>$1.090$</td>
                                  <td></td>
                                   <td></td>
                                </tr>

                                <tr>
                                 <td>$3$</td>
                                  <td>$0.5$</td>
                                  <td>$1.343$</td>
                                  <td>$-$</td>
                                  <td></td> 
                                   <td></td>
                                </tr>
                              </tbody>
        </table>

        Observe que a última linha da nova coluna ficou vazia. Isso é algo que observamos na tabela das diferenças divididas: a coluna da diferença dividida de ordem $n$ sempre terá uma linha a menos que a coluna da diferença dividida de ordem $n-1$. <br><br>

        Agora, vamos calcular as diferenças divididas de $2ª$ ordem. Devemos ficar atentos ao denominador nesses próximos cálculos. Teremos duas diferenças divididas a serem calculadas usando os valores da coluna anterior: <br><br>

         <div class="equacao">
        $$f[x_0,x_1,x_2] = \frac{f[x_2,x_1]-f[x_1,x_0]}{x_2-x_0} = \frac{0.610-0.280}{0.3-0} = 1.100$$
        </div>

         <div class="equacao">
        $$f[x_1,x_2,x_3] = \frac{f[x_3,x_2]-f[x_2,x_1]}{x_3-x_1} = \frac{1.090-0.610}{0.5-0.2} = 1.600$$
        </div>

        Observe que a diferença no denominador sempre será <b>entre os $x_i$ extremos</b> da diferença dividida sendo calculada.<br><br>

        Atualizando a tabela:<br><br>

        <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$\triangle ^0y_i$</td>
                                  <td>$ \triangle ^1y_i$</td>
                                  <td>$ \triangle ^2y_i$</td>
                                  <td>$ \triangle ^3y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$0$</td>
                                  <td>$1.008$</td>
                                  <td>$0.280$</td>
                                  <td>$1.100$</td>
                                   <td></td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$0.2$</td>
                                  <td>$1.064$</td>
                                  <td>$0.610$</td>
                                  <td>$1.600$</td>
                                   <td></td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$0.3$</td>
                                  <td>$1.125$</td>
                                  <td>$1.090$</td>
                                  <td>$-$</td>
                                   <td></td>
                                </tr>

                                <tr>
                                 <td>$3$</td>
                                  <td>$0.5$</td>
                                  <td>$1.343$</td>
                                  <td>$-$</td>
                                  <td>$-$</td> 
                                   <td></td>
                                </tr>
                              </tbody>
        </table>

        Por fim, devemos calcular a diferença dividida restante de $3ª$ ordem. Temos que: <br>

         <div class="equacao">
        $$f[x_0,x_1,x_2,x_3] = \frac{f[x_1,x_2,x_3]-f[x_0,x_1,x_2]}{x_3-x_0} = \frac{1.600-1.100}{0.5-0} = 1$$ 
        </div>

        Obtemos a tabela final: <br><br>

        <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$\triangle ^0y_i$</td>
                                  <td>$ \triangle ^1y_i$</td>
                                  <td>$ \triangle ^2y_i$</td>
                                  <td>$ \triangle ^3y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$0$</td>
                                  <td>$1.008$</td>
                                  <td>$0.280$</td>
                                  <td>$1.100$</td>
                                   <td>$1$</td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$0.2$</td>
                                  <td>$1.064$</td>
                                  <td>$0.610$</td>
                                  <td>$1.600$</td>
                                   <td>$-$</td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$0.3$</td>
                                  <td>$1.125$</td>
                                  <td>$1.090$</td>
                                  <td>$-$</td>
                                   <td>$-$</td>
                                </tr>

                                <tr>
                                 <td>$3$</td>
                                  <td>$0.5$</td>
                                  <td>$1.343$</td>
                                  <td>$-$</td>
                                  <td>$-$</td> 
                                   <td>$-$</td>
                                </tr>
                              </tbody>
        </table>

        Quando temos a tabela das diferenças divididas completa, praticamente já temos o polinômio interpolador de Newton para o conjunto de pontos dessa tabela. Como já sabemos que os $b_i$ vão ser as diferenças divididas de ordem $i$, fica fácil: tudo que precisamos fazer é substituí-los pelas respectivas $\triangle ^iy_i$ que estão na primeira linha da nossa tabela. Como temos $4$ pontos, temos um polinômio de $3ª$ ordem: <br><br>

        <div class="equacao">
        $$
          \begin{aligned} 
            P_3(x) = &\; 1.008 \\
                  &+ 0.280(x-0) \\
                  &+ 1.100(x-0)(x-0.2) \\
                  &+ 1(x-0)(x-0.2)(x-0.3).
          \end{aligned}
        $$
        </div>

        Esse é o polinômio interpolador de grau $3$ que passa por todos esses pontos. Para verificar, é só substituir o $x$ por um $x_i$ e verificar que o resultado é exatamente $y_i$.

          </div>
        </div>
    </div>
</div>

<br><br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
            Dados os pontos abaixo, calcule o polinômio interpolador de $2º$ grau utilizando a interpolação de Newton e faça uma estimativa para $f(2.2)$. <br><br>

             <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$1.6$</td>
                                  <td>$2$</td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$2$</td>
                                  <td>$8$</td>
                                </tr>
                                <tr>
                                 <td>$2$</td>
                                  <td>$2.5$</td>
                                  <td>$14$</td>
                                </tr>
                              </tbody>
        </table>

        O primeiro passo é construir a tabela de diferenças divididas.<br><br>

        <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$\triangle ^0y_i$</td>
                                  <td>$ \triangle ^1y_i$</td>
                                  <td>$ \triangle ^2y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$1.6$</td>
                                  <td>$2$</td>
                                  <td>$ $</td>
                                  <td>$ $</td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$2$</td>
                                  <td>$8$</td>
                                  <td>$ $</td>
                                  <td>$ $</td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$2.5$</td>
                                  <td>$14$</td>
                                  <td>$ $</td>
                                  <td>$ $</td>
                                </tr>
                              </tbody>
        </table>

        Vamos calcular as diferenças divididas de $1ª$ ordem:<br>

         <div class="equacao">
        $$f[x_0,x_1] = \frac{y_1-y_0}{x_1-x_0} = \frac{8-2}{2-1.6} = 15$$
        </div>

         <div class="equacao">
        $$f[x_1,x_2] = \frac{14-8}{2.5-2} = 12$$
        </div>

        <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$\triangle ^0y_i$</td>
                                  <td>$ \triangle ^1y_i$</td>
                                  <td>$ \triangle ^2y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$1.6$</td>
                                  <td>$2$</td>
                                  <td>$15$</td>
                                  <td>$ $</td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$2$</td>
                                  <td>$8$</td>
                                  <td>$12$</td>
                                  <td>$ $</td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$2.5$</td>
                                  <td>$14$</td>
                                  <td>$-$</td>
                                  <td>$ $</td>
                                </tr>
                              </tbody>
        </table>

        Agora, calculamos a diferença dividida restante de $2ª$ ordem: <br>

         <div class="equacao">
        $$
        \begin{aligned}
        f[x_0,x_1,x_2] &= \frac{f[x_1,x_2]-f[x_0,x_1]}{x_2-x_0} \\
                    &= \frac{12-15}{2.5-1.6} = -3.33.
        \end{aligned}
        $$
        </div>

        <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$\triangle ^0y_i$</td>
                                  <td>$ \triangle ^1y_i$</td>
                                  <td>$ \triangle ^2y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$1.6$</td>
                                  <td>$2$</td>
                                  <td>$15$</td>
                                  <td>$-3.33$</td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$2$</td>
                                  <td>$8$</td>
                                  <td>$12$</td>
                                  <td>$-$</td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$2.5$</td>
                                  <td>$14$</td>
                                  <td>$-$</td>
                                  <td>$-$</td>
                                </tr>
                              </tbody>
        </table>

        E por fim, substituímos as diferenças divididas calculadas na primeira linha no polinômio de Newton: <br><br>

         <div class="equacao">
        $$P_2(x) = 2 + 15(x-1.6) - 3.33(x-1.6)(x-2)$$ 
        </div>

        <p style="text-align: center;"">
                  <img src="imagens/InterpolNewt1.svg"alt=""   style="width:90%;"/> 
                  </p><br><br>

        Obtendo nosso polinômio interpolador. Como queremos estimar o valor de $f(2.2)$, temos que: <br>

        <div class="equacao">
        $$
        \begin{aligned}
        P_2(2.2) &= 2 + 15(2.2-1.6) - 3.33(2.2-1.6)(2.2-2) \\
        &= 10.6.
        \end{aligned}
        $$
        </div>

        <b>E se quiséssemos adicionar um novo ponto?</b><br><br>

        A resposta é simples: basta adicionar um novo ponto no final da tabela e calcular as novas diferenças divididas normalmente. Lembrando que se adicionarmos um ponto, estaremos aumentando a ordem da última diferença dividida. Vamos adicionar o ponto $[3.2,15]$:<br><br>

        <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$\triangle ^0y_i$</td>
                                  <td>$ \triangle ^1y_i$</td>
                                  <td>$ \triangle ^2y_i$</td>
                                  <td>$ \triangle ^3y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$1.6$</td>
                                  <td>$2$</td>
                                  <td>$15$</td>
                                  <td>$-3.33$</td>
                                  <td>$ $</td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$2$</td>
                                  <td>$8$</td>
                                  <td>$12$</td>
                                  <td>$ $</td>
                                  <td>$-$</td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$2.5$</td>
                                  <td>$14$</td>
                                  <td>$ $</td>
                                  <td>$-$</td>
                                  <td>$-$</td>
                                </tr>

                                <tr>
                                 <td>$3$</td>
                                  <td>$3.2$</td>
                                  <td>$15$</td>
                                  <td>$-$</td>
                                  <td>$-$</td>
                                  <td>$-$</td>
                                </tr>
                              </tbody>
        </table>

        Agora, teremos uma diferença dividida a mais em cada coluna. Ou seja, devemos calcular mais uma diferença dividida de cada ordem devido a adição do novo ponto: <br><br>

         <div class="equacao">
        $$
        \begin{aligned} 
        f[x_2, x_3] &= \frac{15-14}{3.2-2.5} \\
        &= 1.43 
        \end{aligned}
        $$
        </div>

         <div class="equacao">
        $$
        \begin{aligned} 
        f[x_1, x_2, x_3] &= \frac{1.43-12}{3.2-2} \\
        &= -8.81
        \end{aligned}
        $$
        </div>

        <div class="equacao">
        $$
        \begin{aligned} 
        f[x_0, x_1, x_2, x_3] &= \frac{-8.81- (-3.33)}{3.2-1.6} \\
        &= -3.43.
        \end{aligned}
        $$
        </div>


        <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <td>i</td>
                                  <td>$x_i$</td>
                                  <td>$\triangle ^0y_i$</td>
                                  <td>$ \triangle ^1y_i$</td>
                                  <td>$ \triangle ^2y_i$</td>
                                  <td>$ \triangle ^3y_i$</td>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>$0$</td>
                                  <td>$1.6$</td>
                                  <td>$2$</td>
                                  <td>$15$</td>
                                  <td>$-3.33$</td>
                                  <td>$-3.43$</td>
                                </tr>
                                <tr>
                                 <td>$1$</td>
                                  <td>$2$</td>
                                  <td>$8$</td>
                                  <td>$12$</td>
                                  <td>$-8.81$</td>
                                  <td>$-$</td>
                                </tr>

                                <tr>
                                 <td>$2$</td>
                                  <td>$2.5$</td>
                                  <td>$14$</td>
                                  <td>$1.43$</td>
                                  <td>$-$</td>
                                  <td>$-$</td>
                                </tr>

                                <tr>
                                 <td>$3$</td>
                                  <td>$3.2$</td>
                                  <td>$15$</td>
                                  <td>$-$</td>
                                  <td>$-$</td>
                                  <td>$-$</td>
                                </tr>
                              </tbody>
        </table>


        O polinômio interpolador após a adição do novo ponto será: <br><br>

         <div class="equacao">
        $$
        \begin{aligned}
        P_3(x) = &\; P_2(x) \\
          &+ \triangle ^3y_i (x-x_0)(x-x_1)(x-x_2),
        \end{aligned}  
        $$
        </div>

        <div class="equacao">
        $$
        \begin{aligned}
        P_3(x) = &\; 2 + 15(x-1.6) - 3.33(x-1.6)(x-2) \\
        &- 3.43(x-1.6)(x-2)(x-2.5).
        \end{aligned}  
        $$ 
        </div>

        <p style="text-align: center;"">
        <img src="imagens/interpolNewt2.svg"alt=""   style="width:90%;"/> 
        </p>
        <br><br>

            </p>

          </div>
        </div>
    </div>
</div>







<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
     <p>Considerando a tabela abaixo contendo 4 pontos, encontre um polinômio de grau 3 usando o método de Newton para estimar o valor $P_3(10.12)$</p>
           <br>

             <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${y}$</th>
                                  <th>${x}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>${0.54}$</td>
                                  <td>${0.84}$</td>
                                </tr>
                                <tr>
                                <td>${4}$</td>
                                <td>${2}$</td>
                                </tr>

                                <tr>
                                <td>${8}$</td>
                                 <td>${-1.16}$</td>
                                </tr>
                                <tr>
                                <td>${11}$</td>
                                 <td>${6}$</td>
                                </tr>

                              </tbody>
                            </table>
            
            <p>Solução:</p>
             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>

            <br>
            
            </div>
        </div>
    </div>
</div>



<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }

</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":800,"height":600,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":false,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/InterpolacaoNewton.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-1,28,-16,8);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}

     
       
    }

    function anterior (){
         if(n >=0){

             ggbApplet.setCoordSystem(-1,28,-16,8);
        }
        else{

             ggbApplet.setCoordSystem(-1,28,-16,8);
        }

             if (this.n%2==0){
       
    }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
         if(n <=2){

             ggbApplet.setCoordSystem(-1,28,-16,8);
        }
        
        else{

             ggbApplet.setCoordSystem(-1,28,-16,8);
        }


        if (this.n <= 2){
            this.n++;
            ggbApplet.setValue('n', this.n)
          
            

             
                
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>

</script>

    <div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/diferencas_div.svg"alt=""   style="width:90%;"/> 
                  </p>
                </p>
                <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/newton_interpol.svg"alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/ZzU8UicToH0" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/interpolacaoNewton.sce"></script>
          </div>
        </div>
    </div>
</div>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false},
    {left: "\\begin{aligned}", right: "\\end{aligned}", display: true}
  ]});
</script>