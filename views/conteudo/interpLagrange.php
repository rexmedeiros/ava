<?php
use yii\helpers\Html;
use frontend\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Interpolação - Lagrange';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Introdução</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


            <p align="justify">
                O método de interpolação descrito nessa seção foi desenvolvido por Joseph Louis de Lagrange, motivo pelo qual  o método é conhecido por Interpolação de Lagrange.   <br>
            </p>

             <p align="justify">
                Antes de apresentarmos a fórmula geral e para ilustrar bem como funciona o cálculo do Polinômio Interpolador de Lagrange vamos considerar os seguintes pontos $(x_i,y_i)$, $i = 0,\dots, 2$:  
                $$
                  (-1;3),    (2;6),    (4;-2).
                $$  
               O único polinômio de grau 2 que passa exatamente por todos esses pontos é $-x^2+2x+6$. <br><br>

               A técnica de Lagrange fornece uma alternativa de como calcular esse mesmo polinômio que passa pelos três pontos utilizando três funções distintas (que também são polinômios), <strong>uma função $L_i(x)$ correspondente a cada ponto ($x_i,y_i)$</strong>, as quais possuem características bem definidas. <br><br>

               Essas funções são denominadas de polinômios de Lagrange $L_i(x$), e são calculadas da seguinte forma: <br>
                
                <div class = "equacao">
                $$L_i(x) = \frac{(x-x_0)(x-x_1)...(x-x_{i+1})(x-x_n)}{(x_i-x_0)(x_i-x_1)...(x_i-x_{i+1})(x_i-x_n)}$$ 
                </div>

                Calculando o $L_0(x)$ obtemos o primeiro polinômio referente ao primeiro ponto. 
                
                <div class = "equacao">
                $$L_0(x) = \frac{(x-2)(x-4)}{(-1-2)(-1-4)}  = \frac{1}{15}{(x-2)(x-4)}$$
                </div>

                Perceba que ao substituirmos o $x$ do polinômio $L_0(x)$ pelo $x_0$ do nosso primeiro ponto obtemos o valor 1, ou seja, $L_0(1) = 1$, e ao substiruir o $x$ pelos $x$ dos outros pontos, $x_1 = 2$ e $x_2 = 4$, obtemos o valor zero, $L_0(2) = 0$ e $L_0(4) = 0$. Com isso, concluímos que os valores $x=2$ e $x=4$ são raízes do polinômio $L_0(x)$. Essas são as características que os polinômios de Lagrange devem possuir.

                <br><br>
                Dessa forma, para $(x_i,y_i)$, o polinômio $L_i(x)$ deve obedecer:<br>
                
                <ol>
                  <li> $L_i(x_i) = 1 $ </li>
                  <li> 
                    $ L_i(x) = 0 \text{ para } x \neq x_i$.
                  </li>
                </ol>
                
                <br>
                
                Agora, vamos continuar com os cálculos para a obtenção do polinômio interpolador de Lagrange calculando o $L_1(x)$ para o ponto $(2,6)$ e o $L_2(x)$ para o ponto $(4,2)$: 
                
                <div class = "equacao">
                $$L_1(x) = \frac{(x+1)(x-4)}{(2+1)(2-4)}  = \frac{-1}{6}{(x+1)(x-4)}$$ 
                </div>
            
                <div class = "equacao">
                $$L_2(x) = \frac{(x+1)(x-2)}{(4+1)(4-2)}  = \frac{1}{10}{(x+1)(x-2)}$$ 
                </div>
                
                Veja que os polinômios $L_1(x)$ e $L_2(x)$ também obedecem às condições 1 e 2 mostradas para o primeiro ponto. <br><br>

                Concluído o cálculo dos polinônios $L_0(x)$, $L_1(x)$ e $L_2(x)$, devemos por fim encontrar um novo polinônio que passará exatamente por todos os três pontos e este será o Polinômio Interpolador de Lagrange que denotaremos por $P_n(x)$, onde o $n$ representará o grau do polinônio encontrado. A fórmula geral é: 

                $$
                 P_n(x) = \sum_{i=0}^n y_i L_i(x).
                $$ 
              
                Estendendo a fórmula para o nosso exemplo em que temos um polinômio de grau $n = 2$ e $n + 1$ pontos, ou seja, $3$ pontos. Temos: <br>

               <div class="equacao">
               $$
               \begin{aligned}
                 P_2(x) = &\, y_0\frac{(x - x_1)(x - x_2)}{(x_0 - x_1)(x_0 - x_2)} \\
                        &+ y_1\frac{(x - x_0)(x - x_2)}{(x_1 - x_0)(x_1 - x_2)} \\
                        &+ y_2\frac{(x - x_0)(x - x_1)}{(x_2 - x_0)(x_2 - x_1)}. 
                 \end{aligned}
               $$
               </div>
              
              Como solução do nosso exemplo, concluímos que o polinômio interpolador que passar exatemente pelos pontos $(-1,3) \ (2,6) \ (4,-2)$ é:
              
              <div class="equacao">
              $$
              \begin{aligned}
               P_2(x) = &\; 3\frac{1}{15}{(x-2)(x-4)} \\
                        &+ 6\frac{-1}{6}{(x+1)(x-4)} \\
                        &- 2\frac{1}{10}{(x+1)(x-2)},
               \end{aligned}
              $$
              </div>
              ou,
              $$
              P_2(x) = -x^2+2x+6.
              $$

              Perceba que ao se calcular o produto $y_iL_i(x_i)$ temos como resultado o valor de $ P_n(x_i) = y_i $ no ponto $x_i$, isso nos assegura que o polinômio $P_n(x)$ passa exatamente pelo ponto $(x_i,y_i)$. <br><br>

              Como já foi mostrado anteriormente, uma das condições que o polinômio $L_i(x)$ deve obedecer é que $L_i(x_i) = 1$, logo, quando multiplicamos isso pelo $y_i$ temos como resultado o próprio valor de $y_i$. Quando $L_i(x_i) = 1$ for satisfeito para algum dos pontos, os outros termos de $P_n(x)$ serão iguais a zero. Isso se dá devido à segunda condição que $L_i(x_i)$ deve obedecer, essa condição trata os outros pontos analisados como raízes do polinômio. <br><br>

              Veja a ilustração que mostra o raciocínio por trás dos polinômios de Lagrange. A figura abaixo mostra o caso do exemplo feito anteriormente. Podemos ver cada um dos três termos da equação passando por um dos pontos dados e tendo valor zero nos outros pontos. Podemos ver também a soma dos três termos que resultou em um polinômio de segundo grau que passa exatamente por todos os três pontos. <br><br>

               <p style="text-align: center; ">
            <img src="imagens/lagrange_demonstracao.svg" alt=""   style="width:90%;"/>
            </p>



          </div>
        </div>
    </div>
</div>
<br><br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


            <p align="justify">
              Usando o método de Lagrange, encontre o polinômio interpolador que passa pelos quatro pontos da tabela abaixo: <br><br>
              <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${x}$</th>
                                  <th>${y}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>${1.3}$</td>
                                  <td>${3.2}$</td>
                                </tr>
                                <tr>
                                 <td>${1.8}$</td>
                                <td>${4.3}$</td>
                                </tr>

                                <tr>
                                 <td>${2.6}$</td>
                                <td>${0.5}$</td>
                                </tr>

                                <tr>
                                 <td>${3.9}$</td>
                                <td>${-1.7}$</td>
                                </tr>
                              </tbody>
                            </table>

            </p>

            <b>Solução: </b><br>
             <div class="equacao">
            $$L_0 = \frac{(x-x_1)(x-x_2)(x-x_3)}{(x_0-x_1)(x_0-x_2)(x_0-x_3)} $$
            </div>

            <div class="equacao">
            $$L_1 = \frac{(x-x_0)(x-x_1)(x-x_2)}{(x_1-x_0)(x_1-x_0)(x_1-x_3)}$$
            </div>

             <div class="equacao">
            $$L_2 = \frac{(x-x_0)(x-x_1)(x-x_3)}{(x_2-x_0)(x_2-x_1)(x_2-x_3)}$$
            </div>

             <div class="equacao">
            $$L_3 = \frac{(x-x_0)(x-x_1)(x-x_2)}{(x_3-x_0)(x_3-x_1)(x_3-x_2)}$$
            </div>

            <div class="equacao">
            $$P_3(x) = y_0L_0 + y_1L_1 + y_2L_2 + y_3L_3$$
            </div>
            <br>

            Substituindo os valores, teremos 
            <br>
            <div class="equacao">
            $$L_0 = \frac{(x-1.8)(x-2.6)(x-3.9)}{(1.3-1.8)(1.3-2.6)(1.3-3.9)}$$
            </div>

             <div class="equacao">
            $$L_1 = \frac{(x-1.3)(x-2.6)(x-3.9)}{(1.8-1.3)(1.8-2.6)(1.8-3.9)}$$
            </div>

             <div class="equacao">
            $$L_2 = \frac{(x-1.3)(x-1.8)(x-3.9)}{(2.6-1.3)(2.6-1.8)(2.6-3.9)}$$
            </div>

             <div class="equacao">
            $$L_3 = \frac{(x-1.3)(x-1.8)(x-2.6)}{(3.9-1.3)(3.9-1.8)(3.9-2.6)}$$
            </div>

             <div class="equacao">
            $$P_3(x) = 3.2L_0 + 4.3L_1 + 0.5L_2 + (-1.7)L_3$$
            </div>

            Uma forma de conferir se a interpolação está correta é verificar se os quatro pontos dados pertencem ao polinômio $P_3(x)$. Por exemplo, se calcularmos $P_3(1.3)$, então a resposta tem que ser $3.2$ e assim por diante. Plotando a função obtida acima, temos o seguinte: <br><br>

            <p style="
    
           text-align: center;
          ">


            <img src="imagens/interpolLagrange1.svg" alt=""   style="width:90%;"/>
            </p>

            <br><br>


            Podemos observar que o polinômio interpolador passa por todos os pontos dados.

          </div>
        </div>
    </div>
</div>

<br><br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 2</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


            <p align="justify">
              Considere os pontos da tabela e encontre o <b>melhor polinômio de Lagrange de segundo grau</b> para estimar (interpolar) $f(4.5)$:
              <br><br>
              <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${x}$</th>
                                  <th>${y}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>${1}$</td>
                                  <td>${0.000}$</td>
                                </tr>
                                <tr>
                                 <td>${2}$</td>
                                <td>${0.6931}$</td>
                                </tr>

                                <tr>
                                 <td>${3.5}$</td>
                                <td>${1.2528}$</td>
                                </tr>

                                <tr>
                                 <td>${5}$</td>
                                <td>${1.6094}$</td>
                                </tr>
                                <tr>
                                 <td>${7}$</td>
                                <td>${1.9459}$</td>
                                </tr>
                              </tbody>
                            </table>

            </p>
            <p align="justify">

            <b>Solução: </b><br><br>
            Como iremos fazer uma interpolação para encontrar um polinômio de grau $2$, devemos escolher $n + 1$ pontos. Sendo $n = 2$ (grau do polinômio), devemos escolher os $3$ melhores pontos para calcular  $f(4.5)$.<br><br>

            Ao analisar a tabela, podemos ver que os 3 valores mais proximos de $x$ são: $3.5, 5$ e $7$.<br> Observe que poderíamos utilizar o $x=2$ também, pois ele se encontra na mesma distância $(2.5)$ de $7$, em relação a $4.5$. Então, se fizermos tanto com $x=2$ quanto com $x=7$, o resultado final será compatível. <br> <br>

            Para calcular o polinômio, precisaremos dos valores de $x$ e $y$ de cada um dos três pontos, conforme tabela à seguir: <br><br>
            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${x}$</th>
                                  <th>${y}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>${3.5}$</td>
                                  <td>${1.2528}$</td>
                                </tr>
                                <tr>
                                 <td>${5}$</td>
                                <td>${1.6094}$</td>
                                </tr>

                                <tr>
                                 <td>${7}$</td>
                                <td>${1.9459}$</td>
                                </tr>
                                <tr>
                                </tr>
                              </tbody>
                            </table>

                            <br>
                            Neste exemplo, precisamos encontrar o polinômio de segundo grau que passa por 3 pontos. Então, precisamos determinar três polinômios de Lagrange:  <br>
                            
                            <div class="equacao">
                            $$L_0 = \frac{(x-5)(x-7)}{(3.5-5)(3.5-7)},$$
                            </div>

                             <div class="equacao">
                            $$L_1 = \frac{(x-3.5)(x-7)}{(5-3.5)(5-7)},$$
                            </div>

                             <div class="equacao">
                            $$L_2 = \frac{(x-3.5)(x-5)}{(7-3.5)(7-5)},$$
                            </div>

                             <div class="equacao">
                            $$P_2(x) = (1.2528)L_0 + (1.6094)L_1 + (1.9459)L_2.$$ 
                            </div>

                            Ao substituir $x$ por $4.5$ em $P_2(x)$, obtemos $f(4.5) \approx P_2(4.5) = 1.5005$. Abaixo ilustra-se o resultado obtido: <br><br>

                             <p style="
    
                               text-align: center;
                              ">


                                <img src="imagens/interplagex2_.svg" alt=""   style="width:90%;"/>
                             </p>

            <br><br>
            </p>

          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 3</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


            <p align="justify">
              Considerando a tabela onde estão representados alguns pontos da função $f(x) = \sqrt[3]{x}$, determine o valor aproximado de $0.5^{3}$.
              <br><br>
              <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${x}$</th>
                                  <th>${f(x)}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>${0}$</td>
                                  <td>${0}$</td>
                                </tr>
                                <tr>
                                 <td>${0.008}$</td>
                                <td>${0.2}$</td>
                                </tr>

                                <tr>
                                 <td>${0.064}$</td>
                                <td>${0.4}$</td>
                                </tr>

                                <tr>
                                 <td>${0.216}$</td>
                                <td>${0.6}$</td>
                                </tr>
                                <tr>
                                 <td>${0.512}$</td>
                                <td>${0.8}$</td>
                                </tr>
                              </tbody>
                            </table>

            </p>
            <p align="justify">

            <b>Solução: </b><br><br>
            Note inicialmente que interpolar entre pontos da tabela fornece estimativas para $\sqrt[3]{x}$. No entando, estamos interessados numa estimativa da função $x^3$ para $x = 0.5$. Perceba que $x^3$ é a função inversa de $\sqrt[3]{x}$. 
                        
            <br><br>Dessa forma teremos que fazer uma interpolação reversa, ou seja, em vez de utilizar os valores de $x$ para escrever o polinômio, utilizaremos os valores de $y$. <br><br>Então, vamos calcular um polinômio de grau $3$ usando os $4$ valores de $y$ mais proximos de $0.5$ e seus correspondes em $x$. Fazendo a escolha dos melhores pontos, teremos: <br><br>
            <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${y}$</th>
                                  <th>${x}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>${0.2}$</td>
                                  <td>${0.008}$</td>
                                </tr>
                                <tr>
                                <td>${0.4}$</td>
                                <td>${0.064}$</td>
                                </tr>

                                <tr>
                                <td>${0.6}$</td>
                                 <td>${0.216}$</td>
                                </tr>
                                <tr>
                                </tr>

                                <tr>
                                <td>${0.8}$</td>
                                 <td>${0.512}$</td>
                                </tr>
                                <tr>
                                </tr>
                              </tbody>
                            </table>
                            Com os melhores pontos escolhidos, já podemos fazer a interpolação: <br>

                            <div class="equacao">
                              $$L_0 = \frac{(y-y_1)(y-y_2)(y-y_3)}{(y_0-y_1)(y_0-y_2)(y_0-y_3)}$$
                              </div>

                               <div class="equacao">
                              $$L_1 = \frac{(y-y_0)(y-y_1)(y-y_2)}{(y_1-y_0)(y_1-y_2)(y_1-y_3)}$$
                              </div>

                               <div class="equacao">
                              $$L_2 = \frac{(y-y_0)(y-y_1)(y-y_3)}{(y_2-y_0)(y_2-y_1)(y_2-y_3)}$$
                              </div>

                              <div class="equacao">
                              $$L_3 = \frac{(y-y_0)(y-y_1)(y-y_2)}{(y_3-y_0)(y_3-y_1)(y_3-y_2)}$$
                              </div>

                            <div class="equacao">
                              $$y = x_0L_0 + x_1L_1 + x_2L_2 + x_3L_3$$<br>
                            </div>

                              Substituindo os valores de $x$ e $y$ ficaremos com <br>
                            <div class="equacao">
                              $$L_0 = \frac{(y-0.4)(y-0.6)(y-0.8)}{(0.2-0.4)(0.2-0.6)(0.2-0.8)}$$
                            </div>

                             <div class="equacao">
                              $$L_1 = \frac{(y-0.2)(y-0.6)(y-0.8)}{(0.4-0.2)(0.4-0.6)(0.4-0.8)}$$
                             </div>

                              <div class="equacao">
                              $$L_2 = \frac{(y-0.2)(y-0.4)(y-0.8)}{(0.6-0.2)(0.6-0.4)(0.6-0.8)}$$
                              </div>

                              <div class="equacao">
                              $$L_3 = \frac{(y-0.2)(y-0.4)(y-0.6)}{(0.8-0.2)(0.8-0.4)(0.8-0.6)}$$
                              </div>

                            <div class="equacao">
                              $$x = (0.008)L_0 + (0.064)L_1 + (0.216)L_2 + (0.512)L_3$$ <br>
                            </div>

                              Fazendo $y = 0.5$, teremos que $x = 0.125$. Ilustrando o resultado obtido: <br><br>

                              <p style="
    
                               text-align: center;
                              ">


                                <img src="imagens/interplagex3_.svg" alt=""   style="width:90%;"/>
                             </p>





            </p>

          </div>
        </div>
    </div>
</div>

<br><br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Problemas com a interpolação de Lagrange</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            A interpolação de Lagrange tem um inconveniente: se tivermos feito essa interpolação para obter um polinômio interpolador para $n$ pontos e se quiséssemos acrescentar mais um ponto para melhorar a aproximação, por exemplo, teríamos que calcular o polinômio praticamente do zero. <br><br>

            Esse problema é solucionado com a interpolação de Newton, que veremos a seguir.
              

          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
     <p>Considerando a tabela abaixo contendo 5 pontos, encontre um polinômio de grau 3 usando o método de Lagrange para estimar f(5)</p>
           <br>
             <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${y}$</th>
                                  <th>${x}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>${0.84}$</td>
                                  <td>${0.64}$</td>
                                </tr>
                                <tr>
                                <td>${3.46}$</td>
                                <td>${3.2}$</td>
                                </tr>

                                <tr>
                                <td>${6}$</td>
                                 <td>${-1}$</td>
                                </tr>

                                <tr>
                                <td>${9}$</td>
                                 <td>${4}$</td>
                                </tr>

                                <tr>
                                <td>${13}$</td>
                                 <td>${2.2}$</td>
                                </tr>

                              </tbody>
                            </table>

            
            <p>Solução:</p>
             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>

            <br>
            
            </div>
        </div>
    </div>
</div>



<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }

</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":800,"height":600,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":false,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/InterpolacaoLagrange_.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-1,14,-12,7);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}

     
       
    }

    function anterior (){
         if(n>=0){

             ggbApplet.setCoordSystem(-1,14,-12,7);
        }
        else{

             ggbApplet.setCoordSystem(-1,14,-12,7);
        }

             if (this.n%2==0){
       
    }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
         if(n <=4){

             ggbApplet.setCoordSystem(-1,14,-12,7);
        }
        
        else{

             ggbApplet.setCoordSystem(-1,14,-12,7);
        }


        if (this.n <= 4){
            this.n++;
            ggbApplet.setValue('n', this.n)
          
            

             
                
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>

</script>

    <div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/lagrange_interpol.svg"alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/PnbjX1WLLPE" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/interpolacaoLagrange.sce"></script>
          </div>
        </div>
    </div>
</div>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false},
    {left: "\\begin{aligned}", right: "\\end{aligned}", display: true}
  ]});
</script>