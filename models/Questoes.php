<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "questoes".
 *
 * @property integer $id
 * @property integer $exercicios_id
 * @property string $enunciado
 * @property integer $id_questao_instanciada
 * @property integer $usos
 *
 * @property Alternativas[] $alternativas
 * @property Exercicios $exercicios
 * @property Respostas[] $respostas
 */
class Questoes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questoes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exercicios_id', 'enunciado'], 'required'],
            [['exercicios_id', 'id_questao_instanciada', 'usos'], 'integer'],
            [['exercicios_id'], 'exist', 'skipOnError' => true, 'targetClass' => Exercicios::className(), 'targetAttribute' => ['exercicios_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'exercicios_id' => 'Exercicios ID',
            'enunciado' => 'Enunciado',
            'id_questao_instanciada' => 'Id Questao Instanciada',
            'usos' => 'Usos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlternativas()
    {
        return $this->hasMany(Alternativas::className(), ['questoes_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExercicios()
    {
        return $this->hasOne(Exercicios::className(), ['id' => 'exercicios_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRespostas()
    {
        return $this->hasMany(Respostas::className(), ['questao_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlternativasFormatadas()
    {
        $novasAlternativas = array();
        if ($this->exercicios->imagens){  
            foreach ($this->alternativas as $alternativa){
                $url = Url::to('@web/imagens/exercicios/'.$this->exercicios_id.'/'.$alternativa->arquivo.'p.svg');
                $novasAlternativas[$alternativa->letra] = ' <span  style="display: inline-block"><img class="imagem-alternativa" src = "'.$url.'"></span> ';
            } 
        }
        else{
            foreach ($this->alternativas as $alternativa){
                $novasAlternativas[$alternativa->letra] = stripslashes($alternativa->alternativa);
            }            
        }
        return $novasAlternativas; 
    }

    /**
     * @inheritdoc
     * @return QuestoesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuestoesQuery(get_called_class());
    }
}
