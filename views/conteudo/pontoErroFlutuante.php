<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Integração Numérica - Simpson 1/3';
$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
<script 
src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-AMS-MML_HTMLorMML' async>
</script>

<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { equationNumbers: { autoNumber: "AMS" } }
});
</script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">\
                <h2><i class="fa fa-binoculars"></i> Videoaula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <iframe width="600" height="400" src="https://www.youtube.com/embed/OVuyMcnPKOc?start=769" frameborder="0" allowfullscreen></iframe>
                <br><br>

                    </div>
                </div>
            </div>  
        </div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Maior e menor representáveis</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

            Quando o expoente é menor ocorre underflow.Quando um o expoente do número é maior que expoente máximo, ocorre o chamado overflow. Desta forma, pode-se encontrar o minimo representavel pelo sistema sendo o menor numero antes que ocorra underflow e o maior como o maior numero antes que ocorra overflow.        
<br>
<p style="
    
 text-align: center;
">


<img src="imagens/minimoPonto.PNG">
</p>
  </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Erros de arredondamento</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

Ao representar um número real em um sistema em ponto flutuante ocorre um erro. Esse erro deve-se a precisão utilizada na mantissa. A mantissa é truncada para valores 
menores que a precisão do sistema.
<br><br>
Veja abaixo o erro no sistema:
<br>
<p style="
    
 text-align: center;
">


<img src="imagens/erroPontoFlutuante.PNG">
</p>
Dessa forma, para esse sistema pode-se saber o erro máximo. O erro máximo é:

$$   \epsilon = \frac{\beta^{(1-p)}}{2} $$

$$\epsilon = \frac{2^{-5}}{2}= 0.015625 > 0.004785$$

Bem maior que o erro cometido anteriormente.
</div>
        </div>  
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>  Aritmética em ponto flutuante </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

A múltiplicação e a divisão em ponto flutuante são parecidas. Na multiplicação, faz-se o produto da mantissa e soma 
se os expoentes. Na divisão, divide-se as mantissas e subtrai-se os expoentes.
<br><br>
Diferentemente, a soma e a subtração em ponto flutuante requer passos a mais. Primeiramente deve-se igualar o expoente do
menor número. Após isso realiza-se a soma ou subtração da mantissa. 

</div>
        </div>
    </div>
</div>




<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">   
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
     <p>Abaixo encontra-se como realizar as operações básicas em ponto flutuante</p>
          <br>
         <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
           
            <div id="applet_container" align="center"></div>

            <br>
            
            </div>
        </div>
    </div>
</div>







<script type="text/javascript"  > 
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }

</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/aritmeticPontoF.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-1,1,-1,1);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun; 

            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
}
   function mensagens(n){
        ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}

     
       
    }

function ggbpic1(){

n2 = ggbApplet.getValue("numero2")
n1 = ggbApplet.getValue("numero1")
ob = ggbApplet.getValue("b")
op = ggbApplet.getValue("p")
om = ggbApplet.getValue("m")
ok = ggbApplet.getValue("k")
r1 = ggbApplet.getValue("resto1")
r2 = ggbApplet.getValue("resto2")
binario1 = ggbApplet.getValue("text9")
binario2 = ggbApplet.getValue("text9")

ggbApplet.setValue('n',1)

}
function ggbbutton1(){

$contador = 0;

if(n1>1){


$novoN1 = n1;
$cont = 0;



while($novoN1>=1){
$cont = $cont+1;
$novoN1= $novoN1/ob;

if($cont>ok){
alert("OVERFLOW");
break;

    }


}

alert(binario1);

ggbApplet.setValue('numero1',n1);

}
    
}
function ggbInputBox3() { 


} 

 
   
    function anterior (){
         

             if (this.n%2==0){
       
    }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
        


        if (this.n <= 10){
            this.n++;
            ggbApplet.setValue('n', this.n)
          
            

             
                
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>