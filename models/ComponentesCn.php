<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "componentes_cn".
 *
 * @property integer $id
 * @property string $codigo
 * @property string $nome
 * @property integer $user_id
 * @property integer $criado_em
 *
 * @property User $user
 */
class ComponentesCn extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'componentes_cn';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codigo', 'nome', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['codigo'], 'string', 'max' => 15],
            [['nome'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo' => 'Codigo',
            'nome' => 'Nome',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getMatrizComponentes()
    {
        $matriz = false;
        $todosComponentes = $this->find()->all();
        if ($todosComponentes){
            foreach ($todosComponentes as $componente){
                $todosComponentes[] = $componente->codigo;
            }
        }
        return $todosComponentes;
    }

}
