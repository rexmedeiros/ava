<?php
use yii\helpers\Html;
/* @var $this yii\web\View */


$this->title = 'Equações Diferenciais - Introdução';
$this->params['breadcrumbs'][] = $this->title;
?>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
<script 
src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-AMS-MML_HTMLorMML' async>
</script>

<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { equationNumbers: { autoNumber: "AMS" }, extensions: ["AMSmath.js"] }
});
</script>

<code>Mesmas observações de Integração</code>
<BR>


<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Introdução</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
<p>A modelagem de sistemas dinâmicos é normalmente realizada por meio de equações diferenciais. Como exemplo, considere um paraquedista que salta de um avião e no instante $t_0$ abre o paraquedas quando está a uma velocidade de queda $v_0$ . Baseado na segunda Lei de Newton, temos a seguinte equação que descreve a aceleração do paraquedista em função do tempo da queda:</p>

                $\begin{pmatrix}-0,083\\-0,166\\-0,302\end{pmatrix}$


\begin{align}\label{eqS3}
  \frac{\mathrm dv}{\mathrm dt} 
& = g - \frac{c}{m}v, 
\end{align}

onde <i>g</i> é aceleração da gravidade, <i>c</i> é coeficiente de arrasto, <i>m</i> é a massa do paraquedista e <i>v</i> sua velocidade. <br> <br>

Na equação (\ref{eqS3}) a quantidade que está sendo derivada, <i>v</i>, é chamada de <b>variável dependente</b>. Já a quantidade em relação a qual <i>v</i> é derivada, <i>t</i>, é chamada de <b>variável independente</b>. Quando temos <b>uma única variável independente</b>, a equação é chamada de <b>Equação Diferencial Ordinária (EDO)</b>.
<br><br>
As EDOs também são classificadas em relação a sua ordem. A equação (\ref{eqS3}) é chamada de equação de primeira ordem, pois a derivada mais alta é de primeira ordem. 
<br><br>
</div>
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-taxi"></i> Solução Analítica de uma EDO</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

Algumas EDO podem ser resolvidas analiticamente. Dentre elas estão as equações ditas separáveis, nas quais os termos relativos às variáveis dependente e independente podem ser isolados em cada lado da igualdade. Como exemplo, suponha que tenhamos a seguinte equação diferencial ordinária:</p>

\begin{equation}\label{eqS13}
      \frac{\text{d}y}{\text{d}x} = -2x³+12x²-20x+8.5.
\end{equation}

<p>
Sabemos que se quisermos obter a função $y$, devemos apenas integrar os dois lados da equação em relação a $x$. Por ser um polinômio, essa integração é fácil de ser feita. Sendo assim:
</p>

\begin{align}\label{eqS53}
      \int_{}^{} \frac{\text{d}y}{\text{d}x}dx = \int_{}^{}(-2x³+12x²-20x+8.5) dx 
\end{align}
\begin{equation}\label{eqS33}
      y= -0.5x⁴+4x³-10x²+8.5x+C,
\end{equation}
em que $C$ é uma constante qualquer.
</div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-window-maximize"></i> Problema de Valor inicial</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
<p align="justify">
Ao analisar a equação (\ref{eqS33}),  observamos o surgimento de uma constante <i>C</i> introduzida ao resolver a integral indefinida.  Essa constante indica que a EDO possui infinitas soluções. Se quisermos obter uma solução em particular, a constante citada deve ser <b>fornecida no problema</b>, embora que inplicitamente. Na prática, essa constante é determinada a partir do conhecimento do valor $y = y_0$ da função para algum valor de $x = x_0$. Quando a EDO é fornecida juntamente com a condição $y_0 = f(x_0)$, chamamos esse problema de Problema de Valor Inicial (PVI).
</p>
</div>
        </div>
    </div>
</div>
<br>
<p align="justify">
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-question"></i> Por que usar métodos numéricos para solucionar EDOS?</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
Vimos um caso onde a solução de uma EDO pode ser obtida analiticamente. Porém, muitas EDOs de importância prática não possuem uma solução analítica e exata. Por esse motivo, devemos recorrer aos métodos numéricos. 
<br><br>Nesse tópico veremos resoluções de EDOs através dos seguintes métodos: Euler, Heun (Euler Modificado) e os métodos de Runge-Kutta.
</p>

            </div>
        </div>
    </div>
</div>






