<?php
use yii\helpers\Html;
use app\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Sistemas Lineares - Decomposição LU';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js"
        integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js"
        integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH"
        crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Gauss-Seidel</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/HwotCPT4WTY" allowfullscreen></iframe>
               </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Método iterativo de Gauss-Seidel</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                O nome para esse método é uma homenagem aos dois matemáticos alemães que fizeram maior contribuição para esse desenvolvimento:  Carl Friedrich Gauss e  Philipp Ludwig von Seidel. O método de Gauss-Seidel é um método iterativo para resolução de sistemas de equações lineares, e é muito semelhante ao método de Jacobi.    
            </p>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Conteúdo</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                Da mesma forma que no método de Jacobi, no método de Gauss-Seidel o sistema linear $Ax = b$ é escrito na forma equivalente $x = Bx + d$ por separação da diagonal.<br><br>

                O método de Gauss-Seidel difere do de Jacobi por um detalhe: no momento de se calcular $x_{j}^{(k+1)}$ usamos todos os valores $x_{1}^{(k+1)},...,x_{j-1}^{(k+1)}$ que já foram calculados anteriormente na mesma iteração. Dessa forma, o método de Gauss-Seidel é superior ao método de Jacobi no que diz respeito ao número de iterações para a convergência da resolução do sistema linear. <br><br>

                A nova solução $x^{(k+1)}$ dado uma solução $x^{(k)}$ é calculada da seguinte maneira: 

                 <div class ="equacao"> 
                 $$ \begin{cases} x_{1}^{(k+1)} =  \frac{1}{a_{11}}(b_{1} - a_{12}x_{2}^{(k)} - a_{13}x_{3}^{(k)} - ... - a_{1n}x_{n}^{(k)})
                \\ x_{2}^{(k+1)} =  \frac{1}{a_{22}}(b_{2} - a_{21}x_{1}^{(k+1)} - a_{23}x_{3}^{(k)} - ... - a_{2n}x_{n}^{(k)})
                \\x_{3}^{(k+1)} =  \frac{1}{a_{33}}(b_{3} - a_{31}x_{1}^{(k+1)} - a_{32}x_{2}^{(k+1)} - ... - a_{n3}x_{n}^{(k)})
                \\. \space \space\space\space\space\space\space\space\space\space\space\space\space\space\space\space . \space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space \space\space . \space\space\space\space\space\space\space\space\space\space\space\space .
                \\. \space \space\space\space\space\space\space\space\space\space\space\space\space\space\space\space . \space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space \space\space .  \space\space\space\space\space\space\space\space\space\space\space\space .
                \\. \space \space\space\space\space\space\space\space\space\space\space\space\space\space\space\space . \space\space\space\space\space\space\space\space\space\space\space\space\space\space\space\space \space\space .  \space\space\space\space\space\space\space\space\space\space\space\space .
                \\ x_{n}^{(k+1)} =  \frac{1}{a_{nn}}(b_{n} - a_{n1}x_{1}^{(k+1)} - a_{n2}x_{2}^{(k+1)} - ... - a_{n,n-1}x_{n-1}^{(k+1)})
                \end{cases}$$ 
                </div>
            
                
            Em suma, para calcular o valor de $x_2$ dentro da mesma iteração usamos o valor de $x_1$ calculado na mesma iteração. Para calcular o valor de $x_3$, usamos os valores de $x_1$ e $x_2$ da iteração atual e assim por diante. Podemos entender melhor esse processo iterativo com o exemplo a seguir.    
            </p>
            
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                Resolva o sistema linear:

                $$ \begin{cases} 5x_{1} + x_{2} + x_{3} = 5
                \\ 3x_{1} + 4x_{2} + x_{3} = 6
                \\ 3x_{1} + 3x_{2} + 6x_{3} = 0
                \end{cases}$$

                pelo método de Gauss-Seidel com $x^{(0)} = \space \begin{bmatrix}0 \\ 0 \\ 0 \end{bmatrix}$ e o erro $= 0.05$ 

           <br><br> O primeiro passo é escrever o sistema na forma de um processo iterativo, isolando a varíavel $x_i$ na equação $i$:

           <div class="equacao">
              $$ \begin{cases} x_{1} = -0.2x_{2} - 0.2x_{3} + 1 
                \\ x_{2} = -0.75x_{1} - 0.25x_{3} +1.5  
                \\ x_{3} = -0.5x_{1} - 0.5x_{2}
                \end{cases}$$
            </div>

            Lembrando que as matrizes $B$ e $d$ do processo iterativo são:

            <div class="equacao">
             $$
             \begin{aligned}
            B &= \begin{bmatrix}0 & -0.2 & -0.2 \\ -0.75 & 0 & -0.25 \\ -0.5 & -0.5 & 0 \end{bmatrix},\\
            d &= \begin{bmatrix}1 \\ 1.5 \\ 0 \end{bmatrix}.
            \end{aligned}
            $$
            </div>

            Especialmente a matriz $B$ é importante para sabermos se o processo iterativo é convergente (ver <a href = "<?=Url::toRoute(['conteudo/sislin-cconvergencia'])?>">Critérios de Convergência</a>).

            <br><br>

            Agora rotulamos as variáveis que já foram calculadas na iteração atual, para que possam ser usadas dentro da mesma iteração:
            
              <div class="equacao">
              $$ \begin{cases} x_{1}^{(k+1)} = -0.2x_{2}^{(k)} - 0.2x_{3}^{(k)} + 1 
                \\ x_{2}^{(k+1)} = -0.75x_{1}^{(k+1)} - 0.25x_{3}^{(k)} +1.5  
                \\ x_{3}^{(k+1)} = -0.5x_{1}^{(k+1)} - 0.5x_{2}^{(k+1)}  
                \end{cases}$$
                </div>
            
                Observe que no método de Gauss-Seidel nós estamos utilizando os valores de $x$ que já foram calculados anteriormente, em vez de usarmos o valor do chute inicial. Dessa forma, o sistema convergirá mais rápido.<br><br>

                <strong>Iteração 1:</strong>
            
                <div class="equacao">
                $$ \begin{cases} x_{1}^{(1)} = -0.2x_{2}^{(0)} - 0.2x_{3}^{(0)} + 1 = -0.2*0 - 0.2*0 + 1 = 1
                \\ x_{2}^{(1)} = -0.75x_{1}^{(1)} - 0.25x_{3}^{(0)} + 1.5 = -0.75*1 - 0.25*0 + 1.5 = 0.75
                \\ x_{3}^{(1)} = -0.5x_{1}^{(1)} - 0.5x_{2}^{(1)} = -0.5*1 - 0.5*0.75 = -0.875
                \end{cases}$$                 
                </div>

                Ou seja,
           $$
            x^{(1)} =\begin{bmatrix}x_{1}\\ x_{2} \\ x_{3} \end{bmatrix}
              = \begin{bmatrix}1 \\ 0.75 \\ -0.875 \end{bmatrix}
            $$
            
            Agora podemos fazer o cálculo do erro para vermos o quão próximo estamos do erro que queremos obter. Para isso, calculamos o erro relativo usando a norma linha:

            <div class="equacao">
            $$
            \begin{aligned}
                E_r &= \frac{\left|x^{(1)}-x^{(0)}\right|_L }{\left|x^{(1)}\right|_L}\\
                    & = \frac{\left|
                        \begin{bmatrix}1 \\ 0.75 \\ -0.875 \end{bmatrix}
                        -  \begin{bmatrix}0 \\ 0 \\ 0 \end{bmatrix}
                    \right|_L }{\left|\begin{bmatrix}1 \\ 0.75 \\ -0.875 \end{bmatrix}\right|_L}\\    
                    &= 1.
            \end{aligned}
            $$
            </div>
            

            Como $1 > 0.05$, o precesso iterativo continua. Prosseguindo as iterações, nós teremos:
            <br><br>

            <strong>Iteração 2:</strong>

                <div class="equacao">
                $$ \begin{cases} x_{1}^{(2)} = -0.2x_{2}^{(1)} - 0.2x_{3}^{(1)} + 1 = -0.2*0.75 - 0.2*(-0.875) + 1 = 1.025
                \\ x_{2}^{(2)} = -0.75x_{1}^{(2)} - 0.25x_{3}^{(1)} + 1.5 = -0.75*1.025 - 0.25*(-0.875) + 1.5 = 0.95
                \\ x_{3}^{(2)} = -0.5x_{1}^{(2)} - 0.5x_{2}^{(2)} = -0.5*1.025 - 0.5*0.95 = -0.9875
                \end{cases}$$
                </div>
                
                Ou seja,
                 $$
            x^{(2)} =\begin{bmatrix}x_{1}\\ x_{2} \\ x_{3} \end{bmatrix} 
              =\begin{bmatrix}1.025 \\ 0.95 \\ -0.9875 \end{bmatrix}
            $$
            
            O erro relativo é:

            <div class="equacao">
            $$
            \begin{aligned}
                E_r &= \frac{\left|x^{(2)}-x^{(1)}\right|_L }{\left|x^{(2)}\right|_L}\\
                    & = \frac{\left|
                        \begin{bmatrix}1.025 \\ 0.95 \\ -0.9875 \end{bmatrix}
                        \begin{bmatrix}1 \\ 0.75 \\ -0.875 \end{bmatrix}                        
                    \right|_L }{\left|\begin{bmatrix}1.025 \\ 0.95 \\ -0.9875 \end{bmatrix}\right|_L}\\    
                    &=  0.195.
            \end{aligned}
            $$
            </div>

            Como $0.195 > 0.05$, o precesso iterativo continua.<br><br>


            <strong>Iteração 3:</strong>

                <div class="equacao">
                $$ \begin{cases} x_{1}^{(3)} = -0.2x_{2}^{(2)} - 0.2x_{3}^{(2)} + 1 = -0.2*0.95 - 0.2*(-0.9875) + 1 = 1.0075
                \\ x_{2}^{(3)} = -0.75x_{1}^{(3)} - 0.25x_{3}^{(2)} + 1.5 = -0.75*1.0075 - 0.25*(-0.9875) + 1.5 = 0.99125
                \\ x_{3}^{(3)} = -0.5x_{1}^{(3)} - 0.5x_{2}^{(3)} = -0.5*1.0075 - 0.5*0.99125 = -0.99947
                \end{cases}$$ 
                </div>
                
                Ou seja,,

                 $$
                x^{(3)} =\begin{bmatrix}x_{1}\\ x_{2} \\ x_{3} \end{bmatrix}
                = \begin{bmatrix}1.0075 \\ 0.99125 \\ -0.99947 \end{bmatrix}.
                $$

            Prosseguindo para o cálculo do erro: 

            <div class="equacao">
            $$
            \begin{aligned}
                E_r & = \frac{\left|
                    \begin{bmatrix}1.0075 \\ 0.99125 \\ -0.99947 \end{bmatrix}
                       - \begin{bmatrix}1.025 \\ 0.95 \\ -0.9875 \end{bmatrix}                        
                    \right|_L }{\left|\begin{bmatrix}1.0075 \\ 0.99125 \\ -0.99947 \end{bmatrix}\right|_L}\\    
                    &=  0.041.
            \end{aligned}
            $$
            </div>

            que é menor que $0.05$, satisfazendo a condição imposta pelo enunciado da questão.
            <br><br>           

            Então, a solução $x$ do sistema linear acima, com erro menor que $0.05$, obtida pelo método de Gauss-Seidel, é:
            
            $$
            x = \begin{bmatrix}1.0075 \\ 0.99125 \\ -0.99947 \end{bmatrix}.
            $$


            </p>
            </div>
        </div>
    </div>
</div>

</script>
    <div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/seidel.svg"alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/9GNIMe-S2b8" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/sistemasLinearesGaussSeidel.sce"></script>
          </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método de Gauss-Seidel',
          'todosExercicios' => $todosExercicios[20],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>
</div>

<script>
  renderMathInElement(document.body, {
    delimiters: [
      {left: "$$", right: "$$", display: true},
      {left: "$", right: "$", display: false}
    ]
  });
</script>
