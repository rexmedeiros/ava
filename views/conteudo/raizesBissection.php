    

<?php
use yii\helpers\Html;
use app\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View 
http://conteudo.icmc.usp.br/pessoas/mari/NumPri2012/Posicaofalsa.pdf
http://www.univasf.edu.br/~jorge.cavalcanti/4CN_Parte2.1_Metodos.pdf
http://paginas.fe.up.pt/~faf/mnum/mnum-faf-handout.pdf


*/




$this->title = 'Raizes';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>
<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i>Vídeoaula</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="https://www.youtube.com/embed/p87tHA7HK_U" allowfullscreen></iframe>
        </div>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Métodos Intervalares</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        O método da Bisseção, que estudaremos a seguir, faz parte de uma classe de métodos chamados de Intervalares.
        Para que esse método funcione corretamente, é necessário conhecer um intervalo $[a;b]$ em que a raiz da equação
        $F(x) = 0$ se encontra. Além disso, a função $F(x)$ deve ser contínua no intervalo $[a;b]$.

        <br><br>
        A base para o funcionamento dos métodos intervalares é o Teorema de Bolzano.
        <br>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Teorema de Bolzano</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        Seja $F(x)$ uma função contínua no intervalo $[a;b]$. Se $F(a)F(b) < 0$, então a função $F(x)$ tem pelo menos um
        zero no intervalo aberto $(a;b)$.

        <br><br>

        A figura abaixo ilustra o Teorema de Bolzano. Tome, por exemplo, o intervalo $[1;3]$. Pelo gráfico, é fácil
        verificar que $F(1) < 0$ e $F(3)>0$. Dessa forma, $F(1)F(3) < 0$. Como a função é contínua no intervalo
        considerado, então a equação $F(x)=0$ tem pelo menos uma raiz no intervalo aberto $(1;3)$.


        <br><br>

        Em suma, o Teorema de Bolzado é um método analítico para verificar se uma equação possui raízes em um intervalo
        determinado.
        <p style="text-align: center;">
          <img src="imagens/RaizesBiss.svg" alt="" style="width:70%;"/>
        </p><br>

        Deve-se notar que, quando $F(a)F(b) > 0$, então nada podemos afirmar quanto a existência de zeros da função no
        intervalo $(a;b)$. Se tomarmos o intervalo $[-3,3]$ no exemplo anterior, $F(-3)F(3) > 0$. O Teorema de Bolzano
        não garante a existência de raízes no intervalo, embora graficamente podemos ver que a função possui dois zeros
        no intervalo $(-3;3)$.

      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> &nbsp; Exemplo - Teorema de Bolzano</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        Para a função $\;x^{2}-6x+1$ plotada abaixo, vamos usar o teorema de Bolzano para determinar intervalos
        que contenham os zeros da função:
        <br>

        <div id="applet_container2" align="center"></div>

        <br>

        Verificamos que $F(0) F(1)= -4 < 0 \; $ e $ \; F(5)  F(6) = -4 <
        0$. Logo podemos afirmar que os dois zeros da função estão nos intervalos $(0;1)$ e $(5;6)$.
        <br><br>
      </div>

    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Método da Bissecção</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        Considere que a raiz procurada da equação $f(x) = 0$ esteja no intervalo $(a;b)$, e que a função $f(x)$ seja contínua no intervalo $[a;b]$. O método da bissecção consiste subdividir o intervalo $[a;b]$ em dois subintervalos utilizando o ponto médio, ou seja, calculando
        $$
          x_1 = \frac{a+b}{2}.
        $$
        Os dois subintervalos serão $[a;x_1]$ e $[x_1;b]$. A não ser que a raiz procurada seja exatamente $x_1$ (pouco provável), então, devido à continuidade da função $f(x)$, a raiz procurada encontra-se em um dos dois subtintervalos. Para saber em qual deles a raiz se encontra, usa-se o Teorema de Bolzano, ou seja,
        <p style="padding-left: 5%; ">
          Se $ \;f(a)f(x_1) < 0 $, $\; $ entao a raiz está em $(a;x_1)$.<br>
          Se $\;f(a)  f(x_1) > 0 $, $\; $ então a raiz esta no outro subintervalo, $(x_1;b)$.
        </p>

        O subintervalo em que a raiz se encontra é conservado, enquanto que o outro é descartado.

        <br><br>

        O processo é reiniciado considerando o intervalo conservado; uma nova subdivisão é feita e o novo ponto médio é chamado de $x_2$. O processo termina quando se atinge uma precisão determinada, ou seja,
        $$
          E_r = \left| \frac{x_n - x_{n-1}}{x_n} \right| < \epsilon.
        $$
        Em geral, $\epsilon = 10^{-p}$, em que $p$ é a quantidade de algarismos significativos que a solução deve ter.
        <br>

      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        Considerando a equação

        $\; x^{2}-5 = 0 \;$, encontre a raiz positiva de forma a que o erro percentual máximo
        seja menor ou igual a $5\%$, ou seja, $\epsilon = 0,05$.
        <br>
        <br>

        Para se obter um intervalo inicial que contém a raiz procurada, faz-se o gráfico da função
        $\; f(x) = x^{2}-5.$ <br>
        <br>

        <p style="text-align: center;">
          <img src="imagens/RaizesBiss.svg" alt="" style="width:70%;"/>
        </p><br>

        É possível notar que:

        <div class="equacao">
          $$f(-2)  f(-3) < 0, $$
          $$f( 2)  f(3)< 0. $$
        </div>

        Pelo teste de Bolzano, a raiz positiva encontra-se no intervalo $[2;3]$. Fazendo $a = 2$ e $b = 3$:

        <br>
        <strong>Iteração 1</strong> <br>

        <div class="equacao">
          $$
          \begin{aligned}
          x_{1} &= \frac{a+b}{2}\\
          &= \frac{2+3}{2} = 2.5
          \end{aligned}
          $$
        </div>

        O subtintervalo inicial foi dividido em dois subintervalos, $[2; 2.5]$ e $[2.5; 3]$.
        Fazendo o teste de Bolzano. Como

        $$ f(a)  f(x_{1}) < 0, $$
        a raiz está no subintervalo $[2; 2.5]$.

        <br><br>
        <strong>Iteração 2</strong> <br>

        <div class="equacao">
          $$
          \begin{aligned}
          x_{2} &= \frac{a+b}{2}\\
          &= \frac{2+2.5}{2} = 2.25
          \end{aligned}
          $$
        </div>

        Calculando o erro porcentual:

        <div class="equacao">
          $$
          \begin{aligned}
          \varepsilon_{a} &= {\left|\frac{2.25 - 2.5}{2.25}\right |}  100 \; \% \\
          &= 11.11 \%.
          \end{aligned}
          $$
        </div>

        Fazendo o teste de Bolzano:

        $$ f(a)  f(x_{2}) = -0.625 < 0. $$
        Logo, a raiz está nos subintervalo $[2; 2.25]$.

        <br><br>
        <strong>Iteração 3</strong> <br>

        <div class="equacao">
          $$
          \begin{aligned}
          x_{3} &= \frac{a+b}{2}\\
          &= \frac{2+2.25}{2} = 2.125
          \end{aligned}
          $$
        </div>

        Calculando o erro porcentual:

        <div class="equacao">
          $$
          \begin{aligned}
          \varepsilon_{a} &= {\left|\frac{2.125 - 2.25}{2.125}\right |}  100 \; \% \\
          &= 5.88 \%.
          \end{aligned}
          $$
        </div>

        Fazendo o teste de Bolzano:

        $$ f(a)  f(x_{3}) = -0.4844 < 0. $$
        Logo, a raiz está nos subintervalo $[2; 2.125]$.

        <br><br>
        <strong>Iteração 4</strong> <br>

        <div class="equacao">
          $$
          \begin{aligned}
          x_{4} &= \frac{a+b}{2}\\
          &= \frac{2.125+2.25}{2} = 2.1875
          \end{aligned}
          $$
        </div>

        Calculando o erro porcentual:

        <div class="equacao">
          $$
          \begin{aligned}
          \varepsilon_{a} &= {\left|\frac{2.1875 - 2.125}{2.1875}\right |}  100 \; \% \\
          &= 2.857 \% < 5\%.
          \end{aligned}
          $$
        </div>

        Fazendo o teste de Bolzano:

        $$ f(a)  f(x_{3}) = -0.4844 < 0. $$
        Logo, a raiz está nos subintervalo $[2; 2.125]$.

        <br><br>
        Então, para um erro < $\;5\%,\;$ podemos inferir que a raiz é aproximadamente $\;x = \; 2.1875\;$.


      </div>
    </div>
  </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com o GeoGebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                Use o método da Bisseção para encontrar a raiz positiva da equação abaixo com erro relativo
                $E_r < \;0.01\;$

                $$ \frac{x^2}{5}  -2x=3.$$

                <br>

                <p>Solução:</p>
                <form id="geo">
                    <input value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                    <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">
                </form>
                <div id="applet_container" align="center"></div>

                <br>
                Deve-se inicialmente reescrever a equação na forma
                $$
                    f(x) = 0.
                $$
                Escolhemos $f(x) = \frac{x^2}{5}  -2x -3 $.
                <ol id="demo" style=" padding-left: 17px">

                    <li>
                        Fazendo-se uma análise gráfica, estima-se a raiz no subintervalo $[a;b]$,
                        $a = 10\,$ e $b = 12.$

                    </li>
                    <br>
                </ol>


            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <p>
                <p style="text-align: center;"">
                <img src="imagensDev/Massao/bissecao.svg" alt="" style="width:70%;"/>
                </p>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/qKvNGpuhK7c" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo de Implementação Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/raizesBisseccao.sce"></script>
          </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método da Bisseção',
          'todosExercicios' => $todosExercicios[12],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>
</div>

<script type="text/javascript">
    function diminuirFonte() {
        document.getElementById("conteudo").style.fontSize = "1em";
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
        "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
        "errorDialogsActive":true,"useBrowserForJS":true,
        "filename":"<?= Url::base() ?>/geogebra/RAIZESBisettGeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');





    function mensagens(n){
        // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 4;
        ggbApplet.setValue('n',this.n)

        x = ggbApplet.getValue('X(1,0)').toFixed(decimais);


        a = ggbApplet.getValue('A(1,1)').toFixed(decimais);
        b = ggbApplet.getValue('B(1,1)').toFixed(decimais);


        switch (n)
        {
            case 1:
                saida = "Iteração 1: \
               \
               \$\$\
               \\begin{aligned} \
                                     x &= \\frac{a+b }{2} \\\\  \
                      &= \\frac{"+a+"+"+b+  "}{2}  \\\\ \
                    &= "+x+"  \
                    \\end{aligned}\
                \$\$";
                break;

            case 2:saida = "Como temos apenas um valor de x, não é possível calcular o erro relativo. \
                    Fazendo o teste de Bolzano:\
                    \$\$\
                    f("+a+") \\cdot f("+x+") > 0.\
                    \$\$ \
                    Logo, redefine-se \$ a = x\$ e o novo intervalo será \$["+x+";"+b+"] \$.";

                break;
            case 3:
                saida = "Iteração 2: \
                    \$\$ \
                       x = \\frac{"+a+"+"+b+  "}{2} = "+x+" \
                    \$\$";
                break;
            case 4:
                saida = "Verifica-se o erro relativo: \
                    \$\$\
                    \\begin{aligned} \
                           \\varepsilon_{r}  &=  \\frac{|x - x_a|}{x} \\\\ \
                                &=  \\frac{|"+ x+" - 11.0000|}{"+x+"} \\\\  \
                                &= 0.0435 > 0.01. \
                    \\end{aligned} \
                    \$\$\
                    Fazendo o teste de Bolzano:\
                    \$\$\
                           f("+a+") \\cdot f("+x+")<0\
                    \$\$ \
                    Logo, redefine-se \$ b = x\$ e o novo intervalo será \$["+a+";"+x+"] \$.";
                break;
            case 5:
                saida = "Iteração 3: \
                    \$\$ \
                       x = \\frac{"+a+"+"+b+  "}{2} = "+x+" \
                    \$\$";
                break;
            case 6:
                saida = "Verifica-se o erro relativo: \
                    \$\$\
                    \\begin{aligned} \
                           \\varepsilon_{r}  &=  \\frac{|x - x_a|}{x} \\\\ \
                                &=  \\frac{|"+ x+" - 11.5000|}{"+x+"} \\\\  \
                                &= 0.0222 > 0.01. \
                    \\end{aligned} \
                    \$\$\
                    Fazendo o teste de Bolzano:\
                    \$\$\
                           f("+a+") \\cdot f("+x+")>0\
                    \$\$ \
                    Logo, redefine-se \$ a = x\$ e o novo intervalo será \$["+x+";"+b+"] \$.";
                break;
            case 7:
                saida = "Iteração 4: \
                    \$\$ \
                       x = \\frac{"+a+"+"+b+  "}{2} = "+x+" \
                    \$\$";
                break;
            case 8:
                saida = "Verifica-se o erro relativo: \
                    \$\$\
                    \\begin{aligned} \
                           \\varepsilon_{r}  &=  \\frac{|x - x_a|}{x} \\\\ \
                                &=  \\frac{|"+ x+" - 11.2500|}{"+x+"} \\\\  \
                                &= 0.011 > 0.01. \
                    \\end{aligned} \
                    \$\$\
                    Fazendo o teste de Bolzano:\
                    \$\$\
                           f("+a+") \\cdot f("+x+")<0\
                    \$\$ \
                    Logo, redefine-se \$ b = x\$ e o novo intervalo será \$["+a+";"+x+"] \$.";

                break;
            case 9:
                saida = "Iteração 5: \
                    \$\$ \
                       x = \\frac{"+a+"+"+b+  "}{2} = "+x+" \
                    \$\$";
                break;
            case 10:
                saida = "Verifica-se o erro relativo: \
                    \$\$\
                    \\begin{aligned} \
                           \\varepsilon_{r}  &=  \\frac{|x - x_a|}{x} \\\\ \
                                &=  \\frac{|"+ x+" - 11.3750|}{"+x+"} \\\\  \
                                &= 0.0055 < 0.01. \
                    \\end{aligned} \
                    \$\$\
                    Logo a raíz aproximada é:\
                    \$\$\
                           x = "+x+"\
                    \$\$ \
                    com dois algarismos significativos.";
                break;

            //alert('Default case');
        }


        return saida;
    }

    function anterior (){
        if (this.n%2==0){
            ggbApplet.evalCommand("  ZoomIn[1/2.2,(11.3204,0)]")
        }
        if (this.n >= 1){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);

            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }


    }

    function proximo (){
        if (this.n <= 9){

            this.n++;
            ggbApplet.setValue('n', this.n)

            if (this.n%2==0){
                ggbApplet.evalCommand("  ZoomIn[2.2,(11.3204,0)]")
            }


            texto = mensagens(this.n);
            acrescentarItem(this.n,texto)

            renderizarKatex()

            return true;
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>


<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>


<script type="text/javascript">
function renderizarKatex(){
    renderMathInElement(document.body,{delimiters: [
        {left: "$$", right: "$$", display: true},
        {left: "$", right: "$", display: false}
    ]});
}
</script>


<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em";
    }

</script>

<script type="text/javascript">

    var parameters2 = {
        "id": "ggbApplet2",
        "prerelease": false,
        "width": 600,
        "height": 400,
        "showToolBar": false,
        "borderColor": null,
        "showMenuBar": false,
        "showAlgebraInput": false,
        "showResetIcon": true,
        "enableLabelDrags": false,
        "enableShiftDragZoom": true,
        "enableRightClick": false,
        "capturingThreshold": null,
        "showToolBarHelp": false,
        "errorDialogsActive": true,
        "useBrowserForJS": true,
        "filename": "<?= Url::base() ?>/geogebra/RAIZESInitGeoGebraSite.ggb"
    };
    var applet2 = new GGBApplet(parameters2, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-4,20,-13 ,15);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
        if (param == "ggbApplet2") {
            ggbApplet2.setCoordSystem(-3, 7, -8.5, 6);
            ggbApplet2.setValue('bissec', true); // Valor de h
            ggbApplet2.setValue('falseposic', false)  // Mostra Euler;
            ggbApplet2.setValue('newton', false)  // Mostra Heun;
            ggbApplet2.setValue('secante', false)  // Mostra Ponto Medio;
        }
    }

    window.onload = function () {
        applet2.inject('applet_container2');
        applet.inject('applet_container');
    }



</script>