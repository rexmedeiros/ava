<?php

use yii\helpers\Html;
use frontend\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'MMQ - Polinomial';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>


<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Introdução</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">
          Sistemas físicos podem não apresentar uma relação linear entre entrada e saída. Dessa forma, nem sempre iremos utilizar a regressão linear para obter a "melhor" função que se encaixa em um conjunto de dados. <br><br>
          Existem muitos casos em que o modelo obedece a um comportamento polinomial. Sendo assim, é necessário adaptar o ajuste para uma função polinomial de grau superior. <br><br> Para compreendermos melhor isso, podemos observar a Figura $1$, que mostra uma regressão linear aplicada à um conjunto de dados em comparação a uma regressão polinomial aplicada ao mesmo conjunto de dados.

          <br><br>
          <p style="text-align: center; ">
            <img src="imagens/mmqpol_1.svg" style="width:70%;"> <br>(Figura 1)
          </p>
          <br>

          Para entender a metemática por trás disso, veremos abaixo como é feita a regressão polinomial, que é uma generalização da regressão linear.
        </p>


      </div>
    </div>
  </div>
</div>
<br><br>
<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Ajustes</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">

          O procedimento dos mínimos quadrados pode ser prontamente estendido para ajustar dados por um polinômio de grau mais alto. Por exemplo, suponha que se queira ajustar um polinômio de segundo grau

          <div class="equacao">
            $$y = a_0 + a_1x + a_2x^2 + e.$$
          </div>

          Nesse caso, a soma dos quadrados dos resíduos é:

          <div class="equacao">
            $$ S_{r} = \sum_{i=1}^{n} \left(y_i - a_0 - a_1x_1 - a_2x_2^2\right)^2. $$
          </div>

          Agora vamos tomar a derivada com relação a cada um dos coeficientes desconhecidos do polinômio. Assim:

          <div class="equacao">
            $$
            \frac{\partial S_r}{\partial a_0} = -2\sum \left(y_i - a_0 - a_1x_i - a_2x_i^2\right),
            $$
          </div>
          <div class="equacao">
            $$
            \frac{\partial S_r}{\partial a_1} = -2\sum x_i\left(y_i - a_0 - a_1x_i - a_2x_i^2\right),
            $$
          </div>
          <div class="equacao">
            $$
            \frac{\partial S_r}{\partial a_2} = -2\sum x_i^2\left(y_i - a_0 - a_1x_i - a_2x_i^2\right).
            $$
          </div>
          <br>

          Para minimizar os resíduos, igualamos essas equações a zero e reorganizamos para determinar o seguinte conjunto de equações normais:

          <div class="equacao">
            $$
            (n)a_0 + \left(\sum x_i\right)a_1 + \left(\sum x_i^2\right)a_2 = \sum y_i,
            $$
          </div>
          <div class="equacao">
            $$
            \left(\sum x_i\right)a_0 + \left(\sum x_i^2\right)a_1 + \left(\sum x_i^3\right)a_2 = \sum x_iy_i,
            $$
          </div>
          <div class="equacao">
            $$
            \left(\sum x_i^2\right)a_0 + \left(\sum x_i^3\right)a_1 + \left(\sum x_i^4\right)a_2 = \sum x_{i}^2y_{i},
            $$
          </div>

          onde todos os somatórios vão de $i = 1$ até $n$. Podemos observar que as três equações são lineares e têm três incógnitas $a_0$, $a_1$ e $a_2$. Os coeficientes das incógnitas podem ser calculados diretamente dos dados observados.<br><br>

          Podemos expressar o resultado obtido na forma de produto matricial de maneira a obter um resultado semelhante ao o que obtivemos na regressão linear:

          $$
          X^TXa = X^TY.
          $$

          Dessa vez, a matriz $X$ é uma matriz de Vandermonde de grau $2$ com $n$ linhas e $3$ colunas:

          $$
          \begin{bmatrix}1 & x & x^2
          \\ 1 & x & x^2
          \\ & \vdots
          \\ 1 & x & x^2
          \end{bmatrix}.
          $$

          Podemos expressar também esse resultado como:

          <div class="equacao">
            $$ \begin{bmatrix} n & \sum x & \sum x^2 \\ \sum x & \sum x^2 & \sum x^3 \\ \sum x^2 & \sum x^3 & \sum x^4 \end{bmatrix} \begin{bmatrix}a_0 \\a_1 \\ a_2 \end{bmatrix} = \begin{bmatrix}\sum y \\ \sum xy \\ \sum x^2y \end{bmatrix}.
            $$
          </div>

          A regressão quadrática pode ser facilmente estendida para um polinômio de grau $m$ como

          <div class="equacao">
            $$y = a_0 + a_1x + a_2x^2 + ... + a_mx^m + e.$$
          </div>

          Pode-se perceber que determinar os coeficientes de um polinômio de grau $m$ é equivalente a resolver um sistema de $m + 1$ equações lineares simultâneas. Nesse caso, os coeficientes do ajuste polinomial de grau $m$ serão dados pela relação:

          $$
          X^TXa = X^TY,
          $$

          onde a matriz X é uma matriz de Vandermonde de grau $m$ que contém $n$ linhas e $m+1$ colunas:
          <div class="equacao">
            $$
            \begin{bmatrix}1 & x & x^2 & \dots & x^{m-1} & x^m
            \\ 1 & x & x^2 & \dots & x^{m-1} & x^m
            \\ & & & \vdots
            \\ 1 & x & x^2 & \dots & x^{m-1} & x^m
            \end{bmatrix}.
            $$
          </div>
        </p>
      </div>
    </div>
  </div>
</div>

<br>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">
          Ajuste um polinômio do segundo grau ao conjunto de pontos abaixo.<br>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>${x_i}$</th>
                <th>${y_i}$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>${0}$</th>
                <td>${2.1}$</td>
              </tr>
              <tr>
                <th>${1}$</th>
                <td>${7.7}$</td>
              </tr>
              <tr>
                <th>${2}$</th>
                <td>${13.6}$</td>
              </tr>
              <tr>
                <th>${3}$</th>
                <td>${27.2}$</td>
              </tr>
              <tr>
                <th>${4}$</th>
                <td>${40.9}$</td>
              </tr>
              <tr>
                <th>${5}$</th>
                <td>${61.1}$</td>

              </tr>

            </tbody>
          </table>

          <br>
          <b>Solução: </b>
          <br>

          Queremos achar o polinônio de segundo grau que melhor se ajusta ao conjunto de pontos. Defina

          $$
          P_2(x) = a_0 + a_1x + a_2x^2.
          $$

          Para encontrar os coeficientes $[a_0,a_1,a_2]$, devemos resolver o seguinte sistema: 

          <div class="equacao">
          $$ 
          \begin{bmatrix} n & \sum x & \sum x^2 \\ \sum x & \sum x^2 & \sum x^3 \\ \sum x^2 & \sum x^3 & \sum x^4 \end{bmatrix} \begin{bmatrix}a_0 \\a_1 \\ a_2 \end{bmatrix} = \begin{bmatrix}\sum y \\ \sum xy \\ \sum x^2y \end{bmatrix}.
          $$
          </div>

          Calculando os somatórios: 

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>$n = 6$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th> $ \sum x = 0 + 1 + 2 + 3 + 4 + 5 = 15 $</th>
              </tr>
              <tr>
                <th>$ \sum x^2 = 0^2 + 1^2 + 2^2 + 3^2 + 4^2 + 5^2 = 55 $</th>
              </tr>
              <tr>
                <th>$ \sum x^3 = 0^3 + 1^3 + 2^3 + 3^3 + 4^3 + 5^3 = 225 $</th>
              </tr>
              <tr>
                <th>$ \sum x^4 = 0^4 + 1^4 + 2^4 + 3^4 + 4^4 + 5^4 = 979 $</th>
              </tr>
              <tr>
                <th>$ \sum y = 2.1 + 7.7 + 13.6 + 27.2 + 40.9 + 61.1 = 152.6 $ </th>
              </tr>
              <tr>
                <th>$ \sum xy = 0*2.1 + 1*7.7 + 2*13.6 + 3*27.2 + 4*40.9 + 5*61.1 = 585.6 $ </th>
              </tr>
              <tr>
                <th>$ \sum x^2y = 0^2*2.1 + 1^2*7.7 + 2^2*13.6 + 3^2*27.2 + 4^2*40.9 + 5^2*61.1 = 2488.8 $ </th>
              </tr>
            </tbody>
          </table>


          Portanto, as equações lineares simultâneas são

          <div class="equacao">
          $$
          \begin{bmatrix}6 & 15 & 55 \\ 15 & 55 & 225 \\ 55 & 225 & 979 \end{bmatrix}
          \begin{bmatrix}a_0 \\ a_1 \\ a_2 \end{bmatrix}=
          \begin{bmatrix}152.6 \\ 585.6 \\ 2488.8 \end{bmatrix}.
          $$
          </div>
          
          Se escrevermos a matriz aumentada, podemos resolver esse sistema pela técnica de eliminação de Gauss (se você não lembra dela, pode consultar a seção <b>Sistemas Lineares</b> e procurar por <b>Método de Gauss</b>).

          <div class="equacao">
          $$
          \begin{bmatrix} 6 & 15 & 55 &| \space 152.6\\ 15 & 55 & 225 &| \space 585.6\\ 55 & 225 & 979 &| 2488.8 \end{bmatrix}.
          $$
          </div>

          Ao fazermos o escalonamento, encontramos os seguintes coeficientes para compor o nosso polinômio: 
          
          $$        
          a_0 = 2.47857,\\
          a_1 = 2.35929,\\
          a_2 = 1.86071.
          $$

          Portanto, a equação quadrática por mínimos quadrados é: 

          <div class="equacao">
            $$y = 2.47857 + 2.35929x + 1.86071x^2.$$
          </div>
          
          A Figura 2 ilusta o resultado obtido.
          
          <p style="text-align:center;">
            <img src="imagens/mmqpolex_1.svg" style="width:75%;"> <br>(Figura 2)<br><br>
          </p>

          Através dessa análise gráfica, podemos ver que a função que encontramos representa bem os pontos da amostra. <br><br>

          Para calcular o erro quadrático, primeiro calculamos o vetor erro $\vec e$ dado pela diferença dos $y$ da amostra e os $y$ aproximados: 
          
          <div class="equacao">
          $$
          \vec e = \begin{bmatrix} 2.1 \\ 7.7 \\ 13.6 \\ 27.2 \\ 40.9 \\ 61.6 \end{bmatrix} - \begin{bmatrix} 2.47857 + 2.35929*0 + 1.86071*0^2 \\ 2.47857 + 2.35929*1 + 1.86071*1^2 \\ 2.47857 + 2.35929*2 + 1.86071*2^2 \\ 2.47857 + 2.35929*3 + 1.86071*3^2 \\ 2.47857 + 2.35929*4 + 1.86071*4^2 \\ 2.47857 + 2.35929*5 + 1.86071*5^2 \end{bmatrix} = \begin{bmatrix} -0.37857 \\ 1.00143 \\ -1.04000 \\ 0.89717 \\ -0.78709 \\ 0.30723 \end{bmatrix}.
          $$ 
          </div>

          O erro quadrático é dado por: 

          $$
          |\vec e|^2 = e^Te = 3.74657.
          $$

        </p>

      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="conteudo">
        <p>Considerando a tabela abaixo, faça uma regressão polinomial para encontrar um polinômio de grau 2 que melhor se ajusta aos pontos:</p>
        <br>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>${y}$</th>
              <th>${x}$</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>${1.6}$</td>
              <td>${2.9}$</td>
            </tr>
            <tr>
              <td>${5.17}$</td>
              <td>${5.37}$</td>
            </tr>

            <tr>
              <td>${6}$</td>
              <td>${8}$</td>
            </tr>

            <tr>
              <td>${13}$</td>
              <td>${2}$</td>
            </tr>

          </tbody>
        </table>


        <p>Solução:</p>
        <form id="geo">
          <input value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

          <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">
        </form>
        <div id="applet_container" align="center"></div>

        <br>

      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="https://www.youtube.com/embed/YpCYOt7FCLQ" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/mmqPolinomial.sce"></script>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function diminuirFonte() {
    document.getElementById("conteudo").style.fontSize = "1em";
  }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
  var n = 0;
  var list = document.getElementById('demo');
  var parameters = {
    "id": "ggbApplet",
    "prerelease": false,
    "width": 800,
    "height": 600,
    "showToolBar": false,
    "borderColor": null,
    "showMenuBar": false,
    "showAlgebraInput": false,
    "showResetIcon": true,
    "enableLabelDrags": false,
    "enableShiftDragZoom": false,
    "enableRightClick": false,
    "capturingThreshold": null,
    "showToolBarHelp": false,
    "errorDialogsActive": true,
    "useBrowserForJS": true,
    "filename": "<?= Url::base() ?>/geogebra/MMQ_RPolinomial.ggb"
  };
  var views = {
    'is3D': 0,
    'AV': 1,
    'SV': 0,
    'CV': 0,
    'EV2': 0,
    'CP': 0,
    'PC': 0,
    'DA': 0,
    'FI': 0,
    'PV': 0,
    'macro': 0
  };
  var applet = new GGBApplet(parameters, '5.0');
  //when used with Math Apps Bundle, uncomment this:
  //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

  window.onload = function() {
    applet.inject('applet_container');
  }


  function ggbOnInit(param) {
    if (param == "ggbApplet") {
      ggbApplet.setCoordSystem(-1, 15, -10, 10);
      ggbApplet.setValue('bissec', true); // Valor de h
      ggbApplet.setValue('falseposic', false) // Mostra Euler;
      ggbApplet.setValue('newton', false) // Mostra Heun;
      ggbApplet.setValue('secante', false) // Mostra Ponto Medio;
    }
  }

  function mensagens(n) {
    // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
    decimais = 2;
    ggbApplet.setValue('n', this.n)
    if (this.n % 2 == 0) {
      x = ggbApplet.getValue('X(1,0)');
    }
  }

  function anterior() {
    if (n >= 0) {
      ggbApplet.setCoordSystem(-1, 15, -10, 10);
    } else {
      ggbApplet.setCoordSystem(-1, 15, -10, 10);
    }
    if (this.n % 2 == 0) {}
    if (this.n >= 0) {
      this.n--;
      ggbApplet.setValue('n', this.n);
      return true;
    } else {
      alert("Vá para a próxima etapa.");
      return false;
    }

  }

  function proximo() {
    if (n <= 4) {
      ggbApplet.setCoordSystem(-1, 15, -10, 10);
    } else {
      ggbApplet.setCoordSystem(-1, 15, -10, 10);
    }

    if (this.n <= 4) {
      this.n++;
      ggbApplet.setValue('n', this.n)
      return true;
    } else {
      alert("Fim do exercício.");
      return false;
    }
  }
</script>

<script>
  renderMathInElement(document.body, {
    delimiters: [{
        left: "$$",
        right: "$$",
        display: true
      },
      {
        left: "$",
        right: "$",
        display: false
      }
    ]
  });
</script>