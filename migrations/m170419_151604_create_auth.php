<?php

use yii\db\Migration;

class m170419_151604_create_auth extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
          // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
          $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }
      
      $this->createTable('auth', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer()->notNull(),
          'source' => $this->string()->notNull(),
          'source_id' => $this->biginteger()->defaultValue(0),
      ]);

      $this->addForeignKey('fk-auth-user_id-user-id', 'auth', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('auth');
    }
}
