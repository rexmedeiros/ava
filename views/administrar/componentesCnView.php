<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
/* @var $this yii\web\View */
/* @var $model frontend\models\ComponentesCn */

$this->title = $model->codigo.' - '.$model->nome;
$this->params['breadcrumbs'][] = ['label' => 'Componentes', 'url' => ['componentes-cn-index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="componentes-cn-view">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['componentes-cn-update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Excluir', ['componentes-cn-delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza que deseja apagar este item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'user_id',
            'codigo',
            'nome',
            //'created_at',
            //'updated_at',
        ],
        
    ]) ?>

</div>
