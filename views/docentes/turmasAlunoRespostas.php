<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
use  peterziv\echarts\Pie;
use  peterziv\echarts\Echarts;

/* @var $this yii\web\View */
/* @var $model frontend\models\TurmasDocentes */

$this->title = 'Estatísticas de '.$dadosAluno->user->shortName;
$this->params['breadcrumbs'][] = ['label' => 'Turmas', 'url' => ['turmas-alunos-view', 'idTurma' => $idTurma]];
$this->params['breadcrumbs'][] = ['label' => $dadosTurma->codigo.' - '.$dadosTurma->turma.$dadosTurma->subturma, 'url' => ['turmas-alunos-view', 'idTurma' => $idTurma]];
;
$this->params['breadcrumbs'][] = $dadosAluno->user->shortName;
?>
<?php
foreach ($dataProvider as $k => $dados) {
  $theOptionsAll[$k] = '{
      "tooltip" : {
          "trigger": "item"
      },
      "series" : [
          {
              "name": "Exercício sobre: '.$dados['descricao'].'",
              "type": "pie",
              "radius" : "80%",
              "center": ["50%", "50%"],
              "data":[
                  {"value":'.$dados[0].', "name":"Erros"},
                  {"value":'.$dados[1].', "name":"Acertos"}
              ],
              "itemStyle": {
                  "emphasis": {
                      "shadowBlur": "10",
                      "shadowOffsetX": "0",
                      "shadowColor": "rgba(0, 0, 0, 0.5)"
                  }
              }
          }
      ]
  }
  ';
}
?>

<div class="turmas-docentes-view">

    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>

    <h1><?= Html::encode($this->title) ?></h1>

<div class="clearfix"></div>

<div class="row">

<?php foreach ($theOptionsAll as $k => $theOptions) : ?>
  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?= $dataProvider[$k]['descricao'] ?></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
      <?= Echarts::widget([
              'options'=>$theOptions,
              'htmlOptions' => ['style' => 'height: 250px;'],
            ]);
       ?>
      </div>
    </div>
  </div>
<?php  endforeach; ?>

</div>
<div class="clearfix"></div>




</div>
