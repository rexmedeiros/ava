

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\ExercicioWidget;
/* @var $this yii\web\View 
https://pt.slideshare.net/renangpsoares/aula-07-renan-cn
http://www1.univap.br/spilling/CN/CN_Capt2.pdf
http://paginas.fe.up.pt/~faf/mnum/mnum-faf-handout.pdf
http://slideplayer.com.br/slide/332798/

*/




$this->title = 'Raizes';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css"
      integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js"
        integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js"
        integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH"
        crossorigin="anonymous"></script>

<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i>Vídeoaula</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="https://www.youtube.com/embed/pouK7yX1GKo" allowfullscreen></iframe>
        </div>

      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Newton - Introdução</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">


        A partir de agora estudaremos dois métodos que são classificados como "Abertos": o método de Newton-Rapson e o método da Secante. Diferentemente dos métodos intervalores, nos métodos abertos não é preciso trabalhar com um intervalo no qual a raiz se encontra. Como veremos, no método de Newton precisamos somente de uma estimativa inicial (chute inicial) próxima onde da raiz procurada. O valor desse chute inicial pode ser obtido pelo conhecimento que se tem a respeito da solução do problema ou por uma análise gráfica.

        <br><br>


      </div>
    </div>
  </div>
</div>

<!--
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Critérios de convergência</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


                Para que o método de Newton convirja alguns critérios precisam ser satisfeitos:<br><br>


                <p style=" padding-left: 5%">
                    1. Encontrar uma estimativa $\;x0\;$ próximo suficiente da raiz
                    <br></p>
                <p style=" padding-left: 5%">
                    2. Não ter pontos de máximos ou mínimos próximos a raiz , $\;F'(x) = 0$.
                    <br></p>
                <p style=" padding-left: 5%">
                    3. Não ter ponto de inflexão na raiz,$\;F''(x) = 0$.
                    <br></p>
                <br>


                A imagem abaixo mostra uma região de inflexão na raiz.

                <br><br>
                <p style=" text-align: center;">
                    <img src="imagens/newton1.png">
                </p>

                <br>

                Pode-se ver que a reta tangente em $F(x_{1}) $ retorna $x_{0} $ pelo método, e assim nunca há
                convergência.
                <br><br>
            </div>
        </div>
    </div>
</div>
-->

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Newton</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Considere o problema de encontrar uma raiz da equação
        $$
        f(x) = 0.
        $$
        Seja $\;x_n\;$ uma estimativa relativamente próxima da raiz procurada (o algoritmo começa com um chute
        inicial, $x_0$). O próximo valor estimado
        para a raiz, $x_{n+1}$, é determinado da seguinte forma: traça-se uma reta tangente à função
        $f(x)$ em ($x_n, f(x_n)).$ Onde essa reta (em azul) cortar o eixo das abscissas é o valor de $x_{n+1}$, conforme
        figura abaixo:

        <p style="text-align: center;">
          <img src="imagens/raizesnew2.svg" style="width:60%;"/>
        </p>

        <br>
        O coeficiente angular da reta tangente à função $f(x)$ em $x = x_n$ é:

        $$
        f'(x_{n}) = \frac{f(x_{n})\; -\; 0}{x_{n}\;- \; x_{n+1}}.
        $$

        Logo, o ponto de intersecção será

        $$
        x_{n+1} = x_{n} - \frac{f(x_{n})}{f'(x_{n})}.
        $$
        <br>
        O procedimento é repetido até que se atinja a precisão desejada de $p$ algarismos signivicativos, ou seja,
        até que o erro relativo seja
        $$
        \varepsilon_{r} = \left|\frac{x_{n+1}-x_n}{x_{n+1}}\right| < 10^{-p}.
        $$

        <br><br>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="conteudo">
        <p>Encontre a raiz positiva da equação $f(x) = 0$ com precisão de dois algarismos significativos,
          onde
          $$
          f(x) = \frac{\; \,x^2}{5} -2x - 3
          $$
        </p>

        <br>

        <p>Solução:</p>
        <form id="geo">
          <input value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

          <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">
        </form>
        <div id="applet_container" align="center"></div>

        <br>
        <ol id="demo" style="padding-left: 17px">
          <li>
            Fazendo-se uma Análise Gráfica, estima-se um $x_0 = 11.4,$ suficientemente próximo.
          </li>
        </ol>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p>
        <p style="text-align: center;"">
        <img src="imagensDev/Massao/raizes_newton.svg" alt="" style="width:70%;"/>
        </p>
        </p>
      </div>
    </div>
  </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/K82wOCyccZA" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/raizesNewton.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método de Newton',
          'todosExercicios' => $todosExercicios[14],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>
</div>


<script type="text/javascript">
    function diminuirFonte() {
        document.getElementById("conteudo").style.fontSize = "1em";
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
    var n = 1;
    var list = document.getElementById('demo');
    var parameters = {
        "id": "ggbApplet",
        "prerelease": false,
        "width": 600,
        "height": 400,
        "showToolBar": false,
        "borderColor": null,
        "showMenuBar": false,
        "showAlgebraInput": false,
        "showResetIcon": true,
        "enableLabelDrags": false,
        "enableShiftDragZoom": true,
        "enableRightClick": false,
        "capturingThreshold": null,
        "showToolBarHelp": false,
        "errorDialogsActive": true,
        "useBrowserForJS": true,
        "filename": "<?= Url::base() ?>/geogebra/RAIZESBisettGeoGebraSite.ggb"
    };
    var views = {
        'is3D': 0,
        'AV': 1,
        'SV': 0,
        'CV': 0,
        'EV2': 0,
        'CP': 0,
        'PC': 0,
        'DA': 0,
        'FI': 0,
        'PV': 0,
        'macro': 0
    };
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function () {
        applet.inject('applet_container');
    }


    function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-4, 20, -13, 15);
            ggbApplet.setValue('bissec', false); // Valor de h
            ggbApplet.setValue('falseposic', false)  // Mostra Euler;
            ggbApplet.setValue('newton', true)  // Mostra Heun;
            ggbApplet.setValue('secante', false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n) {
        // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 4;
        ggbApplet.setValue('n', this.n)

        x = ggbApplet.getValue('X(1,0)').toFixed(decimais);


        a = ggbApplet.getValue('A(1,1)').toFixed(decimais);
        b = ggbApplet.getValue('B(1,1)').toFixed(decimais);
        y = ggbApplet.getValue('h(X(1,0))').toFixed(decimais);
        z = ggbApplet.getValue('f_1(X(1,0))').toFixed(decimais);
        x1 = ggbApplet.getValue('N_2(1,0)').toFixed(decimais);
        x2 = ggbApplet.getValue('N_3(1,0)').toFixed(decimais);
        switch (n) {
            case 1:
                saida = "Iteração 1: \
               \
               \\begin{aligned} \
                                     x = \\frac{a+b }{2}  \
\
       \\end{aligned} \
                    \
                  \\begin{aligned} \
                    \\newline \
                                     x = \\frac{" + a + "+" + b + "}{2}  = " + x + " \
                                     \\newline \
                                     \\newline \
                    \\end{aligned}";
                break;

                break;
            case 2:
                saida = " Iteração 1: \
                    \$\$\
                    \\begin{aligned} \
                           x_1 &= x_0 - \\frac{f(x_0)}{f'(x_0)} \\\\ \
                             &= " + x + " - \\frac{f(" + x + ")}{f'(" + x + ")} \\\\\
                             &= " + x + " - \\frac{" + z + "}{" + y + "} \\\\ \
                             &= " + x1 + ". \
                    \\end{aligned} \
                    \$\$";
                break;
            case 3:
                saida = "Cálculo do erro relativo: \
                    \$\$\
                    \\begin{aligned} \
                           \\varepsilon_{r}  &=  \\left|\\frac{x_1 - x_0}{x_1}\\right| \\\\ \
                                 &=  \\left|\\frac{" + x + " - - 11.4000}{" + x + "}\\right| \\\\ \ \
                                 &= 0.0066 > 0.001. \
                    \\end{aligned} \
                    \$\$\
                \ Logo devemos fazer outra iteração. \ ";
                break;
            case 4:
                saida = " Iteração 2: \
                    \$\$\
                    \\begin{aligned} \
                           x_2 &= x_1 - \\frac{f(x_1)}{f'(x_1)} \\\\ \
                             &= " + x + " - \\frac{f(" + x + ")}{f'(" + x + ")} \\\\\
                             &= " + x + " - \\frac{" + z + "}{" + y + "} \\\\ \
                             &= " + x2 + ". \
                    \\end{aligned} \
                    \$\$";

                break;
            case 5:
                saida = "Cálculo do erro relativo: \
                    \$\$\
                    \\begin{aligned} \
                           \\varepsilon_{r}  &=  \\left|\\frac{x_2 - x_1}{x_2}\\right| \\\\ \
                                 &=  \\left|\\frac{" + x + " - - 11.3250}{" + x + "}\\right| \\\\ \ \
                                 &= 0.000035 < 0.001. \
                    \\end{aligned} \
                    \$\$\
                    Logo o critério de parada foi atendido e o valor da raiz será $x = " + x2 + " $ \
                    com três algarismos significativos.";

                break;
            case 6:
                saida = "";
                break;
            case 7:
                saida = "Iteração 4: \
               \
               \
                    \\begin{aligned} \
                                     x = \\frac{" + a + "+" + b + "}{2} = " + x + " \
                    \\end{aligned}";
                break;
            case 8:
                saida = "Verifica o erro relativo: \
                           \\begin{aligned} \
                           \\newline \
                           \\varepsilon_{r}  =  \\frac{|" + x + " - 11.2500|}{" + x + "} \
                           \\newline \
                           \\newline \
                           \\end{aligned} \
                         \
                          \\begin{aligned} \
                           \\varepsilon_{r} \ = 0.011 > 0.01 \
                           \\newline \
                           \\newline \
                                  \\end{aligned} \
                            \\begin{aligned} \
                           F(" + a + ") \\cdot F(" + x + ")<0\
                           \\newline \
                           \\newline \
                            \\end{aligned} \
                                \\begin{aligned} \
                           b = x \
                    \\end{aligned} ";
                break;
            case 9:
                saida = "Iteração 5: \
               \
               \
                    \\begin{aligned} \
                                     x = \\frac{" + a + "+" + b + "}{2} = " + x + " \
                    \\end{aligned}";
                break;
            case 10:
                saida = "Verifica o erro relativo: \
                           \\begin{aligned} \
                           \\newline \
                           \\varepsilon_{r}  =  \\frac{|" + x + " - 11.3750|}{" + x + "} \
                           \\newline \
                           \\newline \
                           \\end{aligned} \
                         \
                          \\begin{aligned} \
                           \\varepsilon_{r} \ = 0.0055 < 0.01 \
                           \\newline \
                           \\newline \
                         \
                                  \\end{aligned} \
                                  \ Então a raiz é: \
                                  \\begin{aligned} \
                                  \ x = " + x + " \
                                  \\end{aligned}";
                break;
                break;


            //alert('Default case');
        }


        return saida;
    }

    function anterior() {
        if (this.n % 2 == 0) {
            ggbApplet.evalCommand("  ZoomIn[1/49,(11.32455,0)]")
        }
        if (this.n >= 1) {
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);

            removerItem(this.n + 1);
            return true;
        }
        else {
            alert("Vá para a próxima etapa.");
            return false;
        }


    }

    function proximo() {
        if (this.n <= 4) {

            this.n++;
            ggbApplet.setValue('n', this.n)

            if (this.n % 2 == 0) {
                ggbApplet.evalCommand("  ZoomIn[49,(11.32455,0)]")
            }


            texto = mensagens(this.n);
            acrescentarItem(this.n, texto)

            renderizarKatex()
            return true;
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId, texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id', 'item' + this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid) {
        var item = document.getElementById('item' + itemid);
        list.removeChild(item);
    }

</script>


<script>
    renderMathInElement(document.body, {
        delimiters: [
            {left: "$$", right: "$$", display: true},
            {left: "$", right: "$", display: false}
        ]
    });
</script>


<script type="text/javascript">
    function renderizarKatex() {
        renderMathInElement(document.body, {
            delimiters: [
                {left: "$$", right: "$$", display: true},
                {left: "$", right: "$", display: false}
            ]
        });
    }
</script>