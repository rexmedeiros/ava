<?php

use yii\db\Migration;

class m170419_151951_add_name_column_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'name', $this->string(100));
        $this->addColumn('user', 'photo_id', $this->integer());
        $this->addColumn('user', 'photo_key', $this->string(100));
        $this->addColumn('user', 'vinculo_id', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'name');
        $this->dropColumn('user', 'photo_id');
        $this->dropColumn('user', 'photo_key');
        $this->dropColumn('user', 'vinculo_id');
    }
}
