<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yiister\gentelella\widgets\Panel;
use yiister\gentelella\widgets;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
?>

<?php Pjax::begin(); ?>

<style type="text/css">
    @media screen and (max-width: 830px) {
        .imagem-questao {
            width: 485px !important;
        }
    }

    @media screen and (max-width: 767px) {
        .imagem-questao {
            width: 530px !important;
        }
    }

    @media screen and (max-width: 600px) {
        .imagem-questao {
            width: 475px !important;
        }
    }

    @media screen and (max-width: 530px) {
        .imagem-questao {
            width: 380px !important;
        }
    }

    @media screen and (max-width: 450px) {
        .imagem-questao {
            width: 95% !important;
        }
    }

    .imagem-questao {
        width: 620px;
    }

    .imagem-alternativa {
        width: 120%;
    }
</style>

<div class="row">
    
    <?php    
    Panel::begin(
        [
            'header' => $titulo,
            'icon' => 'cog',
        ]
    )
    ?>

    <?php if (Yii::$app->user->isGuest): ?>
        <br><br>
        Você precisa fazer login no sistema para responder os exercícios. Clique em <strong>Entrar no Sistema.</strong>
        <br><br><br><br>
    
    <?php else: ?>

        <?= \yiister\gentelella\widgets\FlashAlert::widget(['showHeader' => true, 'flashes' => [
            'success' => [
                'class' => 'success',
                'header' => 'Parabéns!',
                'icon' => 'check',
            ],
            'info' => [
                'class' => 'info',
                'header' => 'Info!',
                'icon' => 'info-circle',
            ],
            'warning' => [
                'class' => 'warning',
                'header' => 'Warning!',
                'icon' => 'warning',
            ],
            'error' => [
                'class' => 'error',
                'header' => 'Resposta incorreta!',
                'icon' => 'ban',
            ],
        ]])
        ?>
        <?php $form = ActiveForm::begin(['id' => 'resposta-form' . $questao->id, 'options' => ['data-pjax' => true]]); ?>
        <div class="imagem-questao">
            <picture>
                <source media="(max-width: 530px)" srcset="<?= Url::to('@web/imagens/exercicios/' . $questao->exercicios_id . '/' . $questao->arquivo . 'p.svg') ?>">
                <img src="<?= Url::to('@web/imagens/exercicios/' . $questao->exercicios_id . '/' . $questao->arquivo . 'm.svg') ?>" width="100%" alt="Enunciado">
            </picture>
        </div>
        <br>
        <?= $form->field($modeloResposta, 'resposta')->radioList($alternativasFormatadas, ['separator' => '', 'label' => false, 'encode' => false, 'tag' => 'div']) ?>
        <?= $form->field($modeloResposta, 'questao_id')->hiddenInput(['value' => $questao->id])->label(false); ?>
        <?= Html::submitButton('Enviar resposta', ['class' => 'btn btn-primary', 'name' => 'resposta-button']) ?>
        <?= array_key_exists('idQuestao', $saida) ? Html::a('Nova Questão', Url::to(), ['class' => 'btn btn-success']) : '' ?>
        <?= Html::submitButton('<i class="fa fa-check"></i> Enviar resposta', ['class' => 'btn btn-success', 'name' => 'resposta-button']) ?>
        <?php ActiveForm::end(); ?>

    <?php endif ?>
    <?php Panel::end() ?>
</div>

<?php Pjax::end(); ?>