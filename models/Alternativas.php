<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alternativas".
 *
 * @property integer $id
 * @property integer $questoes_id
 * @property text $alternativa
 * @property string $letra
 * @property string $matriz
 *
 * @property Questoes $questoes
 */
class Alternativas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alternativas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['questoes_id', 'alternativa'], 'required'],
            [['questoes_id'], 'integer'],
            [['letra', 'matriz'], 'string', 'max' => 2],
            [['questoes_id'], 'exist', 'skipOnError' => true, 'targetClass' => Questoes::className(), 'targetAttribute' => ['questoes_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'questoes_id' => 'Questoes ID',
            'alternativa' => 'Alternativa',
            'letra' => 'Letra',
            'matriz' => 'Matriz',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestoes()
    {
        return $this->hasOne(Questoes::className(), ['id' => 'questoes_id']);
    }

    /**
     * @inheritdoc
     * @return AlternativasQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AlternativasQuery(get_called_class());
    }
}
