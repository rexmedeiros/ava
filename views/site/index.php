<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Ambiente de Aprendizagem';
?>


<h2>Ambiente Virtual de Aprendizagem de Computação Numérica</h2>

<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Recursos</h2>
                <div class="clearfix"></div>
            </div>
            <div class="container">

                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <div align="center">
                                <?= Html::img('@web/imagens/ilustracaoConteudo.png', [
                                    'alt' => 'Conteúdo',
                                    'class'=> '.img-fluid. max-width: 120%'
                                ]) ?>
                            </div>
                            <div class="carousel-caption">
                                <h5>Resumos preparados pelos monitores do componente curricular.</h5>
                            </div>
                        </div>
                        <div class="item">
                            <div align="center">
                                <?= Html::img('@web/imagens/ilustracaoVideoaulas.jpg', ['alt' => 'GeoGebra',
                                    'class'=> '.img-fluid. max-width: 120%']) ?>
                            </div>
                            <div class="carousel-caption">
                                <h5>Conteúdos audiovisuais usados na metodologia da sala de aula invertida.</h5>
                            </div>
                        </div>
                        <div class="item">
                            <div align="center">
                                <?= Html::img('@web/imagens/ilustracaoMultiprova.png', ['alt' => 'Exercícios',
                                    'class'=> '.img-fluid. max-width: 120%']) ?>
                            </div>
                            <div class="carousel-caption">
                                <h5>Questões objetivas personalizadas com parâmetros variantes.</h5>
                            </div>
                        </div>
                        <div class="item">
                            <div align="center">
                                <?= Html::img('@web/imagens/ilustracaoEstatisticas.png', ['alt' => 'Estatísticas',
                                    'class'=> '.img-fluid. max-width: 120%']) ?>
                            </div>
                            <div class="carousel-caption">
                                <h5>Estatísticas sobre a resolução dos exercícios disponíveis para o docente da turma.</h5>
                            </div>
                        </div>
                        <div class="item">
                            <div align="center">
                                <?= Html::img('@web/imagens/ilustracaoSIGAA2.jpg', ['alt' => 'Sigaa',
                                    'class'=> '.img-fluid. max-width: 120%']) ?>
                            </div>
                            <div class="carousel-caption">
                                <h5>Use suas credenciais do SIGAA para acessar o Ambiente de Aprendizagem.</h5>
                            </div>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>

            </div>
            <div class="new-caption-area"></div>
        </div>
    </div>
</div>


<?php
$script = <<< JS
    jQuery(function ($) {
        $('.carousel').carousel({
            interval: 3000
        });
        var caption = $('div.item:nth-child(1) .carousel-caption');
        $('.new-caption-area').html(caption.html());
        caption.css('display', 'none');

        $(".carousel").on('slide.bs.carousel', function (evt) {
            var caption = $('div.item:nth-child(' + ($(evt.relatedTarget).index() + 1) + ') .carousel-caption');
            $('.new-caption-area').html(caption.html());
            caption.css('display', 'none');
        });
    });
JS;
$this->registerJs($script);
?>



<style>
    .bs-example, .new-caption-area {
        background-color: rgba(0,0,0,.5);
        left: 0;
        right: 0;
        bottom: 0px;
        z-index: 10;
        padding: 2px 0 5px 25px;
        color: #fff;
        text-align: left;
        border-radius: 15px;
    }

    .carousel-caption {
        color: #000;
    }
    .img-responsive,
    .thumbnail > img,
    .thumbnail a > img,
    .carousel-inner > .item > img,
    .carousel-inner > .item > a > img {
        display: block;
        width: 100%;
        height: auto;
    }

    /* ------------------- Carousel Styling ------------------- */

    .carousel-inner {
        border-radius: 15px;
    }

    .carousel-caption {
        background-color: rgba(0,0,0,.5);
        position: absolute;
        left: 0;
        right: 0;
        bottom: -100px;
        z-index: 10;
        padding: 0 0 10px 25px;
        color: #fff;
        text-align: left;
    }

    .carousel-indicators {
        position: absolute;
        bottom: 0;
        right: 0;
        left: 0;
        width: 100%;
        z-index: 15;
        margin: 0;
        padding: 0 25px 25px 0;
        text-align: right;
    }

    .carousel-control.left,
    .carousel-control.right {
        background-image: none;
    }

</style>

