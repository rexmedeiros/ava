<?php
use yii\helpers\Html;
/* @var $this yii\web\View */


$this->title = 'Interpolação - Introdução';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Interpolação</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


            <p align="justify">
              Suponha que temos um conjunto de pontos que descreve uma amostra e queremos construir uma aproximação de uma função $f(x)$ que passe por todos os pontos coletados. Essa função $f(x)$ que passa por todos os pontos é chamada de <b>função interpoladora</b>, e o processo de obtenção dessa função é a <b>interpolação</b>. <br><br>

              As funções interpoladoras podem ser de vários tipos, mas por motivos de simplicidade estudaremos as funções interpoladoras <b>polinomiais</b>. <br><br>
              
              Lembre-se de que a fórmula geral para um polinômio de grau $n$ é <br><br>
               <div class="equacao">
              $$f(x) = a_0 + a_1x + a_2x^2 + ... + a_nx^n.$$ <br>
              </div>

              Para $n + 1$ pontos distintos, existe um e somente um polinômio de grau $n$ que passa por todos os pontos. Por exemplo, existe uma única reta (isto é, um polinômio de primeiro grau) que liga dois pontos. Analogamente, existe uma única parábola que passa por um conjunto de três pontos.<br><br> 

              A interpolação polinomial consiste em determinar o único polinômio de grau $n$ que passa por $n + 1$ pontos distintos. <br><br>
                <b>Para dois pontos nós teremos uma reta: </b>
                 <p style="text-align: center;"">
                  <img src="imagens/InterpolRETA2.svg"alt=""   style="width:90%;"/> 
                  </p>
                  <b>Para três pontos nós teremos uma parábola: </b>
                  <p style="text-align: center;"">
                  <img src="imagens/InterpolParabola2.svg"alt=""   style="width:90%;"/> 
                  </p>
                  <b>Para quatro pontos nós teremos uma função cúbica:</b><br>
                  <p style="text-align: center;"">
                  <img src="imagens/Interpol3grau2.svg"alt=""   style="width:90%;"/>
                  </p>

                E assim por diante. Aqui, especialmente, iremos abordar a interpolação de Newton e o método desenvolvido por Lagrange para o cálculo do polinômio de interpolação.

              
            </p>

          </div>
        </div>
    </div>
</div>
<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>