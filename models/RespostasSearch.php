<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Respostas;

/**
 * RespostasSearch represents the model behind the search form about `app\models\Respostas`.
 */
class RespostasSearch extends Respostas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'aluno_id', 'exercicio_id', 'questao_id', 'turma_id', 'acertou', 'created_at'], 'integer'],
            [['resposta', 'matriz'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $exerciciosAluno = Respostas::find()->addSelect(["exercicio_id"])->distinct()->where($params)->all();
        $saida = array();

        if (count($exerciciosAluno)){
            foreach($exerciciosAluno as $exercicioAluno){
                $params['exercicio_id'] = $exercicioAluno->exercicio_id;
                $sumarios = Respostas::find();
                // add conditions that should always apply here
                $sumarios->addSelect(["acertou","COUNT('id') AS total"])->where($params)->groupBy(['acertou'])->all();

                $saida[$exercicioAluno->exercicio_id]['descricao'] = $exercicioAluno->exercicio->descricao;

                foreach ($sumarios->each() as $sumario){
                    $saida[$exercicioAluno->exercicio_id][$sumario->acertou] = $sumario->total;
                }
            }
        }
        return $saida;
        
    }
}
