<?php
use yii\helpers\Html;
use app\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Sistemas Lineares - Sistemas Triangulares';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css"
      integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js"
        integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js"
        integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH"
        crossorigin="anonymous"></script>

<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Videoaula</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="https://www.youtube.com/embed/qFiqwcRp5co" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i>&nbsp; Sistemas Triangulares</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">
          Consideraremos a resolução de sistemas triangulares superiores. Dado um sistema linear do tipo $Ax = b$, sendo
          $A$ uma matriz $n\times n$ triangular superior, com elementos da diagonal principal diferentes de zero,
          podemos escrever as equações desse sistema da seguinte forma: <br><br>

        <div class="equacao">
          $$\begin{cases}
          \begin{array}{rcr} a_{11}x_{1} + a_{12}x_{2} + a_{13}x_{3} + \cdots + a_{1n}x_{n}= b_{1} \\
          a_{22}x_{2} + a_{23}x_{3} + \cdots + a_{2n}x_{n} = b_{2} \\
          a_{33}x_{3} + \cdots + a_{3n}x_{n} = b_{3} \\
          \vdots \;\;\;\;\;\;\;\;\;\;\;\;\;\; \\
          a_{nn}x_{n} = b_{n},
          \end{array}
          \end{cases}
          $$
        </div>

        <br>

        com $a_{ii} \neq 0$. Da última equação, tiramos que:
        $$ x_{n} = \frac{b_{n}}{a_{nn}}. $$ <br>

        O valor de $x_{n_{-1}}$ pode então ser obtido a partir da penúltima equação:


        $$ x_{n_{-1}} = \frac{b_{n_{-1}}-a_{n_{-1}n}x_{n}}{a_{n_{-1}n_{-1}}}. $$ <br>

        Fazendo substituições sucessivas, obtemos $x_{n_{-2}}$ , $\cdots$ , $x_{2}$ e, finalmente, $x_{1}$: <br><br>

        <div class="equacao">
          $$ x_{1} = \frac{b_{1}-a_{12}x_{2}-a_{13}x_{3}- ... -a_{1n}x_{n}}{a_{11}} .$$
        </div>

        <br><br>

        De maneira similar, um sistema de equações trianguar inferiror possui a seguinte forma:

        <br><br>
        <div class="equacao">
          $$
          \begin{cases}
          \begin{array}{lcr}
          a_{11}x_1 = b_1
          \\ a_{21}x_1 + a_{22}x_2 = b_2
          \\ a_{31}x_1 + a_{32}x_2 + a_{33}x_3= b_3
          \\ \ \ \ \ \ \ \vdots \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ddots
          \\ a_{n1}x_1 + a_{n2}x_2 + a_{n3}x_3 + \dots + a_{nn}x_n= b_n,
          \end{array}
          \end{cases}
          $$ <br>
        </div>
        em que $a_{ii} \neq 0$. Da primeira equação, temos que:
        $$ x_{1} = \frac{b_{1}}{a_{11}}. $$ <br>

        O valor de $x_{2}$ pode ser obtido a partir da segunda equação:
        $$
        x_{2} = \frac{b_{2}-a_{21}x_{1}}{a_{22}}.
        $$ <br>

        Fazendo substituições sucessivas, obtemos $x_3, x_4, \dots , x_{n_{-1}}$ e, finalmente, $x_n$: <br><br>
        <div class="equacao">
          $$
          x_{n} = \frac{b_{n}-a_{n1}x_{1} - a_{n2}x_{2} - ... - a_{nn_{-1}}x_{n_{-1}}}{a_{nn}}.
          $$
        </div>

        </p>

      </div>
    </div>
  </div>
</div>

<br><br>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p align="justify">
          Obter a solução do seguinte sistema: <br><br>

          $$
          \begin{cases}
          \begin{array}{rcr}
          x_1 + x_2 + 2x_3 = 1
          \\2x_2 + x_3 = -3
          \\ 5x_3 = -5
          \end{array}
          \end{cases}
          $$ <br>

          <b>Solução:</b> <br><br>

          Como temos um sistema cuja matriz de coeficientes é triangular superior, podemos obter a solução do sistema
          através da resolução retroativa, começando pela última equação. Primeiramente obtemos $x_3$: <br>

          $$ x_3 = \frac{-5}{5} = -1. $$<br>

          Em seguida, obtemos o valor de $x_2$: <br>

        <div class="equacao">
          $$ x_2 = \frac{-3 - x_3}{2} = \frac{-3 + 1}{2} = -1. $$
        </div>
        <br>

        Por fim, obtemos $x_1$: <br>
        <div class="equacao">
          $$ x_1 = \frac{1 - x_2 - 2x_3}{1} = 1 + 1 + 2 = 4.$$<br>
        </div>
        Portanto, a solução do sistema é: <br>

        $$
        x = \begin{bmatrix}4 \\ -1 \\ -1 \end{bmatrix}.
        $$

        </p>

      </div>
    </div>
  </div>
</div>

<br><br>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo 2</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">

          Obter a solução do seguinte sistema: <br><br>

          $$
          \begin{cases}
          \begin{array}{lcr}
          2x_1 = 1
          \\x_1 +2x_2 = -2
          \\ 2x_1 + 4x_2 + x_3= -12
          \end{array}
          \end{cases}
          $$ <br>

          <b>Solução:</b> <br><br>

          Como temos um sistema cuja matriz de coeficientes é triangular inferior, podemos obter a solução do sistema
          através da resolução retroativa, começando pela primeira equação. Primeiramente obtemos $x_1$: <br>

          $$x_1 = \frac{1}{2} = 0.5.$$<br>

          Em seguida, obtemos o valor de $x_2$: <br>
        <div class="equacao">
          $$ x_2 = \frac{-2 - x_1}{2} = \frac{-2 - 0.5}{2} = -1.25. $$
        </div>
        <br>

        Por fim, obtemos $x_3$: <br>


        <div class="equacao">
          $$ x_3 = \frac{-12 - 2x_1 - 4x_2}{1} = -12 - 1 + 5 = -8.$$
        </div>
        <br>

        Portanto, a solução do sistema é: <br>

        $$
        x = \begin{bmatrix}0.5 \\ -1.25 \\ -8 \end{bmatrix}.
        $$


        </p>

      </div>
    </div>
  </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/Ri433TU8R-M" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/sistemasLinearesTriangulares.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Sistemas Triangulares',
          'todosExercicios' => $todosExercicios[16],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>
</div>

<script>
  renderMathInElement(document.body, {
    delimiters: [
      {left: "$$", right: "$$", display: true},
      {left: "$", right: "$", display: false}
    ]
  });
</script>
