<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\ExercicioWidget;

/* @var $this yii\web\View */


$this->title = 'Sistemas Lineares - Método de Gauss';
$this->params['breadcrumbs'][] = $this->title;
?>


<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css"
      integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js"
        integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js"
        integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH"
        crossorigin="anonymous"></script>


<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Videoaula</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="https://www.youtube.com/embed/qewmx4Pi0GM" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> &nbsp; Eliminação de Gauss</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">
          O método da Eliminação de Gauss consiste em transformar um sistema linear em um sistema linear triangular
          superior equivalente, pois um sistema dessa forma pode ser resolvido diretamente com a resolução retroativa.
          <br><br>
          Assim, o método resume-se em aplicar sucessivas operações elementares em um sistema linear para o transformar
          num sistema de mais fácil resolução, tendo este as mesmas soluções que o original.

          <br><br>

          A Eliminação de Gauss é muito simples, pois já estamos familiarizados com ela desde a Álgebra Linear através
          de escalonamento de matrizes. Apesar de existir diversas maneiras de escalonar uma uma matriz, o algoritmo da
          Eliminação de Gauss é o mais eficiente computacionalmente.
        </p>

      </div>
    </div>
  </div>
</div>

<br><br>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">

          Seja um sistema linear: 
          
          $$ \begin{cases} 3x_{1} + 2x_{2} + 4x_{3} = 1
          \\ x_{1} + x_{2} + 2x_{3} = 2
          \\ 4x_1 + 3x_2 - 2x_{3} = 3

          \end{cases}$$

          Podemos escrevê-lo na forma matricial do tipo $Ax$ = $b$, onde: 

          $$
          A =\begin{bmatrix}3 & 2 & 4 \\ 1 & 1 & 2 \\ 4 & 3 & -2 \end{bmatrix}
          \space b = \begin{bmatrix}1 \\ 2 \\ 3 \end{bmatrix}.
          $$

          E a partir delas escrevemos a matriz aumentada do sistema:

          $$
          Ab = \begin{bmatrix} 3 & 2 & 4 & | & 1 \\ 1 & 1 & 2 & | & 2 \\ 4 & 3 & -2 & | & 3 \end{bmatrix}.
          $$

          Vamos usar o método da Eliminação de Gauss para obter um sistema equivamente mais simples de resolver.

          <br><br> Inicialmente, vamos identificar o primeiro pivô, que vai ser o elemento da diagonal principal da
          primeira linha. Neste caso, é o $3$, e precisamos zerar todos os elementos que estão abaixo desse
          pivô.
          <br><br>

          Para isso, vamos diminuir da linha $2$ ($L_2$) um fator multiplicado pela linha $1$ ($L_1$). Esse fator é
          resultado da divisão do elemento dessa linha que queremos zerar pelo pivô, nesse caso,

          $$f_{21} = \frac{A_{21}}{A_{11}} = \frac{1}{3}.$$

          Depois de calcular o fator, fazemos

          $$L_2 = L_2 – f_{21}L_1,$$

          ou seja, diminuímos de cada elemento da linha $2$ um fator multiplicado pelo elemento correspondente da linha
          do pivô. Fazendo isso obtemos:

          $$
          \begin{bmatrix} 3 & 2 & 4 & | & 1 \\ 0 & \frac{1}{3} & \frac{2}{3} & | & \frac{5}{3} \\ 4 & 3 & -2 & | & 3
          \end{bmatrix}.
          $$

          Podemos ver que conseguimos zerar o elemento. Porém, ainda não terminamos a eliminação para o primeiro pivô.
          Resta-nos zerar o elemento da linha $3$ ($L_3$) que está abaixo do pivô. Calculamos o fator

          $$f_{31} = \frac{A_{31}}{A_{11}} = \frac{4}{3}$$

          e fazemos

          $$L_3 = L_3 – \frac{4}{3}L_1,$$

          obtendo:

          $$
          \begin{bmatrix} 3 & 2 & 4 & | & 1 \\ 0 & \frac{1}{3} & \frac{2}{3} & | & \frac{5}{3} \\ 0 & \frac{1}{3} &
          -\frac{22}{3} & | & \frac{5}{3} \end{bmatrix}.
          $$

          Agora podemos ir para coluna $2$. Na coluna $2$ temos que identificar o pivô, que será o elemento da diagonal
          principal da linha $2$, ou seja, pivô = $A_{22} = \frac{1}{3}$. Agora vamos zerar o elemento abaixo desse pivô
          seguindo os mesmos passos anteriores.
          
          <br><br>

          Calculamos o fator que vai ser

          $$f_{32} = \frac{A_{32}}{A_{22}} = \frac{\frac{1}{3}}{\frac{1}{3}} = 1$$

          e fazemos

          $$L_3 = L_3-f_{32}L_2,$$ obtendo: 


          $$
          \begin{bmatrix} 3 & 2 & 4 & | & 1 \\ 0 & \frac{1}{3} & \frac{2}{3} & | & \frac{5}{3} \\ 0 & 0 & -8 & | & 0
          \end{bmatrix}.
          $$

          Observe que agora obtemos um sistema de equações equivalente do tipo triangular superior. Já podemos resolver
          diretamente através da resolução retroativa, como vimos acima.

          <div class="equacao">
          $$\begin{cases}
          \begin{array}{rcr} 3x_{1} + 2x_{2} +4x_{3} = 1
          \\ \frac{1}{3}x_{2} + \frac{2}{3}x_{3} = \frac{5}{3}
          \\ -8x_{3} = 0
          \end{array}
          \end{cases} $$
          </div>

          Logo,

          $$
          x_{3} = \frac{b_{3}}{a_{33}} = \frac{0}{-8} = 0.
          $$

          A partir do valor de $x_{3}$ podemos obter os valores de $x_{2}$ e $x_{1}$: 

          $$x_{2} = \frac{\frac{5}{3} - \frac{2}{3}*0}{\frac{1}{3}} = 5$$

        <div class="equacao">
          $$x_{1} = \frac{1 - 2*5 - 4*0}{3} = -3$$
        </div>

        Ou seja, a solução do sistema é dada pelo vetor

        $$ x = \begin{bmatrix} -3 \\ 5 \\ 0 \end{bmatrix}.$$
        </p>

      </div>
    </div>
  </div>
</div>

<br><br>
<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Mal Condicionamento</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p align="justify">
          Um sistema é dito mal condicionado quando uma pequena alteração no valor dos coeficientes causa uma grande
          alteração da solução exata. Esse mal condicionamento muitas vezes é causado pela presença de um pivô próximo
          de zero. Por exemplo, considere o sistema 

          $$
          \begin{cases}0.0003x_{1} + 59.14x_{2} = 59.17 \\ 5.291x_{1} - 6.130x_{2} = 46.78 \end{cases}
          $$

          cuja solução exata é $x_{1} = 10$ e $x_{2} = 1$. Vamos resolver esse sistema através da Eliminação de Gauss
          com 4 dígitos de precisão. Escrevendo a matriz aumentada do sistema:

          $$
          \begin{bmatrix}0.0003& 59.14 & | & 59.17 \\ 5.291 & -6.130 & | & 46.78 \end{bmatrix}
          $$

          Temos que o primeiro pivô é $A_{11} = 0.0003$. Para zerar o elemento abaixo dele na segunda linha, calculamos
          <div class="equacao">
            $$f_{21} = \frac{A_{21}}{A_{11}} = \frac{5.291}{0.0003} = 1763.6667 \approx 1764$$
          </div>
          e fazemos

          $$L_2 = L_2-1764L1$$

          obtendo:
            
          <div class="equacao">
          $$
          \left[\begin{matrix}0.0003& 59.14 & | & 59.17 \\ 0 & -104300 & | & -104400 \end{matrix}\right]
          $$
          </div>

          A partir da resolução retroativa temos que: 

          <div class="equacao">
            $$x_{2} = \frac{-104400}{-104300} = 1.001$$
          </div>
          <div class="equacao">
            $$ x_{1} = \frac{59.17 - 1.001*59.14}{0.003}= -10 \neq 10,$$
          </div>

          ou seja, o valor obtido para $x_{1}$ não é nada condizente com a solução real. Esse problema é causado pelo
          fato de termos feito uma divisão com um número próximo de zero, devido ao pivô ter um valor muito baixo.
          <br><br>
          Uma possível solução para esse problema é aumentar a precisão, usando mais algoritmos significativos. Porém,
          isso causa um aumento do esforço computacional e pode não ser a solução mais adequada em todos os casos.
          <br><br>
          Uma maneira simples de melhorar a solução nesses casos é utilizar a técnica de pivotação, que veremos a
          seguir.
        </p>

      </div>
    </div>
  </div>
</div>


<br><br>
<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Pivotação Parcial</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p align="justify">
          A pivotação parcial consiste na permutação de linhas de um sistema de maneira que o maior elemento em módulo
          de uma determinada coluna do sistema seja o pivô. 
          
          <br><br> Vamos utilizar a técnica de pivotação parcial para
          resolver o exemplo anterior. 

          $$
          \begin{cases}0.0003x_{1} + 59.14x_{2} = 59.17 \\ 5.291x_{1} - 6.130x_{2} = 46.78 \end{cases}
          $$

          Vamos analisar a primeira coluna. Vemos que o maior elemento em módulo é o elemento da segunda linha, $5.291$.
          Portanto, vamos permutar a primeira linha com a segunda de maneira que ele se torne o pivô: 

          $$
          \begin{cases} 5.291x_{1} - 6.130x_{2} = 46.78 \\ 0.0003x_{1} + 59.14x_{2} = 59.17 \end{cases}
          $$

          $$
          \begin{bmatrix} 5.291 & -6.130 & | & 46.78 \\ 0.0003& 59.14 & | & 59.17 \end{bmatrix}
          $$

          Depois disso, é só prosseguir e utilizar a Eliminação de Gauss normalmente. Para zerar o elemento abaixo do
          pivô, calculamos
        <div class="equacao">
          $$f_{21} = \frac{A_{21}}{A_{11}} = \frac{0.0003}{5.291} = 0.0000567$$
        </div>
        e fazemos
        $$L_2 = L_2 - f_{21}L_1,$$
        obtendo,

        $$
        \begin{bmatrix} 5.291 & -6.130 & | & 46.78 \\ 0& 59.14 & | & 59.17 \end{bmatrix}
        $$

        A partir da resolução retroativa, calculamos que 

        $$ x_{2} = \frac{59.17}{59.14} = 1.001$$

        <div class="equacao">
          $$ x_{1} = \frac{46.78 + 6.130*1.001}{5.291} = 10.$$
        </div>

        Obtendo um resultado condizente com a solução exata.

      </div>
    </div>
  </div>
</div>

<!--        
<br><br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Pivotação Completa</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
            A Pivotação Completa segue o mesmo princípio da Pivotação Parcial, que é de fazer com que o maior elemento seja o pivô. 
            <br><br>
            A diferença é que, ao contrário da Pivotação Parcial, em que só se fazia a permutação de linhas de maneira que o maior elemento de uma coluna fosse o pivô, na completa se olha toda a matriz de maneira que o maior elemento possível seja o pivô, independente da coluna que esse elemento esteja. <br><br>

            Vamos resolver o seguinte sistema utilizando a Eliminação de Gauss com $4$ dígitos significativos e com Pivotação Completa. <br><br>

            $$ \begin{cases} 3x_{1} + 7x_{2} + 2x_{3} = 12
                \\ -4x_{1} + x_{2} + 5x_{3} = 20
                \\ 6x_1 + -9x_2 + x_{3} = 8

                \end{cases}
            $$
            <br>

            Escrevendo a matriz aumentada do sistema: <br>

            $$
            \begin{bmatrix}3 & 7 & 2 & | & 12 \\ -4 & 1 & 5 & | & 20 \\ 6 & -9 & 1 & | & 8 \end{bmatrix}
            $$
            <br>
            O primeiro passo é vermos qual é o maior elemento em módulo da matriz de coeficientes. Sabemos que é $-9$, que está na posição $A_{32}$. Faremos com que ele seja o pivô, e para isso permutaremos a primeira linha e a terceira: <br><br>

            $$
            \begin{bmatrix}6 & -9 & 1 & | & 8 \\ -4 & 1 & 5 & | & 20 \\ 3 & 7 & 2 & | & 12  \end{bmatrix}
            $$
            <br>

            Agora seguiremos pra fazer a Eliminação de Gauss considerando o $-9$ como pivô, ou seja, vamos zerar o que está abaixo dele. Para zerar o elemento da segunda linha, primeiro calculamos $$\frac{A_{22}}{A_{12}} = \frac{1}{-9}$$ e realizamos a operação $$L_2 = L_2 - \frac{1}{-9}*L_1$$ obtendo:<br>

            $$
            \begin{bmatrix}6 & -9 & 1 & | & 8 \\ -3.333 & 0 & 5.111 & | & 20.89 \\ 3 & 7 & 2 & | & 12  \end{bmatrix}
            $$
            <br>

            Zerando o elemento da terceira linha: $$\frac{A_{32}}{A_{12}} = \frac{7}{-9} \\ L_3 = L_3 - \frac{7}{-9}*L_1$$ 

            $$
            \begin{bmatrix}6 & -9 & 1 & | & 8 \\ -3.333 & 0 & 5.111 & | & 20.89 \\ 7.667 & 0 & 2.778 & | & 18.22  \end{bmatrix}
            $$
            <br>

            Agora que já temos os elementos abaixo do nosso primeiro pivô zerados, prosseguimos para a escolha do segundo pivô. Analisando a matriz dos coeficientes, vemos que o  maior elemento é o $7.667$ na posição $A_{31}$. Então permutamos agora a segunda e a terceira linha (não mexemos mais com a primeira linha porque já trabalhamos em zerar os elementos abaixo do pivô dela). <br>

            $$
            \begin{bmatrix} 6 & -9 & 1 & | & 8  \\  7.667 & 0 & 2.778 & | & 18.22 \\   -3.333 & 0 & 5.111 & | & 20.89\end{bmatrix}
            $$
            <br>

            E prosseguimos com a Eliminação de Gauss para zerar o elemento abaixo do nosso pivô. Temos que $$\frac{A_{31}}{A_{21}} = \frac{-3.333}{7.667}$$ e fazemos: $$L_3 = L_3 - \frac{-3.333}{7.667}*L_2$$ obtendo:<br>

            $$
            \begin{bmatrix} 6 & -9 & 1 & | & 8  \\  7.667 & 0 & 2.778 & | & 18.22 \\   0 & 0 & 6.319 & | & 28.81\end{bmatrix}
            $$

            <br>

            E escrevendo na sintaxe de sistema:

            <br>

            $$ \begin{cases} 
            \begin{array}{rcr}
            6x_{1} + -9x_{2} + x_{3} = 8
                \\ 7.667x_{1} + 2.778x_{3} = 18.22
                \\ 6.319x_{3} = 8
                \end{array}
                \end{cases}
            $$ <br>

            Para resolvermos esse sistema é simples: <br><br>
            
            Primeiro iremos calcular o $x_{3}$.
            $$x_{3} = \frac{8}{6.319} \therefore x_{3} = 1.266$$ <br>

            Com o valor de $x_{3}$ calculado, agora podemos calcular $x_{1}$ a partir da segunda equação.

            $$7.667x_{1} + 2.778x_{3} = 18.22$$ <br>

            Então, substituindo $x_{3}$, teremos:

            $$7.667x_{1} + 2.778(1.266) = 18.22$$<br>

            Dessa forma, podemos isolar $x_{1}$.


             <div class = "equacao">
            $$x_{1} = \frac{18.22 - 2.778(1.266)}{7.667} \therefore x_{1} \approx 1.918$$<br>
            </div><br>

            Assim, com os valores de $x_{1}$ e $x_{3}$, podemos calcular o valor de $x_{2}$ utilizando a primeira equação do sistema, então:

            $$6x_{1} + -9x_{2} + x_{3} = 8$$<br>

            Se substituirmos $x_{1}$ e $x_{3}$, teremos:
            $$6(1.9177) + -9x_{2} + 1.266 = 8$$<br>

            Agora, isolando $x_{2}$, ficaremos com:

             <div class = "equacao">
            $$x_{2} = \frac{8 - 6(1.9177) - 1.266}{-9} \therefore x_{2} \approx 0.53$$<br>
            </div><br>

            Sendo assim, a solução do sistema será o seguinte vetor:<br>
             $$
                x = \begin{bmatrix} 1.918 \\  0.53 \\ 1.266\end{bmatrix}
            $$

            </p>

            </div>
        </div>
    </div>
</div>
-->

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p>
        <p style="text-align: center;"">
        <img src="imagensDev/Massao/retroativo.svg" alt="" style="width:90%;"/>
        </p>
        </p>
        <p>
        <p style="text-align: center;"">
        <img src="imagensDev/Massao/Gauss_Retroativo.svg" alt="" style="width:90%;"/>
        </p>
        </p>
      </div>
    </div>
  </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/C34a_5LdTEo" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/sistemasLinearesGauss.sce"></script>
          </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Eliminação de Gauss',
          'todosExercicios' => $todosExercicios[17],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>
</div>

<script>
  renderMathInElement(document.body, {
    delimiters: [
      {left: "$$", right: "$$", display: true},
      {left: "$", right: "$", display: false}
    ]
  });
</script>

