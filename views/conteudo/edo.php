<?php
use yii\helpers\Html;
/* @var $this yii\web\View */


$this->title = 'Equações Diferenciais - Introdução';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>


<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Introdução</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
<p>A modelagem de sistemas dinâmicos é normalmente realizada por meio de equações diferenciais. Como exemplo, considere um paraquedista que salta de um avião e no instante $t_0$ abre o paraquedas quando está a uma velocidade de queda $v_0$ . Baseado na segunda Lei de Newton, temos a seguinte equação que descreve a aceleração do paraquedista em função do tempo de queda:</p>

$$
  \frac{\mathrm dv}{\mathrm dt}  = g - \frac{c}{m}v, 
$$

onde $g$ é a aceleração da gravidade, $c$ é o coeficiente de arrasto, $m$ é a massa do paraquedista e $v$ a sua velocidade. <br> <br>

Na equação acima, a quantidade que está sendo derivada, $v$, é chamada de <b>variável dependente</b>. Já a quantidade em relação a qual $v$ é derivada, $t$, é chamada de <b>variável independente</b>. Quando temos <b>uma única variável independente</b>, a equação é chamada de <b>Equação Diferencial Ordinária (EDO)</b>.
<br><br>
As EDOs também são classificadas em relação a sua ordem. A equação diferencial em tela é chamada de equação de primeira ordem, pois a derivada mais alta é de primeira ordem. 
<br><br>
</div>
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-taxi"></i> Solução Analítica de uma EDO</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

Algumas EDO podem ser resolvidas analiticamente. Dentre elas estão as equações ditas separáveis, nas quais os termos relativos às variáveis dependente e independente podem ser isolados em cada lado da igualdade. Como exemplo, suponha que tenhamos a seguinte equação diferencial ordinária:</p>

<div class="equacao">
$$
      \frac{\text{d}y}{\text{d}x} = -2x^3+12x^2-20x+8.5.
$$
</div>
<p>
Sabemos que se quisermos obter a função $y$, devemos apenas integrar os dois lados da equação em relação a $x$. Por ser um polinômio, essa integração é fácil de ser feita. Sendo assim:
</p>

<div class="equacao"> 
$$
      \int \frac{\text{d}y}{\text{d}x}dx = \int(-2x^3+12x^2 -20x+8.5) dx, 
$$
</div>
<div class="equacao"> 
$$
      y= -0.5x^4+4x^3-10x^2+8.5x+C,
$$
</div>
em que $C$ é uma constante qualquer.
</div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-window-maximize"></i> Problema de Valor inicial</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
<p align="justify">
Ao analisar a equação do exemplo anterior,  observamos o surgimento de uma constante <i>C</i> introduzida ao resolver a integral indefinida.  Essa constante indica que a EDO possui infinitas soluções. Se quisermos obter uma solução em particular, a constante citada deve ser <b>fornecida no problema</b>, embora que implicitamente. Na prática, essa constante é determinada a partir do conhecimento do valor $y = y_0$ da função para algum valor de $x = x_0$. Quando a EDO é fornecida juntamente com a condição $y_0 = f(x_0)$, chamamos esse problema de Problema de Valor Inicial (PVI).
</p>
</div>
        </div>
    </div>
</div>
<br>
<p align="justify">
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-question"></i> Por que usar métodos numéricos para solucionar EDOS?</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
Vimos um caso onde a solução de uma EDO pode ser obtida analiticamente. Porém, muitas EDOs de importância prática não possuem uma solução analítica e exata. Por esse motivo, devemos recorrer aos métodos numéricos. 
<br><br>Nesse tópico veremos resoluções de EDOs através dos seguintes métodos: Euler, Heun (Euler Modificado) e os métodos de Runge-Kutta.
</p>

            </div>
        </div>
    </div>
</div>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>




