<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "turmas_docentes".
 *
 * @property integer $id
 * @property integer $vinculo_id
 * @property integer $turma_id
 * @property integer $ano
 * @property integer $periodo
 *
 * @property Turmas $turma
 * @property Vinculos $vinculo
 */
class TurmasDocentes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'turmas_docentes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vinculo_id', 'turma_id'], 'required'],
            [['vinculo_id', 'turma_id', 'ano', 'periodo'], 'integer'],
            [['turma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Turmas::className(), 'targetAttribute' => ['turma_id' => 'id']],
            [['vinculo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vinculos::className(), 'targetAttribute' => ['vinculo_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vinculo_id' => 'Vinculo ID',
            'turma_id' => 'Turma ID',
            'ano' => 'periodo',
            'periodo' => 'periodo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurma()
    {
        return $this->hasOne(Turmas::className(), ['id' => 'turma_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVinculo()
    {
        return $this->hasOne(Vinculos::className(), ['id' => 'vinculo_id']);
    }

    public function getTurmaNome() {
        return $this->turma->codigo.' - '.$this->turma->nome;
    }

    public function getTurmaSemestre() {
        return $this->ano.'.'.$this->periodo;
    }

    public function getTurmaCodigo() {
        return $this->turma->turma.$this->turma->subturma;
    }

    public function giveAlunosDocente($ano,$periodo) {
        $turmasDocente = TurmasDocentes::find()->where([
                    'vinculo_id' => Yii::$app->user->identity->vinculo_id, 
                    'ano' => $ano,
                    'periodo' => $periodo
                ])->all();
        //Para cada turma, pegar alunos
        $todasTurmas = array();
        $i = 0;
        foreach ($turmasDocente as $turmaDocente){
            $turma = $turmaDocente->turma;
            $todasTurmas[$i]['nome'] = $turma->nome;
            $todasTurmas[$i]['codigo'] = $turma->codigo;
            $todasTurmas[$i]['turma'] = $turma->turma;
            $todasTurmas[$i]['subturma'] = $turma->subturma;
            $alunosTurma = $turma->turmasAlunos;
            $todasTurmas[$i]['alunos'] = array();
            foreach ($alunosTurma as $alunoTurma){
                $todasTurmas[$i]['alunos'][$alunoTurma->vinculo->id]['nome'] = $alunoTurma->vinculo->user->name;
                $todasTurmas[$i]['alunos'][$alunoTurma->vinculo->id]['matricula'] = $alunoTurma->vinculo->matricula;
                //$todasTurmas[$i]['alunos'][$alunoTurma->vinculo->id]['nome'] = $alunoTurma->vinculo->user->name;
                //$todasTurmas[$i]['alunos'][$alunoTurma->vinculo->id]['matricula'] = $alunoTurma->vinculo->matricula;
            }
            $i++;
        }
        return $todasTurmas;
    }     
}
