<p align="center">
    <h1 align="center">Template Yii 2 - SIGAA</h1>
    <br>
</p>

INTRODUÇÃO
----------
Template para a construção de aplicações em Yii 2 com conexão à API do SIGAA/UFRN.

INSTALAÇÃO E CONFIGURAÇÃO
-------------------------
1. Clone o repositório.
2. Entre no diretório e execute

~~~
php composer.phar update
~~~

3. Configure um banco de dados da sua preferência.
4. Edite o arquivo `config/db.php` e insira as credenciais de acesso ao banco de dados.
5. Edite o arquivo `config/web.php` e insira as credenciais da API do SIGAA (parâmetros `clientId` e `clientSecret`).
6. Edite o arquivo `components/Sigaa.php` e insira/atualize os endereços dos servidores da API do SIGAA, bem como o parâmetro x-api-key da sua aplicação.
7. Faça a migração do banco de dados. No diretório do projeto, digite:

~~~
./yii migrate
~~~

8. Inicie o sistema de permissões/autorizações

~~~
./yii rbac
~~~

9. Pronto! Sua aplicação já está funcionando e efetuando login usando o SIGAA!
