<?php

if (YII_ENV_DEV){
    $parametros = [
        'adminEmail' => 'admin@example.com',
        'pdflatexDir' => '/usr/bin/',
        'pdf2svgDir' => '/usr/bin/',
    ];
}
else{
    $parametros = [
        'adminEmail' => 'admin@example.com',
        'pdflatexDir' => '/usr/bin/',
        'pdf2svgDir' => '/usr/bin/',
    ];
}

return $parametros;
