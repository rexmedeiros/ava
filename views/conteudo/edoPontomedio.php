<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\ExercicioWidget;

/* @var $this yii\web\View */


$this->title = 'Equações Diferenciais - Método do Ponto Médio';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>


<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Videoaula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/HuRzoQjkZEs" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Método do Ponto Médio</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p>O método do ponto médio é semelhante ao método desenvolvido por Heun no sentido de que é realizado o cálculo de duas inclinações. Podemos analisar isto observando o desevolvimento a seguir: </p>
            <p> Dado um problema de valor inicial
            $$
            \begin{cases}
            \frac{\text{d}y}{\text{d}x}= g(x,y), \\
            y(x_0) = y_0,
            \end{cases}
            $$
            <br>
            a primeira inclinação é calculada de maneira similar ao Método de Heun, ou seja, no início do intervalo:
            $$
              {k_1 = g(x_i,y_i)}.
            $$

           A segunda inclinação é calculada no ponto médio do intervalo $[x_i;x_{i+1}]$, ou seja, em $x = x_i+h/2$:<br><br>
           <div class="equacao">
           $$
              {k_2 = g\left(x_i + \frac{h}{2}, y_i + k_1 \frac{h}{2}\right)}.
           $$
           </div>     
           Observe que no cálculo de $k_2$ foi usado o método de Euler para estimar o valor de $y$ no ponto médio do intervalo.
           
           A inclinação  $k_2$ será usada para estimar o prómixo valor de $y$, $y_{i+1}$:
            </p>
            
           $$
              y_{i+1} = y_i + k_2h.
            $$

            <br>

             Podemos entender melhor o método do Ponto Médio através das figuras a seguir:
           </p><br>
           <p style="text-align: center;">
            <img src="imagens/edopmedio1_.svg" style="width: 90%;"> 
             <img src="imagens/edopmedio2_.svg" style="width: 90%;"> 
            <br><br>
            </p>
            <p>Ao resolvermos a EDO pelo método do Ponto Médio e fizermos uma comparação com a resolução pelo método Heun, veremos que a diferença é extremamente pequena, ratificando o fato de que os métodos possuem a mesma ordem de convergência.</p>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
            <p>Use o método do Ponto Médio para resolver o PVI</p>
            $$
            \begin{cases}
                \frac{dy}{dx}= -1.2y + 7e^{-0.3x} = g(x,y)\\
                y(0) = 3
            \end{cases}
            $$                
            <br/>
            de $a = 0$ até $b = 2.5$ com um passo $h = 0.5$.
            
            <br><br>
            Compare o resultado com a solução analítica: 
            <br/>
             $$
                y =  \frac{70}{9}e^{-0.3x} - \frac{43}{9}e^{-1.2x}
             $$ 

             <b>Solução</b><br />
             <p>A solução analítica está representada pela curva em verde no gráfico a seguir.</p>
             
            <div id="applet_container" align="center"></div>

            <br>
            <ol id="demo">
                <li>
                Partimos do ponto inicial $(0;3)$, como mostrado no gráfico acima.<br><br><br>
                </li>
            </ol>
            <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            </div>
        </div>
    </div>
</div>
<br>










<script type="text/javascript">
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/EDOGeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
            ggbApplet.setValue('Dx',0.5); // Valor de h
            ggbApplet.setValue('t',false)  // Mostra Euler;
            ggbApplet.setValue('u',false)  // Mostra Heun;
            ggbApplet.setValue('v',true)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
        ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        h = ggbApplet.getValue('Dx').toFixed(decimais);
        x0 = ggbApplet.getValue('x0');
        x1 = ggbApplet.getValue('x1').toFixed(decimais);
        x2 = ggbApplet.getValue('x2').toFixed(decimais);
        x3 = ggbApplet.getValue('x3').toFixed(decimais);
        x4 = ggbApplet.getValue('x4').toFixed(decimais);
        x5 = ggbApplet.getValue('x5').toFixed(decimais);
        x6 = ggbApplet.getValue('x6').toFixed(decimais);
        ym0 = ggbApplet.getValue('y0');
        ym1 = ggbApplet.getValue('ym1').toFixed(decimais);
        ym2 = ggbApplet.getValue('ym2').toFixed(decimais);
        ym3 = ggbApplet.getValue('ym3').toFixed(decimais);
        ym4 = ggbApplet.getValue('ym4').toFixed(decimais);
        ym5 = ggbApplet.getValue('ym5').toFixed(decimais);
        ym6 = ggbApplet.getValue('ym6').toFixed(decimais);
        yv1 = ggbApplet.getValue('Sol(x1)').toFixed(decimais);
        yv2 = ggbApplet.getValue('Sol(x2)').toFixed(decimais);
        yv3 = ggbApplet.getValue('Sol(x3)').toFixed(decimais);
        yv4 = ggbApplet.getValue('Sol(x4)').toFixed(decimais);
        yv5 = ggbApplet.getValue('Sol(x5)').toFixed(decimais);
        yv6 = ggbApplet.getValue('Sol(x6)').toFixed(decimais);
        e1 = (yv1 - ym1).toFixed(decimais);
        e2 = (yv2 - ym2).toFixed(decimais);
        e3 = (yv3 - ym3).toFixed(decimais);
        e4 = (yv4 - ym4).toFixed(decimais);
        e5 = (yv5 - ym5).toFixed(decimais);
        e6 = (yv6 - ym6).toFixed(decimais);

        km1 = ggbApplet.getValue('kh1').toFixed(decimais);
        km2 = ggbApplet.getValue('kh2').toFixed(decimais);
        km3 = ggbApplet.getValue('kh3').toFixed(decimais);
        km4 = ggbApplet.getValue('kh4').toFixed(decimais);
        km5 = ggbApplet.getValue('kh5').toFixed(decimais);
        km6 = ggbApplet.getValue('kh6').toFixed(decimais);

        km11 = ggbApplet.getValue('kh11').toFixed(decimais);
        km21 = ggbApplet.getValue('kh21').toFixed(decimais);
        km31 = ggbApplet.getValue('kh31').toFixed(decimais);
        km41 = ggbApplet.getValue('kh41').toFixed(decimais);
        km51 = ggbApplet.getValue('kh51').toFixed(decimais);
        km61 = ggbApplet.getValue('kh61').toFixed(decimais);
        switch (n)
        {
            case 1:
                saida = 'Obtemos a inclinação do Ponto Médio fazendo \
                    \$\$ \\begin{aligned} \
                        k_1 &= g(x_0,y_0) = g('+x0+','+ym0+') = '+km11+' \\\\ \
                        k_2 &= g(x_0 + \\frac{1}{2}h,y_0 + \\frac{1}{2}k_1h) \\\\ \
                            &= g('+x0+' + \\frac{1}{2}'+h+', '+ym0+' + \\frac{1}{2}'+km11+'*'+h+') \\\\ \
                            &=  '+km1+'. \
                     \\end{aligned} \$\$';
                break;
            case 2:
                saida = "O ponto $y_1$ é calculada como sendo \
                    \$\$ \\begin{aligned} \
                        y_1 &= y_0 + k_2h  \\\\ \
                            &="+ym0+" + "+km1+" * "+h+" = "+ym1+". \
                     \\end{aligned} \$\$ \
                    Como o valor exato de $y_1 = f(x_1) = "+yv1+"$, o erro cometido foi $e_1 = "+e1+".$";
                break;
            case 3: 
                saida = ' A nova inclinação do Ponto Médio é \
                \$\$ \\begin{aligned} \
                        k_1 &= g(x_1,y_1) = g('+x1+','+ym1+') = '+km21+' \\\\ \
                        k_2 &= g(x_1 + \\frac{1}{2}h,y_1 + \\frac{1}{2}k_1h) \\\\ \
                            &= g('+x1+' + \\frac{1}{2}'+h+', '+ym1+' + \\frac{1}{2}'+km21+'*'+h+') \\\\ \
                            &=  '+km2+'. \
                     \\end{aligned} \$\$';
                break;
            case 4:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \$\$ \\begin{aligned} \
                        y_2 &= y_1 + g(x_1,y_1)h  \\\\ \
                            &="+ym1+" + ("+km2+") * "+h+" = "+ym2+". \
                     \\end{aligned} \$\$ \
                    Como o valor exato de $y_2 = f(x_2) = "+yv2+"$, o erro cometido foi $e_2 = "+e2+".$";

                break;   
            case 5: 
                saida = 'Calculado o segmento de reta com inclinação \
                    \$\$ \\begin{aligned} \
                        k_1 &= g(x_2,y_2) = g('+x2+','+ym2+') = ' + km31 + ' \\\\ \
                        k_2 &= g(x_2 + \\frac{1}{2}h,y_2 + \\frac{1}{2}k_1h) \\\\ \
                            &= g('+x2+' + \\frac{1}{2}'+h+', '+ym2+' + \\frac{1}{2}'+km31+'*'+h+') \\\\ \
                            &=  '+km3+'. \
                     \\end{aligned} \$\$';
                break;
            case 6:
                saida = 'A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \$\$ \\begin{aligned} \
                        y_3 &= y_2 + g(x_2,y_2)h  \\\\ \
                            &= '+ym2+' + ('+km3+') * '+h+' = '+ym3+'. \
                     \\end{aligned} \$\$ \
                    Como o valor exato de \$y_3 = f(x_3) = '+yv3+'\$, o erro cometido foi \$e_3 = '+e3+'.\$';

                break;  
            case 7: 
                saida = 'Calculado o segmento de reta com inclinação \
                    \$\$ \\begin{aligned} \
                        k_1 &= g(x_3,y_3) = g('+x3+','+ym3+') = '+km41+' \\\\ \
                        k_2 &= g(x_3 + \\frac{1}{2}h,y_3 + \\frac{1}{2}k_1h) \\\\ \
                        &= g('+x3+' + \\frac{1}{2}'+h+', '+ym3+' + \\frac{1}{2}'+km41+'*'+h+') \\\\ \
                           &=  '+km4+'. \
                     \\end{aligned} \$\$';
                break;
            case 8:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \$\$ \\begin{aligned} \
                        y_4 &= y_3 + g(x_3,y_3)h  \\\\ \
                            &= "+ym3+" + ("+km4+") * "+h+" = "+ym4+". \
                     \\end{aligned} \$\$ \
                    Como o valor exato de $y_4 = f(x_4) = "+yv4+"$, o erro cometido foi $e_4 = "+e4+".$";
                break;  
            case 9: 
                saida = 'Calculado o segmento de reta com inclinação \
                    \$\$ \\begin{aligned} \
                        k_1 &= g(x_4,y_4) = g('+x4+','+ym4+') = '+km5+' \\\\ \
                        k_2 &= g(x_4 + \\frac{1}{2}h,y_4 + k_1h) \\\\ \
                            &= g('+x4+' + \\frac{1}{2}'+h+', '+ym4+' + \\frac{1}{2}'+km51+'*'+h+') \\\\ \
                            &=  '+km5+'. \
                     \\end{aligned} \$\$';
                break;
            case 10:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \$\$ \\begin{aligned} \
                        y_5 &= y_4 + g(x_4,y_4)h  \\\\ \
                            &= "+ym4+" + ("+km5+") * "+h+" = "+ym5+". \
                     \\end{aligned} \$\$";
                break;  
            case 11: 
                saida = 'Calculado o segmento de reta com inclinação \
                    \$\$ \\begin{aligned} \
                        k_1 &= g(x_5,y_5) = g('+x5+','+ym5+') = '+km61+' \\\\ \
                        k_2 &= g(x_5 + \\frac{1}{2}h,y_5 + \\frac{1}{2}k_1h) \\\\ \
                            &= g('+x5+' + \\frac{1}{2}'+h+', '+ym5+' + \\frac{1}{2}'+km61+'*'+h+') \\\\ \
                            &=  '+km6+'. \
                     \\end{aligned} \$\$';
                break;
            case 12:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                        \$\$ \\begin{aligned} \
                        y_6 &= y_5 + g(x_5,y_5)h  \\\\ \
                            &= "+ym5+" + ("+km6+") * "+h+" = "+ym6+". \
                     \\end{aligned} \$\$";
                break;  
            default: 
            //alert('Default case');
        }
        

        return saida;   
    }

    function anterior (){
        if (this.n >= 1){
            this.n--;
            ggbApplet.setValue('cd', this.n);
            //mensagens(this.n);
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
                                
    }

    function proximo (){
        if (this.n <= 10){
            this.n++;
            ggbApplet.setValue('cd', this.n)
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) ;
            renderizarKatex();
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>

<br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/pontoMedio.svg"alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/mUT-nv-rSvY" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/edoPontoMedio.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método do Ponto Médio',
          'todosExercicios' => $todosExercicios[2],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>

</div>


<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>




<script type="text/javascript">
function renderizarKatex(){
    renderMathInElement(document.body,{delimiters: [
        {left: "$$", right: "$$", display: true},
        {left: "$", right: "$", display: false}
    ]});
}
</script>
