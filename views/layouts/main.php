

<?php
/** http://www-di.inf.puc-rio.br/~endler/courses/inf1612/aula-9.pdf
/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

$bundle = AppAsset::register($this);
//$bundle = yiister\gentelella\assets\Asset::register($this);

// Itens do menu lateral
$itensMenu[] = ["label" => "Página Inicial", "url" => Yii::$app->getHomeUrl(), "icon" => "home"];

// Ver se eh administrador do Site
$autorizacao = \Yii::$app->authManager;
$atribuicoes = $autorizacao->getAssignments(Yii::$app->user->id);
if (array_key_exists('admin', $atribuicoes)){
    array_push($itensMenu, 
        [ "label" => "Gerenciar Site", "url" => "#", "icon" => "cog", "items" => [
            [ "label" => "Exercícios Multiprova", "url" => ["/administrar/exercicios-index"]],
            [ "label" => "Componentes Curriculares", "url" => ["administrar/componentes-cn-index"]]
        ]]
    );    
}

/*
if (array_key_exists('docente', $atribuicoes)){
    array_push($itensMenu, 
        [ "label" => "Minhas Turmas", "url" => ["/docentes/turmas-index"], "icon" => "cog" ]
    );    
}
*/

// Os usuarios logados acessam os conteudos
//if (Yii::$app->user->isGuest){

//}
//else{ // todos logados
    array_push($itensMenu, 
        /*
        [
            "label" => "Ponto Flutuante",
            "icon" => "files-o",
            "url" => "#!",
            "items" => [
                ["label" => "Introdução", "url" => ["conteudo/ponto-flutuante"]],
                ["label" => "Sistemas em ponto flutuante", "url" => ["conteudo/ponto-sistemas"]],
                ["label" => "Erro em ponto flutuante", "url" => ["conteudo/ponto-erro"]],
            ],
        ],
        */
        [
            "label" => "Série de Taylor",
            "icon" => "files-o",
            "url" => "#!",
            "items" => [
                ["label" => "Conteúdo", "url" => ["conteudo/serie-taylor"]],

            ],
        ],

        [
            "label" => "Raízes",
            "icon" => "files-o",
            "url" => "#!",
            "items" => [
                ["label" => "Introdução", "url" => ["conteudo/raizes"]],
                ["label" => "Bissecção", "url" => ["conteudo/raizes-bissection"]],
                ["label" => "Falsa Posição", "url" => ["conteudo/raizes-falsepos"]],
                ["label" => "Newton-Raphson", "url" => ["conteudo/raizes-newton"]],
                ["label" => "Secante", "url" => ["conteudo/raizes-secante"]],

                  
            ],
        ],
        [
            "label" => "Sistemas Lineares",
            "icon" => "files-o",
            "url" => "#!",
            "items" => [
                ["label" => "Introdução", "url" => ["conteudo/sislinear"]],
                ["label" => "Sistemas Triangulares", "url" => ["conteudo/sislin-tri"]],
                ["label" => "Método de Gauss", "url" => ["conteudo/sislin-gauss"]],
                ["label" => "Decomposição LU", "url" => ["conteudo/sislin-lu"]],
                ["label" => "Jacobi", "url" => ["conteudo/sislin-jacobi"]],
                ["label" => "Gauss-Seidel", "url" => ["conteudo/sislin-seidel"]],
                ["label" => "Critérios de Convergência", "url" => ["conteudo/sislin-cconvergencia"]],
            ],
        ],
            [
            "label" => "MMQ",
            "icon" => "files-o",
            "url" => "#!",
            "items" => [
                ["label" => "Introdução", "url" => ["conteudo/mmq"]],
                ["label" => "Regressão Linear", "url" => ["conteudo/mmq-rlinear"]],
                ["label" => "Regressão Polinomial", "url" => ["conteudo/mmq-rpolin"]],
                ["label" => "Caso Geral", "url" => ["conteudo/mmq-cgeral"]],
                ["label" => "Linearização", "url" => ["conteudo/mmq-linearizacao"]],
            ],
        ],

        [
            "label" => "Interpolação",
            "icon" => "files-o",
            "url" => "#!",
            "items" => [
                ["label" => "Introdução", "url" => ["conteudo/interpolacao"]],
                ["label" => "Lagrange", "url" => ["conteudo/interp-lagrange"]],
                ["label" => "Newton", "url" => ["conteudo/interp-newton"]],

            ],
        ],

        [
            "label" => "Integração",
            "icon" => "files-o",
            "url" => "#!",
            "items" => [
                ["label" => "Introdução", "url" => ["conteudo/integracao"]],
                ["label" => "Regra do Trapézio", "url" => ["conteudo/integ-trapezio"]],
                ["label" => "Simpson 1/3", "url" => ["conteudo/integ-simpson13"]],
                ["label" => "Simpson 3/8", "url" => ["conteudo/integ-simpson38"]],
            ],
        ],
        [
            "label" => "Equações Diferenciais",
            "icon" => "files-o",
            "url" => "#!",
            "items" => [
                ["label" => "Introdução", "url" => ["conteudo/edo"]],
                ["label" => "Método de Euler", "url" => ["conteudo/edo-euler"]],
                ["label" => "Método de Heun", "url" =>  ["conteudo/edo-heun"]],
                ["label" => "Método do Ponto Médio", "url" =>  ["conteudo/edo-pontomedio"]],
                ["label" => "Métodos de Runge-Kutta", "url" => ["conteudo/edo-rungekutta"]],
                ["label" => "Sistemas de EDOs", "url" => ["conteudo/edo-sistemas"]],
                ["label" => "EDO de ordem superior", "url" => ["conteudo/edo-ordemsuperior"]],
            ],
        ]


    );

    array_push($itensMenu, 
        [ "label" => "Créditos", "url" => ["site/creditos"], "icon" => "user"],
        [ "label" => "Entre em contato", "url" => ["site/contato"], "icon" => "edit"]
    );
//}

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-md">
<?php $this->beginBody(); ?>
                    <?php echo  Html::beginForm(['site/logout'], 'post', ['id' => 'formu'])
                                                . Html::endForm();
                    ?> 
<div class="container body">

    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="<?= Yii::$app->getHomeUrl() ?>" class="site_title"><img src="<?=Url::to('@web/imagens/logo-ect.png') ?>" border="0" width="35"><span> &nbsp; ECT/UFRN</span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <?php if (!Yii::$app->user->isGuest) { ?> 
                            <?php if (Yii::$app->user->identity->photo_id) { ?>
                                <img src="https://sigaa.ufrn.br/shared/verArquivo?idArquivo=<?= Yii::$app->user->identity->photo_id ?>&key=<?= Yii::$app->user->identity->photo_key ?>"" alt="..." class="img-circle profile_img">
                            <?php }else { ?>
                                <img src="http://placehold.it/128x128" alt="..." class="img-circle profile_img">     
                        <?php } } else { ?> 
                            <img src="http://placehold.it/128x128" alt="..." class="img-circle profile_img">
                        <?php } ?>
                    </div>
                    <div class="profile_info">
                        <span>Bem vindo(a),</span>
                        <?php if (Yii::$app->user->isGuest) { ?>
                            <h2>Visitante</h2>
                        <?php } else { ?>
                            <h2><?= Yii::$app->user->identity->shortName  ?></h2>
                        <?php } ?>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->

                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>&nbsp;</h3>
                        <?=
                        \yiister\gentelella\widgets\Menu::widget(
                            [
                                "items" => $itensMenu,
                            ]
                        )
                        ?>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <!--
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                -->
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>                   
                    <ul class="nav navbar-nav navbar-right">
                        <?php if (Yii::$app->user->isGuest) { ?>
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="http://placehold.it/128x128" alt="">Entrar no Sistema
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="<?= Url::toRoute(['site/auth', 'authclient' => 'sigaa']) ?>">  Login via SIGAA</a>
                                    </li>
                                    <li>
                                        <a href="<?= Url::toRoute(['site/login'])?>">
                                            <span>Entrar localmente</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } else { ?>
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <?php if (Yii::$app->user->identity->photo_id) { ?>
                                    <img src="https://sigaa.ufrn.br/shared/verArquivo?idArquivo=<?= Yii::$app->user->identity->photo_id ?>&key=<?= Yii::$app->user->identity->photo_key ?>" alt="">
                                    <?php } else { ?> 
                                    <img src="http://placehold.it/128x128" alt="">
                                    <?php } ?>
                                    <?= Yii::$app->user->identity->shortName  ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">                            
                                    <li>
                                        <a href="<?= Url::toRoute(['site/contato'])?>">Entre em contato</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" onClick="javascript:document.getElementById('formu').submit()" ><i class="fa fa-sign-out pull-right"></i>Sair do Sistema</a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>                        
                        

                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>

            <?= $content ?>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com" rel="nofollow" target="_blank">Colorlib</a><br />
                Extension for Yii framework 2 by <a href="http://yiister.ru" rel="nofollow" target="_blank">Yiister</a>
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
