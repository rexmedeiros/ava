

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\ExercicioWidget;
/* @var $this yii\web\View */




$this->title = 'Raizes';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Bisseção - vídeo aula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/Q9xqQ2NMzTo" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>




</div>
        </div>
    </div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Método da Bissecção</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">





Esse método consiste em dividir um determinado intervalo contínuo e que apresenta no mínimo uma raiz em dois subintervalos, e verificando em qual dos 
intervalos a raiz está contida. Faz-se então esse procedimento quanto necessário até uma aproximação desejada. 
<br><br>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> O Método</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
Para verificar se o subintervalo contém a raiz, fazemos:
<br><br>
<p style ="padding-left: 5%;  ">Se $ \;F(a) \cdot F(\frac {a+b}{2}) < 0 $, $\; $ entao esse subintervalo tem uma raiz,<br>
Se $\;F(a) \cdot F(\frac {a+b}{2}) = 0 $, $\; $  então $ \; x = \frac{a+b}{2} \;$ é raiz,<br>
Se $\;F(a) \cdot F(\frac {a+b}{2}) > 0 $, $\; $  então a raiz esta no outro subintervalo.
</p>
<br>
Repetindo esse procedimento até um resultado suficientemente preciso
<br><br>
  </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
Vamos considerar a seguinte função:

$\; x^{2}-5 = 0 \;$ e utilizar o Método da Bissecção até um erro porcentual máximo de $5\%$
<br>
<br>


<p style="text-align: center;"">
                  <img src="imagens/RaizesBiss.svg"alt=""   style="width:50%;"/> 
</p><br><br>


Estimativa inicial 
<br><br>
Analisando o gráfico de $\; x^{2}-5 = 0, \;$ temos: <br>
<br>

<div class="equacao">
$$F(-2) \cdot F(-3) < 0 \; \; \; \; (1)$$
$$F(\; \; \;2) \cdot F(\; \; \;3)< 0  \; \; \; \, (2)$$
</div>
<br>

Há duas raizes, uma em cada subintervalo (1) e (2). 

Encontrando a raiz no subintervalo positivo, estima-se $a = 2$ e $b = 3$.

 <br><br><br>
Iteração $1$ <br><br>

  <div class="equacao">
              \begin{eqnarray}
                x_{1} &=& \large\frac{a+b}{2}\\
                      &=& \large \frac{2+3}{2} \\
                      &=& 2.5
              \end{eqnarray}
  </div>

<br>

Como temos apenas um valor de $x$, não é possível calcular o erro, então verifica que:
<br><br>
<p style = " padding-left: 5%">$ F(a) \cdot F(x_{1}) < 0 $ <br></p>
<p style = " padding-left: 5%">$ -1 \, \,\cdot \; 1.25 \;  \, < 0 , \; \;  $ a raiz está nesse subintervalo, $\; b = 2.5$

</p>
<br><br>
Iteração $2$ <br><br>
    
    <div class="equacao">
              \begin{eqnarray}
                x_{2} &=& \large\frac{a+b}{2}\\
                      &=& \large \frac{2+2.5}{2} \\
                      &=& 2.25
              \end{eqnarray}
  </div>
  <br>

Verifica o erro porcentual:
<br><br>
<p style = " padding-left: 5%">$ \varepsilon_{a} = \large {\left|\frac{2.25 - 2.5}{2.25}\right |} \normalsize \cdot 100 \; \% $ <br><br>
$  \varepsilon_{a} = 11.11  \% \; > \;5\% \;,$  como o erro é maior que $5 \%\;$: 
<br><br>
<p style = " padding-left: 5%">$ F(a) \cdot F(x_{2}) < 0 $ <br></p>
<p style = " padding-left: 5%">$  -1 \, \,\cdot \; 0.625 \;  \, < 0 , \; \;  $ a raiz encontra-se nesse subintervalo, $ \;b = 2.25$



</p>


</p>

 <br><br>
Iteração $3$ <br>
<br>

   <div class="equacao">
              \begin{eqnarray}
                x_{3} &=& \large\frac{a+b}{2}\\
                      &=& \large \frac{2+2.25}{2} \\
                      &=& 2.125
              \end{eqnarray}
  </div>
  <br>
Verifica o erro porcentual:
<br><br>
<p style = " padding-left: 5%">$ \varepsilon_{a} = \large {\left|\frac{2.125 - 2.25}{2.125}\right |} \normalsize \cdot 100 \; \% $ <br><br>
$  \varepsilon_{a} = 5.88  \% \; > \;5\% \;,$ novamente verifica que: 
<br><br>
<p style = " padding-left: 5%">$ F(a) \cdot F(x_{3}) < 0 $ <br></p>
<p style = " padding-left: 5%">$  -1 \, \,\cdot \; -0.4844 \;  \, > 0 , \; \;  $  a raiz está no outro subintervalo, $\;a = 2.125$



</p>


</p>
<br>
 <br>
Iteração $4$ 
<br><br>

  <div class="equacao">
              \begin{eqnarray}
                x_{4} &=& \large\frac{a+b}{2}\\
                      &=& \large \frac{2.125+2.25}{2} \\
                      &=& 2.1875
              \end{eqnarray}
  </div>
<br>
 Verifica o erro porcentual:
<br><br>
<p style = " padding-left: 5%">$ \varepsilon_{a} = \large {\left|\frac{2.1875 - 2.125}{2.1875}\right |} \normalsize \cdot 100 \; \% $ <br><br>
$  \varepsilon_{a} = 2.857  \% \; < \;5\% \;,\;$ satisfazendo nosso critério de parada. 
</p>

<br>
Então para um erro < $\;5\%,\;$ podemos inferir que a raiz é aproximadamente   $\;x = \; 2.1875\;$.

<br><br>
</div>
        </div>
    </div>
</div>










<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
            <p>Considere o método da Bisseção para resolver a função com erro relativo $< \;0.01\;$ no eixo $\;x\;$ positivo: </p><br>
            <p style = " padding-left: 5%">$\large  \frac{\; \,x^2}{5} \normalsize -2x\;=\;3$ 
</p>

<br>
            
            <p>Solução:</p>
             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>

            <br>
            <ol id="demo" style= " padding-left: 17px">
               
                <li  >
                Fazendo-se uma Análise Gráfica, estima-se a raiz no subintervalo   $a = 10\,$ e $b = 12$
               
                </li>
                <br><br>
            </ol>

            </div>
        </div>
    </div>
</div>
<br>


<div class="row">
<div class="col-xs-12">
        <div id="w0" class="x_panel">
          

          <?='inserir'
          /* 
          ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método de Heun',
          'todosExercicios' => $todosExercicios[5],
          'modeloResposta' => $modeloResposta,  
          ]) 
          */
          ?>
    </div>

</div>


  

</div>















<script type="text/javascript">
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/RAIZESBisettGeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-4,20,-13 ,15);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 4;
        ggbApplet.setValue('n',this.n)
        
        x = ggbApplet.getValue('X(1,0)').toFixed(decimais);

     
        a = ggbApplet.getValue('A(1,1)').toFixed(decimais);
        b = ggbApplet.getValue('B(1,1)').toFixed(decimais);
       
     
        switch (n)
        {
            case 1:
                   saida = "Iteração 1: \
               \
               \\begin{eqnarray*} \
                                     x = \\frac{a+b }{2}  \
\
       \\end{eqnarray*} \
                    \
                  \\begin{eqnarray*} \
                    \\newline \
                                     x = \\frac{"+a+"+"+b+  "}{2}  = "+x+" \
                                     \\newline \
                                     \\newline \
                    \\end{eqnarray*}";
                break;
               
                break;
            case 2:saida = "Como temos apenas um valor de x, não é possível calcular o erro relativo, então verifica que:\
                    \\begin{eqnarray*} \
                           F(a) \\cdot F(x) > 0 \
                           \\newline \
                           \\newline \
                             \\end{eqnarray*} \
                             \\begin{eqnarray*} \
                           F("+a+") \\cdot F("+x+")>0\
                           \\newline \
                           \\newline \
                            \\end{eqnarray*} \
                                \\begin{eqnarray*} \
                           a = x \
                    \\end{eqnarray*} ";
                
                break;
            case 3: 
                  saida = "Iteração 2: \
               \
               \
                    \\begin{eqnarray*} \
                                     x = \\frac{"+a+"+"+b+  "}{2} = "+x+" \
                    \\end{eqnarray*}";
                break;
            case 4:
                 saida = "Verifica o erro relativo: \
                    \\begin{eqnarray*} \
                           \\varepsilon_{r}  =  \\frac{|x - x_a|}{x} \
                           \\newline \
                           \\end{eqnarray*} \
                           \\begin{eqnarray*} \
                           \\newline \
                           \\varepsilon_{r}  =  \\frac{|"+ x+" - 11.0000|}{"+x+"} \
                           \\newline \
                           \\newline \
                           \\end{eqnarray*} \
                         \
                          \\begin{eqnarray*} \
                           \\varepsilon_{r} \ = 0.0435 > 0.01 \
                           \\newline \
                           \\newline \
                                  \\end{eqnarray*} \
                            \\begin{eqnarray*} \
                           F("+a+") \\cdot F("+x+")<0\
                           \\newline \
                           \\newline \
                            \\end{eqnarray*} \
                                \\begin{eqnarray*} \
                           b = x \
                    \\end{eqnarray*} ";
                break;
            case 5: 
                  saida = "Iteração 3: \
               \
               \
                    \\begin{eqnarray*} \
                                     x = \\frac{"+a+"+"+b+  "}{2} = "+x+" \
                    \\end{eqnarray*}";
                break;
            case 6:
                 saida = "Verifica o erro relativo: \
                           \\begin{eqnarray*} \
                           \\newline \
                           \\varepsilon_{r}  =  \\frac{|"+ x+" - 11.5000|}{"+x+"} \
                           \\newline \
                           \\newline \
                           \\end{eqnarray*} \
                         \
                          \\begin{eqnarray*} \
                           \\varepsilon_{r} \ = 0.0222 > 0.01 \
                           \\newline \
                           \\newline \
                                  \\end{eqnarray*} \
                            \\begin{eqnarray*} \
                           F("+a+") \\cdot F("+x+")>0\
                           \\newline \
                           \\newline \
                            \\end{eqnarray*} \
                                \\begin{eqnarray*} \
                           a = x \
                    \\end{eqnarray*} ";
                break;  
            case 7: 
                    saida = "Iteração 4: \
               \
               \
                    \\begin{eqnarray*} \
                                     x = \\frac{"+a+"+"+b+  "}{2} = "+x+" \
                    \\end{eqnarray*}";
                break; 
            case 8:
                 saida = "Verifica o erro relativo: \
                           \\begin{eqnarray*} \
                           \\newline \
                           \\varepsilon_{r}  =  \\frac{|"+ x+" - 11.2500|}{"+x+"} \
                           \\newline \
                           \\newline \
                           \\end{eqnarray*} \
                         \
                          \\begin{eqnarray*} \
                           \\varepsilon_{r} \ = 0.011 > 0.01 \
                           \\newline \
                           \\newline \
                                  \\end{eqnarray*} \
                            \\begin{eqnarray*} \
                           F("+a+") \\cdot F("+x+")<0\
                           \\newline \
                           \\newline \
                            \\end{eqnarray*} \
                                \\begin{eqnarray*} \
                           b = x \
                    \\end{eqnarray*} ";
                break; 
            case 9: 
                      saida = "Iteração 5: \
               \
               \
                    \\begin{eqnarray*} \
                                     x = \\frac{"+a+"+"+b+  "}{2} = "+x+" \
                    \\end{eqnarray*}";
                break; 
             case 10: 
                 saida = "Verifica o erro relativo: \
                           \\begin{eqnarray*} \
                           \\newline \
                           \\varepsilon_{r}  =  \\frac{|"+ x+" - 11.3750|}{"+x+"} \
                           \\newline \
                           \\newline \
                           \\end{eqnarray*} \
                         \
                          \\begin{eqnarray*} \
                           \\varepsilon_{r} \ = 0.0055 < 0.01 \
                           \\newline \
                           \\newline \
                         \
                                  \\end{eqnarray*} \
                                  \ Então a raiz é: \
                                  \\begin{eqnarray*} \
                                  \ x = "+x+" \
                                  \\end{eqnarray*}"; 
                break; 
                break; 


            //alert('Default case');
        }
        

        return saida;   
    }

    function anterior (){
             if (this.n%2==0){
        ggbApplet.evalCommand("  ZoomIn[1/2.2,(11.3204,0)]")
    }
        if (this.n >= 1){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
        if (this.n <= 9){
           
                this.n++;
            ggbApplet.setValue('n', this.n)
          
                 if (this.n%2==0){
               ggbApplet.evalCommand("  ZoomIn[2.2,(11.3204,0)]")
            }

             
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 

            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>





</script>
    <div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/bissecao.svg"alt=""   style="width:70%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>




<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>






