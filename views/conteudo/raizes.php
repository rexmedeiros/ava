

<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */




$this->title = 'Raizes';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Vídeoaula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/_GhTMRhApl8" allowfullscreen></iframe>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Raízes - Motivação</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                Seja uma equação do tipo

                $$
                f(x) = 0.
                $$
                Os valores de $x$ que satisfazem a equação são chamados de raízes da equação ou zeros da função $f(x)$.
                Desde cedo estudamos equações matemáticas e formas de solucioná-las. Por exemplo, a solução de uma de uma equação linear $\; 3x + 6 = 0$ pode ser encontrada de forma
                trivial isolando o valor de  $x$. Já os valores de $x$ que satisfazem a uma equação quadrática do tipo $a_2x^2 + a_1x+a_0 = 0$ podem ser determinados usando a fórmula de Báskara.
                <br><br>

                Por outro lado, equações polinomiais com grau maior que 4 ou equações não-polinomiais,
                como $ \; 4^{x}- x sen{(x+4)}=0, \;$ podem não apresentar uma maneira simples de serem
                solucionadas.
                Por esse motivo, veremos a seguir métodos numéricos para determinar raízes de uma equação com uma precisão arbitrária.

                <br><br>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Raízes - Interpretação gráfica </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                Graficamente, encontrar as raízes de uma equação equivale a encontrar os valores de $x$ dos pontos em o
                gráfico da função
                $F(x)$ toca o eixo das abscissas. Na figura abaixo vemos que, no intervalo que está plotado da função
                $F(x)$, a mesma apresenta duas raízes, uma delas destacadas em vermelho.

                Os métodos numéricos que estudaremos partem do princípio que um valor aproximado da raiz é conhecido.
                Então, fazer o gráfico da função nas imediações da raiz procurada é de grande importância para os
                métodos numéricos, conforme veremos nas seções posteriores.


                <p style="text-align: center;">
                    <img src="imagens/RaizesIntro.svg" alt="" style="width:90%;"/>
                </p>
                <br><br>

            </div>
        </div>
    </div>
</div>


<script>
    renderMathInElement(document.body, {
        delimiters: [
            {left: "$$", right: "$$", display: true},
            {left: "$", right: "$", display: false}
        ]
    });
</script>