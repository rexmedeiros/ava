<?php
use yii\helpers\Html;
use app\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Sistemas Lineares - Decomposição LU';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js"
        integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js"
        integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH"
        crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Resolução de sistemas lineares: Método de Jacobi - vídeo aula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/ksGH52MMy4I" allowfullscreen></iframe>
               </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Introdução</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                O método de Jacobi é uma simplicação do algorítmo de valores próprios de Jacobi, sendo ele formulado pelo matemático alemão Carl Gustav Jakob Jacobi no final do século XVIII. 
                <br><br>Para resolução de sistemas lineares com poucas dimensões, dificilmente usa-se métodos iterativos, uma vez que eles levam maior tempo para obter uma aproximação do real valor se comparados com os métódos diretos, como por exemplo a eliminação gaussiana. 
                <br><br>Entretanto, para sistemas grandes, como os que encontramos ao fazermos a análise de determinados circuitos ou na resolução de equações diferenciais parciais, essas técnicas iterativas tornam-se mais eficientes. 
            </p>

            </div>
        </div>
    </div>
</div>

<br><br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Conteúdo</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">


            Suponha que queremos achar a solução de um sistema linear de três variáveis: 
            <div class="equacao">
            $$  
            \begin{cases}a_{11}x_1 + a_{12}x_2 + a_{13}x_3 = b_1 
            \\ a_{21}x_1 + a_{22}x_2 + a_{23}x_3 = b_2 
            \\ a_{31}x_1 + a_{32}x_2 + a_{33}x_3 = b_3 
            \end{cases} 
            $$
            </div>

            Podemos tomar como primeiro passo isolar cada uma das incógnitas na sua linha correspondente:

            $$
            \begin{cases}x_1 =\frac{b_1 -a_{12}x_2 - a_{13}x_3}{a_{11}}
            \\ x_2 =\frac{b_2 -a_{21}x_1 - a_{23}x_3}{a_{22}}
            \\ x_3 =\frac{b_3 -a_{31}x_1 - a_{32}x_2}{a_{33}}
            \end{cases} 
            $$

            Em seguida, escrevemos na forma matricial:
            
            <div class="equacao">
            $$\begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix} = \begin{bmatrix} \frac{-a_{12}x_2 - a_{13}x_3}{a_{11}} +  \frac{b_1}{a_{11}} \\ \frac{-a_{21}x_1 - a_{23}x_3}{a_{22}} +  \frac{b_2}{a_{22}}  \\ \frac{-a_{31}x_1 - a_{32}x_2}{a_{33}} +  \frac{b_3}{a_{33}}  \end{bmatrix}
            $$
            </div>

            Por fim, escrevemos o resultado obtido na forma operações matriciais: 

            <div class="equacao">
            $$
            \begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix} = \begin{bmatrix} 0 & \frac{-a_{12}}{a_{11}} & \frac{-a_{13}}{a_{11}}
            \\ \frac{-a_{21}}{a_{22}} & 0 & \frac{-a_{23}}{a_{22}}
            \\ \frac{-a_{31}}{a_{33}} & \frac{-a_{32}}{a_{33}} & 0
            \end{bmatrix}            
            \begin{bmatrix} x_1 \\ x_2 \\ x_3 \end{bmatrix} 
            +
            \begin{bmatrix} \frac{b_{1}}{a_{11}} \\ \frac{b_{2}}{a_{22}} \\ \frac{b_{3}}{a_{33}} \end{bmatrix}.
            $$ 
            </div> 

            Chamaremos a matriz que multiplica o vetor $x$ de matriz de iteração $B$, e o vetor do lado direito que se soma ao resultado do produto chamaremos de $d$, obtendo: 

            $$
            x = Bx + d.  
            $$

            Observe que temos $x$ dos dois lados, indicando que se obtém $x$ a partir de outro $x$, o que caracteriza um processo iterativo. 
            <br><br>

            Esse processo iterativo da obtenção de uma solução $x$ para o sistema a partir de uma solução obtida anteriormente é denominado <b>Método iterativo de Jacobi</b>. A matriz $B$ é chamada de matriz de iteração porque é ela quem define o processo iterativo. 
            <br><br>

            Iniciamos com um chute inicial $x^{(0)}$ para a solução do sistema. A partir dele, vamos iterando e obtendo novas soluções. Desde que o processo seja convergente, a cada iteração obtemos uma solução que se aproxima cada vez mais da solução exata. Sabemos que: 
            <br><br>

            $$
            x^{(1)} = Bx^{(0)} + d,
            $$ 

            ou, generalizando para uma iteração $k$, obtemos que: 

            $$
            x^{(k+1)} = Bx^{(k)} + d.
            $$ 


            </p>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                Resolva o sistema linear de forma que o erro seja menor ou igual a $0.05$:
            <div class="equacao">
                $$ \begin{cases} 10x_{1} + 2x_{2} + x_{3} = 7
                \\ x_{1} + 5x_{2} + x_{3} = -8
                \\ 2x_{1} + 3x_{2} + 10x_{3} = 6
                \end{cases}$$ 
            </div>

            Isolando a variável $x_i$ na linha $i$, teremos: 

            <div class="equacao">
            $$
            \begin{cases} x_{1}  = -\frac{2}{10}x_{2} - \frac{1}{10}x_{3} + \frac{7}{10}
                \\ x_{2} = -\frac{1}{5}x_{1} -\frac{1}{5}x_{3} -\frac{8}{5}
                \\ x_{3} =- \frac{2}{10}x_{1} - \frac{3}{10}x_{2} + \frac{6}{10}
                \end{cases}
            $$
            </div>

            Em seguida, escrevemos as matrizes $B$ e $d$ do processo iterativo:
            <div class="equacao">
             $$
            B = \begin{bmatrix}0 & -0.2 & -0.1 \\ -0.2 & 0 & -0.2 \\ -0.2 & -0.3 & 0 \end{bmatrix},
            $$
            $$\space d = \begin{bmatrix}0.7 \\ -1.6 \\ 0.6 \end{bmatrix}.
            $$
            
            Feito isso, nós daremos um chute inicial para o $k = 0$, sendo esse chute, o vetor
            $\space x^{(k)} = x^{(0)} = \begin{bmatrix}0.7 \\ -1.6 \\ 0.6 \end{bmatrix}$
            
            <br><br>
            <b>Observação:</b> esse chute poderia ser um outro vetor qualquer, por exemplo: 
            $x^{(0)} = \begin{bmatrix}0 \\ 0 \\ 0 \end{bmatrix}$
            
            <br><br>

            Agora já podemos iniciar o processo iterativo $x^{(k+1)} = Bx^{(k)} + d$: 

            $$
            x^{(1)} = Bx^{(0)} + d.
            $$ 

            <strong>Iteração 1:</strong>

            <div class ="equacao"> 
                $$
                \begin{aligned}
                x^{(1)} &= \begin{bmatrix}0 & -0.2 & -0.1 \\ -0.2 & 0 & -0.2 \\ -0.2 & -0.3 & 0 \end{bmatrix}  \begin{bmatrix} 0.7 \\ -1.6 \\ 0.6 \end{bmatrix} + \begin{bmatrix} 0.7 \\ -1.6 \\ 0.6 \end{bmatrix} \\
                    &= \begin{bmatrix}  (-0.2)(-1.6) + (-0.1)0.6 + 0.7 \\  (-0.2)0.7 + (-0.2)0.6 - 1.6 \\  (-0.2)0.7 + (-0.3)0.6 + 0.6 \end{bmatrix} \\   
                    &= \begin{bmatrix} 0.96 \\ -1.86 \\ 0.94 \end{bmatrix}.   
                    \end{aligned}
                    $$
            </div>

            Agora podemos fazer o cálculo do erro para vermos o quão próximo estamos do erro que queremos obter. Para isso, consideraremos a <strong>norma linha</strong> dos vetores para o cálculo do erro relativo. (Para entender o cálculo da normal linha, veja a seção "<a href = "<?=Url::toRoute(['conteudo/sislin-cconvergencia'])?>">Critérios de Convergência</a>")
            
            $$
            \begin{aligned}
                E_r &= \frac{\left|x^{(1)}-x^{(0)}\right|_L }{\left|x^{(1)}\right|_L}\\
                    & = \frac{0.34}{1.86} = 0.1828.
            \end{aligned}
            $$

            Como $0.1828 > 0.05$, que é o erro que queremos obter, o precesso iterativo continua.<br><br>

            Prosseguindo as iterações, nós teremos:<br><br>

            <strong>Iteração 2:</strong>
            
            $$x^{(2)} = Bx^{(1)} + d$$

            <div class ="equacao"> 
              $$
              \begin{aligned}
                   x^{(2)} &= \begin{bmatrix}0 & -0.2 & -0.1 \\ -0.2 & 0 & -0.2 \\ -0.2 & -0.3 & 0 \end{bmatrix}  \begin{bmatrix} 0.96 \\ -1.86 \\ 0.94 \end{bmatrix} + \begin{bmatrix} 0.7 \\ -1.6 \\ 0.6 \end{bmatrix} \\
                    &= \begin{bmatrix}0.978 \\ -1.98 \\ 0.966 \end{bmatrix}.
              \end{aligned}
              $$
            </div>

            O erro relativo é: 

            <div class ="equacao"> 
            $$ 
            \begin{aligned} 
                E_r &= \frac{\left|\begin{bmatrix}0.978 \\ -1.98 \\ 0.966 \end{bmatrix} - \begin{bmatrix} 0.96 \\ -1.86 \\ 0.94 \end{bmatrix}\right|_L }{\left|\begin{bmatrix}0.978 \\ -1.98 \\ 0.966 \end{bmatrix}\right|_L}\\
                &= \frac{0.12}{1.98} = 0.0606.
                \end{aligned}
            $$ 
            </div>
            
            <strong>Iteração 3:</strong>

             <div class ="equacao"> 
             $$
                \begin{aligned}
                   x^{(3)} &= Bx^{(2)} + d \\
                           &= \begin{bmatrix}0 & -0.2 & -0.1 \\ -0.2 & 0 & -0.2 \\ -0.2 & -0.3 & 0 \end{bmatrix}  \begin{bmatrix} 0.978 \\ -1.98 \\ 0.966 \end{bmatrix} + \begin{bmatrix} 0.7 \\ -1.6 \\ 0.6 \end{bmatrix} \\
                           &= \begin{bmatrix}0.9994 \\ -1.9888 \\ 0.9984 \end{bmatrix} \\
                \end{aligned}
            $$
            </div>

            Com um erro relativo igual a 

            $$ 
                Er = \frac{0.0324}{1.9888} = 0.0163,
            $$ 
            que é menor que $0.05$, satisfazendo a condição imposta pelo enunciado da questão. Então, a solução $x$ do sistema linear acima, com erro menor que $0.05$, obtida pelo método de Jacobi, é 
            $$
            x = \begin{bmatrix}0.9994 \\ -1.9888 \\ 0.9984 \end{bmatrix}.
            $$ 


            </p>
            </div>
        </div>
    </div>
</div>

</script>
    <div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/jacobi_.svg"alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/9GNIMe-S2b8" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/sistemasLinearesJacobi.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método de Jacobi',
          'todosExercicios' => $todosExercicios[19],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>
</div>

<script>
  renderMathInElement(document.body, {
    delimiters: [
      {left: "$$", right: "$$", display: true},
      {left: "$", right: "$", display: false}
    ]
  });
</script>
