<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model frontend\models\TurmasDocentes */

$this->title = $dadosTurma->nome.' - '.$dadosTurma->turma.$dadosTurma->subturma;
$this->params['breadcrumbs'][] = ['label' => 'Turmas', 'url' => ['turmas-index', 'idTurma' =>  $searchModel->turma_id]];
$this->params['breadcrumbs'][] = $dadosTurma->codigo.' - '.$dadosTurma->turma.$dadosTurma->subturma;
?>
<div class="turmas-docentes-view">

    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>

    <h1><?= Html::encode($this->title) ?></h1>


    <?= \yiister\gentelella\widgets\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'hover' => true,
        'columns' => [
            [
                'attribute'=>'matricula',
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return Html::a($searchModel->aluno->matricula, ['turmas-aluno-respostas', 
                        'idAluno' => $searchModel->aluno_id,
                        'idTurma' => $searchModel->turma_id,
                    ]);
                },
                'label' => 'Matrícula',

            ],
            [
                'attribute'=>'name',
                'format' => 'raw',
                'value' => function ($searchModel) {
                    return Html::a($searchModel->aluno->user->name, ['turmas-aluno-respostas', 
                        'idAluno' => $searchModel->aluno->id,
                        'idTurma' => $searchModel->turma_id,
                    ]);
                },
                'label' => 'Nome',
            ],
            
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
