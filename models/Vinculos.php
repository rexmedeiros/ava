<?php

namespace app\models;

use Yii;
//use mdm\admin\models\User;
use app\models\User;

/**
 * This is the model class for table "vinculos".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $id_vinculo
 * @property integer $matricula
 * @property string $descricao
 * @property integer $docente
 * @property integer $ano_ingresso
 * @property integer $periodo_ingresso
 * @property integer $ativo
 *
 * @property TurmasAlunos[] $turmasAlunos
 * @property TurmasDocentes[] $turmasDocentes
 * @property User $user
 */
class Vinculos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vinculos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'id_vinculo', 'matricula'], 'required'],
            [['user_id', 'id_vinculo', 'matricula', 'docente', 'ano_ingresso', 'periodo_ingresso', 'ativo'], 'integer'],
            [['descricao'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'id_vinculo' => 'Id Vinculo',
            'matricula' => 'Matricula',
            'descricao' => 'Descricao',
            'docente' => 'Docente',
            'ano_ingresso' => 'Ano Ingresso',
            'periodo_ingresso' => 'Periodo Ingresso',
            'ativo' => 'Ativo',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMinitestes()
    {
        return $this->hasMany(Minitestes::className(), ['vinculo_id_docente' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMinitestesHabilitados()
    {
        return $this->hasMany(MinitestesHabilitados::className(), ['vinculo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMinitestesRespostas()
    {
        return $this->hasMany(MinitestesRespostas::className(), ['vinculo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tags::className(), ['vinculo_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurmasAlunos()
    {
        return $this->hasMany(TurmasAlunos::className(), ['vinculo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurmasDocentes()
    {
        return $this->hasMany(TurmasDocentes::className(), ['vinculo_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
