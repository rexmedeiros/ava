<?php

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\components\ExercicioWidget;

/* @var $this yii\web\View */

$this->title = 'MMQ - Introdução';
$this->params['breadcrumbs'][] = $this->title;
?>


<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Introdução</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p align="justify">
          Suponha que temos um conjunto de dados obtidos experimentalmente, representados por pontos $(x,y)$ no plano cartesiano: <br><br>
          <p style="text-align: center;">
            <img src="imagens/exemplo_figura1.svg" style="width:90%;" />
            <br>
            (Figura 1)
          </p><br>


          Queremos obter uma função que melhor represente os dados, ou seja, que mais se aproxime do conjunto de pontos. O Método dos Mínimos Quadrados (MMQ) possibilita obter tal função segundo um critério de minimização pré-definido. <br><br>

          O MMQ obtém a função através da minimização do erro quadrático representado pela distância entre os pontos experimentais e os pontos equivalentes da função aproximada, conforme ilustra o gráfico abaixo: <br><br>



          <p style="text-align: center;">
            <img src="imagens/exemplo_figura2.svg" style="width:90%;" />
            <br>(Figura 2)
          </p>
          Suponha que a função a ser obtida seja uma função linear (reta), ou seja, $f(x) = a + bx$.
          A Figura 2 mostra uma reta candidata a melhor representar o conjunto de pontos. Observe que para cada pronto, existe um erro $e$ que é dado pela distância entre a função e o ponto utilizado, como os ilustrados pelos segmentos demarcados com linhas pontilhadas.<br><br>

          Usando as técnicas que estudaremos, será possível obter uma reta que minimize a soma do quadrado desses erros para todos os pontos. Para esse conjunto de dados, essa reta é mostrada na Figura 3. <br><br>

          <p style="text-align: center;">
            <img src="imagens/exemplo_figura3.svg" style="width:90%;" />
            <br>
            (Figura 3)
          </p>

          Perceba que no Método dos Mínimos Quadrados a função não precisa necessariamente passar por todos os pontos. No exemplo, isso só seria possível se todos os pontos do conjutos estivessem alinhados, ou seja, se pertencessem a uma mesma reta. A função procurada é, portanto, aquela que
          minimização o chamado erro quadrático.

        </p>
      </div>
    </div>
  </div>
</div>


<script>
  renderMathInElement(document.body, {
    delimiters: [{
        left: "$$",
        right: "$$",
        display: true
      },
      {
        left: "$",
        right: "$",
        display: false
      }
    ]
  });
</script>