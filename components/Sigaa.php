<?php
/**
 * @link http://www.rexmedeiros.com/
 * @copyright Copyright (c) 2016 Rex Medeiros
 * @license http://www.rexmedeiros.com/
 */

namespace app\components;

use yii\authclient\OAuth2;

/**
 *
 * @author Rex Medeiros
 * @since 2.0
 */
class Sigaa extends OAuth2 
{
    /**
     * @inheritdoc
     */
    
    public $authUrl = YII_ENV_PROD ? 'https://autenticacao.ufrn.br/authz-server/oauth/authorize' : 'https://autenticacao.info.ufrn.br/authz-server/oauth/authorize';

    /**
     * @inheritdoc
     */
    public $tokenUrl = YII_ENV_PROD ? 'https://api.ufrn.br/authz-server/oauth/token' : 'https://api.info.ufrn.br/authz-server/oauth/token';

    /**
     * @inheritdoc
     */
    public $apiBaseUrl = YII_ENV_PROD ? 'https://api.ufrn.br' : 'https://api.info.ufrn.br';

    /**
     * @inheritdoc
     */
    public $xApiKey = YII_ENV_PROD ? 'Ek0gNrPlRlv85TE8OBC57Q6SgNFBT1XcZ5gd2Z50' : 'RD9M5WgxssmReRqzB7a5HXglMffRiTyY6UE2Akzn';

    /**
     * @inheritdoc
     */
    protected function initUserAttributes()
    {
      
        //$response = $this->api('ensino-services/services/consulta/listavinculos/usuario', 'GET');
        //$attributes = $response;
        
        $attributes = array();
        $accessToken = $this->getAccessToken();
        if (is_object($accessToken)) {
            $accessTokenParams = $accessToken->getParams();
            //unset($accessTokenParams['access_token']);
            //unset($accessTokenParams['expires_in']);
            $attributes = array_merge($accessTokenParams, $attributes);
        }

        return $attributes;
    }

    /**
     * @inheritdoc
     */
    public function applyAccessTokenToRequest($request, $accessToken)
    {
        $data = $request->getData();
        //$data['uids'] = $accessToken->getParam('user_id');
        //$data['access_token'] = $accessToken->getToken();
        //$request->setData($data);
        $request->addHeaders([
                'Authorization' => 'bearer '.$accessToken->getToken(),
                'x-api-key' => $this->xApiKey,
            ]);        
    }

    /**
     * @inheritdoc
     */
    protected function defaultName()
    {
        return 'sigaa';
    }

    /**
     * @inheritdoc
     */
    protected function defaultTitle()
    {
        return 'Sigaa UFRN';
    }


}
