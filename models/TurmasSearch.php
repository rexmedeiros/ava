<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Turmas;

/**
 * TurmasSearch represents the model behind the search form about `app\models\Turmas`.
 */
class TurmasSearch extends Turmas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_turma', 'ano', 'periodo', 'checagem', 'hora_checagem'], 'integer'],
            [['codigo', 'nome', 'horario', 'turma', 'subturma'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Turmas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_turma' => $this->id_turma,
            'ano' => $this->ano,
            'periodo' => $this->periodo,
            'checagem' => $this->checagem,
            'hora_checagem' => $this->hora_checagem,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
            ->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'horario', $this->horario])
            ->andFilterWhere(['like', 'turma', $this->turma])
            ->andFilterWhere(['like', 'subturma', $this->subturma]);

        return $dataProvider;
    }
}
