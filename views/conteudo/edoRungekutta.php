<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\ExercicioWidget;

/* @var $this yii\web\View */


$this->title = 'Equações Diferenciais - Método de Runge-Kutta';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>


<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Videoaula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/l8sOUHnPkgw" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Métodos de Runge Kutta</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">Por volta do ano 1900, dois matemáticos alemães, Carl David Tolmé Runge e  Martin Wilhelm Kutta, aprimoraram os métodos iterativos para resoluções de Equações Diferenciais Ordinárias de uma forma extremamente precisa. <br><br>Como Heun, eles perceberam que quanto mais inclinações (derivadas) fossem calculadas, melhor seria a precisão, sendo cada inclinação associada a um determinado coeficiente de compensação. 
            
            <br><br>Assim, analisando o trabalho de Heun, foi possível dar continuidade àquele raciocínio já desenvolvido pelo Alemão. </p>
            </div>
        </div>
    </div>
</div>

<!--  PARAFAZER: Refazer essa seção para mostrar o método de RK de 2a. ordem  Chapra Pg. 605

Colocar a equação genéria e depois mostrar que Heun e PM são dois casos particulares 
   -->
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Métodos de Runge Kutta de ordem 2 (RK2)</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">A versão de segunda ordem para o método de RK segue a seguinte equação:<br>
            $$
              {y_{i+1} = y_i + (a_1k_1 + a_2k_2)h},
            $$
            <br><br>
            em que <br>
             $$k_1 = f(x_i,y_i),$$
             $$k_2 = f(x_i + p_1h, y_i + q_{11}k_1h).$$ 
             
             <br>
             Os valores $a_1, a_2, p_1$ e $q_{11}$ são calculados igualando-se a primeira equação à expansão em série de Taylor até os termos de 2º grau. Fazendo isso, deduzimos três equações para calcular as quatro constantes desconhecidas. As três equações são:
              <br><br>
              $${a_1 + a_2 = 1,}$$ 
              $${a_2p_1 = \frac{1}{2}},$$ 
              $${a_2q_{11} = \frac{1}{2}}.$$
              <br>
              Nós podemos adotar quaisquer valores para as constantes $a_2$ ou $a_1$, posteriormente fazendo o valor adotado obedecer o conjunto de equações, ou seja, nós teremos infinitas maneiras de escrever o método de segunda ordem de RK. <br><br>
              Se fizermos, por exemplo, $a_1 = 0$, $a_2$ será igual a $1. Então teremos 
              
              <br><br>

              $$y_{i+1} = y_i + (a_1k_1 + a_2k_2)h.$$ 

              Substituindo $a_1$ e $a_2$, <br>

              $$y_{i+1} = y_i + k_2h,$$ 

              e nossas inclinações serão: 
              
              <br><br>
              $$k_1 = f(x_i,y_i).$$
              
              <br>
              Para encontrarmos $k_2$ teremos que resolver as equações que restaram. Assim sendo, temos que <br><br>
              
              $$a_2p_1 = \frac{1}{2}.$$ 
              
              Subtituindo o valor de $a_2 (a_2 = 1)$,
               <br>

              $$p_1 = \frac{1}{2}$$ 
              e 
              $$q_{11} = \frac{1}{2}.$$
              
              Dessa forma, a segunda inclinação será:

              $$k_2 = f(x_i + \frac{1}{2}h, y_i + \frac{1}{2}k_1h).$$
              
              Perceba que essa maneira que escrevemos o método de segunda ordem, na verdade, corresponde ao método do <i>Ponto Médio</i>, que já conhecemos. <br><br>

               Se fizermos agora $a_1 = \frac{1}{2}$, a partir disso (usando o conjunto de equações) podemos calcular que $a_2 = \frac{1}{2}$. Isso nos dará a seguinte equação para o calculo do próximo valor de $y$: 

               $$
                {y_{i+1} = y_i + (\frac{1}{2}k_1 + \frac{1}{2}k_2)h},
                $$

              $$k_1 = f(x_i, y_i),$$

              e $a_1p_1 = \frac{1}{2}$. Então, $p_1 = 1$ e $a_2q_{11} = \frac{1}{2}$ implica que $q_{11} = 1$. Sendo assim, 
              
              $$k_2 = f(x_i + h, y_i + q_{11}k_1h).$$

              Essa expressão para o método de RK de segunda ordem também já vimos, ela é o método desenvolvido por $Heun$. <br><br>
               Dessa forma, podemos ver que realmente há infinitas formas de escrevermos a expressão para calcular numéricamente uma EDO usando o método de RK2, sendo <i>Ponto Médio</i> e <i>Heun</i>  casos particulares dos métodos de Runge-Kutta.
             </p>
            </div>
        </div>
    </div>
</div>

<!--  PARAFAZER: Enfatizar que o método abaixo é um caso particular dos métodos de RK de 3a. ordem. 
   -->

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Métodos de Runge Kutta de ordem 3 (RK3)</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <p align="justify">
                A dedução do Método de Runge-Kutta de Terceira Ordem é bem parecida com a dos métodos de segunda ordem. O resultado é um conjunto de seis equações com oito incógnitas. Portanto, devem ser especificados valores, a priori, para as duas incógnitas (como fizemos em RK2) para determinar os parâmetros restantes. Uma, dentre as vesões mais comuns para RK3 é: <br><br>

                $$
                   {y_{i+1} = y_i + \frac{1}{6}(k_1 + 4k_2 + k_3)h},
                $$
                 em que <br><br>
                 <div class="equacao">
                 $$k_1 = f(x_i,y_i),$$
                 $$k_2 = f(x_i + \frac{1}{2}h, y_i + \frac{1}{2}k_1h),$$
                 $$k_3 = f(x_i + h, y_i - k_1h + 2k_2h).$$
                </div>
                  <br>
                 Nesta versão, $k_1$ e $k3$ tem o peso $1$ e $k_2$ tem peso $4$. Devemos lembrar que esse método se trata de um caso particular dos métodos de RK de 3ª ordem.

              </p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Métodos de Runge Kutta de ordem 4 (RK4)</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p>Os métodos de RK4 são os de maior precisão que usaremos aqui no curso. Esses métodos já nos dão uma boa aproximação comparada à resolução analítica da EDO. A seguir, veremos a forma mais comumente usada, que ficou conhecida como método RK de quarta ordem clássico:</p>

            <p>Podemos observar que a ordem do método, na verdade, corresponde ao número de inclinações que teremos de calcular, ou seja, fica claro que para o método de RK4, nós teremos quatro inclinações com novos pesos:</p>
            
            $$
              {k_1 = f(x_i,y_i)},
            $$
            $$
            \begin{aligned}
              k_2 &= f(x_{i+1}, \tilde{y}_{i+1}) \\
                &= f(x_i + \frac{1}{2}h, y_i + \frac{1}{2}k_1 h),
            \end{aligned}
            $$
            $$
            \begin{aligned}
               k_3 &= f(x_{i+1}, \tilde{y}_{i+1}) \\
               &= f(x_i + \frac{1}{2}h, y_i + \frac{1}{2}k_2 h),
            \end{aligned}
            $$
            $$
            \begin{aligned}
               k_4 &= f(x_{i+1}, \tilde{y}_{i+1}) \\
               &= f(x_i + h, y_i + k_3h).
            \end{aligned}
            $$   
            A inclinação resultante será:
            $$
                {k = \frac{1}{6}(k_1 + 2k_2 + 2k_3 + k_4)}.  
            $$
            Sendo assim, já podemos calcular o próximo valor de $y$:<br/>
            $$
                {y_{i+1} = y_i + kh}.  
           $$

            Ilustrando o método graficamente: <br><br>
             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>
            
            <br/>
            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo para Rk4</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               Usando o método de Runge Kutta de quarta ordem, resolva a seguinte equação diferencial 
              <br/>

              <div class="equacao">
                $$\frac{dy}{dx} = -1.2y + 7 e^{-0.3x} = g(x,y)$$
            </div>
              

            de $a = 0$ até $b = 2.5$ com um passo $h = 0.5$ e condição inicial $y = 3$ em $x = 0$. Compare o resultado com a solução analítica: 
              <div class="equacao">$$y = \frac{70}{9}e^{-0.3x} - \frac{43}{9}e^{-1.2x}.$$</div>
              
               <b>Solução:</b><br><br>
              O primeiro ponto da solução é $(0;3)$, que corresponde a condição inicial. Ou seja, no ponto ${i = 1}$ temos ${x_1 = 0}$ e ${y_1 = 3}$. Para o segundo ponto, calcularemos primeiro as inclinações: 

              <div class="equacao">
              $$ 
              \begin{aligned}
                k_1 &= g(x_1,y_1)\\
                    &= -1.2*3 + 7*e^{-0.3*0} = 3.4 \\
                k_2 &= g(x_1 + \frac{h}{2}, y_1 + k_1\frac{h}{2}) \\
                    &=  g(0+0.25, 3 + 3.4*0.25) = g(0.25, 3.85) \\
                    &= -1.2*3.85 + 7*e^{-0.3*0.25} = 1.874 \\
                k_3 &= g(x_1+\frac{h}{2},y_1+k_3\frac{h}{2}) \\
                    &= g(0+0.25, 3 + 1.874*0.25) = g(0.25, 3.468)\\
                    &= -1.2*3.468 + 7*e^{-0.3*0.25} = 2.332 \\
                k_4 &= g(x_1 + h, y_1 + k_3h) \\
                    &= g(0+0.5, 3 + 2.332*0.5) = g(0.5, 4.166) \\
                    & = -1.2*4.166 + 7*e^{-0.3*0.5}  = 1.026
              \end{aligned}
              $$
              </div> 

              Logo,
             
             

              <div class="equacao">
              $$ 
                y_{2} = y_1 + \frac{h}{6}(k_1+2k_2+2k_3+k4) 
              $$
              </div>
              <div class="equacao">
              $$
              \begin{aligned}
                y_{2} &= 3 + \frac{0.5}{6}(3.4+2*1.874+2*2.332+1.026) \\
                      &= 4.070
              \end{aligned}
              $$ 
              </div>

              Comparando com o resultado da solução analítica:

              <div class="equacao">
              $$
              \begin{aligned}
                {y_a} &= \frac{70}{9}e^{-0.3*0} - \frac{43}{9}e^{-1.2*0} \\
                      &=  4.072
              \end{aligned}
              $$ 
              </div>

              <div class="equacao"> $$Erro = y_{a} - y_{2}  = 4.072 - 4.070 = 0.002 $$</div> 
              <br><br>

              O restante da solução podemos ver na tabela a seguir:

<div class="equacao">
              <br/><br/><table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${i}$</th>
                                  <th>${x_i}$</th>
                                  <th>${y_i}$</th>
                                  <th>${y_a}$</th>
                                  <th>${Erro}$</th>
                                  <th>${k_1}$</th>
                                  <th>${k_2}$</th>
                                  <th>${k_3}$</th>
                                  <th>${k_4}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th>${1}$</th>
                                  <td>${0.000}$</td>
                                  <td>${3.000}$</td>
                                  <td>${3.000}$</td>
                                  <td>${0.000}$</td>
                                  <td>${}$</td> 
                                  <td>${}$</td>
                                  <td>${}$</td>
                                  <td>${}$</td>
                                </tr>
                                <tr>
                                  <th>${2}$</th>
                                  <td>${0.500}$</td>
                                  <td>${4.070}$</td>
                                  <td>${4.072}$</td>
                                  <td>${0.002}$</td>
                                  <td>${3.400}$</td> 
                                  <td>${1.874}$</td>
                                  <td>${2.332}$</td>
                                  <td>${1.026}$</td>
                                </tr>
                                <tr>
                                  <th>${3}$</th>
                                  <td>${1.000}$</td>
                                  <td>${4.320}$</td>
                                  <td>${4.323}$</td>
                                  <td>${0.003}$</td>
                                  <td>${1.141}$</td>
                                  <td>${0.363}$</td>
                                  <td>${0.597}$</td>
                                  <td>${-0.056}$</td>
                                </tr>
                                <tr>
                                  <th>${4}$</th>
                                  <td>${1.500}$</td>
                                  <td>${4.168}$</td>
                                  <td>${4.170}$</td>
                                  <td>${0.002}$</td>
                                  <td>${0.001}$</td>
                                  <td>${-0.374}$</td>
                                  <td>${ -0.261}$</td>
                                  <td>${-0.564}$</td>
                                </tr>
                                <tr>
                                  <th>${5}$</th>
                                  <td>${2.000}$</td>
                                  <td>${3.834}$</td>
                                  <td>${3.835}$</td>
                                  <td>${0.001}$</td>
                                  <td>${-0.538}$</td>
                                  <td>${-0.699}$</td>
                                  <td>${-0.651}$</td>
                                  <td>${-0.769}$</td>
                                </tr>
                                <tr>
                                  <th>${6}$</th>
                                  <td>${2.500}$</td>
                                  <td>${3.435}$</td>
                                  <td>${3.436}$</td>
                                  <td>${0.001}$</td>
                                  <td>${-0.759}$</td>
                                  <td>${-0.809}$</td>
                                  <td>${-0.794}$</td>
                                  <td>${-0.818}$</td>
                                </tr>
                              </tbody>
                            </table>
                            </div>
             <br/>
              </p>

            </div>
        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/Rk4.svg"alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/mUT-nv-rSvY" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/edoRk4.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método de Runge-Kutta de Ordem 4',
          'todosExercicios' => $todosExercicios[1],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>

</div>


<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }

</script>



<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var intViewportWidth = window.innerWidth;
    var height = window.height;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":intViewportWidth,"height":height,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":false,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/EDOrk4_.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0'); 
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-1,6.5,-1,5.5);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false);  // Mostra Euler;
            ggbApplet.setValue('newton',false);  // Mostra Heun;
            ggbApplet.setValue('secante',false);  // Mostra Ponto Medio;
            ggbApplet.setValue('n',0);
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}

        if(this.n%2 == 1){
        a = ggbApplet.getValue('A(1,1)').toFixed(decimais);
        b = ggbApplet.getValue('B(1,1)').toFixed(decimais);
    }
        
       
    }

    function anterior (){
            if(n>=5){

             ggbApplet.setCoordSystem(-1,6.5,-1,5.5);
        }
        else{

            ggbApplet.setCoordSystem(-1,6.5,-1,5.5);
        }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
        if(n>=3){

             ggbApplet.setCoordSystem(-1,6.5,-1,5.5);
        }
        else{

            ggbApplet.setCoordSystem(-1,6.5,-1,5.5);
        }
        if (this.n <= 6){
            this.n++;
            ggbApplet.setValue('n', this.n)  
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto);
            renderizarKatex();
            return true;                          
        }
        else {
            alert("Fim da demonstração.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>
<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>

<script type="text/javascript">
function renderizarKatex(){
    renderMathInElement(document.body,{delimiters: [
        {left: "$$", right: "$$", display: true},
        {left: "$", right: "$", display: false}
    ]});
}
</script>
