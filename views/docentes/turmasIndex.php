<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TurmasDocentesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Estatísticas da Turma - Respostas aos exercícios';
$this->params['breadcrumbs'][] = 'Estatísticas de respostas';
?>
<div class="turmas-docentes-index">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    
    <h1><?= Html::encode($this->title) ?></h1>


    <?= \yiister\gentelella\widgets\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        //'searchModel' => $searchModel,
        'hover' => true,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute'=>'turmaSemestre',
                'format' => 'raw',
                'value' => function ($searchModel) {
                      return Html::a($searchModel->turmaSemestre, ['turmas-alunos-view', 'idTurma' => $searchModel->id]);
                },

            ],
            [
                'attribute'=>'turmaNome',
                'format' => 'raw',
                'value' => function ($searchModel) {
                      return Html::a($searchModel->turmaNome, ['turmas-alunos-view', 'idTurma' => $searchModel->id]);
                },

            ],
            [
                'attribute'=>'turmaCodigo',
                'format' => 'raw',
                'value' => function ($searchModel) {
                      return Html::a($searchModel->turmaCodigo, ['turmas-alunos-view', 'idTurma' => $searchModel->id]);
                },

            ],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
