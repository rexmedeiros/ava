<?php

use yii\db\Migration;
use mdm\admin\components\Configs;

/**
 * Handles the creation of table `componentes_cn`.
 */
class m170427_131121_create_sigaa_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $userTable = Configs::instance()->userTable;
        

        // Vinculos, incluindo alunos e docentes
        if ($this->db->schema->getTableSchema('vinculos', true) === null) { 
            $this->createTable('vinculos', [
                'id' => $this->primaryKey(),
                'user_id' => $this->integer()->notNull(),
                'id_vinculo' => $this->integer()->notNull(),
                'matricula' => $this->biginteger()->notNull(),
                'descricao' => $this->string(255),
                'docente' => $this->smallinteger()->defaultValue(0),
                'ano_ingresso' => $this->smallinteger()->notNull(),
                'periodo_ingresso' => $this->smallinteger()->notNull(),
                'ativo' => $this->smallinteger()->defaultValue(0),
            ]);
            // creates index for column `user_id`
            $this->createIndex(
                'idx-vinculos-user_id',
                'vinculos',
                'user_id'
            );

            $this->addForeignKey('fk-vinculos-user_id-user-id', 'vinculos', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        }


        // turmas
        if ($this->db->schema->getTableSchema('turmas', true) === null) { 
            $this->createTable('turmas', [
                'id' => $this->primaryKey(),
                'id_turma' => $this->integer()->notNull(),
                'codigo' => $this->string(15)->notNull(),
                'nome' => $this->string(150)->notNull(),
                'horario' => $this->string(200)->notNull(),
                'turma' => $this->string(4)->notNull(),
                'subturma' => $this->string(4),
                'ano' => $this->smallinteger()->notNull(),
                'periodo' => $this->smallinteger()->notNull(),
                'checagem' => $this->biginteger()->defaultValue(1),
                'hora_checagem' => $this->integer()->defaultValue(0),
            ]);

            // creates index for column `id_turma`
            $this->createIndex(
                'idx-turmas-id_turma',
                'turmas',
                'id_turma'
            );
        }

        // Matriculas nas turmas
        if ($this->db->schema->getTableSchema('turmas_alunos', true) === null) { 
            $this->createTable('turmas_alunos', [
                'id' => $this->primaryKey(),
                'vinculo_id' => $this->integer()->notNull(),
                'matricula' => $this->biginteger()->notNull(),
                'turma_id' => $this->integer()->notNull(),
                'ano' => $this->smallinteger()->notNull(),
                'periodo' => $this->smallinteger()->notNull(),
            ]);

            $this->addForeignKey('fk-turmas_alunos-vinculo_id-vinculos-id', 'turmas_alunos', 'vinculo_id', 'vinculos', 'id', 'CASCADE', 'CASCADE');
            $this->addForeignKey('fk-turmas_alunos-turma_id-turmas-id', 'turmas_alunos', 'turma_id', 'turmas', 'id', 'CASCADE', 'CASCADE');
        }

        // Docentes das turmas
        if ($this->db->schema->getTableSchema('turmas_docentes', true) === null) { 
            $this->createTable('turmas_docentes', [
                'id' => $this->primaryKey(),
                'vinculo_id' => $this->integer()->notNull(),
                'turma_id' => $this->integer()->notNull(),
                'ano' => $this->smallinteger()->notNull(),
                'periodo' => $this->smallinteger()->notNull(),
            ]);

            $this->addForeignKey('fk-turmas_docentes-vinculo_id-vinculos-id', 'turmas_docentes', 'vinculo_id', 'vinculos', 'id', 'CASCADE', 'CASCADE');
            $this->addForeignKey('fk-turmas_docentes-turma_id-turmas-id', 'turmas_docentes', 'turma_id', 'turmas', 'id', 'CASCADE', 'CASCADE');
        }


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('turmas_alunos');
        $this->dropTable('turmas_docentes');
        $this->dropIndex(
            'idx-turmas-id_turma',
            'turmas'
        );
        $this->dropTable('turmas');   
        $this->dropTable('vinculos');

    }
}
