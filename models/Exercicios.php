<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "exercicios".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $descricao
 * @property integer $evento_id
 * @property integer $imagens
 * @property integer $created_at
 *
 * @property User $user
 * @property Questoes[] $questoes
 */
class Exercicios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'exercicios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'descricao', 'evento_id'], 'required'],
            [['user_id', 'evento_id', 'imagens'], 'integer'],
            [['descricao'], 'string', 'max' => 150],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'descricao' => 'Descricao',
            'evento_id' => 'Evento ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRespostas()
    {
        return $this->hasMany(Respostas::className(), ['exercicio_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestoes()
    {
        return $this->hasMany(Questoes::className(), ['exercicios_id' => 'id']);
    }
}
