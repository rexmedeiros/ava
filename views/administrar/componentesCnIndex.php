<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yiister\gentelella\widgets\grid\GridView;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ComponentesCnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cadastros de Componentes de CN';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="componentes-cn-index">

    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Cadastar novo componente', ['componentes-cn-create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'user_id',
            'codigo',
            'nome',
            //'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'urlCreator' => function ($action, $model, $key, $index, $thisa) {
                    //return string;
                    if ($action === "view"){
                        return Url::toRoute(['administrar/componentes-cn-view', 'id' => $key]);
                    }
                    if ($action === "update"){
                        return Url::toRoute(['administrar/componentes-cn-update', 'id' => $key]); 
                    }
                    if ($action === "delete"){
                        return Url::toRoute(['administrar/componentes-cn-delete', 'id' => $key]);
                    }
                },
            ],
        ],
    ]); ?>
</div>
