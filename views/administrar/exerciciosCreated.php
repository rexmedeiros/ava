<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model frontend\models\ComponentesCn */

$this->title = 'Resultado do processamento:';
$this->params['breadcrumbs'][] = ['label' => 'Exercícios', 'url' => ['exercicios-index']];
$this->params['breadcrumbs'][] = 'Resultado';
?>

  <?= Breadcrumbs::widget([
      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
  ]) ?>
  <?= Alert::widget() ?>

<div class="page-title">
  <div class="title_left">
    <h3><?= Html::encode($this->title) ?></h3>
  </div>
</div>
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Exercício cadastrado com sucesso</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content2">

        <?= DetailView::widget([
          'model' => $resumo,
          'attributes' => [
              ['attribute' => 'descricao', 'label' => 'Tema da avaliação:'],
              ['attribute' => 'nQuestoes', 'label' => 'Número de questões:'],
              ['attribute' => 'id', 'label' => 'ID do exercício:'],
          ],
        ]) ?>
        <p>
          <?= Html::a('Ver Todos os Exercícios', ['exercicios-index'], ['class' => 'btn btn-primary']) ?>
        </p>

      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>








