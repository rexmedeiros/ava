<?php

namespace app\controllers;

use Yii;
use app\models\Alunos;
use app\models\Turmas;
use app\models\TurmasDocentes;
use app\models\TurmasDocentesSearch;
use app\models\TurmasAlunos;
use app\models\TurmasAlunosSearch;
use app\models\Respostas;
use app\models\RespostasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\data\ActiveDataProvider;

/**
 * DocentesController implements the CRUD actions for TurmasDocentes model.
 */
class DocentesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'turmas-index',
                            'turmas-alunos-view',
                            'turmas-aluno-respostas',
                        ],
                        'allow' => true,
                        'roles' => ['docente','admin'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    return $action->controller->redirect('/site/acesso-negado');
                }
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'exercicios-delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Lists all TurmasDocentes models.
     * @return mixed
     */
    public function actionTurmasIndex()
    {
        $searchModel = new TurmasDocentesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('turmasIndex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TurmasDocentes model.
     * @param integer $id
     * @return mixed
     */
    public function actionTurmasAlunosView($idTurma)
    {
        $searchModel = new TurmasAlunosSearch();
        $dataProvider = $searchModel->search(['turma_id' => $idTurma]);

        $dadosTurma = Turmas::findOne($idTurma);

        if (!TurmasAlunos::find()->where(['turma_id' => $idTurma])->one()){
            Yii::$app->session->setFlash('error', 'Até o momento, nenhum aluno dessa turma acessou o site.');
            return $this->redirect(['turmas-index']);
        }
        else{
            return $this->render('turmasAlunosView', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'dadosTurma' => $dadosTurma,
            ]);            
        }
        
    }

    /**
     * Displays a single TurmasDocentes model.
     * @param integer $id
     * @return mixed
     */
    public function actionTurmasAlunoRespostas($idAluno,$idTurma)
    {
        $searchModel = new RespostasSearch();
        $dataProvider = $searchModel->search(['aluno_id' => $idAluno, 'turma_id' => $idTurma]);

        if (!TurmasAlunos::find()->where(['turma_id' => $idTurma])->one()){
            Yii::$app->session->setFlash('error', 'Até o momento, nenhum aluno dessa turma acessou o site.');
            return $this->redirect(['turmas-index']);
        }
        else{

            // Dados do aluno
            $dadosAluno = Alunos::findOne($idAluno);
            $dadosTurma = Turmas::findOne($idTurma);

            if (count($dataProvider)){
                return $this->render('turmasAlunoRespostas', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
                    'dadosAluno' => $dadosAluno,
                    'dadosTurma' => $dadosTurma,
                    'idTurma' => $idTurma,
                ]);
            }
            else{
                Yii::$app->session->setFlash('error', 'Esse aluno não respondeu nenhum exercício até o momento.');
                return $this->redirect(['turmas-alunos-view', 'idTurma' => $idTurma]);
            }
                            
        }
        
    }


    /**
     * Finds the TurmasDocentes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TurmasDocentes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findTurmasAlunosModel($idTurma)
    {
        if (($model = TurmasAlunos::find()->where(['turma_id' => $idTurma])->all()) !== null) {
            return $model;
        } else {
            Yii::$app->session->setFlash('error', 'Até o momento, nenhum aluno dessa turma acessou o site.');
        }
    }
}
