 <?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model frontend\models\ComponentesCn */

$this->title = 'Cadastrar novo exercício';
$this->params['breadcrumbs'][] = ['label' => 'Exercícios', 'url' => ['exercicios-index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exercicios-create">
	
	<?= Breadcrumbs::widget([
	    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	]) ?>
	<?= Alert::widget() ?>

	<div class="clearfix"></div>
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Importando arquivo do Multiprova:</h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content2">
	        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
	          <?= $form->field($model, 'descricao')->textInput() ?>
	          <?= $form->field($model, 'arquivo')->fileInput() ?>
	          <?= Html::activeHiddenInput($model,'comImagens',['value' => 0]) ?>

	          <div class="ln_solid"></div>

	          <div class="form-group">
		        <?= Html::submitButton('Cadastrar Exercício', ['class' => 'btn btn-success']) ?>
		      </div>
	        <?php ActiveForm::end(); ?>
	      </div>
	    </div>
	  </div>
	</div>
	<!--
	<div class="clearfix"></div>
	<div class="row">
	  <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      <div class="x_title">
	        <h2>Importando a questão instanciada do Multiprova: asdf asd</h2>
	        <ul class="nav navbar-right panel_toolbox">
	          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
	          </li>
	        </ul>
	        <div class="clearfix"></div>
	      </div>
	      <div class="x_content2">
	        <?php $form = ActiveForm::begin(); ?>
	          <?= $form->field($model, 'descricao')->textInput() ?>
	          <?= $form->field($model, 'idAvaliacao')->textInput() ?>
	          <?= Html::activeHiddenInput($model,'comImagens',['value' => 1]) ?>

	          <div class="ln_solid"></div>
	          
	          <div class="form-group">
		        <?= Html::submitButton('Cadastrar exercício', ['class' => 'btn btn-success']) ?>
		      </div>
	        <?php ActiveForm::end(); ?>
	      </div>
	    </div>
	  </div>
	</div>
	-->
</div>





