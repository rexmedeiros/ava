<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TurmasAlunos;

/**
 * TurmasAlunosSearch represents the model behind the search form of `app\models\TurmasAlunos`.
 */
class TurmasAlunosSearch extends TurmasAlunos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'vinculo_id', 'matricula', 'turma_id', 'ano', 'periodo'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TurmasAlunos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vinculo_id' => $this->vinculo_id,
            'matricula' => $this->matricula,
            'turma_id' => $this->turma_id,
            'ano' => $this->ano,
            'periodo' => $this->periodo,
        ]);

        return $dataProvider;
    }
}
