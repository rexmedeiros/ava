<?php

use yii\helpers\Html;
use app\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'MMQ - Ajuste Linear';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>


<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Regressão Linear - vídeo aula</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="https://www.youtube.com/embed/yEzIyTOx3_8" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Regressão Linear</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">
          O exemplo mais simples de aproximação por mínimos quadrados é ajustar uma reta a um conjunto de pares de observação: $(x_{1}, y_{1}), (x_{2}, y_{2}), ... , (x_{n}, y_{n})$. A expressão matemática do ajuste por uma reta é

          $$
          y = a_{0} + a_{1}x + e,
          $$

          onde $a_{0}$ e $a_{1}$ são coeficientes representando a intersecção com o eixo $y$ e a inclinação, respectivamente, e $e$ é o erro ou resíduo entre o modelo e a observação, o qual pode ser representado, depois de se reorganizar a equação acima, por

          $$
          e = y - a_{0} - a_{1}x.
          $$

          Portando, o erro ou resíduo é a discrepância entre o valor verdadeiro de $y$ e o valor aproximado, $a_{0} + a_{1}x$, previsto pela equação linear.

        </p>
      </div>
    </div>
  </div>
</div>

<br><br>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Critérios para o melhor ajuste</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p align="justify">
          Suponha que temos um conjunto de $n$ pontos ($x$,$y$) de uma função e queremos calcular uma reta da forma $y = a_{0} + a_{1}x$ que melhor represente esses pontos. <br><br>


          Para achar os valores dos coeficientes $a_{0}$ e $a_{1}$ que descrevem a reta, poderíamos tentar resolver o sistema: <br><br>

          $$\begin{cases}

          y_{1} = a_{0} + a_{1}x_{1}
          \\ y_{2} = a_{0} + a_{1}x_{2}
          \\ y_{3} = a_{0} + a_{1}x_{3}
          \\ \vdots
          \\ y_{n} = a_{0} + a_{1}x_{n}

          \end{cases} $$ <br><br>

          Porém, temos um sistema com $n$ equações e 2 incógnitas (sobredeterminado). Se os pontos não forem colineares, o sistema será impossível. <br><br>

          Dessa forma, como não podemos encontrar uma reta que passe por todos esses pontos, nos resta fazer o melhor ajuste de maneira a achar esses coeficientes. <br><br>

          O ajuste por Mínimos Quadrados é feito de maneira a minimizar a soma dos quadrados dos erros em todos os pontos, ou seja, a diferença entre o $y$ da função original e o $y$ calculado na aproximação. Dessa forma, temos que a soma total dos resíduos é dada por:

          <div class="equacao">
            $$
            \begin{aligned}
            S_{r} &= \sum_{i=1}^{n} e_i^2 \\
            &= \sum_{i=1}^{n} (y_{i,medido} - y_{i,aprox})^2 \\
            &= \sum_{i=1}^{n} (y_i - a_0 - a_1x_i)^2.
            \end{aligned}
            $$
          </div>


          Para achar os valores de $a_0$ e $a_1$, derivamos a equação acima em relação a cada um dos coeficientes:

          <div class="equacao">
            $$\frac{\partial S_r}{\partial a_0} = -2\sum (y_i - a_0 - a_1x_i),$$
          </div>

          <div class="equacao">
            $$\frac{\partial S_r}{\partial a_1} = -2\sum [(y_i - a_0 - a_1x_i)x_i].$$
          </div>

          Para minimizar esse resíduo, igualamos as derivadas a zero e expandimos as somas:

          <div class="equacao">
            $$ 0 = \sum y_i - \sum a_0 - \sum a_1x_i, $$
          </div>

          <div class="equacao">
            $$0 = \sum y_ix_i - \sum a_0x_i - \sum a_1x_i^2.$$
          </div>

          Manipulando essas equações, temos que

          <div class="equacao">
            $$a_1 = \frac{n \sum x_iy_i - \sum x_iy_i}{n\sum x_i^2 - (\sum x_i)^2},$$
          </div>

          <div class="equacao">
            $$a_0 = \overline{y}- a_1\overline{x}.$$
          </div>

          Podemos expressar esse resultado na forma de um produto matricial:


          $$
          X^TX \hat{a} = X^TY,
          $$

          onde o vetor coluna $\hat{a}$ contém os coeficientes do ajuste $a_0$ e $a_1$ e $X$ é uma matriz de Vandermonde de grau 1, que contém $n$ linhas (número de pontos) e 2 colunas, dada por:

          $$
          X = \begin{bmatrix}
          1 & x
          \\1 & x
          \\ \vdots & \vdots
          \\1 & x
          \end{bmatrix}.
          $$

          De forma alternativa, podemos expressar o resultado como sendo:

          <div class="equacao">
            $$
            \begin{bmatrix} 
            n & \sum x \\ \sum x & \sum x^2 \end{bmatrix} \begin{bmatrix}a_0 \\a_1 \end{bmatrix} = \begin{bmatrix}\sum y \\ \sum xy 
            \end{bmatrix}.
            $$
          </div>

          Resolvendo o sistema, encontraramos a melhor reta que ajusta os pontos segundo o critério de minimização do erro quadrático.  
        </p>
      </div>
    </div>
  </div>
</div>

<br><br>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">
          Ajuste uma reta para o conjunto de pontos ($x$,$y$) de uma função dados abaixo.<br>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>${x_i}$</th>
                <th>${y_i}$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>$1$</td>
                <td>$0.5$</td>
              </tr>
              <tr>
                <td>$2$</td>
                <td>$2.5$</td>
              </tr>
              <tr>
                <td>$3$</td>
                <td>$2.0$</td>
              </tr>
              <tr>
                <td>$4$</td>
                <td>$4.0$</td>
              </tr>
              <tr>
                <td>$5$</td>
                <td>$3.5$</td>
              </tr>
              <tr>
                <td>$6$</td>
                <td>$6.0$</td>
              </tr>
              <tr>
                <td>$7$</td>
                <td>$5.5$</td>
              </tr>
            </tbody>
          </table>

          Queremos achar uma equação linear da forma $y = a_{0} + a_{1}x$ que melhor se ajusta a esse conjunto de pontos. Podemos utilizar da relação:

          <div class="equacao">
            $$ \begin{bmatrix} n & \sum x \\ \sum x & \sum x^2 \end{bmatrix} \begin{bmatrix}a_0 \\a_1 \end{bmatrix} = \begin{bmatrix}\sum y \\ \sum xy \end{bmatrix}$$
          </div>

          para encontrar os melhores coeficientes que ajustam essa reta. Assim, temos que: <br>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>$n = 7$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th> $ \sum x = 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28 $</th>
              </tr>
              <tr>
                <th>$ \sum x^2 = 1^2 + 2^2 + 3^2 + 4^2 + 5^2 + 6^2 + 7^2 = 140 $</th>
              </tr>
              <tr>
                <th>$ \sum y = 0.5 + 2.5 + 2.0 + 4.0 + 3.5 + 6.0 + 5.5 = 24 $ </th>
              </tr>
              <tr>
                <th> $ \sum xy = 1*0.5 + 2*2.5 + 3*2.0 + 4*4.0 + 5*3.5 + 6*6.0 + 7*5.5 = 119.5 $ </th>
              </tr>
            </tbody>
          </table>

          Obtemos, então: <br><br>


          <div class="equacao">
            $$ \begin{bmatrix} 7 & 28 \\ 28& 140 \end{bmatrix} \begin{bmatrix}a_0 \\a_1 \end{bmatrix} = \begin{bmatrix} 24 \\ 119.5 \end{bmatrix}.$$
          </div>

          Na sintaxe de sistema, 

          $$
          \begin{cases}
          7a_{0} + 28a_{1} = 24
          \\ 28a_{0} + 140a_{1} = 140
          \end{cases}. 
          $$

          Podemos resolver facilmente esse sistema para determinar os valores de $a_0$ e o $a_1$: 

          $$
          a_0 = 0.071428
          $$
          $$
          a_1 = 0.839286
          $$<br>

          Portanto, o ajuste por mínimos quadrados é <br><br>

          $$y = 0.071428 + 0.839286x.$$
          
          A Figura (2) ilustra a reta obtida, bem como os pontos originais. <br>

          <p style="text-align: center;">
            <img src="imagens/mmqlinex1.svg" style="width: 70%;"> <br>(Figura 2)<br><br>
          </p>

          Para obter o resíduo (erro total) cometido, precisamos primeiro obter o $y$ aproximado para cada $x$: <br><br>

          <div class="equacao">            
            $$
              \begin{aligned}
                y_1 = 0.071428 + 0.839286x_1 \\
                &= 0.071428 + 0.839286*1 = 0.91071. 
              \end{aligned}
            $$
          </div>
          Da mesma forma:
          $$ y_2 = 1.75000, \\
          y_3 = 2.58928, \\
          y_4 = 3.42857, \\
          y_5 = 4.26785, \\
          y_6 = 5.10714, \\
          y_7 = 5.94643..
          $$

          Depois, é só cálcular o somatório do quadrado das diferenças entre cada $y$ medido e os $y$ aproximados: <br><br>

          <div class="equacao">
            $$ 
            \begin{aligned}
            S_{r} &= \sum_{i=1}^{n} e_i^2 \\
            &= \sum_{i=1}^{n} (y_{i,medido} - y_{i,aprox})^2 \\
            &= (0.5-0.910714)^2 + (2.5 - 1.75)^2 + \\
            &\;\;\;\;\; + (5.5-5.94643)^2 \\
            &= 2.99106. 
            \end{aligned}
            $$
          </div>


        </p>
      </div>
    </div>
  </div>
</div>

<br><br>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo 2</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p align="justify">
          Use a regressão por mínimos quadrados para ajustar uma reta aos pontos tabelados abaixo:

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>${x_i}$</th>
                <th>${y_i}$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>$6$</td>
                <td>$29$</td>
              </tr>
              <tr>
                <td>$7$</td>
                <td>$21$</td>
              </tr>
              <tr>
                <td>$11$</td>
                <td>$29$</td>
              </tr>
            </tbody>
          </table>

        </p>
        Devemos resolver um sistema da forma: 

        $$\begin{cases}
        y_{1} = a_{0} + a_{1}x_{1}
        \\ y_{2} = a_{0} + a_{1}x_{2}
        \\ y_{3} = a_{0} + a_{1}x_{3}
        \\ \vdots
        \\ y_{n} = a_{0} + a_{1}x_{n}
        \end{cases} $$ 

        Substituindo os 3 pontos $(x,y)$, ficamos com:<br><br>

        $$\begin{cases}

        29 = a_{0} + 6a_{1}
        \\ 21 = a_{0} + 7a_{1}
        \\ 29 = a_{0} + 11a_{1}

        \end{cases} $$

        Para encontrar os parâmetros da reta, devemos resolver o sistema: 

        $$
        X^TX \hat{a} = X^TY,
        $$

        ou seja, 

        <div class="equacao">
        $$
        \begin{bmatrix}
        1 & 1 & 1
        \\ 6 & 7 & 11
        \end{bmatrix}
        \begin{bmatrix}
        1 & 6
        \\1 & 7
        \\ 1 & 11
        \end{bmatrix}
        \begin{bmatrix}
        a_0
        \\a_1
        \end{bmatrix}
        =
        \begin{bmatrix}
        1 & 1 & 1
        \\ 6 & 7 & 11
        \end{bmatrix}
        \begin{bmatrix}
        29
        \\21
        \\29
        \end{bmatrix}.
        $$
        </div>

        Resolvendo esses produtos, chegamos a: 

        <div class="equacao">
        $$
        \begin{bmatrix}
        3 & 24
        \\ 24 & 206
        \end{bmatrix}
        \begin{bmatrix}
        a_0
        \\a_1
        \end{bmatrix}
        =
        \begin{bmatrix}
        79
        \\ 640
        \end{bmatrix}.
        $$ 
        </div>

        Resolvendo esse sistema, achamos que <br><br>

        $$a_0 = 21.7619,$$
        $$a_1 = 0.5714.$$ <br>

        Em consequência, a melhor reta que ajusta esse conjunto de pontos é: <br><br>

        $$y = 21.7619 + 0.5714x.$$ <br>

        A Figura abaixo ilustra o resultado obtido: <br><br>

        <p style="text-align: center;">
          <img align="center" src="imagens/mmqlinex_2.svg" style="width: 100%;"> <br><br>
        </p>


        Para calcular o erro quadrático (resíduo) cometido na aproximação, definimos o módulo do erro quadrático $|\vec e|^2 = e^Te$, onde $\vec e$ é o vetor coluna do erro dado pela diferença entre os valores coletados de $y$ e os valores obtidos na aproximação: 

        <div class="equacao">
        $$
        \vec e =
        \begin{bmatrix}
        29
        \\21
        \\29
        \end{bmatrix}
        -
        \begin{bmatrix}
        21.7619 + 0.5714*6
        \\21.7619 + 0.5714*7
        \\21.7619 + 0.5714*11
        \end{bmatrix}
        =
        \begin{bmatrix}
        29
        \\21
        \\29
        \end{bmatrix}
        -
        \begin{bmatrix}
        25.1903
        \\25.7617
        \\28.0473
        \end{bmatrix}
        =
        \begin{bmatrix}
        3.8095
        \\-4.7619
        \\ 0.9524
        \end{bmatrix}
        $$
        </div>

        Logo, o erro quadrático é dado por 

        <div class="equacao">
        $$
        \begin{aligned}
        |\vec e|^2 &=
        \begin{bmatrix}
        3.8095
        &-4.7619
        & 0.9524
        \end{bmatrix}
        \begin{bmatrix}
        3.8095
        \\-4.7619
        \\ 0.9524
        \end{bmatrix}
        \\
        &= 38.0952.
        \end{aligned}
        $$ 
        </div>
      </div>
    </div>
  </div>



  <div class="row">
    <div class="col-xs-12">
      <div id="w0" class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content" id="conteudo">
          <p>Considerando a tabela abaixo, encontre a melhor reta que representa o conjunto de pontos.</p>
          <br>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>${y}$</th>
                <th>${x}$</th>
              </tr>
            </thead>
            <tbody>

              <tr>
                <td>${2}$</td>
                <td>${0.25}$</td>
              </tr>

              <tr>
                <td>${3}$</td>
                <td>${2}$</td>
              </tr>

              <tr>
                <td>${4}$</td>
                <td>${4}$</td>
              </tr>

              <tr>
                <td>${5}$</td>
                <td>${3.35}$</td>
              </tr>

              <tr>
                <td>${6}$</td>
                <td>${6}$</td>
              </tr>

            </tbody>
          </table>


          <p>Solução:</p>
          <form id="geo">
            <input value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

            <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">
          </form>
          <div id="applet_container" align="center"></div>

          <br>

        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <?= ExercicioWidget::widget([
      'titulo' => 'Exercícios - Regressão Linear',
      'todosExercicios' => $todosExercicios[10],
      'modeloResposta' => $modeloResposta,
    ]) ?>
  </div>
</div>

<script type="text/javascript">
  function diminuirFonte() {
    document.getElementById("conteudo").style.fontSize = "1em";
  }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
  var n = 0;
  var list = document.getElementById('demo');
  var parameters = {
    "id": "ggbApplet",
    "prerelease": false,
    "width": 800,
    "height": 600,
    "showToolBar": false,
    "borderColor": null,
    "showMenuBar": false,
    "showAlgebraInput": false,
    "showResetIcon": true,
    "enableLabelDrags": false,
    "enableShiftDragZoom": false,
    "enableRightClick": false,
    "capturingThreshold": null,
    "showToolBarHelp": false,
    "errorDialogsActive": true,
    "useBrowserForJS": true,
    "filename": "<?= Url::base() ?>/geogebra/MMQ_RLinear_.ggb"
  };
  var views = {
    'is3D': 0,
    'AV': 1,
    'SV': 0,
    'CV': 0,
    'EV2': 0,
    'CP': 0,
    'PC': 0,
    'DA': 0,
    'FI': 0,
    'PV': 0,
    'macro': 0
  };
  var applet = new GGBApplet(parameters, '5.0');
  //when used with Math Apps Bundle, uncomment this:
  //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

  window.onload = function() {
    applet.inject('applet_container');
  }


  function ggbOnInit(param) {
    if (param == "ggbApplet") {
      ggbApplet.setCoordSystem(-1, 9, -8, 8);
      ggbApplet.setValue('bissec', true); // Valor de h
      ggbApplet.setValue('falseposic', false) // Mostra Euler;
      ggbApplet.setValue('newton', false) // Mostra Heun;
      ggbApplet.setValue('secante', false) // Mostra Ponto Medio;
    }
  }

  function mensagens(n) {
    // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
    decimais = 2;
    ggbApplet.setValue('n', this.n)
    if (this.n % 2 == 0) {
      x = ggbApplet.getValue('X(1,0)');
    }



  }

  function anterior() {
    if (n >= 6 && n <= 12) {
      ggbApplet.setCoordSystem(-1, 9, -8, 8);
    } else {
      ggbApplet.setCoordSystem(-1, 9, -8, 8);
    }

    if (this.n % 2 == 0) {
    }
    if (this.n >= 0) {
      this.n--;
      ggbApplet.setValue('n', this.n);
      return true;
    } else {
      alert("Vá para a próxima etapa.");
      return false;
    }


  }

  function proximo() {
    if (n >= 4 && n <= 10) {
      ggbApplet.setCoordSystem(-1, 9, -8, 8);
    } else {
      ggbApplet.setCoordSystem(-1, 9, -8, 8);
    }

    if (this.n <= 5) {
      this.n++;
      ggbApplet.setValue('n', this.n)
      return true;
    } else {
      alert("Fim do exercício.");
      return false;
    }
  }

</script>

<script>
  renderMathInElement(document.body, {
    delimiters: [{
        left: "$$",
        right: "$$",
        display: true
      },
      {
        left: "$",
        right: "$",
        display: false
      }
    ]
  });
</script>