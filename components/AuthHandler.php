<?php
namespace app\components;

#use mdm\admin\models\Auth;
use app\models\Auth;
//use mdm\admin\models\User;
use app\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
#use yii\authclient\Clients\Sigaa;
use app\components\Sigaa;
use app\components\SigaaProfile; 
use app\models\Docentes;
use app\models\Turmas; 
use app\models\TurmasDocentes;
use app\models\Alunos;
use app\models\TurmasAlunos;
use app\models\Vinculos;
use yii\filters\AccessControl;
use yii\db\Expression;
/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        // Busda dados do SIGAA
        $clienteProfile = new SigaaProfile($this->client);

        // Info Usuario
        $infoUsuario = $clienteProfile->getInfoUsuario(); 
        /*
        $infoUsuario = [
            'id-usuario' => '308346',
            'cpf-cnpj' => '2210375436',
            'nome-pessoa' => 'REX ANTONIO DA COSTA MEDEIROS',
            'email' => 'desenv@si.ufrn.br',
            'id-foto' => '85341',
            'chave-foto' => 'f484d738e69b0eb8da51286cc2be4dd2',
        ];
        */
        // Tirar o CPF e recolocar id-usuario
        $idUsuario = $infoUsuario['id-usuario'];
        $cpfCnpj = $infoUsuario['cpf-cnpj'];
        //$idUsuario = $infoUsuario['cpf-cnpj'];
        $nome = $infoUsuario['nome-pessoa'];
        $email = $infoUsuario['email'];
        $perfil['nome'] = $infoUsuario['nome-pessoa'];
        $perfil['email'] = $infoUsuario['email'];
        $perfil['foto_id'] = $infoUsuario['id-foto'];
        $perfil['foto_chave'] = $infoUsuario['chave-foto'];
        $perfil['id-usuario'] = $infoUsuario['id-usuario'];


        $vinculos = $clienteProfile->getVinculos($cpfCnpj); // Vinculos

        // Ver se eh aluno e pega disciplinas 
        $perfil['discente'] = false;
        $turmasDiscente = array();
        if (count($vinculos['discentes'])){
            $perfil['discente'] = true;
            $turmasDiscente = $clienteProfile->getTurmasDiscente($vinculos);            
        }    
  
        // Ver se eh professor, pega turmas 
        $perfil['docente'] = false;
        $turmasDocente = array();
        if (count($vinculos['docentes'])){
            $perfil['docente'] = true;
            $turmasDocente = $clienteProfile->getTurmasDocente($vinculos['docentes'][0]['id']);
        }   

        // Agora autenticacao no site e atualizacao do banco de dados
        $attributes = $this->client->getUserAttributes();
        $id = ArrayHelper::getValue($attributes, 'id');


        /* @var Auth $auth */
        $auth = Auth::find()->where([
            //'source' => $this->client->getId(),
            'source_id' => $cpfCnpj,
        ])->one();


        if ($perfil['docente'] || $perfil['discente']){
            if (Yii::$app->user->isGuest) {
                if ($auth) { // login
                    /* @var User $user */
                    $user = $auth->user;

                    // Atualiza as tabelas docentes, alunos, turmas, etc.
                    $this->atualizaInfoUsuario($user, $perfil, $vinculos, $turmasDiscente, $turmasDocente);

                    // Nao faz login agora
                    //Yii::$app->user->login($user,0);
                } 
                else { // signup 
                    //if ($email !== null && User::find()->where(['email' => $email])->exists()) {
                    if (0) {
                        Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $this->client->getTitle()]),
                        ]);
                    } else {
                        // Mudar isso quando colocar o sistema em producao            
                        $password = Yii::$app->security->generateRandomString(6);
                        $user = new User([
                            'username' => $idUsuario,
                            'cpf_cnpj' => $cpfCnpj,
                            'email' => $email,
                            'password' => $password,
                            'name' => $nome,
                            'photo_id' => $perfil['foto_id'],
                            'photo_key' => $perfil['foto_chave']
                        ]);
                        $user->generateAuthKey();
                        $user->generatePasswordResetToken();

                        $transaction = User::getDb()->beginTransaction();

                        if ($user->save()) {
                            $auth = new Auth([
                                'user_id' => $user->id,
                                'source' => $this->client->getId(),
                                'source_id' => $cpfCnpj,
                            ]);
                            if ($auth->save()) {

                            // Mudar isso quando colocar o sistema em producao            
                                // Atualiza as tabelas docentes, alunos, turmas, etc.
                                $this->atualizaInfoUsuario($user, $perfil, $vinculos, $turmasDiscente, $turmasDocente);
                                // Confirma as insercoes no banco e faz o login
                                $transaction->commit();
                                // Nao faz login aqui, tem que escolher vinculo
                                // Yii::$app->user->login($user, 133445566);
                            } else {
                                Yii::$app->getSession()->setFlash('error', [
                                    Yii::t('app', 'Unable to save {client} account: {errors}', [
                                        'client' => $this->client->getTitle(),
                                        'errors' => json_encode($auth->getErrors()),
                                    ]),
                                ]);
                            }
                        } else {
                            Yii::$app->getSession()->setFlash('error', [
                                Yii::t('app', 'Unable to save user: {errors}', [
                                    'client' => $this->client->getTitle(),
                                    'errors' => json_encode($user->getErrors()),
                                ]),
                            ]);
                        }
                    }
                }
            } 
            else { // user already logged in
                if (!$auth) { // add auth provider
                    $auth = new Auth([
                        'user_id' => Yii::$app->user->id,
                        'source' => $this->client->getId(),
                        'source_id' => $cpfCnpj
                    ]);
                    if ($auth->save()) {
                        /** @var User $user */
                        $user = $auth->user;
                        // Atualiza as tabelas docentes, alunos, turmas, etc.
                        $this->atualizaInfoUsuario($user, $perfil, $vinculos, $turmasDiscente, $turmasDocente);
                        Yii::$app->getSession()->setFlash('success', [
                            Yii::t('app', 'Linked {client} account.', [
                                'client' => $this->client->getTitle()
                            ]),
                        ]);
                    } else {
                        Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', 'Unable to link {client} account: {errors}', [
                                'client' => $this->client->getTitle(),
                                'errors' => json_encode($auth->getErrors()),
                            ]),
                        ]);
                    }
                } 
                else { // there's existing auth
                    // Nesse caso, nada a fazer. Tudo OK
                    /*
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app',
                            'Usuário \'{usuario}\' atualmente logado. Assim, não é possível fazer login com a conta do {client}.',
                            ['usuario' => Yii::$app->user->shortName, 'client' => $this->client->getTitle()]),
                    ]);
                    */
                }
            }
        }
        else{
            Yii::$app->getSession()->setFlash('error', [
                            Yii::t('app', "Somente docentes ou alunos matriculados nos cursos de Computação ou Cálculo Numérico podem ter acesso a esse sistema"),
                        ]);
        }    
    }


    /**
     * @param Int $idTurmaCn
    */
    private function atualizaTurma($turma)
    {
        $turmaBd = Turmas::find()->where([
                'id_turma' => $turma['idTurma'],
            ])->one();
        if ($turmaBd){
            return $turmaBd->id;
        }
        else{
            // Nao existe, cria
            $novaTurma = new Turmas([
                'id_turma' => $turma['idTurma'],
                'codigo' => $turma['codigo'],
                'nome' => $turma['nome'],
                'horario' => $turma['horario'],
                'turma' => $turma['turma'],
                'subturma' => $turma['subturma'],
                'ano' => $turma['ano'],
                'periodo' => $turma['periodo'],
                'checagem' => 0,
                'hora_checagem' => new Expression('UNIX_TIMESTAMP(NOW())'),
            ]);
            $novaTurma->save();
            return $novaTurma->id;
        }
    }

    /**
     * @param Int $idTurmaCn
    */
    private function cadastroDocenteTurma($vinculo_id, $turma_id, $ano, $periodo)
    {
        $turmasDocente = TurmasDocentes::find()->where([
                'vinculo_id' => $vinculo_id,
                'turma_id' => $turma_id,
        ])->one();
        if (!$turmasDocente){
            $turmasDocente = new TurmasDocentes();
            $turmasDocente->vinculo_id = $vinculo_id;
            $turmasDocente->turma_id = $turma_id;
            $turmasDocente->ano = $ano;
            $turmasDocente->periodo = $periodo;
            $turmasDocente->save();    
        }
        //return $TurmasDocentes->id;
    }    

    /**
     * @param Int $idTurmaCn
    */
    private function atualizaTurmasDiscente($turmasDiscente)
    {
        // Pega o ano/periodo da matricula
        foreach ($turmasDiscente as $idAluno => $turmas){
            if (count($turmas['disciplinas'])){
                foreach ($turmas['disciplinas'] as $matricula){
                    $ano = $matricula['ano'];
                    $periodo = $matricula['periodo'];
                    break 2;                               
                }
            }
        }

        // Se nao estah setado, indica que o ano nao esta matriculado em nenhuma disciplica
        if (isset($ano) and isset($periodo)){
            foreach ($turmasDiscente as $idAluno => $turmas){
            // Precisa encontrar o vinculo_id
            $vinculo = Vinculos::find()->where(['id_vinculo' => $idAluno])->one();
            $idVinculo = $vinculo->id;            
            // Busca todas as matriculas do vinculo no BD
            $todosMatriculasBd = TurmasAlunos::find()->where([
                'vinculo_id' => $idVinculo,
                'ano' => $ano,
                'periodo' => $periodo,
                //'turma_id' => $idTurma,
            ])->all();
            // Pega somente os turma_id das turmas
            $idsTurmasIdBd = array();
            if ($todosMatriculasBd){
                foreach ($todosMatriculasBd as $matriculaBd){
                    $temp = $matriculaBd->turma->id_turma;
                    $idsTurmasIdBd[] = $temp;
                    // Para o caso de ter que excluir a matricula
                    $idsIdBd[$temp] = $matriculaBd->id;
                    $idsMatriculaBd[$temp] = $matriculaBd->matricula;
                }
            }
            //echo $idVinculo;
            //print_r($idsTurmasIdBd);
            

            // Verifica cada uma das matriculas retornadas pelo SIGAA
            // para verificar se estao no BD local. Se nao estiver, coloca
            $idsTurmasIdSigaa = array();
            if (count($turmas['disciplinas'])){
                foreach ($turmas['disciplinas'] as $idTurma => $matricula){
                    $idsTurmasIdSigaa[] = $idTurma;
                    if (in_array($idTurma, $idsTurmasIdBd)){
                        // A matricula ja esta no nosso BD. Veja Proxima ...
                        continue;
                    }
                    else{
                        // Verifica se a turma jah existe, senao, cria
                        $turmaBdId = $this->atualizaTurma($matricula);
                        // Insere a matricula no BD local
                        $novaTurmaAluno = new TurmasAlunos([
                            'vinculo_id' => $idVinculo,
                            'matricula' => $turmas['matricula'],
                            'turma_id' => $turmaBdId,
                            'ano' => $ano,
                            'periodo' => $periodo,
                        ]);
                        $novaTurmaAluno->save();
                        // Atualiza checksum (checagem)
                        $turmaBd = Turmas::findOne($turmaBdId);
                        $turmaBd->updateCounters(['checagem' => $turmas['matricula']]);
                        //$turmaBd->hora_checagem = new Expression('NOW()');
                        $turmaBd->save();
                        $idsTurmasIdBd[] = $idTurma;
                    }
                }
            }
            
            // Agora verifica se alguma matricula deve ser excluida do BD local
            $matriculasExcluir = array_diff($idsTurmasIdBd, $idsTurmasIdSigaa);
            if (count($matriculasExcluir)){
                foreach ($matriculasExcluir as $idTurmaExcluir){
                    // Encontra matricula
                    $matriculaAluno = TurmasAlunos::findOne($idsIdBd[$idTurmaExcluir]);
                    // Encontra turma correspondete para atualizar a checagem.
                    $turmaRespectiva = $matriculaAluno->turma;
                    $turmaRespectiva->updateCounters(['checagem' => -$idsMatriculaBd[$idTurmaExcluir]]);
                    $turmaRespectiva->save();
                    // Apaga a matricula
                    $matriculaAluno->delete();
                }
            }
            
        }            
        }
        
    }    


    /**
     * @param Turmas $turmaBd
     * @param Array $participantesSigaa
    */
    private function atualizaMatriculasTurma($turmaBd,$participantesSigaa)
    {
        // Pega os vinculo_ID dos participantes do SIGAA para ver se jah
        // tem todos os usuarios locais

        $idVinculoSIGAA = array();
        if (count($participantesSigaa)){
            foreach ($participantesSigaa as $participanteSigaa){
                $temp = $participanteSigaa['id'];
                $idVinculoSIGAA[] = $temp;
                $dadosVinculosSIGAA[$temp] =  $participanteSigaa;
                // Para usar numa comparacao fora bem abaixo
                $temp = $participanteSigaa['matricula'];
                $participantesSigaaMatriculas[$temp] = $participanteSigaa;
            }
            $vinculosExistentesTemp = Vinculos::find()
                    ->select(['id_vinculo'])
                    ->where(['in','id_vinculo',$idVinculoSIGAA])
                    ->asArray()
                    ->all(); 
            $vinculosExistentesBd = array();
            if (count($vinculosExistentesTemp)){
                foreach ($vinculosExistentesTemp as $vinculoExistentesTemp){
                    $vinculosExistentesBd[] = $vinculoExistentesTemp['id_vinculo'];
                }    
            }
            // Ver quais usuarios precisa criar ... e cria
            $vinculosCriar = array_diff($idVinculoSIGAA, $vinculosExistentesBd);
            if (count($vinculosCriar)){
                // Prepara DAO - Cria primeiro os users
                $i = 0;
                foreach($vinculosCriar as $id_vinculo){
                    $dadosUsuario[$i][] = $dadosVinculosSIGAA[$id_vinculo]['idUsuario']; //username
                    $dadosUsuario[$i][] = $dadosVinculosSIGAA[$id_vinculo]['idUsuario']; //cpf_cnpj
                    $dadosUsuario[$i][] = $dadosVinculosSIGAA[$id_vinculo]['email'];     // email
                    $dadosUsuario[$i][] = time();     // email
                    $dadosUsuario[$i][] = time();     // email
                    //$tempPass = "saKJ393skjDjfd".mt_rand(100000,9999999);
                    //$dadosUsuario[$i][] = Yii::$app->security->generatePasswordHash($tempPass);  //password_hash 
                    $dadosUsuario[$i][] = "$2y$13$".Yii::$app->security->generateRandomString(53)."sfd";
                    $dadosUsuario[$i][] = $dadosVinculosSIGAA[$id_vinculo]['nome'];    // name
                    $dadosUsuario[$i][] = $dadosVinculosSIGAA[$id_vinculo]['idFoto'];   // photo_id
                    $dadosUsuario[$i][] = $dadosVinculosSIGAA[$id_vinculo]['chaveFoto']; // photo_key
                    $dadosUsuario[$i][] = Yii::$app->security->generateRandomString();  //auth_key
                    $dadosUsuario[$i][] = Yii::$app->security->generateRandomString() . '_' . time(); //password_reset_token
                    $dadosUsuario[$i][] = $id_vinculo; //vinculo_id
                    $i++;   
                    $cpfCriados[] = $dadosVinculosSIGAA[$id_vinculo]['idUsuario'];
                }
                // Usa DAO para isso
                Yii::$app->db->createCommand()->batchInsert('user', 
                        ['username', 'cpf_cnpj', 'email', 'created_at', 'updated_at','password_hash', 'name', 'photo_id', 'photo_key', 'auth_key', 'password_reset_token', 'vinculo_id'], 
                        $dadosUsuario
                    )->execute();   

                // Agora busca todos os usuarios recentemente criados para criar os vinculos e auth
                // Prepara o DAO para inserir na tabela vinculos
                $usuariosCriados = User::find()->where(['in', 'cpf_cnpj', $cpfCriados])->all();
                $i = 0;
                foreach ($usuariosCriados as $usuarioCriado){
                    $dadosParaAuth[$i][] = $usuarioCriado->id; //user_id
                    $dadosParaAuth[$i][] = 'sigaa'; //source
                    $dadosParaAuth[$i][] = $dadosVinculosSIGAA[$usuarioCriado->vinculo_id]['idUsuario']; //source_id
                    $dadosParaVinculo[$i][] = $usuarioCriado->id; //user_id
                    $dadosParaVinculo[$i][] = $usuarioCriado->vinculo_id; //id_vinculo cuidado!
                    $dadosParaVinculo[$i][] = $dadosVinculosSIGAA[$usuarioCriado->vinculo_id]['matricula']; //matricula
                    $dadosParaVinculo[$i][] = $dadosVinculosSIGAA[$usuarioCriado->vinculo_id]['curso']; //descricao
                    $dadosParaVinculo[$i][] = 0; //docente
                    $dadosParaVinculo[$i][] = 0;
                    $dadosParaVinculo[$i][] = 0;
                    $i++;
                }
                // Usa DAO para isso
                Yii::$app->db->createCommand()
                    ->batchInsert('vinculos', 
                        ['user_id', 'id_vinculo', 'matricula', 'descricao', 'docente', 'ano_ingresso', 'periodo_ingresso'], $dadosParaVinculo)
                    ->execute(); 
                Yii::$app->db->createCommand()
                    ->batchInsert('auth', 
                        ['user_id', 'source', 'source_id'], $dadosParaAuth)
                    ->execute();  
                

            }
        }

        // Busca participantes (vinculomatriculas) do Banco de dados daquela turma
        // e coloca o resultado numa matriz para comparacao
        $matriculasBd = $turmaBd->turmasAlunos;
        $matriculasBdMatriculas = array();
        if ($matriculasBd){
            foreach ($matriculasBd as $matriculaBd){
                $temp = $matriculaBd->matricula;
                $matriculasBdMatriculas[$temp] = $matriculaBd->id;
            }    
        }
        $matriculasBdSomenteMatriculas = array_keys($matriculasBdMatriculas);

        // Coloca os participantes do SIGAA numa matriz para comparacao
        // Isso jah foi feito antes, lah em cima
        /*
        foreach ($participantesSigaa as $participanteSigaa){
            $temp = $participanteSigaa['matricula'];
            $participantesSigaaMatriculas[$temp] = $participanteSigaa;
        }
        */
        $participantesSigaaSomenteMatriculas = array_keys($participantesSigaaMatriculas);

        // Agora usa funcoes do PHP para comparar
        // Quem precisa matricular
        $matricular = array_diff($participantesSigaaSomenteMatriculas,$matriculasBdSomenteMatriculas);
        // Quem precisa excluir matricula
        $excluirMatricula = array_diff($matriculasBdSomenteMatriculas,$participantesSigaaSomenteMatriculas);

        // Excluir matricula eh mais facil!
        if (count($excluirMatricula)){
            foreach ($excluirMatricula as $matricula){
                // pega os IDs das linhas a excluir
                $excluirIds[] = $matriculasBdMatriculas[$matricula];
            }

            TurmasAlunos::deleteAll(['in', 'id', $excluirIds]);
        }
        
        // Insere matriculas na turma
        if (count($matricular)){
            // Procura os vinculos no BD
            $vinculosInserir = Vinculos::find()->where(['in','matricula', $matricular])->all();
            $i = 0;
            foreach ($vinculosInserir as $vinculo){
                $vinculosInserirDAO[$i][] = $vinculo->id;
                $vinculosInserirDAO[$i][] = $vinculo->matricula;
                $vinculosInserirDAO[$i][] = $turmaBd->id;
                $vinculosInserirDAO[$i][] = $turmaBd->ano;
                $vinculosInserirDAO[$i][] = $turmaBd->periodo;
                $i++;
            }
            Yii::$app->db->createCommand()
                    ->batchInsert('turmas_alunos', 
                        ['vinculo_id', 'matricula', 'turma_id', 'ano', 'periodo'], $vinculosInserirDAO)
                    ->execute(); 

        }

        // Agora soh atualiza o contador de chegagem
        $turmaBd->checagem = array_sum($participantesSigaaSomenteMatriculas);
        $turmaBd->save();
    }

    /**
     * @param Turmas $turmaBd
     * @param Array $participantesSigaa
    */
    private function atualizaMatriculasTurmaOld($turmaBd,$participantesSigaa)
    {
        // Busca participantes (matriculas) do Banco de dados daquela turma
        // e coloca o resultado numa matriz para comparacao
        $matriculasBd = $turmaBd->turmasAlunos;
        $matriculasBdMatriculas = array();
        if ($matriculasBd){
            foreach ($matriculasBd as $matriculaBd){
                $temp = $matriculaBd->matricula;
                $matriculasBdMatriculas[$temp] = $matriculaBd->id;
            }    
        }
        $matriculasBdSomenteMatriculas = array_keys($matriculasBdMatriculas);

        // Coloca os participantes do SIGAA numa matriz para comparacao
        foreach ($participantesSigaa as $participanteSigaa){
            $temp = $participanteSigaa['matricula'];
            $participantesSigaaMatriculas[$temp] = $participanteSigaa;
        }
        $participantesSigaaSomenteMatriculas = array_keys($participantesSigaaMatriculas);

        // Agora usa funcoes do PHP para comparar
        // Quem precisa matricular
        $matricular = array_diff($participantesSigaaSomenteMatriculas,$matriculasBdSomenteMatriculas);
        // Quem precisa excluir matricula
        $excluirMatricula = array_diff($matriculasBdSomenteMatriculas,$participantesSigaaSomenteMatriculas);

        // Excluir matricula eh mais facil!
        foreach ($excluirMatricula as $matricula){
            // Encontra a matricula
            $matriculaParaExcluir = TurmasAlunos::findOne($matriculasBdMatriculas[$matricula]);
            // Busca turma e atualiza o contador da checagem
            $turmaRespectiva = $matriculaParaExcluir->turma;
            $turmaRespectiva->updateCounters(['checagem' => -$matricula]);
            $turmaRespectiva->save();
            // Apaga a matricula
            $matriculaParaExcluir->delete();
        }

        // Inserir matricula
        foreach ($matricular as $matricula){
            // Primeiro verifica se o user e vinculo ja existem no BD;
            $vinculo = Vinculos::find()->where(['matricula' => $matricula])->one();
            if ($vinculo){
                $vinculoId = $vinculo->id;
            }
            else{
                // Ixi ... tem que criar um User e depois o Vinculos
                $password = Yii::$app->security->generateRandomString(6);
                $novoUser = new User([
                    'username' => $participantesSigaaMatriculas[$matricula]['idUsuario'],
                    'cpf_cnpj' => $participantesSigaaMatriculas[$matricula]['idUsuario'],
                    'email' => $participantesSigaaMatriculas[$matricula]['email'],
                    'password' => $password,
                    'name' => $participantesSigaaMatriculas[$matricula]['nome'],
                    'photo_id' => $participantesSigaaMatriculas[$matricula]['idFoto'],
                    'photo_key' => $participantesSigaaMatriculas[$matricula]['chaveFoto'],
                ]);
                $novoUser->generateAuthKey();
                $novoUser->generatePasswordResetToken();
                $novoUser->save();
                // Tabela Auth
                $tempN = $participantesSigaaMatriculas[$matricula]['idUsuario'];
                $auth = new Auth([
                    'user_id' => $novoUser->id,
                    'source' => $this->client->getId(),
                    'source_id' => "$tempN",
                ]);
                $auth->save();
                // Tabela Vinculos
                $novoVinculo = new Vinculos([
                    'user_id' => $novoUser->id,
                    'id_vinculo' => $participantesSigaaMatriculas[$matricula]['id'],
                    'matricula' => $participantesSigaaMatriculas[$matricula]['matricula'],
                    'descricao' => $participantesSigaaMatriculas[$matricula]['curso'],
                    'ano_ingresso' => 0,
                    'periodo_ingresso' => 0,
                    'docente' => 0,
                ]);
                $novoVinculo->save();
                $vinculoId = $novoVinculo->id;
            }
            // Agora faz a matricula
            $novaMatricula = new TurmasAlunos([
                'vinculo_id' => $vinculoId,
                'matricula' => $matricula,
                'turma_id' => $turmaBd->id,
                'ano' => $turmaBd->ano,
                'periodo' => $turmaBd->periodo,
            ]);  
            $novaMatricula->save();
            // Atualiza contador checagem  
            $turmaBd->updateCounters(['checagem' => $matricula]);
            $turmaBd->save();        
        }
    }

    /**
     * @param Array $turmasDocente
     * @param Int $idVinculo
    */
    private function atualizaTurmasDocente($turmasDocente, $vinculoId)
    {

        // Pega o id do vinculo
        $vinculo = Vinculos::find()->where(['id_vinculo' => $vinculoId])->one();
        $idVinculo = $vinculo->id;  

        // Pega o ano/periodo das turmas
        if (count($turmasDocente)){
            foreach ($turmasDocente as $idTurma => $turma){
                $ano = $turma['ano'];
                $periodo = $turma['periodo'];
                break;                               
            }            
        }
        else{
            // Se não tem turma, sai e retorna false
            return false;
        }

        $turmasDocenteIdsTurmas = array_keys($turmasDocente);

        // Verifica se o docente esta atribuido a alguma turma
        // no BD local e nao mais estah no SIGAA.
        // Alem disso, verifica quais turmas nao necessita de verificar o checksum
        // So precisa verificar 1x a cada 24h
        $TurmasDocentesBd = TurmasDocentes::find()->where([
                'vinculo_id' => $idVinculo,
                'ano' => $ano,
                'periodo' => $periodo,
                //'turma_id' => $idTurma,
        ])->all();
        $skipTurma = array();
        foreach ($TurmasDocentesBd as $TurmaDocentesBd){
            if (!in_array($TurmaDocentesBd->turma->id_turma, $turmasDocenteIdsTurmas)){
                // Nao estah ... apague
                $TurmaDocentesBd->delete();
            }
            $now = (new \yii\db\Query)->select(new Expression('UNIX_TIMESTAMP(NOW())'))->scalar();
            if ($TurmaDocentesBd->turma->hora_checagem > ($now - 60400)) {
                $temp = $TurmaDocentesBd->turma->id_turma;
                $skipTurma[$temp] = $TurmaDocentesBd->turma->id;
            }
        }
        $skipTurmaIdTurma = array_keys($skipTurma);

        // Busca os integrantes de cada turma no SIGAA
        // Faz calculo do Checksum
        $clienteProfile = new SigaaProfile($this->client);
        foreach ($turmasDocente as $idTurma => $turma){
            // Ver a questao das 24h
            if (in_array($idTurma,$skipTurmaIdTurma))
            continue;
            // Senao, tem que checar
            $participantesSigaa[$idTurma] = $clienteProfile->getParticipantesDiscentes($idTurma); 

            if (count($participantesSigaa[$idTurma])){
                $checagem = 0;
                foreach ($participantesSigaa[$idTurma] as $participante){
                    $checagem += $participante['matricula'];
                }
                $turmasDocente[$idTurma]['checagem'] = $checagem;
            }
            else{
                // A API nao retornou nenhum aluno na turma. Portanto, nao comparar
                // com o banco de dados local  (skip!)
                $skipTurmaIdTurma[] = $idTurma;
            }
        } 

        // Para cada turma do SIGAA, veja se esta atualizada no BD local
        foreach ($turmasDocente as $idTurma => $turma){
            // Ver a questao das 24h
            if (in_array($idTurma,$skipTurmaIdTurma))
            continue;
            // Senao, tem que checar
            $turmaBd = Turmas::find()->where([
                    'id_turma' => $idTurma,
            ])->one();
            if ($turmaBd){
                // turma existe
                // Atualiza cadastro do docente na turma
                $this->cadastroDocenteTurma($idVinculo, $turmaBd->id, $ano, $periodo);
                // faz o checksum
                if ($turmaBd->checagem != $turma['checagem']){
                    // Nao passou no teste da checagem das matriculas. Deve atualizar alunos
                    $this->atualizaMatriculasTurma($turmaBd,$participantesSigaa[$idTurma]);
                    // TODO
                }

            }
            else{
                // Verifica se a turma jah existe, senao, cria
                $turmaBdId = $this->atualizaTurma($turma);
                // Busca objeto da turma criada e atualiza matriculas
                $turmaBd = Turmas::findOne($turmaBdId);
                $this->atualizaMatriculasTurma($turmaBd,$participantesSigaa[$idTurma]);
                // Atualiza cadastro do docente na turma
                $this->cadastroDocenteTurma($idVinculo, $turmaBd->id, $ano, $periodo);
            }
            // Tah atualizada, atualiza a checagem Timestamp
            $turmaBd->hora_checagem = new Expression('UNIX_TIMESTAMP(NOW())');
            $turmaBd->save();
        }
        
    }    

    /**
     * @param User $user
     * @param Array $vinculos
     */
    private function atualizaVinculos(User $user, $vinculos)
    {
        // uniformiza a array vinculos
        $vinculosSIGAA = array();
        $i = 0;
        if (count($vinculos['docentes'])){
            foreach ($vinculos['docentes'] as $temp){
                $vinculosSIGAA[$i]['id'] = $temp['id'];
                $vinculosSIGAA[$i]['matricula'] = $temp['siape'];
                $vinculosSIGAA[$i]['descricao'] = $temp['lotacao'];
                $vinculosSIGAA[$i]['docente'] = 1;
                $vinculosSIGAA[$i]['ano_ingresso'] = 0;
                $vinculosSIGAA[$i]['periodo_ingresso'] = 0;
                $idsSigaa[] = $temp['id'];
                $siapesSigaa[] = 
                $i++;
           }
        }
        if (count($vinculos['discentes'])){
            foreach ($vinculos['discentes'] as $temp){
                $vinculosSIGAA[$i]['id'] = $temp['id'];
                $vinculosSIGAA[$i]['matricula'] = $temp['matricula'];
                $vinculosSIGAA[$i]['descricao'] = $temp['curso'];
                $vinculosSIGAA[$i]['docente'] = 0;
                $vinculosSIGAA[$i]['ano_ingresso'] = $temp['anoIngresso'];;
                $vinculosSIGAA[$i]['periodo_ingresso'] = $temp['periodoIngresso'];;
                $idsSigaa[] = $temp['id'];
                $i++;
            }
        }

        // Apaga os vinculos que estao no DB e nao estao no SIGAA
        Yii::$app->db->createCommand()->delete(
            'vinculos', ['and', ['user_id' => $user->id], ['not in', 'id_vinculo', $idsSigaa]]
        )->execute();

        // Desativa todos os vinculos do usuario (user->id)
        // Para ser reativado antes do Login
        Vinculos::updateAll(['ativo' => 0], 'user_id = '.$user->id);
        

        // Busca todos os vinculos restantes no BD
        $todosVinculosBD = Vinculos::find()->select('id_vinculo')->where(['user_id' => $user->id])->asArray()->all();
        // pega todos os vinculos
        $idsDB = array();
        if ($todosVinculosBD){
            foreach ($todosVinculosBD as $vinculoDB){
                $idsDB[] = $vinculoDB['id_vinculo'];
            }
        }

        // Adiciona vinculo, se necessario
        if (count($vinculosSIGAA)){
            foreach ($vinculosSIGAA as $vinculoSigaa){
                if (!in_array($vinculoSigaa['id'], $idsDB)){
                    $novoVinculo = new Vinculos();
                    $novoVinculo->id_vinculo = $vinculoSigaa['id'];
                    $novoVinculo->user_id = $user->id;
                    $novoVinculo->matricula = $vinculoSigaa['matricula'];
                    $novoVinculo->descricao = $vinculoSigaa['descricao'];
                    $novoVinculo->docente = $vinculoSigaa['docente'];
                    $novoVinculo->ano_ingresso = $vinculoSigaa['ano_ingresso'];
                    $novoVinculo->periodo_ingresso = $vinculoSigaa['periodo_ingresso'];
                    $novoVinculo->save();
                }
            }
        }
        
        if ($i) return true;
        else return false;
    }

    /**
     * @param User $user
     */
    private function atualizaInfoUsuario(User $user, $perfil, $vinculos, $turmasDiscente, $turmasDocente)
    {
        
        $attributes = $this->client->getUserAttributes();
        $user->name = $perfil['nome'];
        $user->username = $perfil['id-usuario'];
        $user->save();

        // Gerenciamento de autorização
        $autorizacao = \Yii::$app->authManager;

        // Atualiza os vinculos
        // retorna falso se nao eh nem aluno nem docente ou se
        // o Sigaa nao retornou nenhum vinculo. Nesse caso, pare 
        $temVinculo = $this->atualizaVinculos($user,$vinculos);

        // Insere dados do Docente
        if ($perfil['docente']){

            // Atualiza a tabela Turmas, se necessario 
            $this->atualizaTurmasDocente($turmasDocente,$vinculos['docentes'][0]['id']);            
            // Agora atribui os papeis para os usuarios, se necessario
            if (!( $autorizacao->getRolesByUser($user->id) )){
                $docenteRole = $autorizacao->getRole('docente');
                $autorizacao->assign($docenteRole, $user->id);
            }
        }

        // Insere dados se discente
        if ($perfil['discente']){


            // Atualiza a tabela Turmas, se necessario
            //  e se a matriz nao vier fazia
            $this->atualizaTurmasDiscente($turmasDiscente);

            
            // Agora atribui os papeis para o aluno, se necessario
            if (!( $autorizacao->getRolesByUser($user->id) )){
                $discenteRole = $autorizacao->getRole('aluno');
                $autorizacao->assign($discenteRole, $user->id);
            }
        }
         // Atualiza foto
        $user->photo_id = $perfil['foto_id'];
        $user->photo_key = $perfil['foto_chave'];
        $user->save();   
        

             
    }

    /**
     * @param Array $disciplinas
     * @param Array $codigosCN
     */
    public function buscarIdUsuario()
    {
        $clienteProfile = new SigaaProfile($this->client);
        $info = $clienteProfile->getInfoUsuario();
        return $info['id-usuario'];
        //return $info['cpf-cnpj'];
    }

    /**
     * @param Array $disciplinas
     * @param Array $codigosCN
     */
    public function buscarCpfCnpj()
    {
        $clienteProfile = new SigaaProfile($this->client);
        $info = $clienteProfile->getInfoUsuario();
        return $info['cpf-cnpj'];
    }
}



















