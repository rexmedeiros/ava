

<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\ExercicioWidget;

/* https://pt.wikipedia.org/wiki/Integra%C3%A7%C3%A3o_num%C3%A9rica 
https://www.math.tecnico.ulisboa.pt/~calves/courses/integra/
https://www.math.tecnico.ulisboa.pt/~calves/courses/integra/capiii32.html */ 

$this->title = 'Integração Numérica';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Regra do Trapézio </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

A ideia principal da Regra do Trapézio é aproximar a função por um polinômio de primeiro grau, ou seja uma reta. Define-se
a reta de forma que passe por $(a, f(a))$ e $(b, f(b))$, onde $a$ e $b$ definem o intervalo de integração.


<br><br>
Desta forma, a integral é a área delimitada entre a reta e o eixo $x$ no intervalo $a,b$. O mesmo que a área de um trapézio.
<br><br>
A área pode ser aproximada por:
<br><br>
$${\int_{a}^{b}} { \;P_{1}dx } \; \; = \; \; \frac{h}{2}  [f(a)+f(b)],$$
<br>
Sendo o comprimento do subintervalo, $h = b - a$.
<br><br>
 
Nessa aproximação, o erro cometido no valor da integral ao aproximar a função $f(x)$ pela reta no intervalo $[a,b]$ pode ser expresso por:


<br><br>
$$ \varepsilon= -\frac{1}{12} f''(\tau)h^3,$$
<br>
em que $\tau $ é um valor entre $a$ e $b$, ou seja, $a\leq \tau\leq b$.
<br><br> 
</div>
        </div>
    </div>
</div>




<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            Suponha que desejamos calcular numericamente o valor de 
            $$
                I = \int_a^b \frac{1}{x}dx
            $$
            para $a>0$ usando o método do trapézio.
            <br><br>
            Sabemos que o valor exato da integral é $I = \ln(b) - \ln(a)$ e o valor aproximado pela regra do trapézio é
            $$
            I \approx \frac{b - a}{2}\left(\frac{1}{a}+\frac{1}{b}\right).
            $$
            Use a aplicação do GeoGebra abaixo para variar os valores de $a$ e $b$ e conferir os valores exato e aproximados da integral usando o método do trapézio, além do valor exato do erro cometido.
            <br><br><br><br>
            COLOCAR O GEOGEGRA DA INTRODUÇÃO AQUI SEM O BOTÃO DE ADICIONAR PONTOS E TROCANDO Xo por a e $x_n$ por $b$. 
            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Regra do Trapézio Composta </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

A Regra do Trapézio apresenta um erro bastante considerável para funções diferentes  de uma reta. Uma forma de diminuir esse erro é utilizar a Regra do Trapézio Composta.
<br><br>
Na Regra Composta, subdivide-se o intervalo inicial $[a,b]$ em $n$ subintervalos, de forma que esses subintervalos tenham largura $h$ igual. Então em cada um deles é calculada a Regra do Trapézio:



 <div class="equacao">
$$A_{1} = \frac{h}{2}[f(x_{0})+f(x_{1})], $$
$$
A_{2} = \frac{h}{2}[f(x_{1})+f(x_{2})],$$
$$...$$
$$
A_{n} = \frac{h}{2}[f(x_{n-1})+f(x_{n})].
$$
</div>
<br>
<p style="
    
 text-align: center;
">


<img src="imagens/integ_trapezio.svg" alt=""   style="width:90%;"/>
</p><br>
Se somarmos cada área calculada, obtêm-se a integral para a Regra do Trapézio Composta:
<br><br>
<div class="equacao">
$$I_{t}= \frac {h}{2}[f(x_{0})+2f(x_{1})+2f(x_{2})+ \;\;... \;+ 2f(x_{n-1})+f(x_{n})].$$ 
</div>

<br>
O erro pode ser calculado como:
<br><br>
$$ \varepsilon= -\frac{n}{12}f''(\tau)h^3,$$
<br>
onde $\tau $ é um valor entre $a$ e $b$, ou seja, $a\leq \tau\leq b$.
<br><br>
</div>
        </div>
    </div>
</div>





<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">

            <p>Calcular a integral abaixo com $\;n = 6 \;$ aplicações da Regra do Trapézio:</p><br>
            $$\int_{-2}^{2} \frac{x^2}{5} +2\sin(x)dx.$$ 


            
            <p>Solução:</p>
             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>

            <br>
           

            </div>
        </div>
    </div>
</div>






<script type="text/javascript"  >
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }

</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":false,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/INTEGRATIONtrappGeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-4,6,-4,4);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false)  // Mostra Euler;
            ggbApplet.setValue('newton',false)  // Mostra Heun;
            ggbApplet.setValue('secante',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}

        if(this.n%2 == 1){
        a = ggbApplet.getValue('A(1,1)').toFixed(decimais);
        b = ggbApplet.getValue('B(1,1)').toFixed(decimais);
    }
        
       
    }

    function anterior (){
            if(n>=5){

             ggbApplet.setCoordSystem(-4,9,-4,4);
        }
        else{

             ggbApplet.setCoordSystem(-4,6,-4,4);
        }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
        if(n>=3){

             ggbApplet.setCoordSystem(-4,9,-4,4);
        }
        else{

             ggbApplet.setCoordSystem(-4,6,-4,4);
        }
        if (this.n <= 13){
            this.n++;
            ggbApplet.setValue('n', this.n)
          
            

             
                
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/L2XaA8y98-A" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/integracaoTrapezio.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método do Trapézio',
          'todosExercicios' => $todosExercicios[7],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>

</div>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>