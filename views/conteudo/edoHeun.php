<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\ExercicioWidget;

/* @var $this yii\web\View */


$this->title = 'Equações Diferenciais - Método de Heun';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Videoaula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/HuRzoQjkZEs" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>  Método de Heun</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p> Iremos entender agora um pouco do Método de Heun, ou Euler modificado, como alguns autores preferem. <br><br>


            Esse método, desenvolvido por Karl Heun, é baseado no método de Euler. Heun percebeu que se fizesse o cálculo de duas inclinações ao invés de uma, então a média entre essas duas inclinações seria uma estimativa mais precisa com vistas a obter a próxima estimativa do valor de $y$.  

            <br> <br>
            Podemos entender melhor o método de Heun através das figuras a seguir:
           </p><br>
           <p style="
  
             text-align: center;
            ">

            <img src="imagens/heun1.svg" style="width: 90%;"> 
             <img src="imagens/heun2.svg" style="width: 90%;"> 
            <br>
            </p>
        
           <p>A inclinação do início do intervalo é dada por: 
           <br/>

           $$
              {k_1 = \left. \frac{\text{d}y}{\text{d}x} \right|_{x = x_i} = g(x_i, y_i)}.
           $$
           <br/>
           A segunda inclinação é uma estimativa da derivada da função no final do intervalo, ou seja, em $x = x_{i+1}$:

            <div class="equacao">
            $$
              {k_2 = \left. \frac{\text{d}y}{\text{d}x} \right|_{x = x_{i + 1}, \tilde{y}_{i+1}} = g(x_{i+1}, \tilde{y}_{i+1})}.
            $$
            </div>  

            Note que para calcular $k_2$ precisamos estimar um valor de $\tilde{y}_{i+1}$ no final do intervalo, ou seja, em $x_{i+1} = x_i + h$. Para isso, utilizamos o valor da primeira inclinação, ${k_1}$:

            $$
              \tilde{y}_{i+1} = y_i + k_1h.
            $$

            Podemos também escrever diretamente

            $$
            \begin{aligned}
              k_2 &= g(x_{i+1}, \tilde{y}_{i+1}) \\
              &= g(x_i + h, y_i + k_1 h).
            \end{aligned}
            $$

            Uma vez calculadas as duas inclinações, a inclinação de Heun é determinada pela média aritmética entre $k_1$ e $k_2$: 
           
            $$
             {k = \frac{1}{2}(k_1 + k_2)}.
            $$

           Finalmente, o calculo do novo ${y}$ será dado por:

           $$
             {y_{i+1} = y_i + kh}.
           $$
           </p>
           <p>Vamos entender melhor esse método através de um exemplo!</p>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 2.1</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
            <p>Use o método de Heun para resolver o PVI</p>
            <div class="equacao">
            $$
            \begin{cases}
                \frac{dy}{dx}= -1.2y + 7e^{-0.3x} = g(x,y)\\
                y(0) = 3
            \end{cases}
            $$
            </div>
            <br/>
            de $a = 0$ até $b = 2.5$ com um passo $h = 0.5$.<br><br>Compare o resultado com a solução analítica: 
            <br/>
            <div class="equacao">
             $$
                y  = \frac{70}{9}e^{-0.3x} - \frac{43}{9}e^{-1.2x}
             $$ 
            </div>
             <b>Solução</b><br />
             <p>A solução analítica está representada pela curva em verde no gráfico a seguir.</p>

            <div id="applet_container" align="center"></div>

            <br>
            <ol id="demo">
                <li>
                Partimos do ponto inicial $(0;3)$, como mostrado no gráfico acima.<br><br><br>
                </li>
            </ol>
             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            </div>
        </div>
    </div>
</div>
<br>










<script type="text/javascript">
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/EDOGeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
            ggbApplet.setValue('Dx',0.5); // Valor de h
            ggbApplet.setValue('t',false)  // Mostra Euler;
            ggbApplet.setValue('u',true)  // Mostra Heun;
            ggbApplet.setValue('v',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
        ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        h = ggbApplet.getValue('Dx').toFixed(decimais);
        x0 = ggbApplet.getValue('x0');
        x1 = ggbApplet.getValue('x1').toFixed(decimais);
        x2 = ggbApplet.getValue('x2').toFixed(decimais);
        x3 = ggbApplet.getValue('x3').toFixed(decimais);
        x4 = ggbApplet.getValue('x4').toFixed(decimais);
        x5 = ggbApplet.getValue('x5').toFixed(decimais);
        x6 = ggbApplet.getValue('x6').toFixed(decimais);
        yh0 = ggbApplet.getValue('y0');
        yh1 = ggbApplet.getValue('yh1').toFixed(decimais);
        yh2 = ggbApplet.getValue('yh2').toFixed(decimais);
        yh3 = ggbApplet.getValue('yh3').toFixed(decimais);
        yh4 = ggbApplet.getValue('yh4').toFixed(decimais);
        yh5 = ggbApplet.getValue('yh5').toFixed(decimais);
        yh6 = ggbApplet.getValue('yh6').toFixed(decimais);
        yv1 = ggbApplet.getValue('Sol(x1)').toFixed(decimais);
        yv2 = ggbApplet.getValue('Sol(x2)').toFixed(decimais);
        yv3 = ggbApplet.getValue('Sol(x3)').toFixed(decimais);
        yv4 = ggbApplet.getValue('Sol(x4)').toFixed(decimais);
        yv5 = ggbApplet.getValue('Sol(x5)').toFixed(decimais);
        yv6 = ggbApplet.getValue('Sol(x6)').toFixed(decimais);
        e1 = (yv1 - yh1).toFixed(decimais);
        e2 = (yv2 - yh2).toFixed(decimais);
        e3 = (yv3 - yh3).toFixed(decimais);
        e4 = (yv4 - yh4).toFixed(decimais);
        e5 = (yv5 - yh5).toFixed(decimais);
        e6 = (yv6 - yh6).toFixed(decimais);

        kh1 = ggbApplet.getValue('kh1').toFixed(decimais);
        kh2 = ggbApplet.getValue('kh2').toFixed(decimais);
        kh3 = ggbApplet.getValue('kh3').toFixed(decimais);
        kh4 = ggbApplet.getValue('kh4').toFixed(decimais);
        kh5 = ggbApplet.getValue('kh5').toFixed(decimais);
        kh6 = ggbApplet.getValue('kh6').toFixed(decimais);

        kh11 = ggbApplet.getValue('kh11').toFixed(decimais);
        kh21 = ggbApplet.getValue('kh21').toFixed(decimais);
        kh31 = ggbApplet.getValue('kh31').toFixed(decimais);
        kh41 = ggbApplet.getValue('kh41').toFixed(decimais);
        kh51 = ggbApplet.getValue('kh51').toFixed(decimais);
        kh61 = ggbApplet.getValue('kh61').toFixed(decimais);

        kh12 = ggbApplet.getValue('kh12').toFixed(decimais);
        kh22 = ggbApplet.getValue('kh22').toFixed(decimais);
        kh32 = ggbApplet.getValue('kh32').toFixed(decimais);
        kh42 = ggbApplet.getValue('kh42').toFixed(decimais);
        kh52 = ggbApplet.getValue('kh52').toFixed(decimais);
        kh62 = ggbApplet.getValue('kh62').toFixed(decimais);
        switch (n)
        {
            case 1:
                saida = 'Obtemos a inclinação de Heun fazendo \
                    \$\$\\begin{aligned} \
                        k_1 &= g(x_0,y_0) = g('+x0+','+yh0+') = '+kh11+' \\\\ \
                        k_2 &= g(x_0 + h,y_0 + k_1h) \\\\ \
                            &= g('+x0+' + '+h+','+yh0+' + '+kh11+'*'+h+') \\\\ \
                            &=  '+kh12+' \\\\ \
                        k &= \\frac{1}{2}(k_1 + k_2) \\\\ \
                          &= \\frac{1}{2}('+kh11+' + '+kh12+') = '+kh1+'. \
                    \\end{aligned} \
                    \$\$';
                break;
            case 2:
                saida = "O ponto $y_1$ é calculada como sendo \
                    \$\$ \
                    \\begin{aligned} \
                        y_1 &= y_0 + kh  \\\\ \
                            &="+yh0+" + "+kh1+" * "+h+" = "+yh1+". \
                    \\end{aligned} \
                    \$\$ \
                    Como o valor exato de $y_1 = f(x_1) = "+yv1+"$, o erro cometido foi $e_1 = "+e1+".$";
                break;
            case 3: 
                saida = ' A nova inclinação de Heun é \
                \$\$ \
                \\begin{aligned} \
                        k_1 &= g(x_1,y_1) = g('+x1+','+yh1+') = '+kh21+' \\\\ \
                        k_2 &= g(x_1 + h,y_1 + k_1h) \\\\ \
                            &= g('+x1+' + '+h+','+yh1+' + '+kh21+'*'+h+') \\\\ \
                            &=  '+kh22+'  \\\\ \
                        k &= \\frac{1}{2}(k_1 + k_2) \\\\ \
                          &= \\frac{1}{2}('+kh21+' + '+kh22+') = '+kh2+'.  \
                    \\end{aligned} \
                    \$\$ \
                    ';
                break;
            case 4:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \$\$ \
                    \\begin{aligned} \
                        y_2 &= y_1 + g(x_1,y_1)h  \\\\ \
                            &="+yh1+" + ("+kh2+") * "+h+" = "+yh2+". \
                    \\end{aligned} \
                    \$\$ \
                    Como o valor exato de $y_2 = f(x_2) = "+yv2+"$, o erro cometido foi $e_2 = "+e2+".$";

                break;   
            case 5: 
                saida = 'Calculado o segmento de reta com inclinação \
                    \$\$ \
                    \\begin{aligned} \
                        k_1 &= g(x_2,y_2) = g('+x2+','+yh2+') = '+kh31+' \\\\ \
                        k_2 &= g(x_2 + h,y_2 + k_1h) \\\\ \
                            &= g('+x2+' + '+h+','+yh2+' + '+kh31+'*'+h+') \\\\ \
                            &=  '+kh32+'  \\\\ \
                        k &= \\frac{1}{2}(k_1 + k_2) \\\\ \
                          &= \\frac{1}{2}('+kh31+' + '+kh32+') = '+kh3+'. \
                    \\end{aligned} \
                    \$\$ ';
                break;
            case 6:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \$\$ \
                    \\begin{aligned} \
                        y_3 &= y_2 + g(x_2,y_2)h  \\\\ \
                            &= "+yh2+" + ("+kh3+") * "+h+" = "+yh3+". \
                    \\end{aligned} \
                    \$\$ \
                    Como o valor exato de $y_3 = f(x_3) = "+yv3+"$, o erro cometido foi $e_3 = "+e3+".$";

                break;  
            case 7: 
                saida = 'Calculado o segmento de reta com inclinação \
                    \$\$ \
                    \\begin{aligned} \
                        k_1 &= g(x_3,y_3) = g('+x3+','+yh3+') = '+kh41+' \\\\ \
                        k_2 &= g(x_3 + h,y_3 + k_1h) \\\\ \
                            &= g('+x3+' + '+h+','+yh3+' + '+kh41+'*'+h+') \\\\ \
                            &=  '+kh42+'  \\\\ \
                        k &= \\frac{1}{2}(k_1 + k_2) \\\\ \
                           &= \\frac{1}{2}('+kh41+' + '+kh42+') = '+kh4+'. \
                    \\end{aligned}\
                    \$\$ ';
                break;
            case 8:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \$\$ \
                    \\begin{aligned} \
                        y_4 &= y_3 + g(x_3,y_3)h  \\\\ \
                            &= "+yh3+" + ("+kh4+") * "+h+" = "+yh4+". \
                    \\end{aligned} \
                    \$\$ \
                    Como o valor exato de $y_4 = f(x_4) = "+yv4+"$, o erro cometido foi $e_4 = "+e4+".$";
                break;  
            case 9: 
                saida = 'Calculado o segmento de reta com inclinação \
                    \$\$ \
                    \\begin{aligned} \
                        k_1 &= g(x_4,y_4) = g('+x4+','+yh4+') = '+kh51+' \\\\ \
                        k_2 &= g(x_4 + h,y_4 + k_1h) \\\\ \
                            &= g('+x4+' + '+h+','+yh4+' + '+kh51+'*'+h+') \\\\ \
                            &=  '+kh52+'  \\\\ \
                        k &= \\frac{1}{2}(k_1 + k_2) \\\\ \
                          &= \\frac{1}{2}('+kh51+' + '+kh52+') = '+kh5+'. \\\\ \
                    \\end{aligned}\
                    \$\$ \
                    ';
                break;
            case 10:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \$\$ \
                    \\begin{aligned} \
                        y_5 &= y_4 + g(x_4,y_4)h  \\\\ \
                            &= "+yh4+" + ("+kh5+") * "+h+" = "+yh5+". \
                    \\end{aligned}\
                    \$\$ ";
                break;  
            case 11: 
                saida = 'Calculado o segmento de reta com inclinação \
                    \$\$ \
                    \\begin{aligned} \
                        k_1 &= g(x_5,y_5) = g('+x5+','+yh5+') = '+kh61+' \\\\ \
                        k_2 &= g(x_5 + h,y_5 + k_1h) \\\\ \
                            &= g('+x5+' + '+h+','+yh5+' + '+kh61+'*'+h+') \\\\ \
                            &=  '+kh62+'  \\\\ \
                        k &= \\frac{1}{2}(k_1 + k_2) \\\\ \
                          &= \\frac{1}{2}('+kh61+' + '+kh62+') = '+kh6+'. \
                    \\end{aligned}\
                    \$\$ ';
                break;
            case 12:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \$\$ \
                    \\begin{aligned} \
                        y_6 &= y_5 + g(x_5,y_5)h  \\\\ \
                            &= "+yh5+" + ("+kh6+") * "+h+" = "+yh6+". \
                    \\end{aligned}\
                    \$\$ ";
                break;  
            default: 
            //alert('Default case');
        }
        

        return saida;   
    }

    function anterior (){
        if (this.n >= 1){
            this.n--;
            ggbApplet.setValue('cd', this.n);
            //mensagens(this.n);
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
                                
    }

    function proximo (){
        if (this.n <= 10){
            this.n++;
            ggbApplet.setValue('cd', this.n)
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            renderizarKatex();
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>

<br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/heun.svg" alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/mUT-nv-rSvY" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/edoHeun.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método de Heun',
          'todosExercicios' => $todosExercicios[3],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>

</div>


<script type="text/javascript">
function renderizarKatex(){
    renderMathInElement(document.body,{delimiters: [
        {left: "$$", right: "$$", display: true},
        {left: "$", right: "$", display: false}
    ]});
}
</script>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>




