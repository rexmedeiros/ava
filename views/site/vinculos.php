<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Vínculo SIGAA';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title"><i class="fa fa-tag"></i>Selecione seu Vínculo</h3>
  </div>
  <div class="box-body">
        <?php $form = ActiveForm::begin(['id' => 'vinculos-form']); ?>
            <div class="form-group">
                <?= $form->field($model, 'vinculo')->radioList($items) ?>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Acessar a Aplicação', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
  </div>
  <!-- /.box-body -->
</div>

