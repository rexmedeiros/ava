<?php
use yii\helpers\Html;
use app\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Equações Diferenciais - Método de Euler';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Métodos de Passo Único</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

<p>Considere um problema de valor inicial:</p>

$$
\begin{cases}
      \frac{\text{d}y}{\text{d}x}= g(x,y), \\
      y(x_1) = y_1.
\end{cases}
$$

<p> De posse do valor inicial da função ${y_1}$ para um determinado ${x_1}$, desejamos calcular os próximos valores para ${y}$, de forma a obter a cada iteração o valor de ${ y_{i+1}}$ para um determinado ${ x_{i+1}}$, variando
$x_1 \leq x \leq x_f$. 

<br><br> 

A solução numérica de uma EDO é formada por um
conjunto de pontos discretos que representam a função
${y(x)}$ de forma aproximada em um dado domínio $[{x_1,x_f}]$ onde $x_1 \leq x \leq x_f$.
<br><br> </p>

<table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${i}$</th>
                                  <th>${x_i}$</th>
                                  <th>${y_i}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th>${1}$</th>
                                  <td>${x_1}$</td>
                                  <td>${y_1}$</td>
                                </tr>
                                <tr>
                                  <th>.<br>.<br>.</th>
                                  <td>.<br>.<br>.</td>
                                  <td>.<br>.<br>.</td>
                                </tr>
                                <tr>
                                  <th>${n}$</th>
                                  <td>${x_n}$</td>
                                  <td>${y_n}$</td>
                                </tr>
                              </tbody>
                            </table>



Os métodos numéricos que estudaremos pertencem a uma classe chamada de Métodos de Passo Único. Partindo de um ponto $(x_i,y_i)$, o próximo ponto $(x_{i+1},y_{i+1})$ é obtido através da estimativa de quanto a função cresceu/descresceu no intervalo $[x_i, x_{i+1}]$. Ou seja, no passo $i$ é determinado o valor de uma inclinação $h$, a partir da qual o valor de $y_{i+1}$ é estimado:

<div class="equacao"> 
$$
      \text{Valor novo } = \text{Valor antigo} + \text{inclinação} * \text{tamanho do passo}.
$$
</div>

<p>Ou, em termos matemáticos,</p>

$$
\begin{aligned}
      y_{i+1} &= y_i + k  h, \\
      x_{i+1} &= x_i + h,
\end{aligned}
$$
<br>
<p> onde $h$ é chamado de passo e $k$ é uma estimativa da inclinação média da função no intervalo $[x_i, x_{i+1}]$.
</p><br>

<?php /*de $ \phi $ é uma estimativa da inclinação(ver figura 1) que é usada para extrapolar um valor antigo da inclinação
$ y_i $ para um valor novo ${ y_{i+1} }$ em uma distância h(que é o passo). Sendo assim, a cada vez que aumentamos o passo, estaremos também variando a iteração, gerando assim uma nova inclinação e consequentemente um novo $ {y_{i+1}} $.</p><br><br>*/
?>

<p>Com os conceitos de métodos numéricos aplicados à resolução de EDOs bem definidos, podemos agora ver o primeiro método, chamado de método de Euler.</p>

</div>
        </div>
    </div>
</div>
<br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>  Método de Euler</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
<p>O método de Euler nos diz que, se quisermos calcular a solução de uma determinada equação diferencial $ \frac{dy}{dx}= g(x,y)$, nós podemos usar, inicialmente, essa equação diferencial e aplicar nela os valores iniciais $(x_i, y_i)$ para descobrir uma primeira inclinação da função:

$$
      {k = g(x_i,y_i)}.
$$

 Esse método presume que a inclinação da função durante todo o intervalo é constante. Dessa forma, podemos aproximar a inclinação média da função no intervalo como sendo a mesma inclinação do início do intervalo. Assim, calculamos o próximo valor da função da seguinte maneira:

</p>

$$
\begin{aligned}
      y_{i+1} &= y_i + g(x_i,y_i)h \\
      x_{i+1} &= x_i + h. 
\end{aligned}
$$

<p>A aproximação feita pelo método de Euler pode ser vista na figura abaixo:</p>

<p style="text-align: center;">
<img src="imagens/euler_.svg" style="width:90%;"> 
</p>

<p> Observamos que quanto maior o tamanho do passo, $h$, maior será o erro cometido pela aproximação. </p>


            </div>
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
            <p>Use o método de Euler para resolver o PVI</p>
           <div class="equacao">
            $$
            \begin{cases}
                \frac{dy}{dx}= -1.2y + 7e^{-0.3x} = g(x,y)\\
                y(0) = 3
            \end{cases}
            $$
            </div>

            <br/>
            de $a = 0$ até $b = 2.5$ com um passo $h = 0.5$.<br><br>Compare o resultado com a solução analítica: 
            <br/>
             <div class="equacao">
             $$
                y = f(x) = \frac{70}{9}e^{-0.3x} - \frac{43}{9}e^{-1.2x}.
             $$ 
             </div>

             <b>Solução</b><br />

             
            <div id="applet_container" align="center"></div>

            <br>
            <ol id="demo">
                <li>
                Partimos do ponto inicial $(0;3)$, como mostrado no gráfico acima.<br><br><br>
                </li>
            </ol>
            <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>

            </div>
        </div>
    </div>
</div>
<br>






<script type="text/javascript">
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/EDOGeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
            ggbApplet.setValue('Dx',0.5); // Valor de h
            ggbApplet.setValue('t',true)  // Mostra Euler;
            ggbApplet.setValue('u',false)  // Mostra Heun;
            ggbApplet.setValue('v',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
        ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        h = ggbApplet.getValue('Dx').toFixed(decimais);
        x0 = ggbApplet.getValue('x0');
        x1 = ggbApplet.getValue('x1').toFixed(decimais);
        x2 = ggbApplet.getValue('x2').toFixed(decimais);
        x3 = ggbApplet.getValue('x3').toFixed(decimais);
        x4 = ggbApplet.getValue('x4').toFixed(decimais);
        x5 = ggbApplet.getValue('x5').toFixed(decimais);
        x6 = ggbApplet.getValue('x6').toFixed(decimais);
        y0 = ggbApplet.getValue('y0');
        y1 = ggbApplet.getValue('y1').toFixed(decimais);
        y2 = ggbApplet.getValue('y2').toFixed(decimais);
        y3 = ggbApplet.getValue('y3').toFixed(decimais);
        y4 = ggbApplet.getValue('y4').toFixed(decimais);
        y5 = ggbApplet.getValue('y5').toFixed(decimais);
        y6 = ggbApplet.getValue('y6').toFixed(decimais);
        yv1 = ggbApplet.getValue('Sol(x1)').toFixed(decimais);
        yv2 = ggbApplet.getValue('Sol(x2)').toFixed(decimais);
        yv3 = ggbApplet.getValue('Sol(x3)').toFixed(decimais);
        yv4 = ggbApplet.getValue('Sol(x4)').toFixed(decimais);
        yv5 = ggbApplet.getValue('Sol(x5)').toFixed(decimais);
        yv6 = ggbApplet.getValue('Sol(x6)').toFixed(decimais);
        e1 = (yv1 - y1).toFixed(decimais);
        e2 = (yv2 - y2).toFixed(decimais);
        e3 = (yv3 - y3).toFixed(decimais);
        e4 = (yv4 - y4).toFixed(decimais);
        e5 = (yv5 - y5).toFixed(decimais);
        e6 = (yv6 - y6).toFixed(decimais);
        k1 = ggbApplet.getValue('F(x0,y0)').toFixed(decimais);
        k2 = ggbApplet.getValue('F(x1,y1)').toFixed(decimais);
        k3 = ggbApplet.getValue('F(x2,y2)').toFixed(decimais);
        k4 = ggbApplet.getValue('F(x3,y3)').toFixed(decimais);
        k5 = ggbApplet.getValue('F(x4,y4)').toFixed(decimais);
        k6 = ggbApplet.getValue('F(x5,y5)').toFixed(decimais);
        switch (n)
        {
            case 1:
                saida = 'Obtemos a inclinação de Euler fazendo \
                    $$k = g(x_0,y_0) = g('+x0+','+y0+') = '+k1+'$$  ';
                break;
            case 2:
                saida = "O ponto $y_1$ é calculada como sendo \
                    \\begin{eqnarray*} \
                        y_1 &=& y_0 + g(x_0,y_0)h  \\\\ \
                            &=&"+y0+" + "+k1+" * "+h+" = "+y1+". \
                    \\end{eqnarray*} \
                    Como o valor exato de $y_1 = f(x_1) = "+yv1+"$, o erro cometido foi $e_1 = "+e1+".$";
                break;
            case 3: 
                saida = "A nova inclinação de Euler é \
                    $$k = g(x_1,y_1) = g("+x1+","+y1+") = "+k2+".$$ ";
                break;
            case 4:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \\begin{eqnarray*} \
                        y_2 &=& y_1 + g(x_1,y_1)h  \\\\ \
                            &=&"+y1+" + ("+k2+") * "+h+" = "+y2+". \
                    \\end{eqnarray*} \
                    Como o valor exato de $y_2 = f(x_2) = "+yv2+"$, o erro cometido foi $e_2 = "+e2+".$";

                break;   
            case 5: 
                saida = "Calculado o segmento de reta com inclinação \
                    \\begin{eqnarray*} \
                        k = g(x_2,y_2) = g("+x2+","+y2+") = "+k3+". \
                    \\end{eqnarray*}";
                break;
            case 6:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \\begin{eqnarray*} \
                        y_3 &=& y_2 + g(x_2,y_2)h  \\\\ \
                            &=& "+y2+" + ("+k3+") * "+h+" = "+y3+". \
                    \\end{eqnarray*} \
                    Como o valor exato de $y_3 = f(x_3) = "+yv3+"$, o erro cometido foi $e_3 = "+e3+".$";

                break;  
            case 7: 
                saida = "Calculado o segmento de reta com inclinação \
                    \\begin{eqnarray*} \
                        k = g(x_3,y_3) = g("+x3+","+y3+") = "+k4+". \
                    \\end{eqnarray*}";
                break;
            case 8:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \\begin{eqnarray*} \
                        y_4 &=& y_3 + g(x_3,y_3)h  \\\\ \
                            &=& "+y3+" + ("+k4+") * "+h+" = "+y4+". \
                    \\end{eqnarray*} \
                    Como o valor exato de $y_4 = f(x_4) = "+yv4+"$, o erro cometido foi $e_4 = "+e4+".$";
                break;  
            case 9: 
                saida = "Calculado o segmento de reta com inclinação \
                    \\begin{eqnarray*} \
                        k = g(x_4,y_4) = g("+x4+","+y4+") = "+k5+". \
                    \\end{eqnarray*}";
                break;
            case 10:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \\begin{eqnarray*} \
                        y_5 &=& y_4 + g(x_4,y_4)h  \\\\ \
                            &=& "+y4+" + ("+k5+") * "+h+" = "+y5+". \
                    \\end{eqnarray*}";
                break;  
            case 11: 
                saida = "Calculado o segmento de reta com inclinação \
                    \\begin{eqnarray*} \
                        k = g(x_5,y_5) = g("+x5+","+y5+") = "+k6+". \
                    \\end{eqnarray*}";
                break;
            case 12:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                        y_6 &=& y_5 + g(x_5,y_5)h  \\\\ \
                            &=& "+y5+" + ("+k6+") * "+h+" = "+y6+". \
                    \\end{eqnarray*}";
                break;  
            default: 
            //alert('Default case');
        }
        

        return saida;   
    }

    function anterior (){
        if (this.n >= 1){
            this.n--;
            ggbApplet.setValue('cd', this.n);
            //mensagens(this.n);
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
                                
    }

    function proximo (){
        if (this.n <= 10){
            this.n++;
            ggbApplet.setValue('cd', this.n)
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>













<br>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Problemas com o método de Euler</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

            <p> O método de Euler, apesar de ser simples, tem o problema de apresentar erros, pois a aproximação feita pelo método não tem uma boa precisão. Dessa forma, além de haver um erro local causado pela aproximação da inclinação em cada iteração, há também o erro acumulado, já que em cada iteração se usa um valor anterior que já continha um erro.
            <br><br>
            Uma solução para diminuir a propagação do erro é diminuir o tamanho do passo $h$. Porém, a medida que diminuímos o valor de $h$, aumentamos o esforço computacional. A melhor solução, então, é utilizar outro método que tenha uma melhor precisão, como é o caso dos métodos de Runge-Kutta que veremos a seguir.


            </p>

            </div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Algoritmo para implementação computacional</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
               <p> 
                 <p style="text-align: center;"">
                  <img src="imagensDev/Massao/euler.svg"alt=""   style="width:90%;"/> 
                  </p>
                </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Implementação Scilab</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/u4mpqw_e9iA" allowfullscreen></iframe>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/edoEuler.sce"></script>
          </div>
        </div>
    </div>
</div>

<br>
<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método de Euler',
          'todosExercicios' => $todosExercicios[4],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>

</div>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>
