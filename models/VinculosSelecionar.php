<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * VinculosSelecionar is the model behind the Selecionar Vinculu form.
 */
class VinculosSelecionar extends Model
{
    public $vinculo;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['vinculo'], 'required', 'message' => 'É necessário selecionar um vínculo!'],
            ['vinculo','integer'],              
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'vinculo' => 'Selecione vínculo para continuar',
        ];
    }

}
