<?php
use yii\helpers\Html;
/* @var $this yii\web\View */


$this->title = 'Equações Diferenciais - Sistemas';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Videoaula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/qXew-9kLjQ8" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Sistemas de EDO</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">


            <p align="justify">Equações diferenciais ordinárias são usadas para
                descrever ou simular processos e sistemas modelados por
              taxa de variação.
              Frequentemente, estes processos e sistemas estão
              associados a muitas variáveis dependentes que afetam
              umas às outras.<br><br>
              Nesses casos, é necessário resolver um sistema de EDOs de
              primeira ordem.
              Sendo assim, agora trabalharemos um pouco sobre essa perspectiva. <br><br>
              É importante entender a solução de sistemas de EDOs antes de trabalharmos com EDOs de ordens superiores a 1. </p>
          </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo </h2>
                </p>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                Resolva o Problema de Valor Inicial abaixo usando o método de Euler com passo $h=0.5$:
               
                <div class="equacao">
                $$
                \begin{cases}
                  \frac{dy}{dx} = -0.5y &= g_{y}(x,y,z) \\
                  \frac{dz}{dx} = 4 -0.3z -0.1y &= g_{z}(x,y,z) \\
                  y(0) = 4 \;\;\;\;\; z(0) = 6.
                \end{cases}
                $$
                </div> 
                
               <b>Solução: </b><br>
               <p> Os primeiros pontos da solução em $x = 0$ é $(0;4)$ para $y$ e $(0;6)$ para $z$, que correspondem as condições iniciais. Ou seja, na iteração ${i = 0}$ temos ${x_0 = 0}$, ${y_0 = 4}$ e ${z_0 = 6}$.  Para obter o valor de $y_1$ e $z_1$ em $x_1 = 0.5$, calculamos primeiramente as inclinações: <br><br></p>
               
               <div class="equacao">
               $$ 
                \begin{aligned}
                  k_y &= g_y(x_0,y_0,z_0) = -0.5{y_0}\\
                      &= -2 \\
                  k_z &= g_z(x_0,y_0,z_0) =  4 - 0.3{z_0} - 0.1{y_0}\\
                      &=  1.8
                \end{aligned}
              $$
              </div>
              Logo,
              <div class="equacao">
              $$ 
                \begin{aligned}
                  y_1 &= y_0 + k_{y}h = 4 - 2*(0.5)\\
                      & = 3 \\
                  z_1 &= z_0 + k_{z}h = 6 + 1.8*(0.5)\\
                      &=  6.9
                \end{aligned}
              $$
              </div>

              Para $x_2 = 1.0$, temos que:
              
              <div class="equacao">
               $$ 
                \begin{aligned}
                  k_y &= g_y(x_1,y_1,z_1) = -0.5{y_1}\\
                      &= -1.5 \\
                  k_z &= g_z(x_1,y_1,z_1) = 4 - 0.3{z_1} - 0.1y_1 \\
                      &=   1.63 
                \end{aligned}
              $$
              </div>
              Logo,
              <div class="equacao">
              $$ 
                \begin{aligned}
                  y_2 &= y_1 + k_{y}h = 3 + (-1.5)*(0.5)\\
                      & =  2.25\\
                  z_2 &= z_1 + k_{z}h =6.9 + 1.63*(0.5)\\
                      &=  7.715
                \end{aligned}
              $$
              </div>

                 O restante da solução podemos ver na tabela a seguir: 
                   
                   <table class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>${i}$</th>
                                  <th>${x_i}$</th>
                                  <th>${y_i}$</th>
                                  <th>${z_i}$</th>
                                  <th>$k_{y_i}$</th>
                                  <th>$k_{z_i}$</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th>${0}$</th>
                                  <td>${0.000}$</td>
                                  <td>${4.000}$</td>
                                  <td>${6.000}$</td>
                                  <td>${}$</td>
                                  <td>${}$</td>
                                </tr>
                                <tr>
                                  <th>${1}$</th>
                                  <td>${0.500}$</td>
                                  <td>${3.000}$</td>
                                  <td>${6.900}$</td>
                                  <td>${-2}$</td>
                                  <td>${1.8}$</td>
                                </tr>
                                <tr>
                                  <th>${2}$</th>
                                  <td>${1.000}$</td>
                                  <td>${2.250}$</td>
                                  <td>${7.715}$</td>
                                  <td>${-1.5}$</td>
                                  <td>${1.63}$</td>
                                </tr>
                                <tr>
                                  <th>${3}$</th>
                                  <td>${1.500}$</td>
                                  <td>${1.688}$</td>
                                  <td>${8.445}$</td>
                                  <td>${-1.125}$</td>
                                  <td>${1.4605}$</td>
                                </tr>
                                <tr>
                                  <th>${4}$</th>
                                  <td>${2.000}$</td>
                                  <td>${1.266}$</td>
                                  <td>${9.094}$</td>
                                  <td>${-0.84375}$</td>
                                  <td>${1.297675}$</td>
                                </tr>
                              </tbody>
                            </table>
            </div>
        </div>
    </div>
</div>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>