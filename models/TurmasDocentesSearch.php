<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TurmasDocentes;

/**
 * TurmasDocentesSearch represents the model behind the search form about `frontend\models\TurmasDocentes`.
 */
class TurmasDocentesSearch extends TurmasDocentes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vinculo_id', 'turma_id'], 'integer'],
            [['ano', 'periodo'],  'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TurmasDocentes::find()->where([
            'vinculo_id' => Yii::$app->user->identity->vinculo_id,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vinculo_id' => $this->vinculo_id,
            'turma_id' => $this->turma_id,
            'ano' => $this->ano,
            'periodo' => $this->periodo,
        ]);

        return $dataProvider;
    }
}
