<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Contato';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Entre em contato</h3>
              </div>            
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                        Dúvidas, críticas ou sugestões, entrar em contato para o prof. Rex Medeiros (rexmedeiros@ect.ufrn.br).

                        <br />
                        <br />
                        <br />

                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
