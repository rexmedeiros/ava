<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 *
 * @property integer $matricula
 * @property integer $qrCodeId
 * @property string $hash
 */ 


class Matriculas extends Model
{
     public $matricula;
     public $qrCodeId;
     public $hash;
    
    /**
     * @return array the validation rules.
     */
     public function attributeLabels()
    {
        return [
            'matricula' => 'Matrícula',
        ];
    }

    public function rules()
    {
        return [
            [['matricula','qrCodeId'], 'integer'],
            ['matricula', 'required'],
        ];
    }

}
