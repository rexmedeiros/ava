<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TurmasDocentesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Geogebra';
$this->params['breadcrumbs'][] = 'Geogebra';
?>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
<script type="text/javascript"
  src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

<script type="text/x-mathjax-config">
MathJax.Hub.Config({
  TeX: { equationNumbers: { autoNumber: "AMS" } }
});
</script>
  

<div class="turmas-docentes-index">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    
    <h1><?= Html::encode($this->title) ?></h1>
</div>    
<p>
<a href="#" onclick="diminuirFonte()">diminuir fonte <i class="fa fa-font"></i><i class="fa fa-long-arrow-up"></i>
</a>
</p>
<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" id="conteudo">
            <p>Use o método de Euler para resolver o PVI</p>
            \begin{cases}
                \frac{dy}{dx}= -1.2y + 7e^{-0.3x} = g(x,y)\\
                y(0) = 3
            \end{cases}

            <br/>
            de $a = 0$ até $b = 2.5$ com um passo $h = 0.5$.<br><br>Compare o resultado com a solução analítica: 
            <br/>
             $$
                y = f(x) = \frac{70}{9}e^{-0.3x} - \frac{43}{9}e^{-1.2x}
             $$ 

             <b>Solução</b><br />

             <form id="geo">
                <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
            </form>
            <div id="applet_container" align="center"></div>

            <br>
            <ol id="demo">
                <li>
                Partimos do ponto inicial $(0;3)$, como mostrado no gráfico acima.
                </li>
            </ol>


            </div>
        </div>
    </div>
</div>
<br>
<script type="text/javascript">
    function diminuirFonte(){
        document.getElementById("conteudo").style.fontSize = "1em"; 
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":600,"height":400,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":true,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/EDOGeoGebraSite.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
            ggbApplet.setValue('Dx',1); // Valor de h
            ggbApplet.setValue('t',true)  // Mostra Euler;
            ggbApplet.setValue('u',false)  // Mostra Heun;
            ggbApplet.setValue('v',false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n){
        ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        h = ggbApplet.getValue('Dx').toFixed(decimais);
        x0 = ggbApplet.getValue('x0');
        x1 = ggbApplet.getValue('x1').toFixed(decimais);
        x2 = ggbApplet.getValue('x2').toFixed(decimais);
        x3 = ggbApplet.getValue('x3').toFixed(decimais);
        x4 = ggbApplet.getValue('x4').toFixed(decimais);
        x5 = ggbApplet.getValue('x5').toFixed(decimais);
        x6 = ggbApplet.getValue('x6').toFixed(decimais);
        y0 = ggbApplet.getValue('y0');
        y1 = ggbApplet.getValue('y1').toFixed(decimais);
        y2 = ggbApplet.getValue('y2').toFixed(decimais);
        y3 = ggbApplet.getValue('y3').toFixed(decimais);
        y4 = ggbApplet.getValue('y4').toFixed(decimais);
        y5 = ggbApplet.getValue('y5').toFixed(decimais);
        y6 = ggbApplet.getValue('y6').toFixed(decimais);
        yv1 = ggbApplet.getValue('Sol(x1)').toFixed(decimais);
        yv2 = ggbApplet.getValue('Sol(x2)').toFixed(decimais);
        yv3 = ggbApplet.getValue('Sol(x3)').toFixed(decimais);
        yv4 = ggbApplet.getValue('Sol(x4)').toFixed(decimais);
        yv5 = ggbApplet.getValue('Sol(x5)').toFixed(decimais);
        yv6 = ggbApplet.getValue('Sol(x6)').toFixed(decimais);
        e1 = (yv1 - y1).toFixed(decimais);
        e2 = (yv2 - y2).toFixed(decimais);
        e3 = (yv3 - y3).toFixed(decimais);
        e4 = (yv4 - y4).toFixed(decimais);
        e5 = (yv5 - y5).toFixed(decimais);
        e6 = (yv6 - y6).toFixed(decimais);
        k1 = ggbApplet.getValue('F(x0,y0)').toFixed(decimais);
        k2 = ggbApplet.getValue('F(x1,y1)').toFixed(decimais);
        k3 = ggbApplet.getValue('F(x2,y2)').toFixed(decimais);
        k4 = ggbApplet.getValue('F(x3,y3)').toFixed(decimais);
        k5 = ggbApplet.getValue('F(x4,y4)').toFixed(decimais);
        k6 = ggbApplet.getValue('F(x5,y5)').toFixed(decimais);
        switch (n)
        {
            case 1:
                saida = 'Obtemos a inclinação de Euler fazendo \
                    $$k = g(x_0,y_0) = g('+x0+','+y0+') = '+k1+'$$  ';
                break;
            case 2:
                saida = "O ponto $y_1$ é calculada como sendo \
                    \\begin{eqnarray*} \
                        y_1 &=& y_0 + g(x_0,y_0)h  \\\\ \
                            &=&"+y0+" + "+k1+" * "+h+" = "+y1+". \
                    \\end{eqnarray*} \
                    Como o valor exato de $y_1 = f(x_1) = "+yv1+"$, o erro cometido foi $e_1 = "+e1+".$";
                break;
            case 3: 
                saida = "A nova inclinação de Euler é \
                    $$k = g(x_1,y_1) = g("+x1+","+y1+") = "+k2+".$$ ";
                break;
            case 4:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo \
                    \\begin{eqnarray*} \
                        y_2 &=& y_1 + g(x_1,y_1)h  \\\\ \
                            &=&"+y1+" + "+k2+" * "+h+" = "+y2+". \
                    \\end{eqnarray*}";

                break;   
            case 5: 
                saida = "Calculado o segmento de reta com inclinação $k = g(x_1,y_1) = g("+x2+","+y2+") = "+k2+"$";
                break;
            case 6:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo $y_2 = y_1 + g(x_1,y_1)h = "+y1+" + "+k2+" * "+h+" = "+y2+"$";
                break;  
            case 7: 
                saida = "Calculado o segmento de reta com inclinação $k = g(x_1,y_1) = g("+x1+","+y1+") = "+k2+"$";
                break;
            case 8:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo $y_2 = y_1 + g(x_1,y_1)h = "+y1+" + "+k2+" * "+h+" = "+y2+"$";
                break;  
            case 9: 
                saida = "Calculado o segmento de reta com inclinação $k = g(x_1,y_1) = g("+x1+","+y1+") = "+k2+"$";
                break;
            case 10:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo $y_2 = y_1 + g(x_1,y_1)h = "+y1+" + "+k2+" * "+h+" = "+y2+"$";
                break;  
            case 11: 
                saida = "Calculado o segmento de reta com inclinação $k = g(x_1,y_1) = g("+x1+","+y1+") = "+k2+"$";
                break;
            case 12:
                saida = "A ordenada do primeiro ponto, $y_2$, é calculada como sendo $y_2 = y_1 + g(x_1,y_1)h = "+y1+" + "+k2+" * "+h+" = "+y2+"$";
                break;  
            default: 
            //alert('Default case');
        }
        

        return saida;   
    }

    function anterior (){
        if (this.n >= 1){
            this.n--;
            ggbApplet.setValue('cd', this.n);
            //mensagens(this.n);
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
                                
    }

    function proximo (){
        if (this.n <= 12){
            this.n++;
            ggbApplet.setValue('cd', this.n)
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>

