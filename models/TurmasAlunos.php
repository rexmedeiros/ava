<?php

namespace app\models;

use Yii;
use app\components\Sigaa;
use app\components\SigaaProfile;

/**
 * This is the model class for table "turmas_alunos".
 *
 * @property integer $id
 * @property integer $vinculo_id
 * @property integer $matricula
 * @property integer $turma_id
 * @property integer $ano
 * @property integer $periodo
 *
 * @property Turmas $turma
 * @property Vinculos $vinculo
 */
class TurmasAlunos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'turmas_alunos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vinculo_id', 'matricula', 'turma_id', 'ano', 'periodo'], 'required'],
            [['vinculo_id', 'matricula', 'turma_id', 'ano', 'periodo'], 'integer'],
            [['turma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Turmas::className(), 'targetAttribute' => ['turma_id' => 'id']],
            [['vinculo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vinculos::className(), 'targetAttribute' => ['vinculo_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vinculo_id' => 'Vinculo ID',
            'matricula' => 'Matricula',
            'turma_id' => 'Turma ID',
            'ano' => 'Ano',
            'periodo' => 'Periodo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurma()
    {
        return $this->hasOne(Turmas::className(), ['id' => 'turma_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVinculo()
    {
        return $this->hasOne(Vinculos::className(), ['id' => 'vinculo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurmasAlunosSemestre()
    {
        $client = new Sigaa();
        $profile = new SigaaProfile($client);
        $semestre = $profile->getSemestreAtual();
        return $this->find()->where([
                'vinculo_id' => Yii::$app->user->identity->vinculo_id,
                'ano' => $semestre['ano'],
                'periodo' => $semestre['periodo']
            ])
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTurmaAlunoComponente($codigos)
    {
        $matriculas = $this->getTurmasAlunosSemestre();
        $matriculaComponente = false;
        if ($matriculas){
            foreach ($matriculas as $matricula){
                if (in_array($matricula->turma->codigo, $codigos)){
                    $matriculaComponente =  $matricula;
                    break;
                }    
            }
        }
        return $matriculaComponente;
    }
}
