<?php

use yii\helpers\Html;
use app\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'MMQ - Linearização';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Linearização - vídeo aula</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="https://www.youtube.com/embed/WohsHaqF68M" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Linearização de relações não-lineares</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">

          O Método dos Mínimos Quadrados possibilita encontrar uma função do tipo

          <div class="equacao">
          $$
          y(x) = a_0f_0(x) + a_1f_1(x) + a_2f_2(x) + ... + a_mf_m(x)
          $$
          </div>

          que melhor se ajusta a um conjunto de pontos ($x_i$,$y_i$).

          <br><br>

          Observe que a função $y(x$) é formada por uma combinação linear de funções $f_i(x)$, cujos valores $a_i$ a determinar são os coeficientes dessa combinação linear. Em algumas situações, desejamos ajustar um conjunto de pontos a uma função $y(x)$ que não pode ser escrita como uma combinação linear na forma da equação acima.

          <br><br>

          Como veremos, para algumas classes de funções, é possível fazer algum tipo de manipulação e/ou linearização na função original de forma a possibilitar a aplicação da técnica dos Mínimos Quadrados para a obtenção dos seus parâmetros.

          <br><br>

          Iremos analisar três tipos de modelos de funções a seguir e ver os passos necessários para linearizá-los.

          <br><br>
          <b>Modelo exponencial: </b><br>

          Seja a função

          $$
          y(x) = \alpha_1 e^{\beta_1 x}.
          $$

          Esse modelo é muito importante para diversos campos da engenharia. Ele é usado para descrever, por exemplo, comportamentos de crescimento populacional e de decaimento radioativo. A imagem abaixo o ilustra graficamente: <br>

          <p style="text-align: center;">
            <img src="imagens/linearizacao1.svg" style="width: 90%;"> <br><br>
          </p>

          Observe que a função $y(x)$ <strong>não</strong> pode ser escrita como uma combinação linear de funções $f_i(x)$, em que os parâmetros a determinar, $\alpha_1$ e $\beta_1$, são os coeficientes dessa combinação. Entretanto, com alguma manipulação, podemos tornar isso possível. <br><br>

          Por exemplo, podemos aplicar o logaritmo natural dos dois lados da igualdade e obter: 

          <div class="equacao">
            $$
            \ln(y(x)) = \ln\left(\alpha_1 e^{\beta_1 x}\right),
            $$
          </div>
          onde podemos manipular o lado direito utilizando de propriedades logarítmicas: 

          <div class="equacao">
            $$
            \begin{aligned}
            \ln(y(x)) &= \ln(\alpha_1) + \ln(e^{\beta_1 x}) \\
            &= \ln(\alpha_1) + \beta_1 x\ln(e).
            \end{aligned}
            $$  
          </div>

          Como $\ln(e) = 1$, obtemos:

          <div class="equacao">
            $$\ln(y(x)) = \ln(\alpha_1) + \beta_1 x.$$
          </div>

          Observe agora que os parâmaetros a determinar são $\ln (\alpha_1)$ e $\beta_1$. Chamando $\ln(y(x))$ de $H(x)$, $\ln(\alpha_1)$ de $a_0$ e $\beta_1$ de $a_1$, temos:

          $$
          H(x) = a_0 + a_1x.
          $$

          Agora, $H(x)$ está escrita como uma combinação linear de funções $f_i(x)$. Além disso, a relação entre as variáveis $H$ e $x$ é linear. O gráfico de $\ln(y(x))$ em função de $x$ irá fornecer uma reta com inclinação $\beta_1$ e uma intersecção com o eixo $y$ em $\ln(\alpha_1)$, como ilustrado abaixo. <br>

          <p style="text-align: center;">
            <img src="imagens/linearizacao2_.svg" style="width: 90%;"> <br><br>
          </p>

          <b>Função Hiperbólica:</b> <br><br>

          Seja a função
          $$
          y(x) = \frac{1}{a + bx}.
          $$
          
          Se invertermos os dois lados da igualdade, teremos que

          $$
          \frac{1}{y(x)} = a + bx.
          $$
          
          Dessa forma, podemos chamar $\frac{1}{y(x)}$ de $H(x)$, $a$ de $a_0$ e $b$ de $a_1$. A equação ficará: 

          $$
          H(x) = a_0 + a_1x.
          $$

          <b>Função Raíz: </b>

          Considere a função

          $$
          y(x) = \sqrt {a + bx}
          $$
          
          Para esse caso, faremos o seguinte: elevaremos os dois lados ao quadrado. Isso nos dará:

          $$
          (y(x))^2 = a + bx.
          $$
          
          Agora vamos unificar a notação, chamando $(y(x))^2$ de $H(x)$, $a$ de $a_0$ e $b$ de $a_1$. Ficaremos então com:

          $$
          H(x) = a_0 + a_1x.
          $$
        </p>

      </div>
    </div>
  </div>
</div>
<br><br>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">
          Utilize a linearização para ajustar o seguinte conjunto de pontos a um modelo exponencial:
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>${x_i}$</th>
                <th>${y_i}$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>${0.4}$</th>
                <td>${800}$</td>
              </tr>
              <tr>
                <th>${0.8}$</th>
                <td>${975}$</td>
              </tr>
              <tr>
                <th>${1.2}$</th>
                <td>${1500}$</td>
              </tr>
              <tr>
                <th>${1.6}$</th>
                <td>${1950}$</td>
              </tr>
              <tr>
                <th>${2}$</th>
                <td>${2900}$</td>
              </tr>
              <tr>
                <th>${2.3}$</th>
                <td>${3600}$</td>
              </tr>
            </tbody>
          </table>

          <b>Solução:</b><br><br>

          Como vimos, o modelo exponencial é descrito pela equação

          $$y(x) = \alpha e^{\beta x}.$$

          Após algumas manipulações matemáticas, chegamos ao modelo linear:

          $$\ln(y) = \ln(\alpha) + \beta x.$$ 

          Chamando $H(x)=\ln(y)$, $a_0 = \ln(\alpha)$ e $a_1 = \beta$, obtemos: 

          $$H(x) = a_0 + a_1 x.$$

          Calculando os valores de $H(x)$:

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>${y}$</th>
                <th>${H(x) = \ln(y)}$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>${800}$</th>
                <td>${6.6846}$</td>
              </tr>
              <tr>
                <th>${975}$</th>
                <td>${6.8824}$</td>
              </tr>
              <tr>
                <th>${1500}$</th>
                <td>${7.3132}$</td>
              </tr>
              <tr>
                <th>${1950}$</th>
                <td>${7.5756}$</td>
              </tr>
              <tr>
                <th>${2900}$</th>
                <td>${7.9725}$</td>
              </tr>
              <tr>
                <th>${3600}$</th>
                <td>${8.1887}$</td>
              </tr>
            </tbody>
          </table>

          Em seguida, devemos resolver, como já vimos anteriormente, o seguinte sistema para achar os valores de $a_0$ e $a_1$ que melhor ajustam a função linear:

          <div class="equacao">
          $$ 
          \begin{bmatrix} n & \sum x \\ \sum x & \sum x^2 \end{bmatrix} \begin{bmatrix}a_0 \\ a_1 \end{bmatrix} = \begin{bmatrix}\sum H \\ \sum xH \end{bmatrix}.
          $$
          </div>

          Calculando os somatórios: <br>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>$ \sum x = 0.4+0.8+1.2+1.6+2+2.3 = 8.3$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>$ \sum x^2 = 0.4^2+0.8^2+1.2^2+1.6^2+2^2+2.3^2= 14.09$</th>
              </tr>
              <tr>
                <th>$ \sum H = 6.6846 + 6.8824 + 7.3132 + 7.5756 + 7.9725 + 8.1887 = 44.617$</th>
              </tr>
              <tr>
                <th>$ \sum x*H = 0.4*6.6846 + 0.8*6.8824 + 1.2*7.3132 + 1.6*7.5756 + 2*7.9725 + 2.3*8.1887 = 63.855$</th>
              </tr>
            </tbody>
          </table>

          Assim, 

          <div class="equacao">
          $$ 
          \begin{bmatrix} 6 & 8.3 \\ 8.3 & 14.09 \end{bmatrix} \begin{bmatrix}a_0 \\ a_1 \end{bmatrix} = \begin{bmatrix} 44.617 \\ 63.855 \end{bmatrix}.
          $$
          </div>

          Resolvendo esse sistema, obtemos:

          $$
            \begin{aligned}
            a_0 = 6.304\\
            a_1 = 0.8185.
            \end{aligned}
          $$

          Como desejamos achar o valor de $\alpha$, devemos usar a relação $a_0 = \ln(\alpha)$. Aplicando a exponencial dos dois lados, obtemos que: 

          $$
          \alpha = e^{a_0} = 546.75.
          $$
          e
          $$\beta = a_1 = 0.8185.$$          

          Como temos agora os valores de $\alpha$ e $\beta$, voltamos ao modelo exponencial e substituimos: 

          $$
          \begin{aligned}
          y(x) &= \alpha e^{\beta x}\\
          &= 546.75e^{0.8185x}.
          \end{aligned}
          $$
          

          Plotando a função obtida no intervalo dos pontos coletados obtemos o seguinte:<br><br>

          <p style="  text-align: center; ">
            <img src="imagens/linearizacao_3.svg" alt="" style="width:90%;" />
          </p> <br>

          Podemos ver que obtemos uma função que expressa bem nossa amostra. <br><br>

          Para finalizar o exemplo, iremos calcular os dois erros que estão envolvidos no ajuste de curvas por linerarização. Calcularemos primeiro, o erro envolvido na própria linearização, que obtemos como sendo $H(x) = a_0 + a_1 x$.Teremos assim, um vetor onde cada elemento corresponde ao erro obtido em cada ponto:

          $$Erro_1 = H(x) - a_0 - a_1x.$$ 

          Assim,

          <div class="equacao">
            $$Erro_1 = \begin{bmatrix} 0.0532 \\ -0.0764 \\ 0.027 \\ -0.038 \\ 0.0315 \\ 0.00215 \end{bmatrix} .$$
          </div>

          Agora, calcularemos o erro quadrático entre o valor analítico (valores encontrados dos pontos) e o valor númerico da função (valores obtidos substituindo $x$ na equação $y(x) = 546.75e^{0.8185x}$). Esse erro é calculado da seguinte forma: 

          <div class="equacao">
            $$ S_{r} = \sum_{i=1}^{n} e_i^2 = \sum_{i=1}^{n} (y_{i,medido} - y_{i,aprox})^2 .$$ 
          </div>
          
          Para o nosso caso,

          <div class="equacao">
            $$Erro_2 = \sum_{i=1}^{6} e_i^2 = \sum_{i=1}^{6} (y_i - H(x_i))^2 .$$ 
          </div>
          Ou
          <div class="equacao">
            $$
            \begin{aligned}
              Erro_2 &= (800 -758.54)^2+(975-1052.36)^2 + (1500-1460)^2 + (1950-2025.54)^2 + (2900-2810.15)^2 + (3600-3592.28)^2\\
              &= 23143.649.
            \end{aligned}
            $$ 
          </div>

          Veja que o erro quadrático entre as coordenadas $y$ coletadas e a função $y(x) = 546.75e^{0.8185x}$ foi bastante alto (já que estamos tratando de valores com ordem de grandeza de $10^3$). <br><br>

          Devemos notar que o Método dos Mínimos Quadrados garante a minimização do erro calculado na função linearizada.

        </p>
      </div>
    </div>
  </div>
</div>
<br><br>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo 2</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p align="justify">
          Ajuste os seguintes dados a um modelo de potências, dado por

          $
            $y = ax^b
          $$.

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>${x_i}$</th>
                <th>${y_i}$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>${2.5}$</th>
                <td>${13}$</td>
              </tr>
              <tr>
                <th>${3.5}$</th>
                <td>${11}$</td>
              </tr>
              <tr>
                <th>${5}$</th>
                <td>${8.5}$</td>
              </tr>
              <tr>
                <th>${6}$</th>
                <td>${8.2}$</td>
              </tr>
              <tr>
                <th>${7.5}$</th>
                <td>${7}$</td>
              </tr>
              <tr>
                <th>${10}$</th>
                <td>${6.2}$</td>
              </tr>
            </tbody>
          </table>

          <b>Solução:</b> <br>

          O primeiro passo é fazer a linearização da função. Como temos uma relação exponencial, podemos tirar logaritmo natural dos dois lados e obter 

          $$ln(y) = ln(ax^b).$$

          Manipulando o lado direito, temos que:

          $$
            \begin{aligned}
              ln(y) &= ln(a) + ln(x^b)\\
              &= ln(a) + b*ln(x)
            \end{aligned}
          $$ 

          Agora temos uma relação linear entre $x$ e $y$. Observe que, para usarmos a regressão linear, vamos precisar não dos valores diretos de $x$ e $y$, mas sim dos valores de $ln(x)$ e de $ln(y)$. Chamaremos $H(x) = ln(y), a_0 = ln(a), a_1 = b$ e $u = ln(x)$. Calculando: <br><br>

          <div class="equacao">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>${x}$</th>
                  <th>${y}$</th>
                  <th>${u = ln(x)}$</th>
                  <th>${H(x) = ln(y)}$</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>${2.5}$</th>
                  <td>${13}$</td>
                  <th>${0.91629}$</th>
                  <td>${2.5649}$</td>
                </tr>
                <tr>
                  <th>${3.5}$</th>
                  <td>${11}$</td>
                  <th>${1.2527}$</th>
                  <td>${2.3979}$</td>
                </tr>
                <tr>
                  <th>${5}$</th>
                  <td>${8.5}$</td>
                  <th>${1.6094}$</th>
                  <td>${2.1401}$</td>
                </tr>
                <tr>
                  <th>${6}$</th>
                  <td>${8.2}$</td>
                  <th>${1.7917}$</th>
                  <td>${2.1041}$</td>
                </tr>
                <tr>
                  <th>${7.5}$</th>
                  <td>${7}$</td>
                  <th>${2.0149}$</th>
                  <td>${1.9459}$</td>
                </tr>
                <tr>
                  <th>${10}$</th>
                  <td>${6.2}$</td>
                  <th>${2.3026}$</th>
                  <td>${1.8245}$</td>
                </tr>
              </tbody>
            </table>
          </div>

          Temos que: 

          <div class="equacao">
            $$ \begin{bmatrix} n & \sum u \\ \sum u & \sum u^2 \end{bmatrix} \begin{bmatrix}a_0 \\ a_1 \end{bmatrix} = \begin{bmatrix}\sum H \\ \sum uH \end{bmatrix}$$
          </div>

          Calculando os somatórios: <br><br>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>$ \sum u = 0.91629 + 1.2527 + 1.6094 + 1.7917 + 2.0149 + 2.3026 = 9.8876$</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th>$ \sum u^2 = 0.91629^2 + 1.2527^2 + 1.6094^2 + 1.7917^2 + 2.0149^2 + 2.3026^2 = 17.571$</th>
              </tr>
              <tr>
                <th>$ \sum H = 2.5649 + 2.3979 + 2.1401 + 2.1041 + 1.9459 + 1.8245 = 12.977$</th>
              </tr>
              <tr>
                <th>$ \sum u*H = 0.91629*2.5649 + 1.2527*2.3979 + 1.6094*2.1401 + 1.79176*2.1041 + 2.0149*1.9459 + 2.3026*1.8245 = 20.690$</th>
              </tr>
            </tbody>
          </table>

          O sistema será:

          <div class="equacao">
            $$ \begin{bmatrix} 6 & 9.8876 \\ 9.8876 & 17.571 \end{bmatrix} \begin{bmatrix}a_0 \\ a_1 \end{bmatrix} = \begin{bmatrix} 12.977 \\ 20.690 \end{bmatrix}$$
          </div>

          Resolvendo esse sistema, obtemos que

          $$
          \begin{aligned}
            a_0 = 3.0601\\
            a_1 = -0.54447.
          \end{aligned}
          $$

          Ou seja:
          $$a_0 = ln(a) \Rightarrow  a = e^{a_0}$$
          
          ou
          
          $$a = 21.330$$

          $$b = a_1 = -0.54447$$ 

          Substituindo os valores obtidos de volta no modelo de potências, obtemos enfim que 

          $$y = 21.330x^{-0.54447}$$ 

          Plotando a função e os pontos, obtemos: <br>
          <p style=" text-align: center;">
            <img src="imagens/linearizacao4.svg" alt="" style="width:90%;" />
          </p>

        </p>
      </div>
    </div>
  </div>
</div>

<br><br>
<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo 3</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p align="justify">
          Faça a linearização da função 

          $$
          y = \frac{1}{\sqrt{e^{\cos(a_0+a_1x+2)}}}
          $$

          <b>Solução:</b> <br>

          Primeiro invertemos os dois lados:

          <div class="equacao">
            $$
            \frac{1}{y} = \sqrt{e^{\cos(a_0+a_1x+2)}}.
            $$
          </div>

          Em seguida, elevamos os dois lados ao quadrado:

          <div class="equacao">
            $$
            \frac{1^2}{y^2} = e^{\cos(a_0+a_1x+2)}.
            $$
          </div>

          Agora, tiramos o logaritmo natural:

          <div class="equacao">
            $$
            \ln\left(\frac{1}{y^2}\right) = \cos(a_0+a_1x+2).
            $$
          </div>

          Por fim, calculamos o arco-cosseno:

          <div class="equacao">
            $$
            \arccos\left(\ln\left(\frac{1}{y^2}\right)\right) = (a_0+2)+a_1x.
            $$
          </div>

          Obtendo, finalmente, nossa função linearizada na forma $H(x) = a + bx$, onde

          <div class="equacao">
          $$
          \begin{aligned}
            H =  \arccos\left(\ln\left(\frac{1}{y^2}\right)\right) ,\\
            a = a_0+2\\
            b = a_1.
          \end{aligned}
          $$
          </div>

        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="conteudo">
        <form id="geo">
          <input value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

          <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">
        </form>
        <div id="applet_container" align="center"></div>

        <br>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <?= ExercicioWidget::widget([
      'titulo' => 'Exercícios - MMQ Linearização',
      'todosExercicios' => $todosExercicios[24],
      'modeloResposta' => $modeloResposta,
    ]) ?>
  </div>
</div>

<script type="text/javascript">
  function diminuirFonte() {
    document.getElementById("conteudo").style.fontSize = "1em";
  }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
  var n = 0;
  var list = document.getElementById('demo');
  var parameters = {
    "id": "ggbApplet",
    "prerelease": false,
    "width": 1100,
    "height": 650,
    "showToolBar": false,
    "borderColor": null,
    "showMenuBar": false,
    "showAlgebraInput": false,
    "showResetIcon": true,
    "enableLabelDrags": false,
    "enableShiftDragZoom": false,
    "enableRightClick": false,
    "capturingThreshold": null,
    "showToolBarHelp": false,
    "errorDialogsActive": true,
    "useBrowserForJS": true,
    "filename": "<?= Url::base() ?>/geogebra/linearizacaoEx.ggb"
  };
  var views = {
    'is3D': 0,
    'AV': 1,
    'SV': 0,
    'CV': 0,
    'EV2': 0,
    'CP': 0,
    'PC': 0,
    'DA': 0,
    'FI': 0,
    'PV': 0,
    'macro': 0
  };
  var applet = new GGBApplet(parameters, '5.0');
  //when used with Math Apps Bundle, uncomment this:
  //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

  window.onload = function() {
    applet.inject('applet_container');
  }


  function ggbOnInit(param) {
    if (param == "ggbApplet") {
      ggbApplet.setCoordSystem(-4, 35, -15, 30);
      ggbApplet.setValue('bissec', true); // Valor de h
      ggbApplet.setValue('falseposic', false) // Mostra Euler;
      ggbApplet.setValue('newton', false) // Mostra Heun;
      ggbApplet.setValue('secante', false) // Mostra Ponto Medio;
    }
  }

  function mensagens(n) {
    // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
    decimais = 2;
    ggbApplet.setValue('n', this.n)
    if (this.n % 2 == 0) {
      x = ggbApplet.getValue('X(1,0)');
    }
  }

  function anterior() {
    if (n >= 0) {
      ggbApplet.setCoordSystem(-4, 35, -15, 30);
    } else {
      ggbApplet.setCoordSystem(-4, 35, -15, 30);
    }

    if (this.n % 2 == 0) {

    }
    if (this.n >= 0) {
      this.n--;
      ggbApplet.setValue('n', this.n);
      return true;
    } else {
      alert("Vá para a próxima etapa.");
      return false;
    }
  }

  function proximo() {
    if (n <= 4) {
      ggbApplet.setCoordSystem(-4, 35, -15, 30);
    } else {
      ggbApplet.setCoordSystem(-4, 35, -15, 30);
    }
    if (this.n <= 4) {
      this.n++;
      ggbApplet.setValue('n', this.n)
      return true;
    } else {
      alert("Fim do exercício.");
      return false;
    }
  }
</script>

<script>
  renderMathInElement(document.body, {
    delimiters: [{
        left: "$$",
        right: "$$",
        display: true
      },
      {
        left: "$",
        right: "$",
        display: false
      }
    ]
  });
</script>