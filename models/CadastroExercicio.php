<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\Exercicios;
use app\models\Questoes;
use app\models\Alternativas;

/**
 * Formulario is the model behind the Formulario form.
 */
class CadastroExercicio extends Model
{
    public $arquivo;
    public $descricao;
    public $comImagens;
    public $idAvaliacao;
    public $avaliacaoCompleta;
    //public $campo3;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['descricao', 'comImagens'], 'required'],
            ['comImagens','boolean'],
            ['idAvaliacao','integer'],
            ['arquivo', 'file', 'skipOnEmpty' => false],
            ['arquivo', 'required', 'when' => function($model) {
                return $model->comImagens == 0;
            }],
            ['idAvaliacao', 'required', 'when' => function($model) {
                return $model->comImagens == 1;
            }],         
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'descricao' => 'Descrição do exercício (Ex.: Método de Newton - Cálculo do erro relativo)',
            'arquivo' => 'Arquivo importado do Multprova',
            'idAvaliacao' => 'ID da avaliação instanciada no Multiprova'
        ];
    }

    public function gravarExercicio()
    {
        $modelExercicio = new Exercicios();
        $modelExercicio->user_id = Yii::$app->user->id;
        $modelExercicio->descricao = $this->descricao;
        $modelExercicio->imagens = 1;
        $modelExercicio->evento_id = (isset($this->avaliacaoCompleta['idEvento'])) ? $this->avaliacaoCompleta['idEvento'] : 0;
        $modelExercicio->save();

        $idExercicio = $modelExercicio->id;

        $resumo['descricao'] = $this->descricao;
        $resumo['id'] = $idExercicio;
        // Primeiro o que veio por arquivo

        $resumo['nQuestoes'] = 0;
        foreach($this->avaliacaoCompleta['provas'] as $avaliacao){             

            $modelQuestao = new Questoes;
            $modelQuestao->exercicios_id = $idExercicio;
            $modelQuestao->enunciado =  addslashes($avaliacao['questoes'][0]['enunciado']);
            $temp = isset($avaliacao['questoes'][0]['idQuestaoInstanciada']) ? $avaliacao['questoes'][0]['idQuestaoInstanciada'] : 0;
            $modelQuestao->id_questao_instanciada = $temp;
            $modelQuestao->save();
            $idQuestao = $modelQuestao->id;

            // Agora as alternativas da questao 1
            $i = 0;
            $todasAlternativas = array();
            foreach ($avaliacao['questoes'][0]['alternativas'] as $v){
                $todasAlternativas[$i][] = $idQuestao;
                $todasAlternativas[$i][] = addslashes($v['texto']);
                $todasAlternativas[$i][] = $v['letra'];
                $todasAlternativas[$i][] = $v['letraMatriz'];
                $i++;
            }
            
            // Usa DAO para isso
            Yii::$app->db->createCommand()->batchInsert('alternativas', 
                ['questoes_id', 'alternativa', 'letra', 'matriz'], 
                $todasAlternativas
            )->execute();
            
            $resumo['nQuestoes']++; 
        }

        $this->gerarImagens($idExercicio);

        return $resumo;        
    }

    public function upload()
    {
        if ($this->validate()) {

            $arquivoOK = true;
            $erro = '';

            ini_set('auto_detect_line_endings','1');
            $fp = fopen ($this->arquivo->tempName,"r");    

            // Le os dados do arquivo
            $conteudoArquivo = '';
            while ( !feof($fp) ) {
                $conteudoArquivo .= fgets($fp); 
            }           
            $conteudoArquivo = trim($conteudoArquivo);
            
            $provaCompleta = json_decode($conteudoArquivo,true);
            
            // Valida JSON
            $numeroProvas = (int)$provaCompleta['numerodeProvas'];          
            $avaliacoes = $provaCompleta['provas'];
            
            if (is_array($avaliacoes)){
                if (count($avaliacoes) == $numeroProvas){
                    foreach($avaliacoes as $k => $provaIndividual){
                        if (is_array($provaIndividual)){
                            if (!(is_array($provaIndividual['questoes'][0]) and is_array($provaIndividual['questoes'][0]['alternativas']))){
                                $arquivoOK = false;
                                $erro .=  "Arquivo não está no formato adequado (questões ou alternativas não identificadas). ";
                                break;
                            }
                        }
                        else{
                            $arquivoOK = false;
                            $erro .=  "Arquivo não está no formato adequado (questões não identificadas). ";
                            break;  
                        }
                    }
                }
                else {
                    $arquivoOK = false;
                    $erro .=  "Arquivo não foi enviado corretamente ou não está no formato adequado (número de avaliações inválido). ";                 
                }
            }
            else{
                $arquivoOK = false; 
                $erro .=  "Arquivo não foi enviado corretamente ou não está no formato adequado (provas não detectadas). ";
            }
            if ($arquivoOK){
                $this->avaliacaoCompleta = $provaCompleta;
                return true;
            }
            else return $erro;
        } else {
            return "Nao falidou";
        }
    }

/**
     * Gerar Imagens dos Exercicios.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
    */
    public function gerarImagens($id)
    {        
        $model = Exercicios::findOne($id);

        /* TODO: Implementar isso */
        // Nao implementado TODO

        // Para o arquivo Tex
        $begin = "
            \begin{my}
        ";
        $end = "
            \\end{my}
        ";

        $pdflatexDir = Yii::$app->params['pdflatexDir'];
        $pdf2svgDir = Yii::$app->params['pdf2svgDir'];
        $baseDir = \Yii::$app->basePath . '/web/imagens/exercicios/'.$id.'/';
        mkdir($baseDir);
        chdir($baseDir);
        
        // Cria as imagens e povoa o arquivo .tex
        $fp = fopen($baseDir."corpo.tex", "w+");
        $fpm = fopen($baseDir."corpom.tex", "w+");

        $nQuestoes = 0;
        $nPaginas = 0;
        $questoes = $model->questoes; 

        $model->imagens = 1;
        $model->save();

        foreach($questoes as $modelQuestao){             
            // Gera o nome do arquivo de imagem.
            $arqSvg = md5("UmaStringUsandoMD5SeraGerada".mt_rand(10000,99999999));
            $nomesArquivos[$nPaginas] = $arqSvg;
            $nomesArquivosMedios[$nQuestoes] = $arqSvg;
            $nPaginas++;

            $modelQuestao->arquivo = $arqSvg; 
            $modelQuestao->save();
            fwrite($fp, $begin.stripslashes($modelQuestao->enunciado).$end);
            fwrite($fpm, $begin.stripslashes($modelQuestao->enunciado).$end);

            // Latex do Enunciado
            $alternativas = $modelQuestao->alternativas;

            $i = 0;
            foreach ($alternativas as $v){
                $arqSvg = md5("UmaStringUsandoMD5SeraGerada".mt_rand(10000,99999999));
                $nomesArquivos[$nPaginas] = $arqSvg;
                $nPaginas++;
                $v->arquivo = $arqSvg; 
                $v->save();
                fwrite($fp, $begin.stripslashes($v->alternativa).$end);
                $i++;
            }            
            $nQuestoes++; 
        }
        fclose($fp);
        fclose($fpm);

        // Cria os PDFs
        exec('cat ../../../../latex/questao1p.tex corpo.tex ../../../../latex/questao2.tex > imagensp.tex');
        exec($pdflatexDir."pdflatex --interaction=nonstopmode imagensp.tex");
        exec('cat ../../../../latex/questao1m.tex corpom.tex ../../../../latex/questao2.tex > imagensm.tex');
        exec($pdflatexDir."pdflatex --interaction=nonstopmode imagensm.tex");

        //Cria os svg
        exec($pdf2svgDir.'pdf2svg imagensp.pdf img%d.svg all');
        exec($pdf2svgDir.'pdf2svg imagensm.pdf imgM%d.svg all');
        foreach ($nomesArquivos as $k => $v){
            $k++;
            rename('img'.$k.'.svg', $v.'p.svg');
        } 
        foreach ($nomesArquivosMedios as $k => $v){
            $k++;
            rename('imgM'.$k.'.svg', $v.'m.svg');
        }
        return true;
    }  





}
