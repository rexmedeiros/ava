<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Créditos';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-title">
    <div class="title_left">
        <h3>Equipe de desenvolvimento</h3>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_content">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                        Este portal foi resultado de uma das ações desenvolvidas no âmbito do projeto de ensino <i>"Monitoria
                            como apoio ao uso de metodologias ativas em turmas grandes"</i>, entre os anos de 2017 e
                        2018.
                        <br/><br/>
                        Confira a listagem dos principais colaboradores:
                    </div>


                    <div class="clearfix"></div>

                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <h4 class="brief"><i>Monitora</i></h4>
                                <div class="left col-xs-7">
                                    <h2>Aleika Fonseca</h2>
                                    <small>
                                        <p>Bacharelado em Ciências e Tecnologia</p>
                                    </small>
                                </div>
                                <div class="right col-xs-5 text-center">
                                    <img src="imagens/monitor-aleika.png" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-6 emphasis">
                                </div>
                                <div class="col-xs-12 col-sm-6 emphasis">
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <i class="fa fa-user"> </i> Contato
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <h4 class="brief"><i>Monitora</i></h4>
                                <div class="left col-xs-7">
                                    <h2>Ana Rute</h2>
                                    <small>
                                        <p>Bacharelado em Ciências e Tecnologia</p>
                                    </small>
                                </div>
                                <div class="right col-xs-5 text-center">
                                    <img src="imagens/monitor-ana.png" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-6 emphasis">
                                </div>
                                <div class="col-xs-12 col-sm-6 emphasis">
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <i class="fa fa-user"> </i> Contato
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <h4 class="brief"><i>Monitor</i></h4>
                                <div class="left col-xs-7">
                                    <h2>Euler Porto</h2>
                                    <small>
                                        <p>Engenharia de Computação</p>
                                    </small>
                                </div>
                                <div class="right col-xs-5 text-center">
                                    <img src="imagens/user.png" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-6 emphasis">
                                </div>
                                <div class="col-xs-12 col-sm-6 emphasis">
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <i class="fa fa-user"> </i> Contato
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <h4 class="brief"><i>Monitor</i></h4>
                                <div class="left col-xs-7">
                                    <h2>Italo Fernandes</h2>
                                    <small>
                                        <p>Bacharelado em Ciências e Tecnologia</p>
                                    </small>
                                </div>
                                <div class="right col-xs-5 text-center">
                                    <img src="imagens/monitor-italo.png" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-6 emphasis">
                                </div>
                                <div class="col-xs-12 col-sm-6 emphasis">
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <i class="fa fa-user"> </i> Contato
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <h4 class="brief"><i>Monitor</i></h4>
                                <div class="left col-xs-7">
                                    <h2>Luis Matias Viana</h2>
                                    <small>
                                        <p>Engenharia de Computação </p>
                                    </small>
                                </div>
                                <div class="right col-xs-5 text-center">
                                    <img src="imagens/monitor-luis.png" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-6 emphasis">
                                </div>
                                <div class="col-xs-12 col-sm-6 emphasis">
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <i class="fa fa-user"> </i> Contato
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <h4 class="brief"><i>Monitor</i></h4>
                                <div class="left col-xs-7">
                                    <h2>Massao da Silva</h2>
                                    <small>
                                        <p>Engenharia Mecatrônica </p>
                                    </small>
                                </div>
                                <div class="right col-xs-5 text-center">
                                    <img src="imagens/monitor-massao.png" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-6 emphasis">
                                </div>
                                <div class="col-xs-12 col-sm-6 emphasis">
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <i class="fa fa-user"> </i> Contato
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                        <div class="well profile_view">
                            <div class="col-sm-12">
                                <h4 class="brief"><i>Coordenador</i></h4>
                                <div class="left col-xs-7">
                                    <h2>Rex Medeiros</h2>
                                    <small>
                                        <p>Docente ECT/UFRN </p>
                                    </small>
                                </div>
                                <div class="right col-xs-5 text-center">
                                    <img src="imagens/rex.jpg" alt="" class="img-circle img-responsive">
                                </div>
                            </div>
                            <div class="col-xs-12 bottom text-center">
                                <div class="col-xs-12 col-sm-6 emphasis">
                                </div>
                                <div class="col-xs-12 col-sm-6 emphasis">
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <i class="fa fa-user"> </i> Página pessoal
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

