<?php
use yii\helpers\Html;
use frontend\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Série de Taylor';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>

<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Conteúdo</h2>
                <div class="clearfix"></div>
            </div>
            <!--<div class="x_content" style="color:red;">
                <p align="justify">

                    Uma série de Taylor é uma representação de uma aproximação de uma função na forma de uma soma de termos        calculados dos valores de suas derivadas em torno de um ponto. <br><br>

                    Uma expansão em série de Taylor existirá se uma função contínua com $n$ derivadas contínuas no intervalo $[a;b]$ existir, ou seja, $f$ $\in$ $C^n [a;b]$ e $f^{(n +1)}(x)$ existe em $[a;b]$ e $x_0 \in$ $[a;b]$, então podemos fazer uma expansão em Série de Taylor, sendo assim, a função incial ficará como: <br><br>

                    <div class="equacao">
                    $f(x) = P_n(x) + R_n(x)$ 
                    </div>
                    <br>
                    onde $P_n$ é o polinômio de Taylor de ordem $n$ e $R_n(x)$ é chamado de erro ou resto. <br><br>
                </p>

                <p align="justify">
                    O termo generalizado da expansão em série de Taylor (ordem $n$) será: <br><br>

                    <div class="equacao">
                    $$P_n(x) = f(x_0) + f^{'}(x_0)*\frac{(x - x_0)}{1!} + f^{''}(x_0)*\frac{(x - x_0)^2}{2!} + f^{'''}(x_0)*\frac{(x - x_0)^3}{3!} + ... + f^{(n)}(x_0)*\frac{(x - x_0)^n}{n!}$$
                    </div>
                    <br><br>
                    onde $f^{'}, f^{''}, ... , f^{(n)}$ são as derivadas. <br>
                    E o polinômio de erro será: <br><br>
                    $$R_n(\tau) = f^{(n+1)}(\tau)* \frac{(x-x_0)^{(n+1)}}{(n+1)!}$$ <br>
                    onde $f^{(n+1)} \tau(x)$ é uma função encontrada na "$n$-ésima derivada + 1" e esta mesma é avaliada em um máximo ou mínimo global dentro do intervalo [a;b], sendo comparado o valor máximo e mínimo em módulo para assim obtermos o maior erro. Isso ficará mais claro com os exemplos. <br><br>
                </p>
                <p align="justify">
                    Existe uma série que é chamada de Série de Maclaurin, ela é basicamente a série de Taylor, sendo que nesta série, $x_0 = 0$. Podemos ver uma expansão em Série de Maclaurin a seguir: <br><br>
                    <div class="equacao">
                    $$M_n(x) = f(0) + f^{'}(0)*\frac{x}{1!} + f^{''}(0)*\frac{x^2}{2!} + f^{'''}(0)*\frac{x^3}{3!} + ... + f^{(n)}(0)*\frac{x^n}{n!}$$ 
                    </div>
                </p>
            </div>-->
            <div class="x_content">
                    A série de Taylor é de grande importância para o estudo de métodos numéricos por fornecer um meio de aproximar uma função $f(x)$ por um polinômio de grau adequado  nas proximidades de um ponto de interesse. Isso nos permite, por exemplo, manipular o polinômio (integrar, derivar, etc.) ao invés de manipular a função em si, resultando numa simplificação dos cálculos em troca de uma perda de precisão aceitável.<br><br> 

                    Suponha que uma função $f(x)$ possua todas as suas derivadas num determinado ponto $x = x_0$. Então, o teorema de Tayor afirma que é possível escrever a função $f(x)$ como série de potência infinita que possui a forma:
                    <div class="equacao">
                    $$
                    f(x) = f(x_0) + \frac{f^{'}(x_0)}{1!}(x - x_0)+ \frac{f^{''}(x_0)}{2!}(x - x_0)^2 + ... +\frac{f^{(n)}(x_0)}{n!}(x - x_0)^n + \dots
                    $$
                    </div>
            
                    
                    Observe que para um $x_0$ em particular, os primeiros $n+1$ termos da série formam um polinômio de grau $n$:
                    
                    <div class="equacao">
                    $$
                    P_n(x) = f(x_0) + \frac{f^{'}(x_0)}{1!}(x - x_0)+ \frac{f^{''}(x_0)}{2!}(x - x_0)^2 + ... +\frac{f^{(n)}(x_0)}{n!}(x - x_0)^n.
                    $$
                    </div>

                    O polinômio $P_n(x)$ é chamado de polinômio de Taylor de grau $n$, que é obtido a partir do truncamento da série de Taylor no termo de ordem $n$.

                    <br><br>
                    Se considerarmos a definição do polinômio de Taylor, então podemos escrever
                    $$
                      f(x) = P_n(x) + R_n(x),
                    $$
                    onde
                    $$
                    R_n(x) = \frac{f^{(n+1)}(x_0)}{(n+1)!}(x - x_0)^{n+1} +  \dots \frac{f^{(n+2)}(x_0)}{(n+2)!}(x - x_0)^{n+2} + \dots
                    $$
                    Taylor também mostrou que o somatório infinito acima equivale a 
                    <div class="equacao">
                    $$
                    R_n(x) = \frac{f^{(n+1)}(\tau)}{(n+1)!}(x - x_0)^{n+1},
                    $$
                    </div>
                    para algum valor de $x_0 \leq \tau \leq x$. $R_n(x)$ é chamado de resto da série de Taylor de ordem $n$ ou resto de Lagrange de ordem $n$.

                    Note que o polinômio de Taylor tem uma quantidade finita de termos e por isso nem sempre irá representar a função de maneira adequada. Porém, quanto mais termos forem utilizados, maior será o grau de $P_n(x)$ e melhor será a aproximação desejada. A seguir, é possível observar como a adição de novos termos influencia no valor de $P_n(x)$:<br><br>

                     <form id="geo">
                        <input  value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

                        <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">                
                    </form>
                    <div id="applet_container" align="center"></div><br><br>

                    
                    O resto de Lagrange representa o que faltou para que o polinômio de Taylor fosse igual à função original. Sendo assim, quando o número de termos tende ao infinito, $R_n(x)$ tende a zero. De maneira análoga, se forem utilizados poucos termos para montar o polinômio de taylor o resto tende a aumentar. <br><br>

                    Em geral, a expansão em série de Taylor de ordem $n$ será exata para um polinômio de grau $n$. Isto deve-se ao fato de que a derivada de ordem $n + 1$ de um polinômio de ordem $n$ é nula e, consequentemente, o termo $R_n(x)$ também é igual a zero. <br><br>

                    Por exemplo, para a função $f(x) = x^4 - 10x^2$, que é o um polinômio de quarta ordem, o polinômio de Taylor de ordem 4 será igual a função $f(x)$, isto é, $P_4(x) = f(x)$. É fácil verificar que o valor de $R_4(x)$ será zero, pois a derivada de ordem 5 é igual a zero.<br><br>

                    Uma situação particular ocorre quando a série de Taylor é centrada em zero, ou seja, $x_0=0$. Nesse caso, a série recebe o nome de <strong>série de Maclaurin</strong>, cuja expansão é mostrada a seguir:

                    <div class="equacao">
                        $$
                        M_n(x) = f(0) + \frac{f^{'}(0)}{1!}(x)+ \frac{f^{''}(0)}{2!}(x)^2 + \frac{f^{'''}(0)}{3!}(x)^3 + ... +\frac{f^{(n)}(0)}{n!}(x)^n \text{.}
                        $$
                    </div>

                </div>
            
        </div>

    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 1</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                Para a função 
                $$ 
                  f(x) = 5x^3 + x + 2,
                $$
                (a) Encontre o polinômio de Taylor de segunda ordem $P_2(x)$ em torno de $x_0 = 1$.<br>
                (b) Use o $P_2(x)$ para aproximar o valor de $f(1.1)$. Qual o erro verdadeiro cometido?<br>
                (c) Encontre a função $R_2(x)$ e estime o erro máximo, em valor absoluto, ao se usar $P_2(x)$ para aproximar o valor de $f(1.1)$. Isso é compatível com erro verdadeiro encontrado no item (b)?
                <br><br>
                <b>Solução:</b><br><br>
                <b>(a)</b>  Primeiro devemos fazer uma expansão em série de Taylor truncada no terceiro termo, ou seja, vamos encontrar um polinômio de Taylor de ordem 2. Sendo assim, precisamos calcular até a segunda derivada de $f(x)$.

                <div class="equacao">
                $$
                    \begin{aligned}
                        f^{'}(x) &= 15x^2 + 1,\\ 
                        f^{''}(x) &= 30x .\\ 
                    \end{aligned}
                $$
                </div>

                Feito isso, agora vamos calcular a função e suas derivadas aplicadas em $x = x_0 = 1$. 

                <div class="equacao">
                    $$
                    \begin{aligned}
                        f(1) &= 8,\\
                        f^{'}(1) &=16,\\
                        f^{''}(1) &=30 .\\
                    \end{aligned}
                    $$
                </div>

                Agora já podemos escrever o nosso polinômio de Taylor de segunda ordem:
                
                <div class="equacao">
                    $$
                        P_2(x) = f(x_0) + \frac{f'(x_0)}{1!}(x-x_0) + \frac{f''(x_0)}{2!}(x-x_0)^2.
                    $$
                </div>

                Substituindo $f(x_0), f'(x_0), f''(x_0)$ e $x_0$, teremos: 
                
                <div class="equacao">
                $$
                    \begin{aligned}
                        P_2(x) &= 8 + \frac{16}{1!}(x-1) + \frac{30}{2!}(x-1)^2\\
                        &= 8 + 16(x-1) + 15(x^2 - 2x + 1) \\
                        &= 8 + 16x - 16 + 15x^2 - 30x + 15\\
                        &= 15x^2 - 14x + 7.
                    \end{aligned}
                $$
                </div>

                 <b>(b)</b>
                 Agora temos que calcular $P_2(x)$ para $x = 1.1$. Assim, teremos:
                 
                 <div class="equacao">
                 $$
                    \begin{aligned}
                        P_2(1.1) &= 15(1.1)^2 - 14(1.1) + 7\\
                        &= 9.75 . 
                    \end{aligned}
                 $$
                 </div>

                 Feito isso, precisamos calcular a função $f(x)$ aplicada em $x = 1.1:$
                 
                <div class="equacao">
                $$    
                    \begin{aligned}
                      f(1.1) &= 5(1.1)^3 + 1.1 + 2 \\
                             &= 9.755.
                    \end{aligned}
                $$
                </div>

                 Com $P_2(1.1)$ e $f(1.1)$ calculados, podemos fazer o cálculo do erro verdadeiro absoluto, que é: 

                 <div class="equacao">
                    $$
                         Erro &= |9.755 - 9.75| = 0.005.
                    $$
                 </div>

                <br>
                 <b>(c)</b> 
                 Agora temos que econtrar a expressão para o Resto ($R_2(x)$), que será dada por: 

                 <div class="equacao">
                 $$R_n(\tau) = f^{(n+1)}(\tau) \frac{(x-x_0)^{(n+1)}}{(n+1)!} .$$ 
                 </div>

                 Como o polinômio de taylor é de segundo grau (n=2), é preciso determinar a derivada de terceira ordem (n+1=3) para o cálculo do resto. Então ficamos com: 

                 <div class="equacao">
                 $$R_2(\tau) = f^{(3)}(\tau) \frac{(x-1)^{(3)}}{(3)!} ,
                 $$
                 </div> 
                onde $1\leq \tau\leq 1.1$.
                 Para determinar o erro máximo esperado, o valor de $\tau$ aplicado é o que maximiza a derivada de ordem n+1 em módulo. Em geral, para se conhecer esse valor máximo, podemos plotar o gráfico da derivada no intervalo entre $x_0$ e $x$. Entratanto, temos que nesse caso $f^{(3)}(x)=30$ para todo valor de $x \in \mathbb{R}$, ou seja, é uma função constante. Com essa informação, não é preciso plotar um gráfico, pois o maior valor que a derivada pode assumir é 30. Dessa forma, $R_2(x)$ pode ser reescrito como:
                 <br><br>

                  
                 <div class="equacao">
                 $$R_2(x) = \frac{30}{3!}(x-1)^{(3)} .$$ 
                 </div>

                 Calculando $R_2(1.1)$, que é o ponto que estamos analisando, temos que: 

                 $$R_2(1.1) = 0.005 .$$ 

                 Nesse caso, a estimativa do erro máximo foi igual ao erro verdadeiro, de onde concluímos que os nossos cálculos são coerentes, uma vez que o erro verdadeiro tem a mesma ordem grandeza que o erro máximo. 
                 <br><br>

                Na Figura 1 temos os gráficos da função $f(x) = 5x^3 + x + 2$ e do polinômio $P_2(x) = 15x^2 - 14x + 7$. Podemos ver  que, próximo do ponto $x_0= 1$, o polinômio que encontramos se aproxima bem da função utilizada. <br><br>

                  <div style=" text-align: center">
                        <img src="imagens/exemplo_2Taylor.svg"  style="width:80%;"><br><strong>Figura 1</strong> <br><br>
                    </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo 2</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">
                    Para a função abaixo: 
                    $$
                       f(x) = \frac{1}{x^2 + 1} + x\cos(x)
                     $$
                     <br>

                     (a) Encontre o polinômio de Taylor de segunda ordem $P_2(x)$ em torno de $x_0 = \pi$.
                     <br><br>
                     (b) Use $P_2(x)$ para aproximar o valor de $f(3.2)$. Qual o erro verdadeiro cometido?
                     <br><br>
                     (c) Encontre a expressão analítica para a função $R_2(\tau)$ (com $x_0 = \pi$ e $x = 3.2$) 
                     <br><br>
                     (d) Estime o erro máximo em módulo ao se usar $P_2(x)$ para aproximar o valor de $f(3.2)$. Isso é compatível com o erro verdadeiro encontrado no item (b)?
                     <br><br>

                     <b>Solução:</b> <br><br>

                    <b>(a)</b> 

                     Inicialmente, como o polinômio desejado é de grau $n=2$, é preciso calcular até a segunda derivada da função. <br><br>

                     <div class="equacao">
                       $$
                          f^{'}(x) = \frac{-2x}{(x^2 + 1)^2} - x\sin(x) + \cos(x).
                       $$
                     </div>
                     
                     <div class="equacao">
                      $$
                          f^{''}(x) = \frac{8x^2}{(x^2 + 1)^3} - \frac{2}{(x^2 + 1)^2} - 2\sin(x) - x\cos(x).
                      $$
                     </div>

                     Em seguida, deve-se avaliar a função e suas derivadas em $x_0 = \pi$.<br><br>

                    <div class="equacao">
                    $$
                        \begin{aligned}
                             f(x_0) &= f(\pi)= -3.050\\
                             f^{'}(x_0) &=f^{'}(\pi)= -1.053\\
                             f^{''}(x_0) &=f^{''}(\pi) = 3.186 .\\
                        \end{aligned}
                    $$
                    </div>

                    Com isso, já é possível  calcular o polinômio de Taylor de segunda ordem. Então.

                     <div class="equacao">
                        $$
                             P_2(x) = f(x_0) + f'(x_0)(x-x_0) + \frac{f''(x_0)}{2!}(x-x_0)^2 .
                        $$
                     </div>

                     Substituindo $f(x_0), f'(x_0), f''(x_0)$ e $x_0$, teremos 

                    <div class="equacao">
                        $$
                             P_2(x) = -3.05 - 1.053(x-\pi) + \frac{3.186}{2!}(x-\pi)^2 .
                        $$
                    </div>  <br>

                    Na Figura 1 temos os gráficos da função $f(x) = 5x^3 + x + 2$ e do polinômio $P_2(x) = 15x^2 - 14x + 7$. Podemos ver  que, próximo do ponto $x_0= 1$, o polinômio que encontramos se aproxima bem da função utilizada. <br><br>

                    <div style=" text-align: center">
                        <img src="imagens/Tay.svg" ><br><strong>Figura 2</strong> <br><br>
                    </div>

                    <b>(b)</b> 
                    Com o polinômio $P_2(x)$ determinado no item anterior, deve-se aplicar $x=3.2$ para obter $P_2(3.2)$.

                    <div class="equacao">
                    $$
                    \begin{aligned}

                        P_2(3.2) &= -3.05 - 1.053(3.2-\pi) + \frac{3.186}{2!}(3.2-\pi)^2 \\
                          &= -3.1056718.
                    \end{aligned}
                    $$
                    </div>

                    Para obter o erro verdadeiro é necesário avaliar a função $f(x)$ no ponto $x = 3.2$. Então, 

                    <div class="equacao">
                      $$
                          f(3.2) = -3.1055753.
                      $$
                    </div>

                     Com $P_2(3.2)$ e $f(3.2)$ calculados o erro absoluto verdadeiro:

                    <div class="equacao">
                    $$
                      \begin{aligned}
                          Erro &= |P_2(3.2) - f(3.2)|\\
                          &=  0.0000965\\
                          &=  9.65\times 10^{-5}.
                      \end{aligned}
                    $$
                    </div>

                     <b>(c)</b> 
                     A expressão para o Resto $R_n(\tau)$ é dada por:

                    <div class="equacao">
                      $$
                          R_n(\tau) = \frac{f^{(n+1)}(\tau)}{(n+1)!}(x-x_0)^{(n+1)}.
                      $$
                    </div>
                      

                    Como $n = 2$, a equação para R_2(n) equivale a<br><br>


                    <div class="equacao">
                      $$
                          R_2(\tau) =\frac{ f^{'''}(\tau)}{3!}(x-\pi)^{3}.
                      $$
                    </div> 
                    $\pi \leq \tau\leq 3.2$.
                    A derivada de terceira ordem de $f(x)$ é dada por

                    <div class="equacao">
                      $$
                        f^{'''}(x) = \frac{24x}{(x^2 + 1)^3} - \frac{48x^3}{(x^2 +1)^4} + x\sin(x) - 3\cos(x) . 
                      $$
                    </div><br>

                    Substituindo a derivada $f'''(\tau)$ e os valores de $x$ e $x_0$ na função do resto, obtém-se a equação abaixo.

                    <div class="equacao">
                      $$
                          R_2(\tau)=\frac{\frac{24\tau}{(\tau^2 + 1)^3} - \frac{48\tau^3}{(\tau^2 +1)^4} + \tau \sin(\tau) - 3\cos(\tau)}{6}(3.2-\pi)^3.
                      $$
                    </div>
                    <br>
                    <b>(d)</b>
                    Ao se utilizar o polinômio $P_2(x)$ para representar a função $f(x)$ no intervalo $[\pi;3.2]$, o erro máximo que podemos cometer é determinado encontrando o valor máximo de $R_2(\tau)$ para o intervalo em questão. Nesse caso, o máximo de $R_2(\tau)$ em $\tau \in [\pi;3.2]$ é obtido para $\tau = \pi$, como mostra o gráfico de $f^{(3)}(\tau)$ plotado abaixo:

                    <br>        
                    <div style=" text-align: center">
                        <img src="imagens/Taylor.svg" alt=""   style="width:60%;"><br><strong>Figura 3</strong> <br><br>
                    </div>

                    O erro máximo é, então:

                    <div class="equacao">
                    $$
                    \begin{aligned}
                        R_2(\pi)&= 0.000098\\
                        &= 9.8\times 10^{-5}.
                    \end{aligned}
                    $$
                    </div>

                    Comparando com o erro verdadeiro calculado no item <strong><em>b</em></strong> de $9.65\times 10^{-5}$, é visível que ambos são de mesma grandeza. A partir disso, conclui-se que os cálculos realizados são coerentes.
                
            </p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="http://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/serieTaylorCosseno.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/serieTaylorExponencial.sce"></script>
          </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js" ></script>
<script type="text/javascript">
    
    var n = 0;
    var intViewportWidth = window.innerWidth;
    var height = window.height;
    var list = document.getElementById('demo');
    var parameters = {"id": "ggbApplet", "prerelease":false,"width":intViewportWidth,"height":height,"showToolBar":false,"borderColor":null,"showMenuBar":false,"showAlgebraInput":false,
    "showResetIcon":true,"enableLabelDrags":false,"enableShiftDragZoom":false,"enableRightClick":false,"capturingThreshold":null,"showToolBarHelp":false,
    "errorDialogsActive":true,"useBrowserForJS":true, 
    "filename":"<?= Url::base() ?>/geogebra/Taylor_exemploAprox.ggb"};
    var views = {'is3D': 0,'AV': 1,'SV': 0,'CV': 0,'EV2': 0,'CP': 0,'PC': 0,'DA': 0,'FI': 0,'PV': 0,'macro': 0};
    var applet = new GGBApplet(parameters, '5.0'); 
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function() {
        applet.inject('applet_container');  
    }


     function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-1,7,-2,4);
            ggbApplet.setValue('bissec',true); // Valor de h
            ggbApplet.setValue('falseposic',false);  // Mostra Euler;
            ggbApplet.setValue('newton',false);  // Mostra Heun;
            ggbApplet.setValue('secante',false);  // Mostra Ponto Medio;
            ggbApplet.setValue('n',0);
        }
    }

    function mensagens(n){
       // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 2;
        ggbApplet.setValue('n',this.n)
        if(this.n%2 == 0){
        x = ggbApplet.getValue('X(1,0)');}

        if(this.n%2 == 1){
        a = ggbApplet.getValue('A(1,1)').toFixed(decimais);
        b = ggbApplet.getValue('B(1,1)').toFixed(decimais);
    }
        
       
    }

    function anterior (){
            if(n>=4){

             ggbApplet.setCoordSystem(-1,7,-2,4);
        }
        else{

            ggbApplet.setCoordSystem(-1,7,-2,4);
        }
        if (this.n >= 0){
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);
            
            removerItem(this.n + 1);
            return true;
        }
        else{
            alert("Vá para a próxima etapa.");
            return false;
        }    
       
                                    
    }

    function proximo (){
        if(n>=3){

             ggbApplet.setCoordSystem(-1,7,-2,4);
        }
        else{

            ggbApplet.setCoordSystem(-1,7,-2,4);
        }
        if (this.n <= 4 ){
            this.n++;
            ggbApplet.setValue('n', this.n)
          
            

             
                
            texto = mensagens(this.n);
            acrescentarItem(this.n,texto) 
            MathJax.Hub.Queue(["Typeset",MathJax.Hub]);
            return true;                          
        }
        else {
            alert("Fim da demonstração.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId,texto) {
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(texto));
        entry.setAttribute('id','item'+this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid){
        var item = document.getElementById('item'+ itemid);
        list.removeChild(item);
    }

</script>


<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>