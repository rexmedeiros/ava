<?php

use yii\db\Migration;
use mdm\admin\components\Configs;

/**
 * Handles the creation of table `componentes_cn`.
 */
class m170427_131123_update_componentes_cn_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('questoes', 'arquivo', $this->string(255));
        $this->addColumn('alternativas', 'arquivo', $this->string(255));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('questoes', 'arquivo');
        $this->dropColumn('alternativas', 'arquivo');
    }
}
