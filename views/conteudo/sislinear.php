<?php
use yii\helpers\Html;
use frontend\components\ExercicioWidget;
use yii\helpers\Url;
/* @var $this yii\web\View */


$this->title = 'Sistemas Lineares - Introdução';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js"
        integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js"
        integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH"
        crossorigin="anonymous"></script>

<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Introdução</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <p align="justify">
          Um sistema de equações lineares é um conjunto de equações de forma que, em cada equação, cada termo contém apenas
          uma variável e cada variável aparece na primeira potência.

          <br><br>

          De maneira geral, um sistema de equações lineares pode ser definido como um conjunto de $m$ equações, sendo $m
          \geq 1$, com $n$ incógnitas $[x_1, x_2, x_3, \dots , x_n]$. Podemos escrever um sistema da seguinte forma:

        <div class="equacao">
          $$
          \begin{cases}
          a_{11}x_{1} + a_{12}x_{2} + a_{13}x_{3} + \cdots + a_{1n}x_{n} &= b_{1} \\
          a_{21}x_{1} + a_{22}x_{2} + a_{23}x_{3} + \cdots + a_{2n}x_{n} &= b_{2} \\
          a_{31}x_{1} + a_{32}x_{2} + a_{33}x_{3} + \cdots + a_{3n}x_{n} &= b_{3} \\
          \;\;\;\;\; \vdots \\
          a_{m1}x_{1} + a_{m2}x_{2} + a_{m3}x_{3} + \cdots + a_{mn}x_{n} &= b_{m}.
          \end{cases}
          $$
        </div>


        </p>

        <p align="justify">
          Esse conjunto de equações pode ser reescrito na forma de uma equação matricial:

          $$
          Ax = b,
          $$
          com
          $$
          A =\begin{bmatrix}a_{11} & a_{12} & a_{13} & \dots & a_{1n}\\ a_{21} & a_{22} & a_{23} & \dots & a_{2n} \\
          a_{31} & a_{32} & a_{33} & \dots & a_{3n} \\ & & \vdots \\ a_{m1} & a_{m2} & a_{m3} & \dots & a_{mn}
          \end{bmatrix},
          $$
          $$
          x = \begin{bmatrix}x_1 \\ x_2 \\ x_3 \\ \vdots \\ x_n \end{bmatrix},
          $$
          $$
          b = \begin{bmatrix}b_1 \\ b_2 \\ b_3 \\ \vdots \\ b_m\end{bmatrix}.
          $$
          <br>

          A matriz $A$ é chamada de coeficientes $A$, $x$ o vetor de incógnitas e $b$ o vetor de termos independentes.
          Resolver um sistema é encontrar valores de $[x_1, x_2, x_3, \dots , x_n]$ que satisfazem simultaneamente a
          todas as equações.

          <br><br>

          Quanto às soluções, podemos classificar um sistema linear como impossível (SPI), possível determinado (SPD) ou
          possível indeterminado (SPI). Para que o sistema seja SPD, o número de equações deve ser igual ao número de
          variáveis e o determinante da sua matriz de coeficientes deve ser diferente de zero. Nesse curso, estudaremos
          a resolução de sistemas possíveis e determinados.
        </p>


      </div>
    </div>
  </div>
</div>


<script>
    renderMathInElement(document.body, {
        delimiters: [
            {left: "$$", right: "$$", display: true},
            {left: "$", right: "$", display: false}
        ]
    });
</script>


