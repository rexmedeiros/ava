<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Alternativas]].
 *
 * @see Alternativas
 */
class AlternativasQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Alternativas[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Alternativas|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
