<?php

    use yii\helpers\Html;
    use app\components\ExercicioWidget;
    use yii\helpers\Url;

    /* @var $this yii\web\View
     * http://conteudo.icmc.usp.br/pessoas/mari/NumPri2012/Posicaofalsa.pdf
     * http://www.univasf.edu.br/~jorge.cavalcanti/4CN_Parte2.1_Metodos.pdf
     * http://paginas.fe.up.pt/~faf/mnum/mnum-faf-handout.pdf
     */


    $this->title = 'Raizes';
    $this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css"
      integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js"
        integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js"
        integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH"
        crossorigin="anonymous"></script>
<div class="text-right">
  Tamanho atual da fonte: <span id="font-size"></span>
  <button id="up">A+</button>
  <button id="default">A</button>
  <button id="down">A-</button>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Videoaula</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="intrinsic-container intrinsic-container-16x9">
          <iframe src="https://www.youtube.com/embed/Q9xqQ2NMzTo" allowfullscreen></iframe>
        </div>

      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Falsa Posição - Motivação</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        Seja o problema de encontrar as raízes da equação
        $$
        f(x) = 0.
        $$

        O método da Bissecção prevê que a raiz encontra-se no ponto médio do intervalo $[a;b]$.
        Uma variante do método da Bissecção é o método da Falsa Posição. Nesse método, a estimativa
        para a raiz é o ponto onde a reta que passa pelos pontos $(a,f(a))$ e $(b,f(b))$ cruza
        o eixo das abscissas, como mostra a ilustração abaixo:

        <p style="text-align: center;">
          <img src="imagens/falsapo.svg" alt="" style="width:90%;"/>
        </p>


        <br>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Falsa Posição</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">


        Dado um intervalo $[a;b]$ que contem a raiz procurada, considere a reta que passa pelos pontos
        $(a,f(a))$ e $(b,f(b))$. Para encontrar o ponto $x$ onde a reta (tracejada) toca o eixo das
        abscissas, usamos semelhança de triângulos:

        $$
        \frac{f(b)-0}{b-x}= \frac{0-f(a)}{x-a}.
        $$


        Isolando $x$ na equação acima:
        $$
        x = a - \frac{(b-a) f(a)}{f(b)-f(a)}.
        $$

        <br>
        Da mesma forma que na Bissecção, esse ponto subdivide o intervalo inicial em dois subintervalos.
        Para identificar em qual deles está a raiz, o teorema de Bolzano é usado novamente:
        <br>
        <blockquote>
          Se $f(a) f(x) < 0$, então a raiz está em $[a;x]$,
          <br>
          Se $f(a) f(x) > 0 $, a raiz está em $[x;b]$.
        </blockquote>

        Esse procedimento é repetido até que o valor de $ \, x\,$ atenda às condições de parada.

        <br>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        Dada a equação $ x^{2}-5 = 0$, encontre a raiz positiva com precisão de dois algarismos significativos,
        ou seja, de forma que o erro relativo seja $Er < 10^{-2} = 0.01$.
        <br><br>
        <strong>Estimativa inicial</strong>
        <br>
        A imagem abaixo mostra o gráfico da função $f(x) = x^{2}-5$.

        <p style="text-align: center;"">
        <img src="imagens/RaizesBiss.svg" alt="" style="width:50%;"/>
        </p>
        Podemos notar que

        $$f(2) f(3)< 0$$

        Logo, a raiz procurada está no intervalo $[a;b]$, com $a = 2$ e $b = 3$.
        <br><br>
        <strong>Iteração 1:</strong><br>

        <div class="equacao">
          $$
          \begin{aligned}
          x_1 & = a - \frac{(b-a)f(a)}{f(b)-f(a)} \\
          & = 2- \frac{1(-1)}{4+1} = \\
          & = 2.2. \\
          \end{aligned}
          $$
        </div>

        <br>
        Esperaremos a próxima iteração para calcular o erro relativo. Pelo teorema de Bolzano:

        $$f(2) f(2.2) > 0 $$

        Logo, a raiz está no intervalo $[x_1,b]$, ou seja, $[2.2;3]$.

        <br>
        <br>
        <strong>Iteração 2</strong>
        <br>

        <div class="equacao">
          $$
          \begin{aligned}

          x_2 &= a - \frac{(b-a) f(a)}{f(b) - f(a)} \\
          &= 2.2- \frac{0.8 (-0.16)}{4 + 0.16 } \\
          &= 2.2308 \\
          \end{aligned}
          $$
        </div>

        Calculando o erro relativo:

        $$
        \begin{aligned}
        \varepsilon_{r} &= \frac{|2.2308 - 2.2|}{2.2308} \\
        &= 0.0138 > 0.01.
        \end{aligned}
        $$

        Vefificando o critério de Bolzano:

        $$f(2.2) f(2.2308) < 0 $$

        Logo a raiz está no intervalo $[a; x_2]$, ou seja, $[2.2; 2.2308]$.

        <br>
        <br>
        <strong>Iteração 3 </strong>
        <br>
        <div class="equacao">
          $$
          \begin{aligned}
          x_3 &= 2.2 - \frac{(2.308 - 2.2) (-0.16)}{0.3255 + 0.16} \\
          &= 2.2355. \\
          \end{aligned}
          $$
        </div>


        <br>
        Calculando o erro relativo:

        $$
        \begin{aligned}
        \varepsilon_{r} &= \frac{| 2.2355 -2.2308 |}{2.2355} \\
        &= 0.0021 < 0.01.
        \end{aligned}
        $$

        Tendo sido atingido o critério de convergência, o valor da raiz será $x = 2.2355$ com dois
        algarismos significativos.
        <br><br>
      </div>


    </div>
  </div>
</div>


<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Exemplo com Geogebra</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" id="conteudo">
        Utilize o método da Falsa Posição para encontrar a raiz positiva da equação abaixo com
        três algarismos significativos, ou seja, tal que $Er < 10^{-3}$.

        $$ f(x) = 0,$$

        com $f(x) = \frac{x^2}{5} -2x - 3$.
        <br>

        <p><strong>Solução:</strong></p>
        <form id="geo">
          <input value="Etapa anterior" onclick="anterior()" type="button" class="btn btn-success">

          <input id="ant" value="Etapa seguinte" onclick="proximo()" type="button" class="btn btn-success">
        </form>
        <div id="applet_container" align="center"></div>

        <br>
        <ol id="demo" style=" padding-left: 17px">

          <li>
            Fazendo-se uma Análise Gráfica da função $f(x)$, estima-se a raiz no subintervalo $[a;b]$, com
            $a = 10\,$ e $b = 12$.

          </li>
          <br><br>
        </ol>

      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div id="w0" class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-binoculars"></i> Algoritmo</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <p>
        <p style="text-align: center;"">
        <img src="imagensDev/Massao/falsaPosicao.svg" alt="" style="width:70%;"/>
        </p>
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><a id="scilab"> <i class="fa fa-binoculars"></i> Exemplo Scilab</a></h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <script src="https://gist-it.appspot.com/github/CNUFRN/Algoritmos/blob/master/raizesFalsaPosicao.sce"></script>
          </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
          <?= ExercicioWidget::widget([
          'titulo' => 'Exercícios - Método da Falsa Posição',
          'todosExercicios' => $todosExercicios[15],
          'modeloResposta' => $modeloResposta,
      ]) ?>
    </div>
</div>

<script type="text/javascript">
    function diminuirFonte() {
        document.getElementById("conteudo").style.fontSize = "1em";
    }
</script>

<script type="text/javascript" src="https://cdn.geogebra.org/apps/deployggb.js"></script>
<script type="text/javascript">
    var n = 0;
    var list = document.getElementById('demo');
    var parameters = {
        "id": "ggbApplet",
        "prerelease": false,
        "width": 600,
        "height": 400,
        "showToolBar": false,
        "borderColor": null,
        "showMenuBar": false,
        "showAlgebraInput": false,
        "showResetIcon": true,
        "enableLabelDrags": false,
        "enableShiftDragZoom": true,
        "enableRightClick": false,
        "capturingThreshold": null,
        "showToolBarHelp": false,
        "errorDialogsActive": true,
        "useBrowserForJS": true,
        "filename": "<?= Url::base() ?>/geogebra/RAIZESBisettGeoGebraSite.ggb"
    };
    var views = {
        'is3D': 0,
        'AV': 1,
        'SV': 0,
        'CV': 0,
        'EV2': 0,
        'CP': 0,
        'PC': 0,
        'DA': 0,
        'FI': 0,
        'PV': 0,
        'macro': 0
    };
    var applet = new GGBApplet(parameters, '5.0');
    //when used with Math Apps Bundle, uncomment this:
    //applet.setHTML5Codebase('GeoGebra/HTML5/5.0/webSimple/');

    window.onload = function () {
        applet.inject('applet_container');
    }


    function ggbOnInit(param) {
        if (param == "ggbApplet") {
            ggbApplet.setCoordSystem(-4, 20, -13, 15);
            ggbApplet.setValue('bissec', false); // Valor de h
            ggbApplet.setValue('falseposic', true)  // Mostra Euler;
            ggbApplet.setValue('newton', false)  // Mostra Heun;
            ggbApplet.setValue('secante', false)  // Mostra Ponto Medio;
        }
    }

    function mensagens(n) {
        // ggbApplet.setCoordSystem(-0.5,7,-0.5,7);
        decimais = 4;
        ggbApplet.setValue('n', this.n)

        x = ggbApplet.getValue('X(1,0)').toFixed(decimais);


        a = ggbApplet.getValue('A(1,1)').toFixed(decimais);
        b = ggbApplet.getValue('B(1,1)').toFixed(decimais);


        switch (n) {
            case 1:
                saida = "Iteração 1: \
                Primeiramente encontra-se o valor de x: \
                <div class=\"equacao\">\
                \$\$\
                \\begin{aligned} \
                    x &= a - \\frac{(b-a) \\cdot f(a)}{f(b)-f(a)} \\\\ \
                      &= " + a + " - \\frac{(" + b + "-" + a + ") f(" + a + ")}{f(" + b + ")-f(" + a + ")}\\\\  \
                      &= " + x + ".\
                \\end{aligned}\
                \$\$ \
                </div>\
                ";
                break;

            case 2:
                saida = "Como não se tem o valor de x de uma iteração anterior, não calcularemos o erro relativo.\
                Utilizando o teorema de Bolzano:" +
                "<div class='equacao'> " +
                "\$\$\
                \\begin{aligned} \
                   f(a) f(x) &= f(" + a + ") f(" + x + ") >0\
                \\end{aligned} \
                \$\$\
                </div>\
                Dessa forma, a raiz está entre o intervalo $[x;b]$, ou seja, $[" + x + ";" + b + "]$.";
                break;
            case 3:
                saida = "Iteração 2:\
                <div class=\"equacao\">\
                \$\$\
                \\begin{aligned} \
                    x &= a - \\frac{(b-a) \\cdot f(a)}{f(b)-f(a)} \\\\ \
                      &= " + a + " - \\frac{(" + b + "-" + a + ") f(" + a + ")}{f(" + b + ")-f(" + a + ")}\\\\  \
                      &= " + x + ".\
                \\end{aligned}\
                \$\$ \
                </div>";
                break;
            case 4:
                saida = "Calculando o erro relativo: \
                \$\$\
                \\begin{aligned} \
                   \\varepsilon_{r}  &=  \\frac{|x - x_a|}{x} \\\\ \
                        &=  \\frac{|" + x + " - 11.2500|}{" + x + "} \\\\ \
                        & = 0.0062 > 0.001. \
                \\end{aligned} \
                \$\$\
                O intervalo em que a raiz se encontra  é verificado usando o teorema de Bolzano: \
                <div class=\"equacao\">\
                \$\$\
                \\begin{aligned} \
                   f(a) f(x) &= f(" + a + ") f(" + x + ") > 0\
                \\end{aligned} \
                \$\$\
                </div>\
                Dessa forma, a raiz está entre o intervalo $[x;b]$, ou seja, $[" + x + ";" + b + "]$.";
                break;
            case 5:
                saida = "Iteração 3:\
                <div class=\"equacao\">\
                \$\$\
                \\begin{aligned} \
                    x &= a - \\frac{(b-a) \\cdot f(a)}{f(b)-f(a)} \\\\ \
                      &= " + a + " - \\frac{(" + b + "-" + a + ") f(" + a + ")}{f(" + b + ")-f(" + a + ")}\\\\  \
                      &= " + x + ".\
                \\end{aligned}\
                \$\$ \
                </div>";

                break;
            case 6:
                saida = "Calculando o erro relativo: \
                \$\$\
                \\begin{aligned} \
                   \\varepsilon_{r}  &=  \\frac{|x - x_a|}{x} \\\\ \
                        &=  \\frac{|" + x + " - 11.3200|}{" + x + "} \\\\ \
                        & = 0.0003 < 0.001. \
                \\end{aligned} \
                \$\$\
                Como o critério de convergência foi  atingido,  a raiz será:\
                \$\$\
                 x = " + x + " \
                \$\$\
                com três algarismos significativos. ";

                break;

            //alert('Default case');
        }


        return saida;
    }

    function anterior() {
        if (this.n % 2 == 0) {
            ggbApplet.evalCommand("  ZoomIn[1/7.9,(11.3204,0)]")
        }
        if (this.n >= 1) {
            this.n--;
            ggbApplet.setValue('n', this.n);
            //mensagens(this.n);

            removerItem(this.n + 1);
            return true;
        }
        else {
            alert("Vá para a próxima etapa.");
            return false;
        }


    }

    function proximo() {
        if (this.n <= 5) {

            this.n++;
            ggbApplet.setValue('n', this.n)

            if (this.n % 2 == 0) {
                ggbApplet.evalCommand("  ZoomIn[7.9,(11.3204,0)]")
            }


            texto = mensagens(this.n);
            acrescentarItem(this.n, texto)

            renderizarKatex();
            return true;
        }
        else {
            alert("Fim do exercício.");
            return false;
        }
    }

    //var list = document.getElementById('demo');

    function acrescentarItem(itemId, texto) {
        var entry = document.createElement('li');
        //entry.appendChild(document.createTextNode(texto));
        entry.innerHTML = texto;
        entry.setAttribute('id', 'item' + this.n);
        list.appendChild(entry);
    }

    function removerItem(itemid) {
        var item = document.getElementById('item' + itemid);
        list.removeChild(item);
    }

</script>


<script>
    renderMathInElement(document.body, {
        delimiters: [
            {left: "$$", right: "$$", display: true},
            {left: "$", right: "$", display: false}
        ]
    });
</script>


<script type="text/javascript">
    function renderizarKatex() {
        renderMathInElement(document.body, {
            delimiters: [
                {left: "$$", right: "$$", display: true},
                {left: "$", right: "$", display: false}
            ]
        });
    }
</script>