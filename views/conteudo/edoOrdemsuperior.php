<?php
use yii\helpers\Html;
/* @var $this yii\web\View */


$this->title = 'Equações Diferenciais - Ordem superior';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.css" integrity="sha384-9tPv11A+glH/on/wEu99NVwDPwkMQESOocs/ZGXPoIiLE8MU/qkqUcZ3zzL+6DuH" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/katex.min.js" integrity="sha384-U8Vrjwb8fuHMt6ewaCy8uqeUXv4oitYACKdB0VziCerzt011iQ/0TqlSlv8MReCm" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/katex@0.10.0-beta/dist/contrib/auto-render.min.js" integrity="sha384-aGfk5kvhIq5x1x5YdvCp4upKZYnA8ckafviDpmWEKp4afOZEqOli7gqSnh8I6enH" crossorigin="anonymous"></script>


<div class="text-right">
    Tamanho atual da fonte: <span id="font-size"></span>
    <button id="up">A+</button>
    <button id="default">A</button>
    <button id="down">A-</button>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i>Videoaula</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="intrinsic-container intrinsic-container-16x9">
                    <iframe src="https://www.youtube.com/embed/JzFk-m0L164" allowfullscreen></iframe>
              </div>

          </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> EDO de ordem superior</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
            <p align="justify">Os métodos de resolução de EDOs são aplicáveis apenas a equações diferenciais de primeira ordem. Sendo assim, se tivermos uma equação diferencial de ordem maior que um, teremos que fazer algumas substituições e transformar essa equação de ordem superior num sistema de equações diferenciais ordinárias de primeira ordem. 
            </p>
          </div>
        </div>
      </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                
                Considere a seguinte EDO de ordem superior

                <div class="equacao">
                $$
                  {y''' - 2x^{2}y'' + sen(x)y' - 3ln(x)y + e^{x} = 0}
                $$
                </div>
                com as condições iniciais $y(1) = 2$, $y'(1) = 3$ e $y''(1) = -4$. Definindo variáveis auxiliares, transforme a EDO de ordem superior em um sistema de EDOs de forma a ser resolvido pelas técnicas já estudadas.

                <br><br>
                <b>Solução: </b><br>

                Aqui, teremos que fazer substituições, de modo que através delas, possamos obter um sistema de EDO de primeira ordem:
                $$
                   z = y',
                $$
                $$
                z' = y'',
                $$
                $$ 
                z'' = y'''.
                $$
               
                Substituindo isso na equação diferencial, teremos:
                <div class="equacao">
                $$
                  {z'' - 2x^{2}z' + sen(x)z - 3ln(x)y + e^{x} = 0} \;\;\;\text{(2)}
                $$
                </div>

                Fazendo o mesmo procedimento para a variável auxiliar $z$:

                $$w = z',$$
                $$w' = z''.$$
                               
                Substituindo isso na Eq. (2), teremos que: 
                <div class="equacao">
                $$
                  {w' - 2x^{2}w + sen(x)z - 3ln(x)y + e^{x} = 0}.
                $$
                </div>

                Dessa forma, já podemos escrever o nosso sistema, assim:
                <div class="equacao">
                $$
                \begin{cases}
                  {y' = z}\\
                  {z' = w}\\
                  {w' = 2x^{2}w - sen(x)z + 3ln(x)y - e^{x}}
                \end{cases}
                $$
                </div>

                Precisamos agora encontrar as condições iniciais para $z(x=1)$ e $w(x=1)$, já que foi dado que $y(x=1) = 2$. Note que:
                $$
                  z(x=1) = \frac{dy}{dx}(x=1) = y'(x=1). 
                $$
                Como $y'(x = 1)$ foi dado e é igua a 3, então  $z(x = 1) = 3$. Fazendo agora para $w$, note que:
                <div class="equacao">
                $$
                  w(x=1) = \frac{dz}{dx}(x=1) =  \frac{d^2y}{dx^2}(x=1) = y''(x=1). 
                $$
                </div>

                Como $y''(x = 1)$ foi dado e é igua a -4, então  $w(x = 1) = -4$. O PVI agora pode ser escrito da seguinte forma:

                <div class="equacao">
                $$
                \begin{cases}
                  y' & = z = g_y(x,y,z,w)\\
                  z' &= w= g_z(x,y,z,w) \\
                  w' &= 2x^{2}w - sen(x)z + 3ln(x)y - e^{x}\\
                      &= g_w(x,y,z,w) \\
                  & \;\; y(1) = 2 \;\;\ z(1) = 3 \;\; w(1) = -4.
                \end{cases}
                $$
                </div>
                
            </div>
        </div>
    </div>
</div>

<br><br>

<div class="row">
    <div class="col-xs-12">
        <div id="w0" class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-binoculars"></i> Exemplo</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                  <p align="justify">Uma pessoa com $y_0 = 1.80m$ de altura lança uma bola para cima com velocidade inicial de $v_0 = 12m/s$. Determine a altura máxima aproximada alcançada pela bola usando o método de Euler, partindo de $t=0s$ com um passo $h = 0.4s$. Considere a gravidade  $g =  9.8m/s^2$<br>

                  <br><b>Solução: </b><br><br>
                  Chamando de $y(t)$ a função que descreve a posição da bola em função do tempo, então sabemos que, no lançamento vertical;
                  $$
                    \frac{d^2y}{dt^2} = -g.
                  $$
                  As condições iniciais são $y(0) = 1.8m$ e $y'(0) = 12m/s$, já que a velocidade é a derivada do espaço em função do tempo. Precisamos transformar a EDO acima num sistema de EDO. Para isso, definimos uma variável auxiliar:
                  $$
                    v = y' \Rightarrow v' = y''.
                  $$
                  Substituindo, na EDO de 2ª ordem:
                  $$
                    v' = -g = -9.8
                  $$
                  Observe que, apesar de ser uma variável auxiliar, $v$ tem um significado físico, ou seja, $v(t)$ é a velocidade da bola em função do tempo! O PVI tem a forma
                  <div class="equacao">
                    $$
                    \begin{cases}
                    y' &=  v &= g_y(t,y,v)\\
                    v' &= -9.8 &= g_v(t,y,v) \\
                      y(0) &= 1.8 \; &v(0) = 12 .
                    \end{cases}
                    $$
                  </div>
                    
                  Com essas informações, já podemos resolver o problema. Em $t = 0.4s$, ao final da primeira iteração, teremos:
                  
                  <div class="equacao">
                    $$
                    \begin{aligned}
                    y_1 &=  y_0 + g_y(t_0,y_0,v_0)h\\
                        &= 1.8 + g_y(0,1.8,12)*0,4 \\
                        &= 1.8 + 12*0.4 = 6.6m\\
                    v_1 &= v_0 + g_v(t_0,y_0,v_0)h\\
                        &= 12 + g_v(0,1.8,12)*0,4\\
                        &= 12 + (-9.8)*0.4 = 8.08m/s.
                    \end{aligned}
                    $$
                  </div>
                  Podemos ver que em $t_1= 0.4s$ após ser lançado, a bola encontra-se a $y_1 = 6.6$ do solo, e sua velocidade caiu para $v_1 = 8.08m/s$. Continuando, para $t_2 = 0.8s$:
                  <div class="equacao">
                    $$
                    \begin{aligned}
                    y_2 &=  y_1 + g_y(t_1,y_1,v_1)h\\
                        &= 6.6 + 8.08*0.4 = 9.83m.\\
                    v_2 &= v_1 + g_v(t_1,y_1,v_1)h\\
                        &= 8.08 + (-9.8)*0.4 = 4.16m/s.
                    \end{aligned}
                    $$
                  </div>

                  Poderemos ver que a bola continua subindo e a velocidade diminuindo pela ação da gravidade. Para $t_3 = 1.2s$:
                  
                  <div class="equacao">
                    $$
                    \begin{aligned}
                    y_3 &=  y_2 + g_y(t_2,y_2,v_2)h\\
                        &=  9.83 + 4.16*0.4 = 11.49m.\\
                    v_3 &= v_2 + g_v(t_2,y_2,v_2)h\\
                        &= 4.16 + (-9.8)*0.4 = 0.24m/s.
                    \end{aligned}
                    $$
                  </div>

                  No instante de tempo $t_3 = 1.2s$, a bola está próxima de alcançar a altura máxima, pois sua velocidade está próxima de zero. Com mais outra iteração, para $t_4 = 1.8s$:


                  <div class="equacao">
                    $$
                    \begin{aligned}
                    y_4 &=  y_3 + g_y(t_3,y_3,v_3)h\\
                        &= 11.49 + 0.24*0.4 = 11.58m.\\
                    v_4 &= v_3 + g_v(t_3,y_3,v_3)h\\
                        &= 0.24 + (-9.8)*0.4 = -3.68m/s.
                    \end{aligned}
                    $$
                  </div>

                  Aqui percebemos que a velocidade trocou de sinal, ou seja, mudou seu sentido para baixo. Como a bola atinge sua altura máxima quando a velocidade é zero, podemos concluir que a bola atinge sua altura máxima entre $t = 1.2s$ e $t=1.6s$, e que essa altura é aproximadamente $11.58m$. <br><br>

                  Podemos obter uma precisão melhor se usarmos um passo $h$ menor ou outro método que seja mais preciso, mas esse exemplo serve para ilustrar que podemos usar a resolução numérica de sistema de EDOs para solucionar problemas envolvendo equações diferenciais em nosso dia a dia.</p>
                  <br><br>   
            </div>
        </div>
      </div>
</div>

<script>
  renderMathInElement(document.body,{delimiters: [
    {left: "$$", right: "$$", display: true},
    {left: "$", right: "$", display: false}
  ]});
</script>